<?php
//<!--Company Name :- Lazlo Software Solution
//    Creation Date :-18/6/2018
//    Modal Name :- Frontmodel
//    Description :- This is the Frontmodel where database operation are performed 
//
//     
//    Database Name:- carrental
//    Table Used :- 'tblclientcompany' --!>

class Frontmodel extends CI_Model {
    function __construct() {
        // Call the Model constructor
  $this->load->library('session');
        $this->load->database();
         $this->load->helper('url'); 
        $this->load->helper('form');
        $this->load->helper('text');
        $this->load->helper('email');
        $this->load->library('email');
 error_reporting(0);
      //  parent::__construct();

    }
    
   //Function for making seession of signup users 
    public function index()
    {
         $email = $this->input->post('emailaddress');
         $username = $this->input->post('username');
         
         $alreadysignup = $this->db->get_where("tblregisclient",array('regisClientEmail'=>$email));
         
         $alreadyuser = $this->db->get_where("tblregisclient",array('regisclientUname'=>$username));
         
         if($alreadyuser->num_rows() > 0)
          {
              return "userexist";
          }
         
         
         if($alreadysignup->num_rows() > 0)
          {
              return "exist";
          }
       
        
        
        
        
        
        
        $newdata = array(
                           'firstname'=>$this->input->post('firstname'),
                            'lastname'  => $this->input->post('lastname'),
                            'username'     =>$this->input->post('username'),
                            'email' =>$this->input->post('emailaddress'),
                            
                            'password' =>$this->input->post('password')

                 
                    );

                    $this->session->set_userdata($newdata);
                    
                    return "true";
                   
    }
    
  
      //Function signup for demo plan
    public function signupDemo()
    {
      
        
        
    $email = $this->session->userdata['email'];
    $demoid = $_POST['demoid'];
   
    $query = $this->db->get_where("tblregisclient",array('regisClientEmail'=>$email))->row_array();
          
     $allplans = $query['allclientplanId'];
        
        $planarr = explode(",",$allplans);
         
      
        if (in_array($demoid, $planarr))
            {
                return "false";
            }
    
    
          else
          {
            $data1['regisclientFname'] =  $this->session->userdata['firstname'];
           $data1['regisclientLname'] =  $this->session->userdata['lastname'];
           $data1['regisclientUname'] =  $this->session->userdata['username'];
           $data1['regisclientpassword'] =  password_hash($this->session->userdata['password'], PASSWORD_DEFAULT);                      
           $data1['regisClientEmail'] = $this->session->userdata['email'];
           $data1['regisClientPlanid'] =  $demoid;
           $data1['regisClientDate'] = date('y-m-d');
           
//           ***************
            $data1['allclientplanId'] = $demoid;
           $data1['allplanregistDate'] =date('y-m-d');
           
           
           
//           *****************
         
           $email123 = $this->session->userdata['email'];
          $pass = $this->session->userdata['password'];
          $planname = $this->db->get_where('tblplan', array('planid' => $demoid))->row_array();
          $pname = $planname['planname'];
          $query =  $this->db->insert("tblregisclient",$data1);
//           *****************************mail functionality*********************************
            
           $templquery = $this->db->get_where('tbltemplate',array('templatetype'=>"signup"))->row_array();
           
           $fullname =  $data1['regisclientFname'].$data1['regisclientLname'];
           
           $template= $templquery['templatedescription'];
      $variables = array(
            '{user}' => $fullname,
            '{email}'=>$email123,
            '{password}'=>$pass,
            '{plan}'=>$pname,
        );


         $txt1= strtr($template, $variables);   
    
           $to = $email123;
        $subject = "Registration";
        $txt = $txt1;
        
           $headers = "MIME-Version: 1.0" . "\r\n";
	$headers.= "Content-type:text/html;charset=UTF-8" . "\r\n";
        
        $headers .= "From: info@srngroups.com" . "\r\n" .
        "CC: lss.tm18@livesoftwaresolution.com";

        mail($to,$subject,$txt,$headers);
          
           
//           *****************************************************************************************
           
           return true;
           
          }

     
    }
    
    //    ******Function for check the login details of user and admin in table 'tblregisclient' for user and for user in 'tblclientuser'*********************
    public function checkLogin() 
    {
        $loginquery = $this->db->get_where('tblregisclient', array('regisClientEmail' => $this->input->post('email')));
        $result = $loginquery->result_array();
        
        
          if (password_verify($this->input->post('password'), $result[0]['regisclientpassword'])) {
          if($loginquery->num_rows() >0)
         {
            $newdata = array(
                           'usertype'=>"client",
                            'username'  => $result[0]['regisclientUname'],
                            'email'     => $result[0]['regisClientEmail'],
                            'companyid'     => $result[0]['regisClientId'],

                            'userid' =>$result[0]['regisClientId']
                    );

                    $this->session->set_userdata($newdata);
              
                    
                  
                 
             return "true";
         }
       
          }
        
        
        
        
       $clientlogin = $this->db->get_where('tblclientuser', array('clientuseremail' => $this->input->post('email')));
//        $result = $loginquery->result_array();
         
        $result1 = $clientlogin->result_array();
        
      
        
//        
//        if($loginquery->num_rows() >0)
//         {
//            $newdata = array(
//                           'usertype'=>"client",
//                            'username'  => $result[0]['regisclientUname'],
//                            'email'     => $result[0]['regisClientEmail'],
//                            'companyid'     => $result[0]['regisClientId'],
//
//                            'userid' =>$result[0]['regisClientId']
//                    );
//
//                    $this->session->set_userdata($newdata);
//              
//                    
//                  
//                 
//             return "true";
//         }
        if (password_verify($this->input->post('password'), $result1[0]['clientuserpass'])) {

         
                    if($clientlogin->num_rows() >0)
                 {


                    $newdata = array(
                                   'usertype'=>"clientuser",
                                    'username'  => $result1[0]['clientusername'],
                                    'email'     => $result1[0]['clientuseremail'],
                                    'companyid'     => $result1[0]['clientcompid'],
                                      'role' =>$result1[0]['clientuserrole'],

                                    'userid' =>$result1[0]['clientuserid']
                            );

                            $this->session->set_userdata($newdata);



                     return "true";
                 }
         
        }
         ////////// 
         
         else
         {
           return "false";
         }
       

    }
    
}
  