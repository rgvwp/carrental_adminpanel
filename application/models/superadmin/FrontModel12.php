<?php
//
//<!--Company Name :- Lazlo Software Solution
//    Creation Date :-6/14/2018
//    Model :- frontModel
//    Description :- This is the frontManager controller   used for managing the frontend data of website from backend
//    Database Name:- carrental
//    Table Used :- 'tblcmscontact'tblfrontContact,tblvisionmission.tblcmsfaq,tblfrontContact -->

class FrontModel extends CI_Model {
    function __construct() {
        // Call the Model constructor
         $this->load->library('session');
          $this->load->database();
        
 
    }
     //function for saving the contact us detail in table 'tblcmscontact'
    public function saveContact()
    {
        $id =$this->input->post('parameter');
        $value['cmscontactno'] = $this->input->post('contactno');
        $value['cmscontactemail'] = $this->input->post('email');
        $value['cmscontactaddr'] = $this->input->post('address');
        $query=  $this->db->update('tblcmscontact', $value, array('cmscontactid' => $id));
        if($query == 1)
        {
            return "true";
        }

    }
     //function for updating the vision ,mission and valies
     public function saveVision()
    {
      
         
         $id = $this->input->post('parameter');
       $value['visiondesc'] =  $this->input->post('vision');
       $value['missiondesc'] =  $this->input->post('mission');
       $value['valuedesc'] =  $this->input->post('value');
        $query=  $this->db->update('tblvisionmission', $value, array('visionmissionid' => $id));
        if($query == 1)
        {
            return "true";
        }
        

    }
  

//   /    public function for  saving the faq in table tblfaq
    public function saveFaq($param="",$param1="")
    {
       $value['cmsfaqquestions'] = $this->input->post('question');
        $value['cmsfaqanswers'] = $this->input->post('answer');
        
      
        
       
        if($param == "create"){
                    $value['cmsfaqstatus'] = 1;

            $this->db->insert('tblcmsfaq',$value);
            return "insert";
        }
      
        
        if($param == "edit"){
        $query=  $this->db->update('tblcmsfaq', $value, array('cmsfaqid' => $param1));
            return "edit";
        }
         
        if($param == "delete"){
                  $this->db->delete('tblcmsfaq', array('cmsfaqid' => $param1));     
                  return "delete";
        }

    }
     //public function for saving the contact fron fonend site in table 'tblfrontContact'
    public function savefrontContact()
    {
        $data['frontContactname'] = $this->input->post('name');
        $data['frontContactEmail']   = $this->input->post('email');
        $data['frontContactMessage'] = $this->input->post('message');
        $data['frontContactdate']  = date('y-m-d');
        
       $query =  $this->db->insert('tblfrontContact',$data);
       if($query == 1)
       {
           return "true";
       }
        
    }
     //function for deleting the contact messages comes from frontend in the table 'tblfrontContact'
    public function deleteContact($param="")
    {
       $query =  $this->db->delete('tblfrontContact', array('frontContactid' => $param));    
       
        if($query == 1)
       {
           return "true";
       }
    } 
    
     //function for saving the banner images in table 'tblcmshome;
    public function saveBanner($param1="",$param2="")
    {
       
       $value['cmshometitle'] = $this->input->post('title');
       $value['cmshomedescription'] = $this->input->post('description');
      
       
  if ($_FILES['image']['name']) 
                            {
                                   $random = substr(number_format(time() * rand(), 0, '', ''), 0, 5);
                                           $file_name = $random . $_FILES['image']['name'];
                                           $file_size = $_FILES['image']['size'];
                                           $file_tmp = $_FILES['image']['tmp_name'];
                                           $file_type = $_FILES['image']['type'];
                                           $file_ext = strtolower(end(explode('.', $_FILES['image']['name'])));
                                           $expensions = array("doc", "docx", "pdf");
                                           if (in_array($file_ext, $expensions) === false) 
                                           {             //$errors[]="extension not allowed, please choose a JPEG or PNG file.";      
                                           }
                                           move_uploaded_file($file_tmp, "uploads/images/" . $file_name);
                                           $value['cmshomeimage'] = $file_name;
                            }
      
        
        
            if($param1 == "create")
            {
                 $value['cmshomestatus'] = 1;
                if($value['cmshomeimage'] == "")
                {
                    $value['cmshomeimage'] = "02.jpg";
                }
                
                
                 $query =   $this->db->insert("tblcmshome",$value);
               if($query == 1)
                    {
                        return "create";
                    }
                    
            }   
            

                if($param1 == "edit")
                {
                     if($file_name!="")
                     {
                        $value['cmshomeimage'] = $file_name; 

                     }
                       $query=  $this->db->update('tblcmshome', $value, array('cmshomeid' => $param2));
                   return "edit";


                }
                if($param1 == "delete")
                {
                   $query =  $this->db->delete('tblcmshome', array('cmshomeid' => $param2));    
       
                        if($query == 1)
                       {
                           return "delete";
                       } 
                 }
                    
                                 
                            
    }
    
     //function for changing the status of banner
    public function changestatusBanner($param="")
    {
     
         $query = $this->db->get_where('tblcmshome',array('cmshomeid'=>$param))->row_array();
        $status = $query['cmshomestatus'];
       if($status == 1)
        {
            $data['cmshomestatus'] = 0;
            $query=  $this->db->update('tblcmshome', $data, array('cmshomeid' =>$param));
            return "inactive" ;
        }
       if($status == 0)
        {
            $data['cmshomestatus'] = 1;
            $query=  $this->db->update('tblcmshome', $data, array('cmshomeid' =>$param));
            return "active";
        }
    }
    
      //function for saving the  copyright and url in table 'tblmanageUrl;
   //function for updating the copyright and url in table ' tblmanagerUrl'
    public function savefrontUrl()
    {
      $data['manageUrltwitter'] = $this->input->post('twitter');
      $data['manageUrlin'] = $this->input->post('in');
      $data['manageUrlgplus'] = $this->input->post('gplus');
      $data['manageUrlfb'] = $this->input->post('facebook');
      $data['managecopyright'] = $this->input->post('copyright');
        $id = $this->input->post('parameter');
          $query=  $this->db->update('tblmanagerUrl', $data, array('	manageUrlid' =>$id));
           return "true";
    }
     //function for changing the status Faq in table 'tblcmsFaq'
    public function changestatusFaq($param="")
    {
        $query = $this->db->get_where('tblcmsfaq',array('cmsfaqid'=>$param))->row_array();
        $status = $query['cmsfaqstatus'];
       if($status == 1)
        {
            $data['cmsfaqstatus'] = 0;
            $query=  $this->db->update('tblcmsfaq', $data, array('cmsfaqid' =>$param));
            return "inactive" ;
        }
       if($status == 0)
        {
            $data['cmsfaqstatus'] = 1;
            $query=  $this->db->update('tblcmsfaq', $data, array('cmsfaqid' =>$param));
            return "active";
        }
        
    }
    
    //function for saving the news and event
    public function saveNews($param1="",$param2="")
    {
         
        
        
        $data['newseventheading'] = $this->input->post('heading');
          $data['newseventtitle'] =  $this->input->post('title');
          $data['newseventdate'] =  $this->input->post('newsdate');
          $data['newseventdesc'] = $this->input->post('description');
          $data['newseventtab'] = $this->input->post('tab');
          if ($_FILES['image']['name']) 
                            {
                                   $random = substr(number_format(time() * rand(), 0, '', ''), 0, 5);
                                           $file_name = $random . $_FILES['image']['name'];
                                           $file_size = $_FILES['image']['size'];
                                           $file_tmp = $_FILES['image']['tmp_name'];
                                           $file_type = $_FILES['image']['type'];
                                           $file_ext = strtolower(end(explode('.', $_FILES['image']['name'])));
                                           $expensions = array("doc", "docx", "pdf");
                                           if (in_array($file_ext, $expensions) === false) 
                                           {             //$errors[]="extension not allowed, please choose a JPEG or PNG file.";      
                                           }
                                           move_uploaded_file($file_tmp, "uploads/images/" . $file_name);
                                           $data['newseventimage'] = $file_name;
                            }
         
          if($param1 == "create")
          {
             
              if($data['newseventimage'] == "")
              {
                 $data['newseventimage'] = "02.jpg"; 
              }
              
              $data['newseventstatus'] = 1;
             
                 $query =   $this->db->insert("tblnewsevent",$data);
               if($query == 1)
                    {
                        return "create";
                    }
          }
          if($param1 == "edit")
          {
             
            $query=  $this->db->update('tblnewsevent', $data, array('newsenentid' =>$param2));
               if($query == 1)
                    {
                        return "edit";
                    }
          }
           if($param1 == "delete")
          {
             
                   $query =  $this->db->delete('tblnewsevent', array('newsenentid' => $param2));    
               if($query == 1)
                    {
                        return "delete";
                    }
          }
 
    }
    
    //function change status of changestatusNews in table ' tblnewsevent'
    public function  changestatusNews($param="")
    {
        $query = $this->db->get_where('tblnewsevent',array('newsenentid'=>$param))->row_array();
        $status = $query['newseventstatus'];
       if($status == 1)
        {
            $data['newseventstatus'] = 0;
            $query=  $this->db->update('tblnewsevent', $data, array('newsenentid' =>$param));
            return "inactive" ;
        }
       if($status == 0)
        {
            $data['newseventstatus'] = 1;
            $query=  $this->db->update('tblnewsevent', $data, array('newsenentid' =>$param));
            return "active";
        } 
    
    }
    
      //function for saving the tab in table tab
    public function updateTab()
    {
       $data['tab1'] = $this->input->post('tab1');
       $data['tab2'] = $this->input->post('tab2');
       $data['tab3'] = $this->input->post('tab3');
        $id = $this->input->post('parameter');
        $query=  $this->db->update('tbltab', $data, array('tabid' =>$id));
            return "update" ;

    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}