<?php
//<!--Company Name :- Lazlo Software Solution
//    Creation Date :-6/6/2018
//    Modal Name :- SuperAdmin_model
//    Description :- This is the SuperAdmin_model where database operation are performed 
//superadmin can update profile ,change password , change settings
//, update template .
//     
//    Database Name:- carrental
//    Table Used :- 'tblsupadmprofile',tblsetting,tblemailsett,tbltemplate,'tblplan --!>

class Usermodel extends CI_Model {
    function __construct() {
        // Call the Model constructor
         $this->load->library('session');
          $this->load->database();
          error_reporting(0);
 
    }
  

//Function for updating the super admin profile in tblsupadmprofile table
    public function profileUpdate() 
    { 
      
       $id = $this->input->post('supid');
       
  //   print_r($_POST);
    // die;
    
       $value['supadmusername'] = $this->input->post('name');
       $value['supadmcontact'] = $this->input->post('contactno');
       $value['supadmuseremail'] = $this->input->post('email');
       $value['supadmuseraddr'] = $this->input->post('address');
//       $value['supadmprofilepass'] = $this->input->post('pass');
       $value['supadmupdate'] = date('y-m-d');
//       
     
        if ($_FILES['image']['name']) 
                            {
                                   $random = substr(number_format(time() * rand(), 0, '', ''), 0, 5);
                                           $file_name = $random . $_FILES['image']['name'];
                                           $file_size = $_FILES['image']['size'];
                                           $file_tmp = $_FILES['image']['tmp_name'];
                                           $file_type = $_FILES['image']['type'];
                                           $file_ext = strtolower(end(explode('.', $_FILES['image']['name'])));
                                           $expensions = array("doc", "docx", "pdf");
                                           if (in_array($file_ext, $expensions) === false) 
                                           {             //$errors[]="extension not allowed, please choose a JPEG or PNG file.";      
                                           }
                                           move_uploaded_file($file_tmp, "uploads/images/" . $file_name);
                                           $value['supadmuserimg'] = $file_name;
                            }

    
            $query=  $this->db->update('tblsupadmuser', $value, array('supadmuserid' => $id));
                return "true";            
                            
    }

   // Function for checking the old password of superadmin 
    public function checkPassword(){
        
        
       
      $id = $this->input->post('supid'); 
        
       $oldpass = $this->input->post('oldPassword');
     
$query1 = $this->db->get_where('tblsupadmuser', array('supadmuserid' => $id))->row_array();
       $pass = $query1['supadmuserpass'];
     
        $as =  password_hash($this->input->post('pass'), PASSWORD_DEFAULT);  
      
       
        if(password_verify($this->input->post('oldPassword'), $pass))
        {
           
            
            $data['supadmuserpass'] = $as;
            $this->db->update('tblsupadmuser', $data, "supadmuserid= $id");
 
           return "match";
        }
        else
        {
           return  "notmatch";
        }
       
       
    }

 
   
   
    
    
}