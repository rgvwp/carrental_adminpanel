<?php
//<!--Company Name :- Lazlo Software Solution
//    Creation Date :-6/6/2018
//    Modal Name :- SuperAdmin_model
//    Description :- This is the SuperAdmin_model where database operation are performed 
//superadmin can update profile ,change password , change settings
//, update template .
//     
//    Database Name:- carrental
//    Table Used :- 'tblsupadmprofile',tblsetting,tblemailsett,tbltemplate,'tblplan --!>

class SuperAdmin_model extends CI_Model {
    function __construct() {
        // Call the Model constructor
  $this->load->library('session');
          $this->load->database();
 error_reporting(0);
      //  parent::__construct();

    }
  

//Function for updating the super admin profile in tblsupadmprofile table
    public function profileUpdate() 
    { 
      
       $id = $this->input->post('supid');
       $data['supadmprofilename'] = $this->input->post('name');
       $data['supadmprofilecontaccnum'] = $this->input->post('contactno');
       $data['supadmprofileemail'] = $this->input->post('email');
       $data['supadmprofileaddress'] = $this->input->post('address');
     //  $data['supadmprofilepass'] = $this->input->post('pass');
       $data['supadmprofileupdatedate'] = date('y-m-d');
        if ($_FILES['image']['name']) 
                            {
                                   $random = substr(number_format(time() * rand(), 0, '', ''), 0, 5);
                                           $file_name = $random . $_FILES['image']['name'];
                                           $file_size = $_FILES['image']['size'];
                                           $file_tmp = $_FILES['image']['tmp_name'];
                                           $file_type = $_FILES['image']['type'];
                                           $file_ext = strtolower(end(explode('.', $_FILES['image']['name'])));
                                           $expensions = array("doc", "docx", "pdf");
                                           if (in_array($file_ext, $expensions) === false) 
                                           {             //$errors[]="extension not allowed, please choose a JPEG or PNG file.";      
                                           }
                                           move_uploaded_file($file_tmp, "uploads/images/" . $file_name);
                                           $data['supadmprofilepicture'] = $file_name;
                            }

                 
          //   $query =  $this->db->update('tblsupadmprofile', $data, "id = $id");
                           
        $query=     $this->db->update('tblsupadmprofile', $data, "supadmprofileid= $id");
          
        return "true";
                            
                            
    }

   // Function for checking the old password of superadmin 
    public function checkPassword()
    {
         $id = $this->input->post('supid'); 
        
      
        
       $oldpass = $this->input->post('oldPassword');
     
       $query = $this->db->get('tblsupadmprofile')->row_array();
       $pass = $query['supadmprofilepass'];
       
      
       
        if(password_verify($this->input->post('oldPassword'), $pass))
        {
          
                        $as =  password_hash($this->input->post('pass'), PASSWORD_DEFAULT);  

            
            
            $data['supadmprofilepass'] = $as;
            $this->db->update('tblsupadmprofile', $data, "supadmprofileid= $id");
 
           return "match";
        }
        else
        {
           return  "notmatch";
        }
        
        
        
       
//        $id = $this->input->post('supid'); 
//        
//       $oldpass = $this->input->post('oldPassword');
//     
//       $query = $this->db->get('tblsupadmprofile')->row_array();
//       $pass = $query['supadmprofilepass'];
//       
//      
//       
//        if($oldpass == $pass)
//        {
//           
//            
//            $data['supadmprofilepass'] = $this->input->post('pass');
//            $this->db->update('tblsupadmprofile', $data, "supadmprofileid= $id");
// 
//           return "match";
//        }
//        else
//        {
//           return  "notmatch";
//        }
       
       
    }

    //Public function updateSitesSettings for updating the social site in table tblsitesetting
    public function updateSitesSettings()
    {
        $data['sitesettingfb'] = $this->input->post('fburl');
       $data['sitesettingtwitter'] = $this->input->post('twitterUrl');
       $data['sitesettinggplus'] = $this->input->post('gurl');
       $data['sitesettinginsta'] = $this->input->post('insta');
       $data['sitesettinglinkdin'] = $this->input->post('linkdin');
       $id = $this->input->post('settingId');
       $query = $this->db->update('tblsitesetting', $data, "sitesettingid= $id");
       if($query == 1)
       {
          return "true";
       }

    }
    // Function for updating ther emaining settings in table tblsetting
    public function updateSettings()
    {
        
       
        $rights =$this->input->post('copyrights');
        $copyid = $this->input->post('copyid');
        $email =  $this->input->post('email');
        $pay = $this->input->post('paypalemail');
       
        $emailst =$this->input->post('emailStatus');
        $notifyst = $this->input->post('notifyStatus');
        $phone = $this->input->post('contactnumber'); 
        $address = $this->input->post('address');
        $website = $this->input->post('website');
        $timezone =$this->input->post('timezone');
        $currency =$this->input->post('currency');
      $paypalcurrency  = $this->input->post('paypalcurrency');
   
        
  
                                    
          if ($_FILES['image']['name']) 
                            {
                                   $random = substr(number_format(time() * rand(), 0, '', ''), 0, 5);
                                           $file_name = $random . $_FILES['image']['name'];
                                           $file_size = $_FILES['image']['size'];
                                           $file_tmp = $_FILES['image']['tmp_name'];
                                           $file_type = $_FILES['image']['type'];
                                           $file_ext = strtolower(end(explode('.', $_FILES['image']['name'])));
                                           $expensions = array("doc", "docx", "pdf");
                                           if (in_array($file_ext, $expensions) === false) 
                                           {             //$errors[]="extension not allowed, please choose a JPEG or PNG file.";      
                                           }
                                           move_uploaded_file($file_tmp, "uploads/images/" . $file_name);
                                           $data['settingcomplogo'] = $file_name;
                            }
        if($rights!=""){                    
        $data['settingcomprights'] = $rights;}
        if($email!=""){
        $data['settingemail'] = $email;}
        if($pay!=""){
        $data['settingpaypalemail'] = $pay;}
        if($emailst!=""){
        $data['settingemailstatus'] = $emailst;}
        if($notifyst!=""){
        $data['settingnotifistatus'] =$notifyst;}
        if($phone!=""){
        $data['settingphone'] = $phone;}
        if($website!=""){
              $data['settingwebsite'] = $website;
        }
        if($address!=""){
            $data['settingaddress'] = $address;
        
        }
        if($timezone!=""){
            $data['settingtimezone'] = $timezone;
        
        }
         if($currency!=""){
            $data['settingcurrency'] = $currency;
        
        }
        if($paypalcurrency!=""){
            $data['settingpaypalcurrency'] = $paypalcurrency;
        
        }
       
       
       $query = $this->db->update('tblsetting', $data, "settingid= $copyid");
       if($query == 1)
       {
           return true;
       }
        
    }
  
     //Function for updating the emailsettings in table 'tblemailsett'
    public function updateEmailSettings()
    {
         $smtp =$this->input->post('smtp');
        $port = $this->input->post('port');
        $username =  $this->input->post('username');
        $pass = $this->input->post('pass');  
        $id =$this->input->post('hostid');
        $data['emailsettehost'] = $smtp;
        $data['	emailsettport'] = $port;
        $data['emailsettusername'] = $username;
        $data['emailsettpass'] = $pass;
       
        $query = $this->db->update('tblemailsett', $data, "emailsettid= $id");
       if($query == 1)
       {
           return true;
       }
        

    }
    // function for updating the  templates in tabele 'tbltemplate'.
    public function templateUpdate()
    {
      $id = $this->input->post('upId');
      $data['templatetitle'] = $this->input->post('templatename');
      $data['templatedescription'] = $this->input->post('discription');
      $data['templatestatus'] = $this->input->post('status');
     $query =  $this->db->update('tbltemplate', $data, array('templateid' => $id));
     if($query == 1)
     {
         return "true";
     }
      
    }
    //Function for saving the plan details in table 'tblplan'
    public function savePlan()
    {
      $type = $this->input->post('planType');
      if($type == "demo"){
     $query = $this->db->get_where('tblplan', array('plantype' => $type));
     if($query->num_rows() > 0){
         return "exist";
     }

        
      }
        
        
        $data['planaccount'] = $this->input->post('account');
        $data['planprivproject'] = $this->input->post('priproject');
        $data['planpubproject'] = $this->input->post('pubproject');
        $data['plandiskspace'] = $this->input->post('diskspace');
         $data['planbandwidth'] = $this->input->post('bandwidth');
        $data['planemailsupport'] = $this->input->post('esupport');
        $data['planphonesupport'] = $this->input->post('psupport');
        $data['planname'] = $this->input->post('planname');
        $data['planduration'] = $this->input->post('duration');
        $data['planprice'] = $this->input->post('planprice');
        $data['planstatus'] = 1;
       // $data['planfeatures'] = implode(",",$this->input->post('option'));
        $data['plancreationdate'] = date('y-m-d');
         $data['plancreatedby'] = $this->session->userdata['id'];
         
         
         if( $data['planprice'] == "")
         {
             $data['plantype'] = "demo";
         }
          if( $data['planprice'] != "")
         {
             $data['plantype'] = "paid";
         }
        
        $query =$this->db->insert('tblplan', $data);
        if($query == 1)
        {
            return "true";
        }else
        {
            return "false";
        }
        
        
    }

    //Public function  for updating the plan details in table tbplan
    public function updatePlan()
    {
          $id = $this->input->post('planId');
          
        $data['planaccount'] = $this->input->post('account');
        $data['planprivproject'] = $this->input->post('priproject');
        $data['planpubproject'] = $this->input->post('pubproject');
        $data['plandiskspace'] = $this->input->post('diskspace');
         $data['planbandwidth'] = $this->input->post('bandwidth');
        $data['planemailsupport'] = $this->input->post('esupport');
        $data['planphonesupport'] = $this->input->post('psupport');
        $data['planname'] = $this->input->post('planname');
        $data['planduration'] = $this->input->post('duration');
        $data['planprice'] = $this->input->post('planprice');
       // $data['planstatus'] = $this->input->post('status');
      //  $data['planfeatures'] = implode(",",$this->input->post('option'));
        $data['plancreationdate'] = date('y-m-d');
         $data['plancreatedby'] = $this->session->userdata['id'];
         
       
      $query=  $this->db->update('tblplan', $data, array('planid' => $id));
       if($query == 1)
        {
            return "true";
        }else
        {
            return "false";
        }
        
    }
    //public function for deleting the plan
    public function deletePlan($param="")
    {
      $query = $this->db->delete('tblplan', array('planid' => $param)); 
        if($query == 1)
        {
            return "true";
        }else
        {
            return "false";
        }
    }
    //Public function saveCompany for saving the rental company in table  'tblclientcompany'
    public function saveCompany($param="",$param1="")
    {
     
       // $six_digit_random_number = mt_rand(100000, 999999);
        
        $data['clientcompname'] = $this->input->post('companyname');
        $data['clientcompcode'] = mt_rand(100000, 999999);
        $data['clientcompcontact'] = $this->input->post('contactnum');
        $data['clientcompaddress'] = $this->input->post('address');
        $data['clientcompregistdate'] = $this->input->post('registdate');
//        $data['clientcompapproval'] = $this->input->post('aprstatus');
         $data['clientcompapproval'] = "pending";

        $data['clientcompstatus'] = $this->input->post('status');
     
      
        if($param == "create"){
        $query =$this->db->insert('tblclientcompany', $data);
        
        return "true";
        }
        
        if($param == "edit"){
            
            
                        $parameter = $this->input->post('parameter');
                            if($parameter == "approved"){
                                $data['clientcompapproval'] = 1;

                            }
                            if($parameter == "dissapproved"){
                                $data['clientcompapproval'] = 0;

                            }
       

            $query=  $this->db->update('tblclientcompany', $data, array('clientcompid' => $param1));
                    return "updated";

         }
        if($param == "delete"){
            
           
                   $query=  $this->db->delete('tblclientcompany', array('clientcompid' => $param1));
                    return "delete";

         }
          
    }
     //Function for changing the status of approved or dispproved of rental company in table ' tblclientcompany'
    public function statusCompany($param="")
    {
        $data['clientcompapproval'] = 1 ;
        $data['clientcompstatus'] = 1;
      
        $query=  $this->db->update('tblclientcompany', $data, array('clientcompid' => $param));
        return "updated";
    }
    
    //Function for printing the csv of car rental company
    public function CSVprint($param="")
    {
         $filename = "contactlist.csv";
$fp = fopen('php://output', 'w');
$header[0] = "Company_name";
$header[1] = "Company_Code";
$header[2] = "Company_Address";
$header[3] = "Registration Date";
//$header[4] = "Status";
$header[5] = "Approval Status";

if($param == "approved"){
$this->db->order_by("clientcompid", "desc");
$this->db->where('clientcompapproval',1);
//$this->db->or_where('clientcompapproval ', "pending");
$query = $this->db->get('tblclientcompany');

}

if($param == "disapproved"){
    $this->db->order_by("clientcompid", "desc");
    $this->db->where('clientcompapproval',0);
    $query = $this->db->get('tblclientcompany'); 
}

if($param == "pending"){
    $this->db->order_by("clientcompid", "desc");
    $this->db->where('clientcompapproval ', "pending");

   $query = $this->db->get('tblclientcompany');
    $rew1=$query->result();
}



$arr = [];

foreach($query->result() as $key=>$value){
$arr[$key]['Company_name'] = $value->clientcompname;
$arr[$key]['Company_Code'] = $value->clientcompcode;
$arr[$key]['Company_Address'] = $value->clientcompaddress;
$arr[$key]['Registration'] = $value->clientcompregistdate;

$st = $value->clientcompstatus;
$ap = $value->clientcompapproval;



if($st == 1){
    $s = "Active";
}else{
    $s="Inactive";
}
if($ap == 1){
    $a = "Approved";
}
if($ap == "" || $ap == 0){
    $a="Disapproved";
}

if($ap == "pending"){
    $a = "Pending";
}

//$arr[$key]['Status'] = $s;
$arr[$key]['Approval'] = $a;
}


header('Content-type: application/csv');
header('Content-Disposition: attachment; filename=' . $filename);
fputcsv($fp, $header);
foreach ($arr as $value1) {
fputcsv($fp, $value1);
}
exit;
        
    }       
     
    ////function for printing th pdf format 
    public function printPdf($param="")
    {
      
        
        
       if($param == "approved"){
$this->db->order_by("clientcompid", "desc");
$this->db->where('clientcompapproval',1);
//$this->db->or_where('clientcompapproval ', "pending");
$query = $this->db->get('tblclientcompany');
$rew1=$query->result();
}

if($param == "disapproved"){ 
    $this->db->order_by("clientcompid", "desc");
    $this->db->where('clientcompapproval',0);
    $this->db->where('clientcompapproval !=', "pending");
    $query = $this->db->get('tblclientcompany'); 
    $rew1=$query->result();
}


if($param == "pending"){
    $this->db->order_by("clientcompid", "desc");
    $this->db->where('clientcompapproval ', "pending");

   $query = $this->db->get('tblclientcompany');
    $rew1=$query->result();
}





      $ab=''; 
     
foreach($rew1 as $row1){
    
    $r = $row1->clientcompstatus;
    $a =$row1->clientcompapproval;
    
   
    if($a == 1){
        $y = "Approved";
    }
   


   
    if($a == 0){
        $y ="Notapproved";
    }
    
    if($a == "pending"){
    $y = "Pending";
}
    
    if($r == 1){
        $s = "Active";
    }else{
        $s ="Inactive";
    }
   
 $ab1=" <tr><td>".$row1->clientcompname."</td>"
         . "<td>".$row1->clientcompcode."</td>"
         . "<td>".$row1->clientcompaddress."</td>"
         . "<td>".$row1->clientcompregistdate."</td>"
         . "<td>".$s."</td>"
         . "<td>".$y."</td></tr>";
    $ab.= $ab1;
}

//echo $ab1;
          include("assets/mpdf/mpdf.php");
           $body= "<html>
<head>
<style>
table, th, td {
    border: 1px solid black;
   
}
table{
width:100%;
}
</style>
</head>
<body>




<table>
  <tr>
    <th>Company Name</th>
    <th>Company Code</th> 
    <th>Company Address</th>
    <th>Registration Date</th>
    <th>Status</th>
    <th>Approve Status</th>
   
  </tr>
  
 ".$ab."

</table>

</body>
</html>
";
  
       
        $mpdf = new mPDF('utf-8', 'A4',0,'',15,15,25,16,4,9,'P');
        
        $mpdf->allow_charset_conversion = true;
        $mpdf->charset_in = 'iso-8859-4';

    
         $mpdf->WriteHTML('<b> </b>');

        $mpdf->WriteHTML(utf8_encode($body));
     
        
        $mpdf->WriteHTML('<div style=�?
        text-align:center;>
        </div>');
        


        $mpdf->Output("abc.pdf","D");
exit;
        
    }
      //function fpor diapproving the company 
    
    public function disapproveCompany($param="")
    {
       $data['clientcompstatus'] = 0;
       $data['clientcompapproval'] = 0;
        $query=  $this->db->update('tblclientcompany', $data, array('clientcompid' => $param));
        return "updated";
    }
    // public function for manageuser for save ,edit,create,delete user of superadmin panel
    public function saveUser($param="",$param1="")
    {
        $data['supadmusername'] = $this->input->post('username');
        $data['supadmcontact'] = $this->input->post('contactnum');
        $data['supadmuseraddr'] = $this->input->post('address');
        $data['supadmjoiningdate'] = $this->input->post('joiningdate');
        $data['supadmuseremail'] = $this->input->post('useremail');
//         $data['supadmuserstatus'] = $this->input->post('status');
        $data['supadmuserrole'] = $this->input->post('role');
        $data['supadmuserpass'] = password_hash($this->input->post('pass'), PASSWORD_DEFAULT);
        $data['usertype'] = "user";
            
                            
       if($param =="create")
       {
           $query12 = $this->db->get_where('tblsupadmuser', array('supadmuseremail' => $data['supadmuseremail']));
        if($query12->num_rows() > 0)
        {
            return "exist";
        }
           
           
           
           
           if ($_FILES['image']['name']) 
                            {
                                   $random = substr(number_format(time() * rand(), 0, '', ''), 0, 5);
                                           $file_name = $random . $_FILES['image']['name'];
                                           $file_size = $_FILES['image']['size'];
                                           $file_tmp = $_FILES['image']['tmp_name'];
                                           $file_type = $_FILES['image']['type'];
                                           $file_ext = strtolower(end(explode('.', $_FILES['image']['name'])));
                                           $expensions = array("doc", "docx", "pdf");
                                           if (in_array($file_ext, $expensions) === false) 
                                           {             //$errors[]="extension not allowed, please choose a JPEG or PNG file.";      
                                           }
                                           move_uploaded_file($file_tmp, "uploads/images/" . $file_name);
                                           $data['supadmuserimg'] = $file_name;
                                           
                                            
                            }
                            
            if( $data['supadmuserimg'] == "")  {              
            $data['supadmuserimg'] = "user.jpg";    }           
           
            
               $data['supadmuserstatus'] = 1;
       
           
           $this->db->insert('tblsupadmuser',$data);
           
        
           return "insert";
       }
        if($param =="edit")
       {
                  
            if ($_FILES['image']['name']) 
                            {
                                   $random = substr(number_format(time() * rand(), 0, '', ''), 0, 5);
                                           $file_name = $random . $_FILES['image']['name'];
                                           $file_size = $_FILES['image']['size'];
                                           $file_tmp = $_FILES['image']['tmp_name'];
                                           $file_type = $_FILES['image']['type'];
                                           $file_ext = strtolower(end(explode('.', $_FILES['image']['name'])));
                                           $expensions = array("doc", "docx", "pdf");
                                           if (in_array($file_ext, $expensions) === false) 
                                           {             //$errors[]="extension not allowed, please choose a JPEG or PNG file.";      
                                           }
                                           move_uploaded_file($file_tmp, "uploads/images/" . $file_name);
                                           $data['supadmuserimg'] = $file_name;
                                           
                                            
                            } 
                          
                            
            
            $query=  $this->db->update('tblsupadmuser', $data, array('supadmuserid' => $param1));
                   
                   return "update";
       }
        if($param =="delete")
       {
                   $query=  $this->db->delete('tblsupadmuser', array('supadmuserid' => $param1));
                   
                   return "delete";
       }
        
      
    }
     //function for saving the role in table ' tblmasterrole'
     public function saveRole($param="",$param1="")
    {
      
         
         
         
        $data['masterrolename'] = $this->input->post('role');
        
        

        
        
        
        $data['masterrolecreatedby'] = $this->session->userdata['id'];
    
        
        
        
         if($param == "create")
         {
                               $data['masterrolestatus'] = 1;

             
             $query12 = $this->db->get_where('tblmasterrole', array('masterrolename' =>$data['masterrolename']));
                            if($query12->num_rows() > 0)
                            {
                                return "exist";
                            }
                            $this->db->insert('tblmasterrole',$data);
                           return "insert"; 
        }
         if($param == "edit"){
                 
     
            $query=  $this->db->update('tblmasterrole', $data, array('masterroleid' => $param1));
            
           
           return "update"; 
        }
         if($param == "delete"){
                   $query=  $this->db->delete('tblmasterrole', array('masterroleid' => $param1));
           return "delete"; 
        }

    }
     //function for saving the permission to a particular role in table 'tblsupadmpermission'
    public function savePermission($param="")
    {
        $data['supadmpermissionrole'] = $this->input->post('role');
        $permission = implode(",",$this->input->post('permission'));
        $data['supadmpermission'] = $permission;
        $data['permissioncreatedby'] = $this->session->alluserdata['id'];
        
        $query12 = $this->db->get_where('tblsupadmpermission', array('supadmpermissionrole' =>$data['supadmpermissionrole']));
        if($query12->num_rows() > 0)
        {
           $query=  $this->db->update('tblsupadmpermission', $data, array('supadmpermissionrole' => $data['supadmpermissionrole']));
           return "update";

        }
        else{
           $this->db->insert('tblsupadmpermission',$data);
           return "update";
        }
        
        
        
    }
      //function for getting permission by ajax in file permission view.php from table 'tblsupadmpermission'
    public function ajaxGetPermission()
    {
        $r =$_POST['roleid'];
        $query = $this->db->get_where('tblsupadmpermission',array('supadmpermissionrole'=>$r))->row_array();
        $permisssion_data = $query['supadmpermission'];
        $data = explode(",",$permisssion_data);
        echo json_encode($data);
    }
    
    //function for active the status of  role on or off
    public function changestatusRole($param="")
    {
        $query = $this->db->get_where('tblmasterrole',array('masterroleid'=>$param))->row_array();
        $status = $query['masterrolestatus'];
       if($status == 1)
        {
            $data['masterrolestatus'] = 0;
            $query=  $this->db->update('tblmasterrole', $data, array('masterroleid' =>$param));
            return "inactive" ;
        }
       if($status == 0)
        {
            $data['masterrolestatus'] = 1;
            $query=  $this->db->update('tblmasterrole', $data, array('masterroleid' =>$param));
            return "active";
        }

    }
    
    
     //function for active the status of  role on or off
    public function changestatusUser($param="")
    {
       
         $query = $this->db->get_where('tblsupadmuser',array('supadmuserid'=>$param))->row_array();
        $status = $query['supadmuserstatus'];
       if($status == 1)
        {
            $data['supadmuserstatus'] = 0;
            $query=  $this->db->update('tblsupadmuser', $data, array('supadmuserid' =>$param));
            return "inactive" ;
        }
       if($status == 0)
        {
            $data['supadmuserstatus'] = 1;
            $query=  $this->db->update('tblsupadmuser', $data, array('supadmuserid' =>$param));
            return "active";
        }
        
        
    }
    
    
      //function for active the status of  plan on or off
    public function changestatusPlan($param="")
    {
       $query = $this->db->get_where('tblplan',array('planid'=>$param))->row_array();
       
      
        $status = $query['planstatus'];
       if($status == 1)
        {
            $data['planstatus'] = 0;
            $query=  $this->db->update('tblplan', $data, array('planid' =>$param));
            return "inactive" ;
        }
       if($status == 0)
        {
            $data['planstatus'] = 1;
            $query=  $this->db->update('tblplan', $data, array('planid' =>$param));
            return "active";
        }  
       

    }
    
   //function for saving the brand in table tblbrand
    
    public function saveBrand($param1="",$param2="")
    {
       $data['brandname'] = $_POST['brand'];
       
           $query12 = $this->db->get_where('tblbrand', array('brandname' =>$_POST['brand']));
                            if($query12->num_rows() > 0)
                            {
                                return "exist";
                            }
       
       if($param1 == "create"){
           
           $this->db->insert("tblbrand",$data);
           return "insert";
       }
       
       if($param1 == "edit"){
          $query=  $this->db->update('tblbrand', $data, array('brandid' =>$param2));
            return "update"; 
       }
       
       if($param1 == "delete"){
            $query=  $this->db->delete('tblbrand', array('brandid' => $param2));
           return "delete"; 
       }
    }  
   
   
    
    
}