<?php
//
//<!--Company Name :- Lazlo Software Solution
//    Creation Date :-6/14/2018
//    Model :- frontModel
//    Description :- This is the frontManager controller   used for managing the frontend data of website from backend
//    Database Name:- carrental
//    Table Used :- 'tblcmscontact'tblfrontContact,tblvisionmission.tblcmsfaq,tblfrontContact -->

class FrontModel extends CI_Model {
    function __construct() {
        // Call the Model constructor
         $this->load->library('session');
          $this->load->database();
        error_reporting(0); 
ini_set('display_errors', 'Off');
 
    }
     //function for saving the contact us detail in table 'tblcmscontact'
    public function saveContact()
    {
        $id =$this->input->post('parameter');
        $value['cmscontactno'] = $this->input->post('contactno');
        $value['cmscontactemail'] = $this->input->post('email');
        $value['cmscontactaddr'] = $this->input->post('address');
        $query=  $this->db->update('tblcmscontact', $value, array('cmscontactid' => $id));
        if($query == 1)
        {
            return "true";
        }

    }
     //function for updating the vision ,mission and valies
     public function saveVision()
    {
            
  if ($_FILES['image']['name']) 
                            {
                                   $random = substr(number_format(time() * rand(), 0, '', ''), 0, 5);
                                           $file_name = $random . $_FILES['image']['name'];
                                           $file_size = $_FILES['image']['size'];
                                           $file_tmp = $_FILES['image']['tmp_name'];
                                           $file_type = $_FILES['image']['type'];
                                           $file_ext = strtolower(end(explode('.', $_FILES['image']['name'])));
                                           $expensions = array("doc", "docx", "pdf");
                                           if (in_array($file_ext, $expensions) === false) 
                                           {             //$errors[]="extension not allowed, please choose a JPEG or PNG file.";      
                                           }
                                           move_uploaded_file($file_tmp, "uploads/images/" . $file_name);
                                           $value['visionimage'] = $file_name;
                            }
      
         
         
         
         
         $id = $this->input->post('parameter');
       $value['visiondesc'] =  $this->input->post('vision');
       $value['missiondesc'] =  $this->input->post('mission');
       $value['valuedesc'] =  $this->input->post('value');
        $query=  $this->db->update('tblvisionmission', $value, array('visionmissionid' => $id));
        if($query == 1)
        {
            return "true";
        }
        

    }
  

//   /    public function for  saving the faq in table tblfaq
    public function saveFaq($param="",$param1="")
    {
       $value['cmsfaqquestions'] = $this->input->post('question');
        $value['cmsfaqanswers'] = $this->input->post('answer');
        
      
        
       
        if($param == "create"){
                    $value['cmsfaqstatus'] = 1;

            $this->db->insert('tblcmsfaq',$value);
            return "insert";
        }
      
        
        if($param == "edit"){
        $query=  $this->db->update('tblcmsfaq', $value, array('cmsfaqid' => $param1));
            return "edit";
        }
         
        if($param == "delete"){
                  $this->db->delete('tblcmsfaq', array('cmsfaqid' => $param1));     
                  return "delete";
        }

    }
     //public function for saving the contact fron fonend site in table 'tblfrontContact'
    public function savefrontContact()
    {
       
        date_default_timezone_set("Asia/Calcutta");
       $form_create_date = date('Y-m-d H:i:s', date(U));
       $Order_date = date('Y-m-d H:i:s', date(U));
         
        $data['frontContactname'] = $this->input->post('name');
        $data['frontContactEmail']   = $this->input->post('email');
        $data['frontContactMessage'] = $this->input->post('message');
        $data['frontContactdate']  =  $Order_date;
        
        
        
        
        
        
        
        
        
        
        
          $query =  $this->db->get('tblsupadmprofile')->row_array();
          $adminemail = $query['supadmprofileemail'];
        
          $to =  $adminemail;
        $subject = "Contact Message";
        $txt = $data['frontContactMessage'];
        $headers = "From: info@srngroups.com" . "\r\n" .
        "CC: lss.tm10@livesoftwaresolution.com";

        mail($to,$subject,$txt,$headers);
        
        
        
       $query =  $this->db->insert('tblfrontcontact',$data);
       if($query == 1)
       {
           return "true";
       }
        
    }
     //function for deleting the contact messages comes from frontend in the table 'tblfrontContact'
    public function deleteContact($param="")
    {
       $query =  $this->db->delete('tblfrontcontact', array('frontContactid' => $param));    
       
        if($query == 1)
       {
           return "true";
       }
    } 
    
     //function for saving the banner images in table 'tblcmshome;
    public function saveBanner($param1="",$param2="")
    {
       
       $value['cmshometitle'] = $this->input->post('title');
       $value['cmshomedescription'] = $this->input->post('description');
      
       
  if ($_FILES['image']['name']) 
                            {
                                   $random = substr(number_format(time() * rand(), 0, '', ''), 0, 5);
                                           $file_name = $random . $_FILES['image']['name'];
                                           $file_size = $_FILES['image']['size'];
                                           $file_tmp = $_FILES['image']['tmp_name'];
                                           $file_type = $_FILES['image']['type'];
                                           $file_ext = strtolower(end(explode('.', $_FILES['image']['name'])));
                                           $expensions = array("doc", "docx", "pdf");
                                           if (in_array($file_ext, $expensions) === false) 
                                           {             //$errors[]="extension not allowed, please choose a JPEG or PNG file.";      
                                           }
                                           move_uploaded_file($file_tmp, "uploads/images/" . $file_name);
                                           $value['cmshomeimage'] = $file_name;
                            }
      
        
        
            if($param1 == "create")
            {
                 $value['cmshomestatus'] = 1;
                if($value['cmshomeimage'] == "")
                {
                    $value['cmshomeimage'] = "02.jpg";
                }
                
                
                 $query =   $this->db->insert("tblcmshome",$value);
               if($query == 1)
                    {
                        return "create";
                    }
                    
            }   
            

                if($param1 == "edit")
                {
                     if($file_name!="")
                     {
                        $value['cmshomeimage'] = $file_name; 

                     }
                       $query=  $this->db->update('tblcmshome', $value, array('cmshomeid' => $param2));
                   return "edit";


                }
                if($param1 == "delete")
                {
                   $query =  $this->db->delete('tblcmshome', array('cmshomeid' => $param2));    
       
                        if($query == 1)
                       {
                           return "delete";
                       } 
                 }
                    
                                 
                            
    }
    
     //function for changing the status of banner
    public function changestatusBanner($param="")
    {
     
         $query = $this->db->get_where('tblcmshome',array('cmshomeid'=>$param))->row_array();
        $status = $query['cmshomestatus'];
       if($status == 1)
        {
            $data['cmshomestatus'] = 0;
            $query=  $this->db->update('tblcmshome', $data, array('cmshomeid' =>$param));
            return "inactive" ;
        }
       if($status == 0)
        {
            $data['cmshomestatus'] = 1;
            $query=  $this->db->update('tblcmshome', $data, array('cmshomeid' =>$param));
            return "active";
        }
    }
    
      //function for saving the  copyright and url in table 'tblmanageUrl;
   //function for updating the copyright and url in table ' tblmanagerUrl'
    public function savefrontUrl()
    {
      $data['manageUrltwitter'] = $this->input->post('twitter');
      $data['manageUrlin'] = $this->input->post('in');
      $data['manageUrlgplus'] = $this->input->post('gplus');
      $data['manageUrlfb'] = $this->input->post('facebook');
      $data['managecopyright'] = $this->input->post('copyright');
        $id = $this->input->post('parameter');
          $query=  $this->db->update('tblmanagerurl', $data, array('	manageUrlid' =>$id));
           return "true";
    }
     //function for changing the status Faq in table 'tblcmsFaq'
    public function changestatusFaq($param="")
    {
        $query = $this->db->get_where('tblcmsfaq',array('cmsfaqid'=>$param))->row_array();
        $status = $query['cmsfaqstatus'];
       if($status == 1)
        {
            $data['cmsfaqstatus'] = 0;
            $query=  $this->db->update('tblcmsfaq', $data, array('cmsfaqid' =>$param));
            return "inactive" ;
        }
       if($status == 0)
        {
            $data['cmsfaqstatus'] = 1;
            $query=  $this->db->update('tblcmsfaq', $data, array('cmsfaqid' =>$param));
            return "active";
        }
        
    }
    
    //function for saving the news and event
    public function saveNews($param1="",$param2="")
    {
         
        
        
        $data['newseventheading'] = $this->input->post('heading');
          $data['newseventtitle'] =  $this->input->post('title');
          $data['newseventdate'] =  $this->input->post('newsdate');
          $data['newseventdesc'] = $this->input->post('description');
          $data['newseventtab'] = $this->input->post('tab');
          if ($_FILES['image']['name']) 
                            {
                                   $random = substr(number_format(time() * rand(), 0, '', ''), 0, 5);
                                           $file_name = $random . $_FILES['image']['name'];
                                           $file_size = $_FILES['image']['size'];
                                           $file_tmp = $_FILES['image']['tmp_name'];
                                           $file_type = $_FILES['image']['type'];
                                           $file_ext = strtolower(end(explode('.', $_FILES['image']['name'])));
                                           $expensions = array("doc", "docx", "pdf");
                                           if (in_array($file_ext, $expensions) === false) 
                                           {             //$errors[]="extension not allowed, please choose a JPEG or PNG file.";      
                                           }
                                           move_uploaded_file($file_tmp, "uploads/images/" . $file_name);
                                           $data['newseventimage'] = $file_name;
                            }
         
          if($param1 == "create")
          {
              if($data['newseventimage'] == "")
              {
                 $data['newseventimage'] = "02.jpg"; 
              }
             
              
              $data['newseventstatus'] = 1;
             
                 $query =   $this->db->insert("tblnewsevent",$data);
               if($query == 1)
                    {
                        return "create";
                    }
          }
          if($param1 == "edit")
          {
              if($data['newseventimage'] == "")
              {
                 $data['newseventimage'] = "02.jpg"; 
              }
            $query=  $this->db->update('tblnewsevent', $data, array('newsenentid' =>$param2));
               if($query == 1)
                    {
                        return "edit";
                    }
          }
           if($param1 == "delete")
          {
             
                   $query =  $this->db->delete('tblnewsevent', array('newsenentid' => $param2));    
               if($query == 1)
                    {
                        return "delete";
                    }
          }
 
    }
    
    //function change status of changestatusNews in table ' tblnewsevent'
    public function  changestatusNews($param="")
    {
        $query = $this->db->get_where('tblnewsevent',array('newsenentid'=>$param))->row_array();
        $status = $query['newseventstatus'];
       if($status == 1)
        {
            $data['newseventstatus'] = 0;
            $query=  $this->db->update('tblnewsevent', $data, array('newsenentid' =>$param));
            return "inactive" ;
        }
       if($status == 0)
        {
            $data['newseventstatus'] = 1;
            $query=  $this->db->update('tblnewsevent', $data, array('newsenentid' =>$param));
            return "active";
        } 
    
    }
    
      //function for saving the tab in table tab
    public function updateTab()
    {
        $data1['tab1'] = $this->input->post('tab1');
       $data2['tab2'] = $this->input->post('tab2');
       $data3['tab3'] = $this->input->post('tab3');
        $id = $this->input->post('parameter');
        
       
        
        if($data['tab1'] !=""){
        
        $query=  $this->db->update('tbltab', $data1, array('tabnumber' =>"tab1"));}
        
        
          
        if($data['tab2'] !=""){
        
        $query=  $this->db->update('tbltab', $data2, array('tabnumber' =>"tab2"));}
        
         if($data['tab3'] !=""){
        
        $query=  $this->db->update('tbltab', $data3, array('tabnumber' =>"tab3"));}
       
        
            return "update" ;


    }
    
    //function for saving the corefeayures in table corefeatures
    public function savecoreFeatures($param1="",$param2="")
    {  
         $data['corefeaturetitle'] = $this->input->post('title');
        $data['corefeaturedesc']=  $this->input->post('description');
        $data['corefeatureicon'] = $this->input->post('icon');
        if ($_FILES['image']['name']) 
                            {
                                   $random = substr(number_format(time() * rand(), 0, '', ''), 0, 5);
                                           $file_name = $random . $_FILES['image']['name'];
                                           $file_size = $_FILES['image']['size'];
                                           $file_tmp = $_FILES['image']['tmp_name'];
                                           $file_type = $_FILES['image']['type'];
                                           $file_ext = strtolower(end(explode('.', $_FILES['image']['name'])));
                                           $expensions = array("doc", "docx", "pdf");
                                           if (in_array($file_ext, $expensions) === false) 
                                           {             //$errors[]="extension not allowed, please choose a JPEG or PNG file.";      
                                           }
                                           move_uploaded_file($file_tmp, "uploads/images/" . $file_name);
                                           $data['corefeatureimage'] = $file_name;
                            }
                            
                            
                 
          if($param1 == "create")
          {
              if($data['corefeatureimage'] == "")
              {
                 $data['corefeatureimage'] = "02.jpg"; 
              }
             
              
              $data['corefeaturestatus'] = 1;
              
            
             
                 $query =   $this->db->insert("tblcorefeature",$data);
               if($query == 1)
                    {
                        return "create";
                    }
          }
          if($param1 == "edit")
          {
           
              
              if($data['corefeatureimage'] == "")
              {
                 $data['corefeatureimage'] = "02.jpg"; 
              }
            $query=  $this->db->update('tblcorefeature', $data, array('corefeaturesid' =>$param2));
               if($query == 1)
                    {
                        return "edit";
                    }
          }
           if($param1 == "delete")
          {
             
                   $query =  $this->db->delete('tblcorefeature', array('corefeaturesid' => $param2));    
               if($query == 1)
                    {
                        return "delete";
                    }
          }
         
              
    }
    
    
     //function change status of changestatusNews in table ' tblnewsevent'
    public function  changestatusCore($param="")
    {
        $query = $this->db->get_where('tblcorefeature',array('corefeaturesid'=>$param))->row_array();
        $status = $query['corefeaturestatus'];
       if($status == 1)
        {
            $data['corefeaturestatus'] = 0;
            $query=  $this->db->update('tblcorefeature', $data, array('corefeaturesid' =>$param));
            return "inactive" ;
        }
       if($status == 0)
        {
            $data['corefeaturestatus'] = 1;
            $query=  $this->db->update('tblcorefeature', $data, array('corefeaturesid' =>$param));
            return "active";
        } 
    
    }
    
      //function for saving th enquiry from frontend in table "tblenquiry"
    public function saveEnquiry()
    {
            
          date_default_timezone_set("Asia/Calcutta");
       $form_create_date = date('Y-m-d H:i:s', date(U));
       $Order_date = date('Y-m-d H:i:s', date(U));
        
        
        
        
        $data['enquiryname'] = $this->input->post('name');
                $data['enquiryemail'] = $this->input->post('email');
                $data['enquirycontact'] = $this->input->post('contact');
                $data['enquirymesg'] = $this->input->post('message');
                $data['enquirydat'] = $Order_date;
                
                
                
          $query =  $this->db->get('tblsupadmprofile')->row_array();
          $adminemail = $query['supadmprofileemail'];
        
          $to =  $adminemail;
        $subject = "Enquiry Message";
        $txt = $data['enquirymesg'];
        $headers = "From: info@srngroups.com" . "\r\n" .
        "CC: lss.tm10@livesoftwaresolution.com";

        mail($to,$subject,$txt,$headers);
        
                
                
                
                
                
               
                $this->db->insert('tblenquiry',$data);
                
               
                return "true";
        
    }
    
    //public function save about US in table
    public function saveaboutUs($param1="",$param2="")
    {
//       $data['aboutusheading'] =  $this->input->post('heading');
       $data['aboutustitle'] = $this->input->post('title');
       $data['aboutusdesc'] = $this->input->post('description');
       
     
       if($param1 == "create"){
           $data['aboutusstatus'] = 1;
            $query =   $this->db->insert("tblaboutus",$data);
              return "create";
       }
       
         if($param1 == "edit"){
                       $query=  $this->db->update('tblaboutus', $data, array('aboutusid' =>$param2));
               
                       return "edit";
                       }
                       if($param1 == "delete")
                     {
             
                   $query =  $this->db->delete('tblaboutus', array('aboutusid' => $param2));    
               if($query == 1)
                    {
                        return "delete";
                    }
                       }

    }
     //function change status of changestatusabout in table ' tblnewsevent'
    public function  changestatusabout($param="")
    {
        $query = $this->db->get_where('tblaboutus',array('aboutusid'=>$param))->row_array();
        $status = $query['aboutusstatus'];
       if($status == 1)
        {
            $data['aboutusstatus'] = 0;
            $query=  $this->db->update('tblaboutus', $data, array('aboutusid' =>$param));
            return "inactive" ;
        }
       if($status == 0)
        {
            $data['aboutusstatus'] = 1;
            $query=  $this->db->update('tblaboutus', $data, array('aboutusid' =>$param));
            return "active";
        } 
    
    }
    
     public function saveRecommended($param1="",$param2="")
    {
        $data['recommendheading'] = $this->input->post('heading');
        $data['recommendtitle']=  $this->input->post('title');
             $data['recommenddesc']=  $this->input->post('description');

        if ($_FILES['image']['name']) 
                            {
                                   $random = substr(number_format(time() * rand(), 0, '', ''), 0, 5);
                                           $file_name = $random . $_FILES['image']['name'];
                                           $file_size = $_FILES['image']['size'];
                                           $file_tmp = $_FILES['image']['tmp_name'];
                                           $file_type = $_FILES['image']['type'];
                                           $file_ext = strtolower(end(explode('.', $_FILES['image']['name'])));
                                           $expensions = array("doc", "docx", "pdf");
                                           if (in_array($file_ext, $expensions) === false) 
                                           {             //$errors[]="extension not allowed, please choose a JPEG or PNG file.";      
                                           }
                                           move_uploaded_file($file_tmp, "uploads/images/" . $file_name);
                                           $data['recommmendimage'] = $file_name;
                            }
            if($param1 == "create"){
                
                
                 if($data['recommmendimage'] == "")
              {
                 $data['recommmendimage'] = "02.jpg"; 
              }
                
                
                
           $data['recommedstatus'] = 1;
            $query =   $this->db->insert("tblrecommend",$data);
              return "create";
       }
       
        if($param1 == "edit"){
                       $query=  $this->db->update('tblrecommend', $data, array('recommendid' =>$param2));
               
                       return "edit";
                       }
                            
                     if($param1 == "delete")
                     {
             
                   $query =  $this->db->delete('tblrecommend', array('recommendid' => $param2));    
               if($query == 1)
                    {
                        return "delete";
                    }
                       }        
                            
                            
                            
    }

    
     //function change status of changestatusabout in table ' tblnewsevent'
    public function  changestatusRecommend($param="")
    {
        $query = $this->db->get_where('tblrecommend',array('recommendid'=>$param))->row_array();
        $status = $query['recommedstatus'];
       if($status == 1)
        {
            $data['recommedstatus'] = 0;
            $query=  $this->db->update('tblrecommend', $data, array('recommendid' =>$param));
            return "inactive" ;
        }
       if($status == 0)
        {
            $data['recommedstatus'] = 1;
            $query=  $this->db->update('tblrecommend', $data, array('recommendid' =>$param));
            return "active";
        } 
    
    }
     //function for saving the newspaper  in table tblnewspaper'
    public function saveNewspaper()
    {
     
        $query1 = $this->db->get_where('tblnewsletter',array('newsletteremail'=>$this->input->post('email')));
        if($query1->num_rows() > 0)
        {
             $data['newsletterstatus'] = 1;
               $query=  $this->db->update('tblnewsletter', $data, array('newsletteremail' =>$this->input->post('email')));
               return "create";
             
        }

        
        
        $data['newsletteremail'] = $this->input->post('email');
       $data['newsletterstatus'] = 1;
       $query =   $this->db->insert("tblnewsletter",$data);
       return "create";

    }
    
     
     //function change status of changestatusabout in table ' tblnewsevent'
    public function  changestatusletter($param="")
    {
        $query = $this->db->get_where('tblnewsletter',array('newsletterid'=>$param))->row_array();
        $status = $query['newsletterstatus'];
       if($status == 1)
        {
            $data['newsletterstatus'] = 0;
            $query=  $this->db->update('tblnewsletter', $data, array('newsletterid' =>$param));
            return "inactive" ;
        }
       if($status == 0)
        {
            $data['newsletterstatus'] = 1;
            $query=  $this->db->update('tblnewsletter', $data, array('newsletterid' =>$param));
            return "active";
        } 
    
    }
    
      public function saveProject($param1="",$param2="")
    {
        
         
          $data['cmsprojetctitle']=  $this->input->post('title');
         
        if ($_FILES['image']['name']) 
                            {
                                   $random = substr(number_format(time() * rand(), 0, '', ''), 0, 5);
                                           $file_name = $random . $_FILES['image']['name'];
                                           $file_size = $_FILES['image']['size'];
                                           $file_tmp = $_FILES['image']['tmp_name'];
                                           $file_type = $_FILES['image']['type'];
                                           $file_ext = strtolower(end(explode('.', $_FILES['image']['name'])));
                                           $expensions = array("doc", "docx", "pdf");
                                           if (in_array($file_ext, $expensions) === false) 
                                           {             //$errors[]="extension not allowed, please choose a JPEG or PNG file.";      
                                           }
                                           move_uploaded_file($file_tmp, "uploads/images/" . $file_name);
                                           $data['cmsprojectimage'] = $file_name;
                            }
            if($param1 == "create"){
                
                
                 if($data['cmsprojectimage'] == "")
              {
                 $data['cmsprojectimage'] = "02.jpg"; 
              }
                
                
                
           $data['cmsprojectstatus'] = 1;
            $query =   $this->db->insert("tblcmsproject",$data);
              return "create";
       }
       
        if($param1 == "edit"){
            
            
            
                       $query=  $this->db->update('tblcmsproject', $data, array('cmsprojectid' =>$param2));
               
                       return "edit";
                     
                       
                       
                       
        }
                            
                     if($param1 == "delete")
                     {
             
                   $query =  $this->db->delete('tblcmsproject', array('cmsprojectid' => $param2));    
               if($query == 1)
                    {
                        return "delete";
                    }
                       }        
                            
                            
                            
    }

     //function change status of changestatusabout in table ' tblnewsevent'
    public function  changestatusProject($param="")
    {
        $query = $this->db->get_where('tblcmsproject',array('cmsprojectid'=>$param))->row_array();
        $status = $query['cmsprojectstatus'];
       if($status == 1)
        {
            $data['cmsprojectstatus'] = 0;
            $query=  $this->db->update('tblcmsproject', $data, array('cmsprojectid' =>$param));
            return "inactive" ;
        }
       if($status == 0)
        {
            $data['cmsprojectstatus'] = 1;
            $query=  $this->db->update('tblcmsproject', $data, array('cmsprojectid' =>$param));
            return "active";
        } 
    
    }
    
    
      public function saveclientFolio($param1="",$param2="")
    {
                          
         
          $data['cmsfolioheading']=  $this->input->post('heading');
          $data['cmsfoliotitle']=  $this->input->post('title');
          $data['cmsfoliodesc']=  $this->input->post('description');
         
                  
        if ($_FILES['image']['name']) 
                            {
                                   $random = substr(number_format(time() * rand(), 0, '', ''), 0, 5);
                                           $file_name = $random . $_FILES['image']['name'];
                                           $file_size = $_FILES['image']['size'];
                                           $file_tmp = $_FILES['image']['tmp_name'];
                                           $file_type = $_FILES['image']['type'];
                                           $file_ext = strtolower(end(explode('.', $_FILES['image']['name'])));
                                           $expensions = array("doc", "docx", "pdf");
                                           if (in_array($file_ext, $expensions) === false) 
                                           {             //$errors[]="extension not allowed, please choose a JPEG or PNG file.";      
                                           }
                                           move_uploaded_file($file_tmp, "uploads/images/" . $file_name);
                                           $data['cmsfolioimg'] = $file_name;
                            }
            if($param1 == "create"){
                
                
                 if($data['cmsfolioimg'] == "")
              {
                 $data['cmsfolioimg'] = "02.jpg"; 
              }
                
                
                
           $data['cmsfoliostatus'] = 1;
            $query =   $this->db->insert("tblcmsfolio",$data);
              return "create";
       }
       
        if($param1 == "edit"){
            
            
            
                       $query=  $this->db->update('tblcmsfolio', $data, array('cmsfolioid' =>$param2));
               
                       return "edit";
                     
                       
                       
                       
        }
                            
                     if($param1 == "delete")
                     {
             
                   $query =  $this->db->delete('tblcmsfolio', array('cmsfolioid' => $param2));    
               if($query == 1)
                    {
                        return "delete";
                    }
                       }        
                            
                            
                            
    }
    
    
      //function change status of changestatusabout in table ' tblnewsevent'
    public function  changestatusFolio($param="")
    {
        $query = $this->db->get_where('tblcmsfolio',array('cmsfolioid'=>$param))->row_array();
        $status = $query['cmsfoliostatus'];
       if($status == 1)
        {
            $data['cmsfoliostatus'] = 0;
            $query=  $this->db->update('tblcmsfolio', $data, array('cmsfolioid' =>$param));
            return "inactive" ;
        }
       if($status == 0)
        {
            $data['cmsfoliostatus'] = 1;
            $query=  $this->db->update('tblcmsfolio', $data, array('cmsfolioid' =>$param));
            return "active";
        } 
    
    }
    
     //function for saving the savehelpSupport in table ' tblhelpSupport'
    public function savehelpSupport($param1="",$param2="")
    {
    
    $data['helpSupportheading'] = $this->input->post('heading');
    $data['helpSupporttitle'] = $this->input->post('title');
    $data['helpSupportdesc'] = $this->input->post('description');
    if($param1=="create"){
        $data['helpSupportstatus'] = 1;
        
        $query =   $this->db->insert("tblhelpsupport",$data);
        return "create";
 }

  if($param1 == "edit"){
               $query=  $this->db->update('tblhelpsupport', $data, array('supportid' =>$param2));
               
                       return "edit";
        }
         
                     if($param1 == "delete")
                     {
             
                   $query =  $this->db->delete('tblhelpsupport', array('supportid' => $param2)); 
                   return "delete";
                       } 
 
    
    }
    
     //function change status of changestatusabout in table ' tblnewsevent'
    public function  changestatussupport($param="")
    {
      
        
        $query = $this->db->get_where('tblhelpsupport',array('supportid'=>$param))->row_array();
        $status = $query['helpSupportstatus'];
       if($status == 1)
        {
            $data['helpSupportstatus'] = 0;
            $query=  $this->db->update('tblhelpsupport', $data, array('supportid' =>$param));
            return "inactive" ;
        }
       if($status == 0)
        {
            $data['helpSupportstatus'] = 1;
            $query=  $this->db->update('tblhelpsupport', $data, array('supportid' =>$param));
            return "active";
        } 
    
    }
        
    public function updatefaqImage()
    {
      
        
        if ($_FILES['image']['name']) 
                            {
                                   $random = substr(number_format(time() * rand(), 0, '', ''), 0, 5);
                                           $file_name = $random . $_FILES['image']['name'];
                                           $file_size = $_FILES['image']['size'];
                                           $file_tmp = $_FILES['image']['tmp_name'];
                                           $file_type = $_FILES['image']['type'];
                                           $file_ext = strtolower(end(explode('.', $_FILES['image']['name'])));
                                           $expensions = array("doc", "docx", "pdf");
                                           if (in_array($file_ext, $expensions) === false) 
                                           {             //$errors[]="extension not allowed, please choose a JPEG or PNG file.";      
                                           }
                                           move_uploaded_file($file_tmp, "uploads/images/" . $file_name);
                                           $data['cmsfaqimage'] = $file_name;
                            }
                   
                    if($data['cmsfaqimage'] !=""){
                   $query=  $this->db->update('tblcmsfaq', $data, array('cmsfaqstatus' =>"image"));
                    }
                   
                           return "true"; 
                            
        
        
    }

    
}