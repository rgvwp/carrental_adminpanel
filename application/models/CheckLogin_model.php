<!--Company Name :- Lazlo Software Solution
    Creation Date :-6/6/2018
    Model Name :- CheckLogin_model
    Description :- In this model we check that username and password are regitered or not if registered user can login to dashboard
    Database Name:- carrental
    Table Used :- 'tblsupadmprofile'. --!>



<?php



class CheckLogin_model extends CI_Model {



    function __construct() {

        // Call the Model constructor

        parent::__construct();

    }
  

//    ******Function for check the login details of user and admin in table 'tblsupadmuser' for user and for admin in 'tblsupadmprofile'*********************
    public function checkLogin() 
    {
        
         $loginquery = $this->db->get_where('tblsupadmprofile', array('supadmprofilename' => $this->input->post('email')));
        $result = $loginquery->result_array();
        
       if (password_verify($this->input->post('password'), $result[0]['supadmprofilepass'])) {
        
           if($loginquery->num_rows() >0)
         {
            $newdata = array(
                           'usertype'=>$result[0]['usertype'],
                            'username'  => $result[0]['supadmprofilename'],
                            'email'     => $result[0]['supadmprofileemail'],
                           
                            'id' =>$result[0]['supadmprofileid']
                    );

                    $this->session->set_userdata($newdata);
              
             return "true";
         }
           
           
           
       }
        
//        $loginquery = $this->db->get_where('tblsupadmprofile', array('supadmprofilename' => $this->input->post('email'),'supadmprofilepass'=>$this->input->post('password')));
        

//        $result = $loginquery->result_array();
        
        
//        if($loginquery->num_rows() >0)
//         {
//            $newdata = array(
//                           'usertype'=>$result[0]['usertype'],
//                            'username'  => $result[0]['supadmprofilename'],
//                            'email'     => $result[0]['supadmprofileemail'],
//                           
//                            'id' =>$result[0]['supadmprofileid']
//                    );
//
//                    $this->session->set_userdata($newdata);
//              
//             return "true";
//         }
         ////////////
//        $userquery = $this->db->get_where('tblsupadmuser', array('supadmusername' => $this->input->post('email'),'supadmuserpass'=>$this->input->post('password')));
//        $result1 = $userquery->result_array();
//
//         if($userquery->num_rows() >0)
//         {
//            $newdata = array(
//                           'usertype'=>$result1[0]['usertype'],
//                            'username'  => $result1[0]['supadmusername'],
//                            'email'     => $result1[0]['supadmuseremail'],
//                            'id' =>$result1[0]['supadmuserid'],
//                  'role' =>$result1[0]['supadmuserrole']
//                    );
//
//                    $this->session->set_userdata($newdata);
//              
//             return "true";
//         }
//         
       
         
              $userquery = $this->db->get_where('tblsupadmuser', array('supadmusername' => $this->input->post('email')));

        
        $result1 = $userquery->result_array();
        
     
         if (password_verify($this->input->post('password'), $result1[0]['supadmuserpass'])) {
                if($userquery->num_rows() >0)
                {
                    
                   $newdata = array(
                                  'usertype'=>$result1[0]['usertype'],
                                   'username'  => $result1[0]['supadmusername'],
                                   'email'     => $result1[0]['supadmuseremail'],
                                   'id' =>$result1[0]['supadmuserid'],
                         'role' =>$result1[0]['supadmuserrole']
                           );

                           $this->session->set_userdata($newdata);

                    return "true";
                }
         
         }
         ////////// 
         
         else
         {
           return "false";
         }
       

    }

     
}
?>