<?php
//<!--Company Name :- Lazlo Software Solution
//    Creation Date :-6/28/2018
//    Modal Name :- ClientuserModel
//    Description :- This is the ClientuserModel where database operation are performed 
//clienuser can update profile ,change password 
//    Database Name:- carrental
//    Table Used :- 'tblclientuser --!>

class ClientuserModel extends CI_Model {
    function __construct() {
        // Call the Model constructor
         $this->load->library('session');
          $this->load->database();
          error_reporting(0);
 
    }
  

//Function for updating the super admin profile in tblclientuser table
    public function profileUpdate() 
    { 
       $id = $this->input->post('supid');
       $data['clientusername'] = $this->input->post('name');
       $data['clientusercontact'] = $this->input->post('contactno');
       $data['clientuseremail'] = $this->input->post('email');
       $data['clientuseraddress'] = $this->input->post('address');
     
        if ($_FILES['image']['name']) 
                            {
                                   $random = substr(number_format(time() * rand(), 0, '', ''), 0, 5);
                                           $file_name = $random . $_FILES['image']['name'];
                                           $file_size = $_FILES['image']['size'];
                                           $file_tmp = $_FILES['image']['tmp_name'];
                                           $file_type = $_FILES['image']['type'];
                                           $file_ext = strtolower(end(explode('.', $_FILES['image']['name'])));
                                           $expensions = array("doc", "docx", "pdf");
                                           if (in_array($file_ext, $expensions) === false) 
                                           {             //$errors[]="extension not allowed, please choose a JPEG or PNG file.";      
                                           }
                                           move_uploaded_file($file_tmp, "uploads/images/" . $file_name);
                                           $data['clientuserimg'] = $file_name;
                            }
                     
        $query=     $this->db->update('tblclientuser', $data, "clientuserid= $id");
       return "true";
                             
    }

   // Function for checking the old password of client user in table 'tblclientuser'
    public function checkPassword(){
        $id = $this->input->post('supid'); 
        $oldpass = $this->input->post('oldPassword');
        
        
        $query1 = $this->db->get_where('tblclientuser', array('clientuserid' => $id))->row_array();
        
        
        $pass = $query1['clientuserpass'];
        
        
       if(password_verify($this->input->post('oldPassword'), $pass))
            {
                
           $as =  password_hash($this->input->post('pass'), PASSWORD_DEFAULT);  
           
           $data['clientuserpass'] = $as;
                $this->db->update('tblclientuser', $data, "clientuserid= $id");
                 return "match";
            }
        else
            {
               return  "notmatch";
            }
       
    }
   
}