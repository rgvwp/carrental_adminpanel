<?php
//<!--Company Name :- Lazlo Software Solution
//    Creation Date :-6/6/2018
//    Modal Name :- SuperAdmin_model
//    Description :- This is the SuperAdmin_model where database operation are performed 
//superadmin can update profile ,change password , change settings
//, update template .
//     
//    Database Name:- carrental
//    Table Used :- 'tblsupadmprofile',tblsetting,tblemailsett,tbltemplate,'tblplan --!>

class Client_model extends CI_Model {
    function __construct() {
        // Call the Model constructor
    $this->load->library('session');
    $this->load->database();
    error_reporting(0);
      //  parent::__construct();

    }
  

//Function for updating the super admin profile in tblsupadmprofile table
    public function profileUpdate() 
    { 
      
       $id = $this->input->post('supid');
       $data['regisclientFname'] = $this->input->post('name');
       $data['regisclientcontact'] = $this->input->post('contactno');
       $data['regisClientEmail'] = $this->input->post('email');
       $data['regisclientaddress'] = $this->input->post('address');
     //  $data['supadmprofilepass'] = $this->input->post('pass');
       $data['regisupdatedate'] = date('y-m-d');
        if ($_FILES['image']['name']) 
                            {
                                   $random = substr(number_format(time() * rand(), 0, '', ''), 0, 5);
                                           $file_name = $random . $_FILES['image']['name'];
                                           $file_size = $_FILES['image']['size'];
                                           $file_tmp = $_FILES['image']['tmp_name'];
                                           $file_type = $_FILES['image']['type'];
                                           $file_ext = strtolower(end(explode('.', $_FILES['image']['name'])));
                                           $expensions = array("doc", "docx", "pdf");
                                           if (in_array($file_ext, $expensions) === false) 
                                           {             //$errors[]="extension not allowed, please choose a JPEG or PNG file.";      
                                           }
                                           move_uploaded_file($file_tmp, "uploads/images/" . $file_name);
                                           $data['regisclientimg'] = $file_name;
                            }

                 
          //   $query =  $this->db->update('tblsupadmprofile', $data, "id = $id");
                           
        $query=     $this->db->update('tblregisclient', $data, "regisClientId= $id");
          
        return "true";
                            
                            
    }

   // Function for checking the old password of superadmin 
    public function checkPassword()
    {
      
        $id = $this->input->post('supid'); 
        
       $oldpass = $this->input->post('oldPassword');
     
       $query = $this->db->get_where('tblregisclient',array('regisClientId'=>$id))->row_array();
       $pass = $query['regisclientpassword'];
      
       
       
        if(password_verify($this->input->post('oldPassword'), $pass))
        {
         
            $as =  password_hash($this->input->post('pass'), PASSWORD_DEFAULT);  
            
            $data['regisclientpassword'] = $as;
           
            
            $this->db->update('tblregisclient', $data, "regisClientId= $id");
 
           return "match";
        }
        else
        {
           return  "notmatch";
        }
       
       
    }

    //Public function updateSitesSettings for updating the social site in table tblsitesetting
    public function updateSitesSettings()
    {
        
               $clientcompanyid  = $this->session->userdata['companyid'];
        $data['clientcompid'] = $clientcompanyid;

        
        $data['compsettingfb'] = $this->input->post('fburl');
       $data['compsettingtwitter'] = $this->input->post('twitterUrl');
       $data['compsettinggplus'] = $this->input->post('gurl');
       $data['compsettinginstaurl'] = $this->input->post('insta');
       $data['compsettinglinkinurl'] = $this->input->post('linkdin');
       $id = $this->input->post('settingId');
       $query = $this->db->update('tblcompsetting', $data, "compsettingid= $id");
       if($query == 1)
       {
          return "true";
       }

    }
    // Function for updating ther emaining settings in table tblsetting
    public function updateSettings()
    {
        
     // print_r($this->session->all_userdata());
      
       $clientcompanyid  = $this->session->userdata['companyid'];
    
      
        $data['clientcompid'] = $clientcompanyid;
        
        if($this->input->post('email')!=""){
        $data['compsettingemail'] = $this->input->post('email');}
        
          if($this->input->post('address')!=""){
          $data['compsettingaddrs'] = $this->input->post('address');}
          
          
           if($this->input->post('contactnumber')!=""){
           $data['compsettingphone'] = $this->input->post('contactnumber');}
           
             if($this->input->post('website')!=""){
           
             $data['compsettingwebsite'] = $this->input->post('website');}
             
                if($this->input->post('header')!=""){
             
                $data['compsettinginvheader'] = $this->input->post('header'); } 
          
          $copyid  = $this->input->post('copyid');
          
        
          
            if ($_FILES['image']['name']) 
                            {
                                   $random = substr(number_format(time() * rand(), 0, '', ''), 0, 5);
                                           $file_name = $random . $_FILES['image']['name'];
                                           $file_size = $_FILES['image']['size'];
                                           $file_tmp = $_FILES['image']['tmp_name'];
                                           $file_type = $_FILES['image']['type'];
                                           $file_ext = strtolower(end(explode('.', $_FILES['image']['name'])));
                                           $expensions = array("doc", "docx", "pdf");
                                           if (in_array($file_ext, $expensions) === false) 
                                           {             //$errors[]="extension not allowed, please choose a JPEG or PNG file.";      
                                           }
                                           move_uploaded_file($file_tmp, "uploads/images/" . $file_name);
                                           $data['compsettinginvlogo'] = $file_name;
                            }
       
                            
         
          
       $query = $this->db->update('tblcompsetting', $data, "compsettingid= $copyid");
        if($this->db->affected_rows() > 0){
            
        }else{
                      $this->db->insert('tblcompsetting',$data);

        }
       
        
        
        
        
        
        
       if($query == 1)
       {
           return true;
       }
        
    }
  
     //Function for updating the emailsettings in table 'tblemailsett'
    public function updateEmailSettings()
    {
        $clientcompanyid  = $this->session->userdata['companyid'];
        $data['clientcompid'] = $clientcompanyid;
        
        $smtp =$this->input->post('smtp');
        $port = $this->input->post('port');
        $username =  $this->input->post('username');
        $pass = $this->input->post('pass');  
        $id =$this->input->post('hostid');
        $data['compsettinghost'] = $smtp;
        $data['compsettingport'] = $port;
        $data['compsettingusername'] = $username;
        $data['compsettingpass'] = $pass;
       
        $query = $this->db->update('tblcompsetting', $data, "compsettingid= $id");
        
        
        if($this->db->affected_rows() > 0){
            
        }else{
                      $this->db->insert('tblcompsetting',$data);

        }
        
        
        
       if($query == 1)
       {
           return true;
       }
        

    }
    // function for updating the  templates in tabele 'tbltemplate'.
    public function templateUpdate()
    {
        $id = $this->input->post('upId');
        
      
      $data['comptemplatename'] = $this->input->post('templatename');
      $data['comptemplatedescr'] = $this->input->post('discription');
     $clientcompanyid  = $this->session->userdata['companyid'];
        $data['clientcompid'] = $clientcompanyid;
      
     
      
      
     $query =  $this->db->update('tblcomptemplate', $data, array('comptemplateid' => $id));
    
        
        if($this->db->affected_rows() > 0){
            
        }else{
                    
           $data['comptemplatestatus'] = 1;
            
            $this->db->insert('tblcomptemplate',$data);

        }
     
     
     
     if($query == 1)
     {
         return "true";
     }
      
    }
    //Function for saving the car  details in table 'tblplan'
    public function savecarDetails($param1="",$param2="")
    {
       
        
        
       $data['cardetailbrand'] = $this->input->post('brand');
        $data['cardetailfueltype'] = $this->input->post('fueltype');
        $data['cardetailcarmodel'] = $this->input->post('model');
      $data['clientcompid'] = $this->session->userdata['companyid'];

//          $data['cardetailkmdriven'] = $this->input->post('drivenkm');
        $data['cardetaildesc'] = $this->input->post('enginedescription');
//        $data['cardetailprice'] = $this->input->post('rental_price'); 
        $data['cardetailduration'] = implode(",",$this->input->post('duration'));
          $data['cardetailincluyear'] = implode(",",$this->input->post('kmyear'));
          $data['cardetailextrakm'] = implode(",",$this->input->post('kmextra'));
          $data['cardetailmontlyfee'] = implode(",",$this->input->post('monthlyprice'));
          $data['cardetailadvance'] = implode(",",$this->input->post('advance'));
                //$data['cardetailadvance'] = $this->input->post('advance');
        //
        
        
           if ($_FILES['image']['name']) 
                            {
                                   $random = substr(number_format(time() * rand(), 0, '', ''), 0, 5);
                                           $file_name = $random . $_FILES['image']['name'];
                                           $file_size = $_FILES['image']['size'];
                                           $file_tmp = $_FILES['image']['tmp_name'];
                                           $file_type = $_FILES['image']['type'];
                                           $file_ext = strtolower(end(explode('.', $_FILES['image']['name'])));
                                           $expensions = array("doc", "docx", "pdf");
                                           if (in_array($file_ext, $expensions) === false) 
                                           {             //$errors[]="extension not allowed, please choose a JPEG or PNG file.";      
                                           }
                                           move_uploaded_file($file_tmp, "uploads/images/" . $file_name);
                                           $data['cardetailimg'] = $file_name;
                            }
                            
                          
                            
                            
       if($param1=="create"){
           
            if( $data['cardetailimg'] == "")  {              
            $data['cardetailimg'] = "car.jpg";    }           
           
           $this->db->insert('tblcardetails',$data);
           return "create";
       }
    
       
        if($param1=="edit")
        {
           $query=  $this->db->update('tblcardetails', $data, array('cardetailid' => $param2));
           return "edit";

        }
        
        
         if($param1 == "delete"){
            
           
                   $query=  $this->db->delete('tblcardetails', array('cardetailid' => $param2));
                    return "delete";

         }
        
        
        
        
        
      
    }

  
    public function saveUser($param="",$param1="")
    {
        $data1['clientusername'] = $this->input->post('username');
        $data1['clientusercontact'] = $this->input->post('contactnum');
        $data1['clientuseraddress'] = $this->input->post('address');
        $data1['clientjoiningdate'] = $this->input->post('joiningdate');
        $data1['clientuseremail'] = $this->input->post('useremail');
        $data1['clientuserrole'] = $this->input->post('role');
        
//        $data1['clientuserpass'] = $this->input->post('pass');
        
               $data1['clientuserpass'] =  password_hash($this->input->post('pass'), PASSWORD_DEFAULT);

        
        $data1['usertype'] = "clientuser";
            $data1['clientcompid'] = $this->session->userdata['companyid'];
                         
            $comp = $this->session->userdata['companyid'];
            
            
       if($param =="create")
       {
                 $data1['clientuserstatus'] = 1;

                
                 
           
           $query12 = $this->db->get_where('tblclientuser', array('clientuseremail' => $data1['clientuseremail'],'clientcompid'=>$comp));
        if($query12->num_rows() > 0)
        {
            return "exist";
        }
           
           
           
           
           if ($_FILES['image']['name']) 
                            {
                                   $random = substr(number_format(time() * rand(), 0, '', ''), 0, 5);
                                           $file_name = $random . $_FILES['image']['name'];
                                           $file_size = $_FILES['image']['size'];
                                           $file_tmp = $_FILES['image']['tmp_name'];
                                           $file_type = $_FILES['image']['type'];
                                           $file_ext = strtolower(end(explode('.', $_FILES['image']['name'])));
                                           $expensions = array("doc", "docx", "pdf");
                                           if (in_array($file_ext, $expensions) === false) 
                                           {             //$errors[]="extension not allowed, please choose a JPEG or PNG file.";      
                                           }
                                           move_uploaded_file($file_tmp, "uploads/images/" . $file_name);
                                           $data1['clientuserimg'] = $file_name;
                                           
                                            
                            }
                            
           if($data1['clientuserimg'] == ""){
              $data1['clientuserimg'] = "user.jpg";
          }
          
           $this->db->insert('tblclientuser',$data1);
                   //   echo "dksvjdsgdtfekjfdhvi";

           
           return "insert";
       }
        if($param =="edit")
       {
                  
            if ($_FILES['image']['name']) 
                            {
                                   $random = substr(number_format(time() * rand(), 0, '', ''), 0, 5);
                                           $file_name = $random . $_FILES['image']['name'];
                                           $file_size = $_FILES['image']['size'];
                                           $file_tmp = $_FILES['image']['tmp_name'];
                                           $file_type = $_FILES['image']['type'];
                                           $file_ext = strtolower(end(explode('.', $_FILES['image']['name'])));
                                           $expensions = array("doc", "docx", "pdf");
                                           if (in_array($file_ext, $expensions) === false) 
                                           {             //$errors[]="extension not allowed, please choose a JPEG or PNG file.";      
                                           }
                                           move_uploaded_file($file_tmp, "uploads/images/" . $file_name);
                                           $data1['clientuserimg'] = $file_name;
                                           
                                            
                            } 
                          
                           
                            
            
            $query=  $this->db->update('tblclientuser', $data1, array('clientuserid' => $param1));
                   
                   return "update";
       }
        if($param =="delete")
       {
                   $query=  $this->db->delete('tblclientuser', array('clientuserid' => $param1));
                   
                   return "delete";
       }
        
      
    }
     //function for saving the role in table ' tblclientrole'
     public function saveRole($param="",$param1="")
    {
      
         
         $data['clientrolename'] = $this->input->post('role');
         
         
         
         
         $data['clientcompid'] = $this->session->userdata['companyid'];
     
     
        
         if($param == "create")
         {
                  $data['clientrolestatus'] =1;
             
             $query12 = $this->db->get_where('tblclientrole', array('clientrolename' =>$data['clientrolename'],'clientcompid'=>$data['clientcompid']));
                            if($query12->num_rows() > 0)
                            {
                                return "exist";
                            }
                            $this->db->insert('tblclientrole',$data);
                           return "insert"; 
        }
         if($param == "edit"){
                 
     
            $query=  $this->db->update('tblclientrole', $data, array('clientroleid' => $param1));
            
           
           return "update"; 
        }
         if($param == "delete"){
                   $query=  $this->db->delete('tblclientrole', array('clientroleid' => $param1));
           return "delete"; 
        }

    }
     //function for saving the permission to a particular role in table 'tblsupadmpermission'
    public function savePermission($param="")
    {
        $data['clientroleid'] = $this->input->post('role');
        $permission = implode(",",$this->input->post('permission'));
        $data['clientrolepermision'] = $permission;
        $data['clientcompid'] = $this->session->userdata['companyid'];
        
        $query12 = $this->db->get_where('tblclientrolepermis', array('clientroleid' =>$data['clientroleid']));
        if($query12->num_rows() > 0)
        {
           $query=  $this->db->update('tblclientrolepermis', $data, array('clientroleid' => $data['clientroleid']));
           return "update";

        }
        else{
           $this->db->insert('tblclientrolepermis',$data);
           return "update";
        }
        
        
        
    }
      //function for getting permission by ajax in file permission view.php from table 'tblsupadmpermission'
    public function ajaxGetPermission()
    {
        $r =$_POST['roleid'];
        $query = $this->db->get_where('tblclientrolepermis',array('clientroleid'=>$r))->row_array();
        
  //      echo $this->db->last_query();
        $permisssion_data = $query['clientrolepermision'];
        $data = explode(",",$permisssion_data);
        echo json_encode($data);
    }
    
    //function for active the status of  role on or off
    public function changestatusRole($param="")
    {
        $query = $this->db->get_where('tblclientrole',array('clientroleid'=>$param))->row_array();
        $status = $query['clientrolestatus'];
       if($status == 1)
        {
            $data['clientrolestatus'] = 0;
            $query=  $this->db->update('tblclientrole', $data, array('clientroleid' =>$param));
            return "inactive" ;
        }
       if($status == 0)
        {
            $data['clientrolestatus'] = 1;
            $query=  $this->db->update('tblclientrole', $data, array('clientroleid' =>$param));
            return "active";
        }

    }
    
    
     //function for active the status of  role on or off
    public function changestatusUser($param="")
    {
      
         $query = $this->db->get_where('tblclientuser',array('clientuserid'=>$param))->row_array();
         
        
        $status = $query['clientuserstatus'];
        
       if($status == 1)
        {
            $data['clientuserstatus'] = 0;
            $query=  $this->db->update('tblclientuser', $data, array('clientuserid' =>$param));
            return "inactive" ;
        }
       if($status == 0)
        {
            $data['clientuserstatus'] = 1;
            $query=  $this->db->update('tblclientuser', $data, array('clientuserid' =>$param));
            return "active";
        }
        
        
    }
    
    
      //function for active the status of  plan on or off
    public function changestatusPlan($param="")
    {
       $query = $this->db->get_where('tblplan',array('planid'=>$param))->row_array();
       
      
        $status = $query['planstatus'];
       if($status == 1)
        {
            $data['planstatus'] = 0;
            $query=  $this->db->update('tblplan', $data, array('planid' =>$param));
            return "inactive" ;
        }
       if($status == 0)
        {
            $data['planstatus'] = 1;
            $query=  $this->db->update('tblplan', $data, array('planid' =>$param));
            return "active";
        }  
       

    }
    
      
    //function for saving the salea team
    public function saveTeam($param1="",$param="")
    {
       $data['saleteamleader'] = $_POST['leader'];
       $data['salesteamusersid'] = implode(",",$_POST['member']);
          $data['salesteamnotes']    =   $_POST['note'];
       $data['clientcompid'] = $this->session->userdata['companyid'];
       $data['saleteamname'] = $_POST['salesteam'];
       
       
         if($param1 == "create")
         {
                $this->db->insert('tblsaleteam',$data);
                           return "insert"; 
        }
        
        if($param1 == "edit")
        {
                       $query=  $this->db->update('tblsaleteam', $data, array('salesteamid' =>$param));
                     return "update";
        }
     
         if($param1 == "delete")
        {
                        $query=  $this->db->delete('tblsaleteam', array('salesteamid' => $param));
           return "delete"; 
        }
        
        
        
    }
   
     //function for saving the meeting data in table ' tblmeeting'
    
    public function createmeet($param1="",$param2="")
    {
     
      
        
        
        
        
        $data['meetingsubject'] = $this->input->post('subject');
        $data['meetingattendies'] = implode(",",$_POST['attendies']);
        $data['meetingstartat'] = $this->input->post('startat');
        $data['meetingendat'] = $this->input->post('endat');
        $data['meetinglocation'] = $this->input->post('location');
        $data['meetingdescription'] = $this->input->post('description');
        $data['clientcompid'] = $this->session->userdata['companyid'];
        
         if($param1 == "create")
         {
                $this->db->insert('tblmeeting',$data);
                return "insert"; 
        }
          if($param1 == "edit")
        {
           $query=  $this->db->update('tblmeeting', $data, array('meetingid' =>$param2));
            return "update";
        }
     
         if($param1 == "delete")
        {
            $query=  $this->db->delete('tblmeeting', array('meetingid' => $param2));
            return "delete"; 
        }
        



    }
    
     //function for saving the lead in table 'tblleads'
    public function saveLead($param1="",$param2="")
  {
      
        
        $data['leadbrand'] = $_POST['brand'];
        $data['leadkmrequested'] = $_POST['kmrequested'];
        $data['leadrentduration'] = $_POST['rentalDuration'];
        $data['leadmodel'] = $_POST['model'];
        $data['leadoppurtunity'] = $this->input->post('opportunity');
        $data['leadcustomeraddres'] = $this->input->post('address');
        $data['leadsalespersonid'] = implode(",",$this->input->post('saleperson'));
        $data['leadteamid'] = $this->input->post('teamid');
       $data['leadcustomerid'] = $this->input->post('customer');
       $data['leadcustomeremail'] = $this->input->post('emailaddress');
       $data['leadcustomerphone'] = $this->input->post('phone');
       $data['leadnotes'] = $this->input->post('notes');
       $data['clientcompid'] = $this->session->userdata('companyid');
       $data['leadsnextfollowdate'] = $this->input->post('followupdate');
       $data['leadtags']  = $this->input->post('leadtag');
  
     
//        if ($_FILES['document']['name']) 
//                            {
//                                   $random = substr(number_format(time() * rand(), 0, '', ''), 0, 5);
//                                           $file_name = $random . $_FILES['document']['name'];
//                                           $file_size = $_FILES['document']['size'];
//                                           $file_tmp = $_FILES['document']['tmp_name'];
//                                           $file_type = $_FILES['document']['type'];
//                                           $file_ext = strtolower(end(explode('.', $_FILES['document']['name'])));
//                                           $expensions = array("doc", "docx", "pdf");
//                                           if (in_array($file_ext, $expensions) === false) 
//                                           {             //$errors[]="extension not allowed, please choose a JPEG or PNG file.";      
//                                           }
//                                           move_uploaded_file($file_tmp, "uploads/document/" . $file_name);
//                                           $data['leaddocument'] = $file_name;
//                                           
//                                            
//                            } 
       foreach($_FILES['document']['name'] as $key=>$val){
               
                     
                 if($_FILES['document']['name'][$key]!=""){
                        $random = substr(number_format(time() * rand(), 0, '', ''), 0, 5);

                         $target_dir = "uploads/document/";
                         
                           $file_tmp = implode($_FILES['document']['name']);
                           $a = $a.",".$random.$_FILES['document']['name'][$key];
                          


                         $target_file = $target_dir.$random.$_FILES['document']['name'][$key];
                         if(move_uploaded_file($_FILES['document']['tmp_name'][$key],$target_file)){
                               $images_arr[] = $target_file;
                          }
                       
                 } 
        }
        
        
          $totaldoc = ltrim($a,",");
          
          
           $data['leaddocument'] = $totaldoc;
           
          
       
       
       date_default_timezone_set("Asia/Kolkata");
       $time =  Date('Y-m-d');
       $data['leadcreationdate'] = $time;
       
        if($param1 == "create"){
            
            $data['leadstatus'] = "pending";
         $this->db->insert('tblleads',$data);
          return "insert";
        }
        
        if($param1 == "edit")
        {
           $editdoc = implode(",",$_POST['editdocument']);
           if($totaldoc !=""){
           $data['leaddocument']  = $totaldoc.','.$editdoc;}
           else{
               $data['leaddocument'] = $editdoc;
           }
           
         
            
            $query=  $this->db->update('tblleads', $data, array('leadid' =>$param2));
            return "update";
        }
         if($param1 == "delete")
        {
            $query=  $this->db->delete('tblleads', array('leadid' => $param2));
            return "delete"; 
        }
       
       
    }
    
//     //function for saving the lead calls
//    public function saveleadCall($param1="",$param2="")
//    {
//      
//       $data['leadcalldate'] =  $this->input->post('leadcall');
//       $data['leadcalltime'] =  $this->input->post('summary');
//       $data['leadcallcontact'] =  $this->input->post('contact');
//       $data['clientcompid'] = $this->session->userdata['companyid'];
//        if($param1 == "create"){
//          
//          $this->db->insert('tblleadcall',$data);
//          return "insert";
//        }
//         if($param1 == "edit")
//        {
//           $query=  $this->db->update('tblleadcall', $data, array('leadcallid' =>$param2));
//           return "update";
//        }
//         if($param1 == "delete")
//        {
//            $query=  $this->db->delete('tblleadcall', array('leadcallid' => $param2));
//            return "delete"; 
//        }
//        
//    }
//    
    
    
    
    
    
     //public function  manage lead tags in table 'tblleadtag'
    
    public function updateLeads($param1="",$param2="")
    {
//        $tags = implode(",",$this->input->post('tag'));
//        $companyid = $this->session->userdata['companyid'];
//        $data['leadtags'] = $tags;
//        $data['clientcompid'] = $companyid;
//       
//        $leadquery=  $this->db->update('tblleadtag', $data, array('clientcompid' =>$companyid));
//         $effect = $this->db->affected_rows();
//        if($effect == 0){
//        $this->db->insert('tblleadtag',$data);
//         
//        }
//         
//       return "true";
     
        
        
         $companyid = $this->session->userdata['companyid'];
      
        $tagquery = $this->db->get_where('tblleadtag',array('clientcompid'=>$companyid));
        
       if($tagquery->num_rows() > 0){
            foreach($tagquery->result() as $key=>$value){
          
            $x  = $value->leadtags;
            $y = $value->leadtagid;
         $data  = $data.','.$x;
         
         $dataid1 = $dataid1.','.$y;
         
         
         }
       
         $f = trim($data,",");
         
         $g = trim($dataid1,",");
         
        $dataarr  = explode(",",$f);
        
        $dataid = explode(",",$g);
        
     
          foreach($dataarr as $t=>$s){
              
              $l= $l.','.$t;
          }
        $l1 = trim($l,",");
          
        $dataarr1 =  explode(",",$l1);    
      
        
           foreach($_POST['tag'] as $key=> $value){
               if (in_array($key, $dataarr1))
                {

                   $data12['leadtags'] = $_POST['tag'][$key];
                   $id =  $dataid[$key];
                   $query =  $this->db->update('tblleadtag', $data12, array('leadtagid' =>$id));
                 }
               else
               {
               
                    $data4['leadtags'] = $_POST['tag'][$key];
                    $data4['clientcompid'] = $companyid;
                    $this->db->insert("tblleadtag",$data4);

                }
                 
           }
           
         return "true";
       }
       else{
            
            $tags = explode(",",$_POST['tag']);
            
           foreach($_POST['tag'] as $k=>$v){
            $data12['leadtags'] = $v;
            $data12['clientcompid'] = $companyid;
            $this->db->insert('tblleadtag',$data12);
          
           }
           return "true";
       }
        
     
//        foreach($tagquery->result() as $key=>$value){
//          
//            $x  = $value->leadtags;
//            $y = $value->leadtagid;
//         $data  = $data.','.$x;
//         
//         $dataid1 = $dataid1.','.$y;
//         
//         
//         }
//       
//         $f = trim($data,",");
//         
//         $g = trim($dataid1,",");
//         
//        $dataarr  = explode(",",$f);
//        
//        $dataid = explode(",",$g);
//        
//     
//          foreach($dataarr as $t=>$s){
//              
//              $l= $l.','.$t;
//          }
//        $l1 = trim($l,",");
//          
//        $dataarr1 =  explode(",",$l1);    
//      
//        
//           foreach($_POST['tag'] as $key=> $value){
//               if (in_array($key, $dataarr1))
//                {
//
//                   $data12['leadtags'] = $_POST['tag'][$key];
//                   $id =  $dataid[$key];
//                   $query =  $this->db->update('tblleadtag', $data12, array('leadtagid' =>$id));
//                 }
//               else
//               {
//               
//                    $data4['leadtags'] = $_POST['tag'][$key];
//                    $data4['clientcompid'] = $companyid;
//                    $this->db->insert("tblleadtag",$data4);
//
//                }
//                 
//           }
//           
//         return "true";
           
        
        
        
        
        
        
        
    }
   
    
     
     //function for updating the opportunity stages in tablr o 'tblopporstages'
    
    public function opportunityStages()
    {
        
        
     
        
         $companyid = $this->session->userdata['companyid'];
      
        $stagequery = $this->db->get_where('tblopportunitystage',array('clientcompid'=>$companyid));
        
       if($stagequery->num_rows() > 0){
            foreach($stagequery->result() as $key=>$value){
          
            $x  = $value->opportunitystages;
            $y = $value->opportunitystageid;
         $data  = $data.','.$x;
         
         $dataid1 = $dataid1.','.$y;
         
         
         }
       
         $f = trim($data,",");
         
         $g = trim($dataid1,",");
         
        $dataarr  = explode(",",$f);
        
        $dataid = explode(",",$g);
        
     
          foreach($dataarr as $t=>$s){
              
              $l= $l.','.$t;
          }
        $l1 = trim($l,",");
          
        $dataarr1 =  explode(",",$l1);    
      
        
           foreach($_POST['stage'] as $key=> $value){
               if (in_array($key, $dataarr1))
                {

                   $data12['opportunitystages'] = $_POST['stage'][$key];
                   $id =  $dataid[$key];
                   $query =  $this->db->update('tblopportunitystage', $data12, array('opportunitystageid' =>$id));
                 }
               else
               {
               
                    $data4['opportunitystages'] = $_POST['stage'][$key];
                    $data4['clientcompid'] = $companyid;
                    $this->db->insert("tblopportunitystage",$data4);

                }
                 
           }
           
         return "true";
       }
       else{
            
        
         
           
            $tags = explode(",",$_POST['stage']);
            
           foreach($_POST['stage'] as $k=>$v){
            $data12['opportunitystages'] = $v;
            $data12['clientcompid'] = $companyid;
            $this->db->insert('tblopportunitystage',$data12);
          
           }
          
           return "true";
       }
        
        
        
    
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
//       $data['opportunitystages'] = implode(",",$_POST['stage']);
//       $companyid = $this->session->userdata['companyid'];
//        $data['clientcompid'] = $companyid;
//       
//        $leadquery=  $this->db->update('tblopportunitystage', $data, array('clientcompid' =>$companyid));
//         $effect = $this->db->affected_rows();
//        if($effect == 0){
//        $this->db->insert('tblopportunitystage',$data);
//         
//        }
//         
//       return "true";
    }
    
    //function for saving the opportunoty
    public function saveOpportunity($param1="",$param2="")
    {
        $data['opportunity']   = $this->input->post('opportunity');
        $data['opportunitysalesperson']   = implode(",",$this->input->post('saleperson'));
        $data['opportunitytag']   = $this->input->post('leadtag');
         $data['opportunitystage']   = $this->input->post('stage');
        $data['opportunityteam']   = $this->input->post('teamid');
        $data['opportunitycustomer']   = $this->input->post('customer');  
       $data['opportunitycustomeraddress']   = $this->input->post('address');
         $data['opportunityemail']   = $this->input->post('emailaddress');
         $data['opportunityphone']   = $this->input->post('phone');
         $data['opportunityprobability']   = $this->input->post('probability');
         $data['opportunitynextaction']   = $this->input->post('nextaction');
         $data['opportunitynextactiondate']   = $this->input->post('nextactiondate');
        $data['opportunitynotes']   = $this->input->post('notes');
        $data['clientcompid'] = $this->session->userdata['companyid'];
        
      
        
        
         if($param1 == "create")
         {
              $data['opportunitystatus'] = "pending";
             $this->db->insert('tblopportunity',$data);
                return "insert"; 
        }
          if($param1 == "edit")
        {
           
              
              $query=  $this->db->update('tblopportunity', $data, array('opportunityid' =>$param2));
              
            
            return "update";
        }
     
         if($param1 == "delete")
        {
            $query=  $this->db->delete('tblopportunity', array('opportunityid' => $param2));
            return "delete"; 
        }
        
        
        
        
        
        
        
        
        
    }
    
    
      public function deletetag()
    {
         $query=  $this->db->delete('tblleadtag', array('leadtagid' => $_POST['deleteid']));
         
         echo $this->db->last_query();

    }
      //function for saving the all months
     public function savecarMonth($param1="",$param2="")
     {
       
          $m = $_POST['month'];
         
         $data['carmonthname'] = $_POST['month'];
                $data['clientcompid'] = $this->session->userdata['companyid'];

        
          $companyid =  $this->session->userdata['companyid'];
        
             $tagquery = $this->db->get_where('tblcarmonth',array('clientcompid'=>$companyid,'carmonthname'=>$m));
             
            if($tagquery->num_rows() > 0){
               
                return "exist";
            }
        
        if($param1 =="create"){
        $this->db->insert('tblcarmonth',$data);
        return "insert";
        }
        
        if($param1 == "edit"){
            $query=  $this->db->update('tblcarmonth', $data, array('carmonthid' =>$param2));
             return "update"; 
        }
         if($param1 == "delete")
        {
            $query=  $this->db->delete('tblcarmonth', array('carmonthid' => $param2));
            return "delete"; 
        }
        
        
        
     }
     
     
     
       //function for saving the lead calls
    public function saveleadCall($param1="",$param2="")
    {
     
        $data['leadcalldate'] = $this->input->post('date');
                
        $data['	leadcalltime'] = $this->input->post('summary');

        $data['leadcallcontact'] = $this->input->post('contact');

        $data['leadcallresponsible'] = implode(",",$this->input->post('attendies'));
        $data['leadcalltype'] = $this->input->post('updateid');
        $data['clientcompid'] = $this->session->userdata['companyid'];
                
      $this->db->insert('tblleadcall',$data);
      
      ?>
        <table class="table table-striped table-hover" id="datatable1" >
                                 <thead>
                                    <tr>
                                        <th>S.No</th>
                                       <th> Date</th>
                                       <th>Summary</th>
                                       <th>Contact</th>
                                        <th>Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                     
                                      <?php 
                                       
                                    $comp = $this->session->userdata['companyid'];
                                    $this->db->where('clientcompid', $comp);
                                  // $this->db->where('leadcalltype', $id);

                                 
                                    $leadcall = $this->db->get('tblleadcall');
$i = 1;
                                foreach($leadcall->result() as $k=>$v)     {                                  ?>
                                    <tr class="gradeX  hide<?php echo $v->leadcallid;?>">
                                        <td><?php echo $i;?></td>
                                        <td><?php echo $v->leadcalldate;?></td>
                                         <td><?php echo $v->leadcalltime;?> </td>
                                        <td><?php echo $v->leadcallcontact;?> </td>

        <td><a onclick="editmeeting('<?php echo $v->leadcallid;?>');" ><i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i></a>
        <a  onclick="return confirmirmation('<?php echo $v->leadcallid;?>');" ><i class="fa fa-trash-o fa-2x" aria-hidden="true" style="color:#f31a04;"></i></a></td>

                                    </tr>
                                  
                                    
                                     
                                     
                                     
                                     
                                <?php $i++ ;} ?>
                                    
                                  
                              
                                 
                                 </tbody>
                              </table>
      
      
 <?php       
    }
     //function for deleting the lead calls 
    
    public function deleteleadcall()
    {
        $id = $_POST['data'];
        $query=  $this->db->delete('tblleadcall', array('leadcallid' => $id));

    }
     //function for edit lead call
    public function editCall()
    {
              $id = $_POST['editid'];
           
             $callquery = $this->db->get_where('tblleadcall',array('leadcallid'=>$id))->row_array();

           
           
           ?>
<link href="<?php echo base_url();?>multiple-select-master/multiple-select.css" rel="stylesheet"/>

           <button type="button" class="btn btn-info btn-lg abcdefg" data-toggle="modal" data-target="#myeditModal" style="display:none;">Open Modal</button>

  <!-- Modal -->
 <div id="myeditModal" style="margin-right:12px;" >
 <div class="modal-dialog modal-confirm">
  <div class="modal-content">
      
      
           <button type="button" class="close asdf123"   style="margin-right: 14px; margin-top: 17px;" data-dismiss="modal" aria-hidden="true">&times;</button>

   <div class="modal-header">
       
    <div class="icon-box">
     <!--<i class="material-icons">&#xE5CD;</i>-->
        <?php if($method == "edit") { ?>
                         <button type="button"  id="demo"   onclick= "test123('<?php echo $id;?>');" class="btn btn-primary"> Create Call</button>
                       
                       <?php } ?>

<!--        <div class="alert alert-block alert-success  modelpop" style="background-color:#3ec0e8 ;display:none;">
                                <a class="close"   style="margin-left: 576px;" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p> Deleted Successfully</p>
                        </div>	-->

    </div>    
   </div>
   
       <form id="edit_call" name="add_call" class="form-validation" accept-charset="utf-8" enctype="multipart/form-data" method="post">
             

 
<!--           <input type="hidden" name="call_type_id" value="1">
               	 <input type="hidden" name="call_type" value="leads">	                        	-->
               	 <div class="modal-body">
                    
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="field-1" class="control-label">Date</label>
                        <input type="texts"  class="form-control caldateedit" value="<?php echo $callquery['leadcalldate'];?>" id="datetimepicker1" name="date"  placeholder="" >
                        <span id="span12" style="color:red"></span>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="field-2" class="control-label">Call	Summary</label>
                        <input type="text" class="form-control" value="<?php echo $callquery['leadcalltime'];?>"       name="summary" id="call_summary12" placeholder="">
                       <span id="span22" style="color:red"></span>
                      </div>
                    </div>
                  </div>
                   <input type="hidden" name="editid" value="<?php echo $callquery['leadcallid'];?>" >
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="field-2" class="control-label">Contact</label>
                        <input type="text" onkeypress="return isNumber(event)"         value="<?php echo $callquery['leadcallcontact'];?>"   class="form-control" name="contact" id="contact12" placeholder="">
                       <span id="span32" style="color:red"></span>
                      </div>
                    </div>
                    
                      
                      
                      <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="field-2" class="control-label">Responsible</label>
                        <br>
                        <select multiple="multiple"   name="attendies[]" style="width:250px" id="ert5">

                                <?php 
                                       $comp = $this->session->userdata['companyid'];
                                     $this->db->where('clientuserstatus',1);
                                    $this->db->where('clientcompid', $comp);
                                 
                                    $member = $this->db->get('tblclientuser');

                                
                                
                             if($method == "create")
                             {
                                foreach($member->result() as $k1=>$v1){?>

        <option value="<?php echo $v1->clientuserid;?>"  ><?php echo $v1->clientusername;?></option>
      
   
                                <?php } 
        
        } ?>
        
        
        
        
        <?php    $mem = explode(",",$callquery['leadcallresponsible']) ;
             
    // print_r($member);
     
                            
                                foreach($member->result() as $k1=>$v1)
                                    
                                    {
                                    
                                           $m =  $v1->clientuserid;
                                    
                                         if (in_array($m, $mem))
                                                                { ?>
        
                   <option value="<?php echo $v1->clientuserid;?>" <?php echo "selected";?> ><?php echo $v1->clientusername;?></option>

                                                

     <?php           }
                                                              else
                                                                { ?>
                                                               
                                                                  
           <option value="<?php echo $v1->clientuserid;?>"  ><?php echo $v1->clientusername;?></option>

                                                         <?php       }

                                           
                                           
                                           
                                   

                                    
                                     
                                       
                                        
                                    }
                                    
                                    
                                    ?>

                    
                                 
        
      
        
        
        
        
        
        
        
        
                                                                       </select>   
                        
                        

                        
                        
                        
                        <span id="span3" style="color:red"></span>
                      </div>
                    </div>
                    
                     
                  </div>
                     
              
                     
                     
                     
                     
                     
                     
                </div>
                 
                  <div id="call_submitbutton" class="modal-footer text-center"><button type="submit" class="btn btn-primary btn-embossed bnt-square"  onclick="editformSubmit();">Update</button></div>
                 
                </form>
      
      
      
    
  </div>
 </div>
</div>
  
  
  
 
  
</div>
 <script src="<?php echo base_url();?>multiple-select-master/multiple-select.js"> </script>

           <script>
  $('#ert5').multipleSelect({
          //  isOpen: true,
                //filter: true
                // selectAll: false
        });
  </script>
  <?php  }
    
  
    //function for saving the edited form submit
    public function editleadSubmit()
    {
        $data['leadcalldate'] = $this->input->post('date');
                
        $data['	leadcalltime'] = $this->input->post('summary');

        $data['leadcallcontact'] = $this->input->post('contact');

        $data['leadcallresponsible'] = implode(",",$this->input->post('attendies'));
       
        $data['clientcompid'] = $this->session->userdata['companyid'];
                
        $id = $this->input->post('editid');
          
        $leadquery=  $this->db->update('tblleadcall', $data, array('leadcallid' =>$id));
        
    //    echo $this->db->last_query();
        
        
        ?>
        
        
            <table class="table table-striped table-hover" id="datatable1" >
                                 <thead>
                                    <tr>
                                        <th>S.No</th>
                                       <th> Date</th>
                                       <th>Summary</th>
                                       <th>Contact</th>
                                        <th>Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                     
                                      <?php 
                                       
                                    $comp = $this->session->userdata['companyid'];
                                    $this->db->where('clientcompid', $comp);
                                  // $this->db->where('leadcalltype', $id);

                                 
                                    $leadcall = $this->db->get('tblleadcall');
$i = 1;
                                foreach($leadcall->result() as $k=>$v)     {                                  ?>
                                    <tr class="gradeX  hide<?php echo $v->leadcallid;?>">
                                        <td><?php echo $i;?></td>
                                        <td><?php echo $v->leadcalldate;?></td>
                                         <td><?php echo $v->leadcalltime;?> </td>
                                        <td><?php echo $v->leadcallcontact;?> </td>

        <td><a onclick="editmeeting('<?php echo $v->leadcallid;?>');" ><i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i></a>
        <a  onclick="return confirmirmation('<?php echo $v->leadcallid;?>');" ><i class="fa fa-trash-o fa-2x" aria-hidden="true" style="color:#f31a04;"></i></a></td>

                                    </tr>
                                  
                                    
                                     
                                     
                                     
                                     
                                <?php $i++ ;} ?>
                                    
                                  
                              
                                 
                                 </tbody>
                              </table>
        
        
        
        
        
        
        
        
        
        

        
<?php    }
    
//function for deleting the stages from table  'tblopportunitystage;
    
    public function deletestage()
    {
         $id = $_POST['deleteid'];
         $query=  $this->db->delete('tblopportunitystage', array('opportunitystageid' => $id));

      
    }
 //function for saving the data in  table tbllogcall
   public function savelogCall($param1="",$param2="")
   {
       $data['logcalldate'] = $this->input->post('leadcall');
       $data['logcallcontact'] =  $this->input->post('contact');
        $data['logcallsummary'] =  $this->input->post('summary');
        $data['logcallresponsible'] =  implode(",",$this->input->post('member'));
        $data['clientcompid'] = $this->session->userdata['companyid'];
      
       
         if($param1 == "create"){
             $this->db->insert('tbllogcall',$data);
             return "insert";
         }
         
           if($param1 == "edit"){                    
        $query=     $this->db->update('tbllogcall', $data, "logcallid= $param2");
       return "update";
           } 
           if($param1 == "delete"){
            
           
                   $query=  $this->db->delete('tbllogcall', array('logcallid' => $param2));
                    return "delete";

         }
           
           
           
           
           
           
      }

        //function for saving the contracts in table tblcontract
   
   public function saveContract($param1="",$param2="")
   {
     
        $data['contractstartdate'] = $this->input->post('startdate');
        $data['contractenddate'] = $this->input->post('enddate');
        $data['contractdescription'] = $this->input->post('notes');
        $data['contractcontact'] = $this->input->post('contact');
        $data['contractresponsible'] = implode(",",$this->input->post('member'));
        $data['clientcompid'] = $this->session->userdata['companyid'];
         if ($_FILES['document']['name']) 
                            {
                                   $random = substr(number_format(time() * rand(), 0, '', ''), 0, 5);
                                           $file_name = $random . $_FILES['document']['name'];
                                           $file_size = $_FILES['document']['size'];
                                           $file_tmp = $_FILES['document']['tmp_name'];
                                           $file_type = $_FILES['document']['type'];
                                           $file_ext = strtolower(end(explode('.', $_FILES['document']['name'])));
                                           $expensions = array("doc", "docx", "pdf");
                                           if (in_array($file_ext, $expensions) === false) 
                                           {             //$errors[]="extension not allowed, please choose a JPEG or PNG file.";      
                                           }
                                           move_uploaded_file($file_tmp, "uploads/document/" . $file_name);
                                           $data['contractdocument'] = $file_name;
                                           
                                            
                            } 
                            
                           
        if($param1 == "create"){
          
            $data['contractstatus'] = 1;
             $this->db->insert('tblcontract',$data);
             return "insert";
        }
        
        if($param1 == "edit"){                    
        $query=     $this->db->update('tblcontract', $data, "contractid= $param2");
       return "update";
           } 
           
           
            if($param1 == "delete"){                    
        
                   $query=  $this->db->delete('tblcontract', array('contractid' => $param2));
                    return "delete";

           } 

   }
 
   //function for changing the status of contract in table tbl contract
   
   public function changestatuscontract($param="")
           
        { 
      
       
           $query = $this->db->get_where('tblcontract',array('contractid'=>$param))->row_array();
             $status = $query['contractstatus'];
             
           //  echo $this->db->last_query();
             if($status == 1)
                {
                    $data['contractstatus'] = 0;
                    $query=  $this->db->update('tblcontract', $data, array('contractid' =>$param));
                    return "inactive" ;
                }
            if($status == 0)
                {
                    $data['contractstatus'] = 1;
                    $query=  $this->db->update('tblcontract', $data, array('contractid' =>$param));
                    return "active";
                }
        
       }
       
       
  //function for search the contract between dates 

    public function searchContract()
    {
           $from = $this->input->post('startdate');
           $to  = $this->input->post('enddate');
           
         //  echo $from;
          // echo $to;
           
           
            $this->db->where('contractstartdate >=', $from);
            $this->db->where('contractenddate <=', $to);
          $query=  $this->db->get('tblcontract');
       //   echo $this->db->last_query();
          
          ?>
                                               
               <table class="table table-striped table-hover" id="datatable1">
                                 <thead>
                                    <tr>
                                        <th style ="margin-right:58px">S.No</th>
                                       <th>Start Date</th>
                                        <th>End Date</th>
                                        <th> Contact</th>
                                         <th>Responsible</th>
                                         <th>Document</th>
                                         <th>Status</th>
                                       <th>Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                     
                                     
                                     <?php
                                   //  $comp = $this->session->userdata('companyid');
                                     
                                   //  $query = $this->db->get('tblcontract',array('clientcompid'=>$comp));
                                     $i=1;
                                     foreach($query->result() as $k=>$vl)
                                     {?>
                                    <tr class="gradeX">
                                        <td style ="margin-right:58px;"><?php echo $i;?></td>
                                        <td><?php echo $vl->contractstartdate;?></td>
                                        <td><?php echo $vl->contractenddate;?>	</td>
                                        <td><?php echo $vl->contractcontact;?></td>
                                          
                                              
                                       <?php
                                       $k2="";
                                       
                                       $member = explode(",",$vl->contractresponsible);
                                       
                                       foreach($member as $t=>$r) {
                                   
                                       $query123 = $this->db->get_where('tblclientuser', array('clientuserid' => $r))->row_array();
                                          
                                       $name = $query123['clientusername'];
                                       
                                       
                                     $k2 = $k2.','."$name";         
                                       }
                                       ?>
                                        
                                       
                                         <td><?php echo $str = ltrim($k2, ',');?></td>
                                        
                                        
                                        
                                         <td>
                                           <?php $doc = $vl->contractdocument;
                                           
                                             if($doc !=""){?>
                                           <a href="<?php echo base_url();?>uploads/document/<?php echo $vl->contractdocument;?>" download><h6 style="color: blue; font-size: 15px;">Document </h6></a></td>
                                             <?php } else {?>
                                       
                                  <a  ><h6 style="color: red; font-size: 15px;">Not Uploaded </h6></a></td>

                                             <?php } ?>
                                        
                                        <?php $st = $vl->contractstatus ; ?>
                                        
                             <td><label class="switch"><input type="checkbox"<?php if($st == 1){echo checked;}?>>
                           <a  onclick="changeStatus('<?php echo $vl->contractid;?>');" <span class="slider round"></a></span></label></td>            
                           
                                        
                             <td ><a href="<?php echo base_url();?>client/Client/createContract/<?php echo $vl->contractid;?>" ><i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i></a>
                  
                                 
                           <a href="<?php echo base_url();?>client/Client/saveContract/delete/<?php echo $vl->contractid?>"   onclick="return confirm('Are you sure want to delete?');"><i class="fa fa-trash-o fa-2x" aria-hidden="true" style="color:#f31a04;"></i></a></td>

                                    </tr>
                                  
                                     <?php $i++; } 
                                     
                                     
                                     
                                     
                                     
                                     ?>
                                  
                              
                                 
                                 </tbody>
                              </table>
 
  <?php   }     
       
   //function for get events of Calendar in table  tblleads and tblmeeting
  
  
  public function geteventCalendar()
  {
      $comp = $this->session->userdata['companyid'];
      $pagedata1 = $this->db->get_where("tblmeeting",array("clientcompid"=>$comp));

//                    $SQL = "SELECT * FROM tblholidaycalender Group BY `tblholidaycalender`.`holiday_random_id`";
//                    $query = $this->db->query($SQL);
//                    $pagedata1 = $query->result_array();
                    $query = array();
                    $data = array();
                    foreach ($pagedata1->result() as $key => $value) {
                        
                        
                        
                       // $d = $value->meetingstartat;
                        $newDate = date("Y-m-d", strtotime($d));
                        
                          
                    $start = $value->meetingstartat;
                    $date = new DateTime($start);
                    $newDate = $date->format('Y-m-d');
                    
                     $end = $value->meetingendat;
                    $date = new DateTime($end);
                    $end = $date->format('Y-m-d');
                        
                        
                        
                        $meetingsubject = $value->meetingsubject;
                        
                        $data[$key]['title'] = "Meeting Subject". " ".$meetingsubject;
                        $data[$key]['start'] = $newDate."T16:00:00";
                        $data[$key]['end'] = $end."T17:00:00";
                      //  $data[$key]['backgroundColor'] ='Theme.colors.green';
                    }

                    $pagedata2 = $this->db->get_where("tblopportunity",array("clientcompid"=>$comp));
                    //print_r($pagedata2); die;
                    $data1 = array();
                    foreach ($pagedata2->result() as $key1 => $value1) {
                        
                        
                     $newDate = date("Y-m-d", strtotime($d));
                      $actiondate = $value1->opportunitynextactiondate;
                     $date = new DateTime($actiondate);
                     $action = $date->format('Y-m-d');
                        
                        $next = $value1->opportunitynextaction          ;
                        
                       $data1[$key1]['title'] = "Opportunity Action"." ".$next;
                        $data1[$key1]['start'] = $action."T17:00:00";
                       // $data1[$key]['backgroundColor'] ='Theme.colors.green';
                  
                        
                        
                        
                        
                        
                    }
//                    $query['d1'] = $data;
//                   $query['d2'] = $data1;
                    
                     $a = $data;
                     $b = $data1;
                    
                   $query['d1'] = array_merge($a,$b);

                    return json_encode($query);
//                    print_r(json_encode($query));
                }     

}