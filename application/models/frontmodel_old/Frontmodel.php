<?php
//<!--Company Name :- Lazlo Software Solution
//    Creation Date :-18/6/2018
//    Modal Name :- Frontmodel
//    Description :- This is the Frontmodel where database operation are performed 
//
//     
//    Database Name:- carrental
//    Table Used :- 'tblclientcompany' --!>

class Frontmodel extends CI_Model {
    function __construct() {
        // Call the Model constructor
  $this->load->library('session');
          $this->load->database();
 error_reporting(0);
      //  parent::__construct();

    }
    
   //Function for making seession of signup users 
    public function index()
    {
        $newdata = array(
                           'firstname'=>$this->input->post('firstname'),
                            'lastname'  => $this->input->post('lastname'),
                            'username'     =>$this->input->post('username'),
                            'email' =>$this->input->post('emailaddress'),
                                        'password' =>$this->input->post('password')

                 
                    );

                    $this->session->set_userdata($newdata);
                    
                    return "true";
                   
    }
    
  
      //Function signup for demo plan
    public function signupDemo()
    {
      
       
    $email = $this->session->userdata['email'];
      
     $demoid = $_POST['demoid'];
    
          $query = $this->db->get_where("tblregisclient",array('regisClientEmail'=>$email,'regisClientPlanid'=>$demoid));
         
          
          if($query->num_rows() > 0)
          {
              
              return "false";
          }
          else
          {
            $data1['regisclientFname'] =  $this->session->userdata['firstname'];
           $data1['regisclientLname'] =  $this->session->userdata['lastname'];
           $data1['regisclientUname'] =  $this->session->userdata['username'];
           $data1['regisclientpassword'] =  $this->session->userdata['password'];
           $data1['regisClientEmail'] = $this->session->userdata['email'];
           $data1['regisClientPlanid'] =  $demoid;
           $data1['regisClientDate'] = date('y-m-d');
           $query =  $this->db->insert("tblregisclient",$data1);
           return true;
           
          }

     
    }
    
}
  