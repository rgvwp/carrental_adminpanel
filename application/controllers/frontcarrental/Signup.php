<?php
/***
 * *Company Name :- Lazlo Software Solution
 **Creation Date :-20/6/2018
 ** Controller Name :- Front
 ** Description :- 1 This is the controller  which has functionality of signup and take plan and package.
 **                2 This is website frontend panel where client can sign up .and can tak eplan
 **  
 ** Database Name:- carrental
 ** Table Used :- '
 **/
class Signup extends CI_Controller {
    
    public function __construct() {

        parent::__construct();
       
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->helper('html');
                        $this->load->model('frontmodel/Frontmodel');

       $this->load->database();
      
         //error_reporting(0);
        
    }
 
    //Function where we get the value of signup login fields and make its session
    public function submitSignup()
    {
       $result =   $this->Frontmodel->index();
       
       
       if($result == "userexist")
        {
            
         $this->session->set_flashdata('permission_message', 'Username already exist.');

          redirect('Frontuser/signup');
        }
       
       
       
        if($result == "exist")
        {
            
         $this->session->set_flashdata('permission_message', 'You have already signed up.');

          redirect('Frontuser/signup');
        }
        
        
       
       
      
      if($result == "true")
      {
          redirect('Frontuser/plan');
      }
    }
    
    //function for adding plan id to session
    public function addplanid()
    {
       $session_data = $this->session->userdata();

      $session_data['planId'] = $_POST['planid'];

      $this->session->set_userdata($session_data);
      
      print_r($this->session->all_userdata());
        
        
    }
    
    //Function signup for demo plan
    public function signupDemo()
    {
       $result =   $this->Frontmodel->signupDemo();
    
        if($result == "true")
        {
            
            $this->session->set_flashdata('permission_message', 'Your Registration is Successfull');
            redirect('Frontuser/loginpage','refresh');
        }
        if($result == "false")
        {
          //  echo "asdf";
          //  die;
            $this->session->set_flashdata('flash_message', 'Your have already registered Demo pack');
            redirect('Frontuser/signup','refresh');
        }
     
    }
    
    
    
    //function for check the status of paypal cancel or success
    public function checkstatus($param="")
    {
       if($param == "success")
       {
           $data1['regisclientFname'] =  $this->session->userdata['firstname'];
           $data1['regisclientLname'] =  $this->session->userdata['lastname'];
           $data1['regisclientUname'] =  $this->session->userdata['username'];
           
           
            
           
//           $data1['regisclientpassword'] =  $this->session->userdata['password'];
           
           $data1['regisclientpassword'] =  password_hash($this->session->userdata['password'], PASSWORD_DEFAULT); 
           
           $data1['regisClientEmail'] = $this->session->userdata['email'];
           $data1['regisClientPlanid'] = $this->session->userdata['planId'];
           $data1['regisClientDate'] = date('y-m-d');
           
//           ****************************
             $data1['allclientplanId'] = $this->session->userdata['planId'];
           $data1['allplanregistDate'] =date('y-m-d');
           
           
           
//           ********************************
           
                 $pass =  $this->session->userdata['password'];
             $email123 =  $data1['regisClientEmail'];
             $planname = $this->db->get_where('tblplan', array('planid' =>  $data1['regisClientPlanid']))->row_array();
    
             $pname = $planname['planname'];
           
//           ******************************template****************
           
              $templquery = $this->db->get_where('tbltemplate',array('templatetype'=>"signup"))->row_array();
           
           $fullname =  $data1['regisclientFname'].$data1['regisclientLname'];
           
           $template= $templquery['templatedescription'];
      $variables = array(
            '{user}' => $fullname,
            '{email}'=>$email123,
            '{password}'=>$pass,
            '{plan}'=>$pname,
        );

          $txt1= strtr($template, $variables);  
             
             
             
             
             
             
             
           
           $html =  'Dear '. $data1['regisclientFname'] .'
                   
                    You registration is sucessfull:
                    Plan Name: '.$pname.'
                    Login Details:
                    Email: '. $email123.'
                    Password: '.$pass.'';
                       
           
           
        
            $to = $email123;
            $subject = "Registration";
            $txt = $txt1;
            $headers = "From: info@srngroups.com" . "\r\n" .
            "CC: lss.tm18@livesoftwaresolution.com";

            mail($to,$subject,$txt,$headers);
           
           
           
           
           
           
           
           $query =  $this->db->insert("tblregisclient",$data1);
           
           
            $this->session->set_flashdata('permission_message', 'Your Registration is Successfull');
            redirect('Frontuser/loginpage','refresh');
        }
        if($param == "fail")
        {
             $this->session->set_flashdata('flash_message', 'Your Registration is Unuccessfull');
            redirect('Frontuser/signup','refresh');
        }
    }
    
    //function for login of client to dashboard
    public function clientLogin()
    {
       $result =   $this->Frontmodel->checkLogin();
       
      
     if($result == "true")
       {
         
                      redirect('client/Client/dashboard','refresh');

       }
       if($result == "false")
       {
            $this->session->set_flashdata('flash_message', 'Your account is not registered please Signup');
            redirect('Frontuser/loginpage','refresh');
       }
    
       
    }
    
    //function for logout of clients to frontend
    public function clientLogout()
    {
      $this->session->set_flashdata('flash_message', 'Logout Successfully');
      redirect('Frontuser/loginpage','refresh');
 
       
    
    }
//    *************************
    
      //public function for update the status
    public function checkupdatestatus($param="")
    {
       
        
        date_default_timezone_set('Asia/Calcutta'); 
        $date =date("Y-m-d");
        
       $companyid = $this->session->userdata['companyid'];
       $updateplanid = $this->session->userdata['planId'];
       $value['regisClientPlanid'] = $updateplanid;
     $allplans = $this->db->get_where('tblregisclient', array('regisClientId' =>$companyid))->row_array();
        
        $alreadyplan = $allplans['allclientplanId'];
        $totalplan = $alreadyplan.','."$updateplanid" ;
        
        $alreadydate = $allplans['allplanregistDate'];
        $totalDate = $alreadydate.','."$date" ;
       
        $value['allplanregistDate'] = $totalDate ;
        $value['allclientplanId'] = $totalplan ;
      $query =  $this->db->update('tblregisclient', $value, array('regisClientId' => $companyid));
    
//       ************************mail of update plan********************************
//       
         $planname = $this->db->get_where('tblplan', array('planid' => $updateplanid ))->row_array();
         $pname = $planname['planname'];
           
           $fname = $this->session->userdata['username'];
           $useremail = $this->session->userdata['email'];
//           **********************************template*****************
             $templquery = $this->db->get_where('tbltemplate',array('templatetype'=>"subs"))->row_array();
           
          // $fullname =  $data1['regisclientFname'].$data1['regisclientLname'];
           
           $template= $templquery['templatedescription'];
      $variables = array(
            '{user}' => $fname,
            '{email}'=>$useremail,
            
            '{plan}'=>$pname,
        );

          $txt1= strtr($template, $variables);  
             
           
           
           
           
//           *************************************
           
           
           $html =  'Dear '. $fname .'
                   
                    You Plan Updation is sucessfull:
//                    Plan Name: '.$pname.'';
//                       
//           
//           
//        
            $to = $useremail;
            $subject = "Updation Of Plan";
            $txt = $txt1;
           $headers = "MIME-Version: 1.0" . "\r\n";
	$headers.= "Content-type:text/html;charset=UTF-8" . "\r\n";
        
        $headers .= "From: info@srngroups.com" . "\r\n" .
        "CC: lss.tm18@livesoftwaresolution.com";
            mail($to,$subject,$txt,$headers);
//           
       
       
        $this->session->set_flashdata('echo_message', 'Your account is Updated please Login');
            redirect('Frontuser/loginpage','refresh');
    }
    
    
    
//  *******************  
    
     //function for updating the demo pack if client not take it firsts
    
    public function updateDemo()
    {
        
        
        $query = $this->db->get_where("tblregisclient",array('regisClientEmail'=>$email))->row_array();
        $allplans = $query['allclientplanId'];
        
        $planarr = explode(",",$allplans);
         
        if (in_array($demoid, $planarr))
            {
                 $this->session->set_flashdata('echo_message', 'Your have already registered demo pack');
            redirect('Frontuser/loginpage','refresh');  
            }
        
        else{
      date_default_timezone_set('Asia/Calcutta'); 
        $date =date("Y-m-d");
        
       $companyid = $this->session->userdata['companyid'];
       $updateplanid = $this->session->userdata['planId'];
       $value['regisClientPlanid'] = $updateplanid;
       $allplans = $this->db->get_where('tblregisclient', array('regisClientId' =>$companyid))->row_array();
        
        $alreadyplan = $allplans['allclientplanId'];
        $totalplan = $alreadyplan.','."$updateplanid" ;
        
        $alreadydate = $allplans['allplanregistDate'];
        $totalDate = $alreadydate.','."$date" ;
       
        $value['allplanregistDate'] = $totalDate ;
        $value['allclientplanId'] = $totalplan ;
      $query =  $this->db->update('tblregisclient', $value, array('regisClientId' => $companyid));
    
//       ************************mail of update plan********************************
//       
         $planname = $this->db->get_where('tblplan', array('planid' => $updateplanid ))->row_array();
         $pname = $planname['planname'];
           
           $fname = $this->session->userdata['username'];
           $useremail = $this->session->userdata['email'];
           
           
//           ******************************Template********************
           
//            **********************************template*****************
             $templquery = $this->db->get_where('tbltemplate',array('templatetype'=>"subs"))->row_array();
           
          // $fullname =  $data1['regisclientFname'].$data1['regisclientLname'];
           
           $template= $templquery['templatedescription'];
      $variables = array(
            '{user}' => $fname,
            '{email}'=>$useremail,
            
            '{plan}'=>$pname,
        );

          $txt1= strtr($template, $variables);  
           
//           *********************************************************
           $html =  'Dear '. $fname .'
                   
                    You Plan Updation is sucessfull:
//                    Plan Name: '.$pname.'';
//                       
//           
//           
//        
            $to = $useremail;
            $subject = "Updation Of Plan";
            $txt = $txt1;
            $headers = "MIME-Version: 1.0" . "\r\n";
	$headers.= "Content-type:text/html;charset=UTF-8" . "\r\n";
        
        $headers .= "From: info@srngroups.com" . "\r\n" .
        "CC: lss.tm18@livesoftwaresolution.com";

            mail($to,$subject,$txt,$headers);
//           
       
       
        $this->session->set_flashdata('flash_message', 'Your account is Updated please Login');
            redirect('Frontuser/loginpage','refresh');  
            
            
        }
    }
    
    
    
    
    
    
    
}