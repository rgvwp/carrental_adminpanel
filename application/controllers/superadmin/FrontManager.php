<?php
//
//<!--Company Name :- Lazlo Software Solution
//    Creation Date :-6/14/2018
//    Controller Name :- frontManager
//    Description :- This is the frontManager controller   used for managing the frontend data of website from backend
//    Database Name:- carrental
//    Table Used :- '. -->
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class FrontManager extends CI_Controller {

    public function __construct() {
        
       
        parent::__construct();
        $this->load->library('session');
     //   $this->load->driver("session");
        $this->load->database();
        $this->load->model('superadmin/FrontModel');
          
 if ($this->session->userdata['id'] == "") {
            redirect('Home', 'refresh');
        }
error_reporting(0); 
ini_set('display_errors', 'Off');

error_reporting(E_ERROR | E_PARSE);
    }

    
    //Function for open the view superadmin dashboard
    public function viewContactus() 
    {
        $this->load->view('superadmin/header');
        $this->load->view('superadmin/front/contactUs',$value);
        $this->load->view('superadmin/footer');
       
    }
    //function for saving the contact us detail in table 'tblcmscontact'
    public function saveContact()
    {
        $result = $this->FrontModel->saveContact();
        $this->session->set_flashdata('permission_message', 'Contact Updated  Successfully');
        redirect('superadmin/frontManager/viewContactus/'.$parameter,'refresh');
    }
    //function for saving the contact us detail in table 'tblcmscontact'
    public function visionMission()
    {
        $this->load->view('superadmin/header');
        $this->load->view('superadmin/front/visionMission',$value);
        $this->load->view('superadmin/footer');
    }
    //function for updating the vision ,mission and valies
     public function saveVision()
    {
       
        $result = $this->FrontModel->saveVision();
         $this->session->set_flashdata('permission_message', ' Updated  Successfully');
        redirect('superadmin/frontManager/visionMission/'.$parameter,'refresh');
    }
    //function for open the view page of FAQ
    public function viewFaq($param="")
    {
        $value['id'] = $param;
        $this->load->view('superadmin/header');
        $this->load->view('superadmin/front/faqView',$value);
        $this->load->view('superadmin/footer');
    }
//    public function for  saving the faq in table tblfaq
    public function saveFaq($param="",$param1="")
    {
        $result = $this->FrontModel->saveFaq($param,$param1);
         if($result == "insert")
            {
                 $this->session->set_flashdata('permission_message', 'Data Saved  Successfully');
                 redirect('superadmin/frontManager/listFaq/'.$parameter,'refresh');
            }
       if($result == "edit")
            {
                 $this->session->set_flashdata('permission_message', 'Updated Successfully');
                 redirect('superadmin/frontManager/listFaq','refresh');
            }
             if($result == "delete")
            {
                 $this->session->set_flashdata('permission_message', 'Deleted Successfully');
                 redirect('superadmin/frontManager/listFaq','refresh');
            }
    }
    //public function for listing the faq
     public function listFaq($param="",$param1="")
    {
       $this->load->view('superadmin/header');
        $this->load->view('superadmin/front/viewFaq',$value);
        $this->load->view('superadmin/footer');
    }
    
    //function for changing the status Faq in table 'tblcmsFaq'
    public function changestatusFaq($param="")
    {
      
       $param = $_POST['statusid'];
        
      //  echo $param;
        
       $result = $this->FrontModel->changestatusFaq($param);
       echo $result;
        
//      if($result == "active")
//      {
//          
//                 $this->session->set_flashdata('permission_message', 'Status active  Successfully');
//                 redirect('superadmin/FrontManager/listFaq','refresh');
//      }
//      if($result == "inactive")
//      {
//         
//                 $this->session->set_flashdata('permission_message', 'Status Inactive Successfully');
//                 redirect('superadmin/FrontManager/listFaq','refresh'); 
//      }
   
    }
    
    
    //public function for opening the page of home of backend in table' ' tblcmshome ' ;
     public function homePage($param="",$param1="")
    {
         
         $value['id'] = $param;
        $this->load->view('superadmin/header');
        $this->load->view('superadmin/front/homepage',$value);
        $this->load->view('superadmin/footer');
    }
    
    //public function for saving the contact fron fonend site
    public function savefrontContact()
    {
         $result = $this->FrontModel->savefrontContact();
          if($result == "true")
            {
                 $this->session->set_flashdata('permission_message', 'Message sended  Successfully');
                 redirect('Frontuser/contact','refresh');
            }
            else
                {
                     $this->session->set_flashdata('flash_message', 'Error in sending message');
                     redirect('Frontuser/contact','refresh');
                }
       
    }
    //Function for view the contact messages from frontend of user from table 'tblfrontContact'
    public function contactMessages()
    {
        $this->load->view('superadmin/header');
        $this->load->view('superadmin/front/frontendContact',$value);
        $this->load->view('superadmin/footer');
    }
    //function for deleting the contact messages comes from frontend in the table 'tblfrontContact'
    public function deleteContact($param="")
    {
     
        
        $result = $this->FrontModel->deleteContact($param);
          if($result == "true")
            {
                 $this->session->set_flashdata('flash_message', 'Deleted  Successfully');
                 redirect('superadmin/FrontManager/contactMessages','refresh');
            } 
    }
    //function for saving the banner images in table 'tblcmshome;
    public function saveBanner($param1="",$param2="")
    {
      
        $result = $this->FrontModel->saveBanner($param1,$param2);
          if($result == "create")
            {
                 $this->session->set_flashdata('permission_message', 'Data saved  Successfully');
                redirect('superadmin/FrontManager/homePageview','refresh');
            } 
        if($result == "edit")
            {
                 $this->session->set_flashdata('permission_message', 'Data updated  Successfully');
                redirect('superadmin/FrontManager/homePageview','refresh');
            } 
             if($result == "delete")
            {
                 $this->session->set_flashdata('permission_message', 'Data deleted  Successfully');
                redirect('superadmin/FrontManager/homePageview','refresh');
            } 
        
    }
    //function for viewing the home page
    public function homePageview()
    {
         $this->load->view('superadmin/header');
        $this->load->view('superadmin/front/homepageview',$value);
        $this->load->view('superadmin/footer');
    }
    
    //function for changing the status of banner
    public function changestatusBanner($param="")
    {
      $param = $_POST['statusid'];
      $result = $this->FrontModel->changestatusBanner($param);
      echo $result;
 
    }
    
    //function for saving the  copyright and url in table 'tblmanageUrl;
    public function manageUrl()
    {
         $this->load->view('superadmin/header');
        $this->load->view('superadmin/front/copyrightUrl',$value);
        $this->load->view('superadmin/footer');  

    }
    //function for updating the copyright and url in table ' tblmanagerUrl'
    public function savefrontUrl()
    {
      
       $result = $this->FrontModel->savefrontUrl();
      if($result == "true")
      {
          
                 $this->session->set_flashdata('permission_message', 'Updated  Successfully');
                 redirect('superadmin/FrontManager/manageUrl','refresh');
      } 
    }
    //Function for oprning the view page of news and event form page
    public function newsEvent($param="")
    {
       $value['id'] = $param;
        $this->load->view('superadmin/header');
        $this->load->view('superadmin/front/newsEvent',$value);
        $this->load->view('superadmin/footer');  
    }
    
     //Function for oprning the view page of news and event form page
    public function viewnewsEvent()
    {
        $this->load->view('superadmin/header');
        $this->load->view('superadmin/front/viewnewsEvent',$value);
        $this->load->view('superadmin/footer');  
    }
    
    
     //Function for oprning the view page of news and event form page
    public function tabName()
    {
        $this->load->view('superadmin/header');
        $this->load->view('superadmin/front/tab',$value);
        $this->load->view('superadmin/footer');  
    }
    
    
    
    //function for saving the news and event
    public function saveNews($param1="",$param2="")
    {
              $result = $this->FrontModel->saveNews($param1,$param2);
               if($result == "create")
            {
                 $this->session->set_flashdata('permission_message', 'Data saved  Successfully');
                redirect('superadmin/FrontManager/viewnewsEvent','refresh');
            } 
        if($result == "edit")
            {
                 $this->session->set_flashdata('permission_message', 'Data updated  Successfully');
                redirect('superadmin/FrontManager/viewnewsEvent','refresh');
            } 
             if($result == "delete")
            {
                 $this->session->set_flashdata('permission_message', 'Data deleted  Successfully');
                redirect('superadmin/FrontManager/viewnewsEvent','refresh');
            } 
 
    }
    
    //function change status of changestatusNews in table ' tblnewsevent'
    public function  changestatusNews($param="")
    { 
        
           $param = $_POST['statusid'];
        
        $result = $this->FrontModel->changestatusNews($param);
       
       echo $result;
        
        
        
//       $result = $this->FrontModel->changestatusNews($param);
//      if($result == "active")
//      {
//          
//                 $this->session->set_flashdata('permission_message', 'Status active  Successfully');
//                 redirect('superadmin/FrontManager/viewnewsEvent','refresh');
//      }
//      if($result == "inactive")
//      {
//         
//                 $this->session->set_flashdata('permission_message', 'Status Inactive Successfully');
//                 redirect('superadmin/FrontManager/viewnewsEvent','refresh'); 
//      }  
    }
    
    //function for saving the tab in table tab
    public function updateTab()
    {
        $result = $this->FrontModel->updateTab();
      if($result == "update")
      {
          
                 $this->session->set_flashdata('permission_message', 'Updated  Successfully');
                 redirect('superadmin/FrontManager/tabName','refresh');
      }

    }
    
      //function for view the form corefetaures
    public function corefeatures($param="")
    {
        $value['id'] = $param;
        $this->load->view('superadmin/header');
        $this->load->view('superadmin/front/corefeatures',$value);
        $this->load->view('superadmin/footer'); 
    }
    
    //function for view the form corefetaures
    public function corefeaturesView()
    {
      
        $this->load->view('superadmin/header');
        $this->load->view('superadmin/front/corefeaturesview',$value);
        $this->load->view('superadmin/footer'); 
    }
    
    //function for saving the corefeayures
    public function savecoreFeatures($param1="",$param2="")
    {
                      $result = $this->FrontModel->savecoreFeatures($param1,$param2);
            if($result == "create")
                {
                     $this->session->set_flashdata('permission_message', 'Data saved  Successfully');
                    redirect('superadmin/FrontManager/corefeaturesView','refresh');
                } 
        if($result == "edit")
                {
                     $this->session->set_flashdata('permission_message', 'Data updated  Successfully');
                    redirect('superadmin/FrontManager/corefeaturesView','refresh');
                } 
             if($result == "delete")
                {
                     $this->session->set_flashdata('permission_message', 'Data deleted  Successfully');
                    redirect('superadmin/FrontManager/corefeaturesView','refresh');
                } 

    }
     //function change status of changestatusNews in table ' tblnewsevent'
    public function  changestatusCore($param="")
    {
      $param  = $_POST['statusid'];
        
       $result = $this->FrontModel->changestatusCore($param);
      echo $result;
    }
    
    //function for saving th enquiry from frontend in table "tblenquiry"
    public function saveEnquiry()
    {
          
        
        $result = $this->FrontModel->saveEnquiry();
         if($result == "true")
      {
          
                 $this->session->set_flashdata('permission_message', 'Enquiry Submitted Successfully');
                 redirect('Frontuser/enquiry','refresh');
      }

    }
    //function for viewing the details of saved enquiry to admin
    public function enquiryMessages()
    {
       $this->load->view('superadmin/header');
        $this->load->view('superadmin/front/enquiry',$value);
        $this->load->view('superadmin/footer');  
    }
    //function for deleting the enquiries in tanble tblenquiry
      public function enquiryDelete($param="")
    {
               $query =  $this->db->delete('tblenquiry', array('enquiryid' => $param));    
               
                 $this->session->set_flashdata('permission_message', 'Enquiry deleted Successfully');
                 redirect('superadmin/FrontManager/enquiryMessages','refresh');

    }
    //function for opening the form of aboutus in file aboutus.php
    public function aboutUs($param="")
    {
       $value['id'] = $param;
       
        $this->load->view('superadmin/header');
        $this->load->view('superadmin/front/aboutus',$value);
        $this->load->view('superadmin/footer');    
        
    }
    
    //function for view the page of view about us from table 'tblaboutus'
    public function viewaboutUs()
    {
         $this->load->view('superadmin/header');
        $this->load->view('superadmin/front/viewaboutus',$value);
        $this->load->view('superadmin/footer'); 
    }
    
    
    //public function save about US in table
    public function saveaboutUs($param1="",$param2="")
    {
      $result = $this->FrontModel->saveaboutUs($param1,$param2);
 if($result == "create")
                {
                     $this->session->set_flashdata('permission_message', 'Data saved  Successfully');
                    redirect('superadmin/FrontManager/viewaboutUs','refresh');
                } 
        if($result == "edit")
                {
                     $this->session->set_flashdata('permission_message', 'Data updated  Successfully');
                    redirect('superadmin/FrontManager/aboutUs','refresh');
                } 
                 if($result == "delete")
                {
                     $this->session->set_flashdata('permission_message', 'Deleted  Successfully');
                    redirect('superadmin/FrontManager/viewaboutUs','refresh');
                } 
    }
    //function for changing  the status of about us page
    public function changestatusabout($param="")
    {
            $param = $_POST['statusid'];
       // echo $param;
        $result = $this->FrontModel->changestatusabout($param);
       echo $result;
//      if($result == "active")
//      {
//          
//                 $this->session->set_flashdata('permission_message', 'Status active  Successfully');
//                 redirect('superadmin/FrontManager/viewaboutUs','refresh');
//      }
//      if($result == "inactive")
//      {
//         
//                 $this->session->set_flashdata('permission_message', 'Status Inactive Successfully');
//                 redirect('superadmin/FrontManager/viewaboutUs','refresh'); 
//      }   
    }
    
    //function for opening the forn of highly recommended 
    public function recommended($param="")
    {
       $value['id'] = $param;
        
        $this->load->view('superadmin/header');
        $this->load->view('superadmin/front/recommended',$value);
        $this->load->view('superadmin/footer');    
    }
    //function for saving the recommended in table 'tblrecommended;
    public function saveRecommended($param1="",$param2="")
    {
         $result = $this->FrontModel->saveRecommended($param1,$param2);
 if($result == "create")
                {
                     $this->session->set_flashdata('permission_message', 'Data saved  Successfully');
                    redirect('superadmin/FrontManager/viewRecommended','refresh');
                } 
        if($result == "edit")
                {
                     $this->session->set_flashdata('permission_message', 'Data updated  Successfully');
                    redirect('superadmin/FrontManager/viewRecommended','refresh');
                } 
                 if($result == "delete")
                {
                     $this->session->set_flashdata('permission_message', 'Deleted  Successfully');
                    redirect('superadmin/FrontManager/viewRecommended','refresh');
                } 
    }
    
    //function for saving the  recommende 
    public function viewRecommended()
    {
         $this->load->view('superadmin/header');
        $this->load->view('superadmin/front/recommendView',$value);
        $this->load->view('superadmin/footer');   
    }

     //function for changing  the status of about us page
    public function changestatusRecommend($param="")
    {
           $param = $_POST['statusid'];
        
        $result = $this->FrontModel->changestatusRecommend($param);
        
        echo $result;
        
        
//        $result = $this->FrontModel->changestatusRecommend($param);
//      if($result == "active")
//      {
//          
//                 $this->session->set_flashdata('permission_message', 'Status active  Successfully');
//                 redirect('superadmin/FrontManager/viewRecommended','refresh');
//      }
//      if($result == "inactive")
//      {
//         
//                 $this->session->set_flashdata('permission_message', 'Status Inactive Successfully');
//                 redirect('superadmin/FrontManager/viewRecommended','refresh'); 
//      }   
    }
    
    //function for saving the newspaper  in table tblnewspaper'
    public function saveNewspaper()
    {
        $result = $this->FrontModel->saveNewspaper($param);
        echo "create";
//        if($result == "create")
//        {
//            
//                 $this->session->set_flashdata('permission_message', 'Enquiry Submitted Successfully');
//                 redirect('Frontuser/enquiry','refresh');
//        }

    }
    

     //function for changing  the status of newsletter
    public function changestatusletter($param="")
    {
    
          
       $param = $_POST['statusid'];
               // echo $param;
        
       $result = $this->FrontModel->changestatusletter($param);
        echo $result;
       // $result = $this->FrontModel->changestatusletter($param);
//      if($result == "active")
//      {
//          
//                 $this->session->set_flashdata('permission_message', 'Status active  Successfully');
//                 redirect('superadmin/FrontManager/allNewsletter','refresh');
//      }
//      if($result == "inactive")
//      {
//         
//                 $this->session->set_flashdata('permission_message', 'Status Inactive Successfully');
//                 redirect('superadmin/FrontManager/allNewsletter','refresh'); 
//      }   
    }
    
    
    
    
    
    
    //function for viewing the all subscribed newspaper
    public function allNewsletter()
    {
         $this->load->view('superadmin/header');
        $this->load->view('superadmin/front/newsletter',$value);
        $this->load->view('superadmin/footer');   
    }
    
    
    
    
     //function for  deleting the news letter in table 'tblnewsletter
    public function newspaperdelete($param="")
    {
        $query =  $this->db->delete('tblnewsletter', array('newsletterid' => $param));    
        
                 $this->session->set_flashdata('permission_message', 'Subscriber deleted Successfully');
                 redirect('superadmin/FrontManager/allNewsletter','refresh'); 
    }
    
    //function for sending mails to all subscriber
    public function sendNewsletter()
    {
        
        $mail = explode(",",$_POST['totalMail']);
       
        foreach($mail as $k=>$v){
            
            
          $to =  $v;
        $subject = "Newsletter Message";
        $txt = "Newsletter" ;
        $headers = "From: info@srngroups.com" . "\r\n" .
        "CC: lss.tm10@livesoftwaresolution.com";

        mail($to,$subject,$txt,$headers);
        }
        
          $this->session->set_flashdata('permission_message', 'Email Sended Successfully');
                 redirect('superadmin/FrontManager/allNewsletter','refresh'); 
        
        
        
    }
    
    //function for opening the forn of latest project
    public function latestProject($param="")
    {
       $value['id']  = $param;
        $this->load->view('superadmin/header');
        $this->load->view('superadmin/front/latestproject',$value);
        $this->load->view('superadmin/footer');    
    }
    
    public function viewProjects()
    {
        $this->load->view('superadmin/header');
        $this->load->view('superadmin/front/viewlatestproject',$value);
        $this->load->view('superadmin/footer');     
    }
       //function for saving the latest projects
    public function saveProject($param1="",$param2="")
    {
        $result = $this->FrontModel->saveProject($param1,$param2);
              if($result == "create")
                    {
                         $this->session->set_flashdata('permission_message', 'Data saved  Successfully');
                        redirect('superadmin/FrontManager/viewProjects','refresh');
                    } 
                if($result == "edit")
                    {
                         $this->session->set_flashdata('permission_message', 'Data updated  Successfully');
                        redirect('superadmin/FrontManager/viewProjects','refresh');
                    } 
                 if($result == "delete")
                    {
                         $this->session->set_flashdata('permission_message', 'Deleted  Successfully');
                        redirect('superadmin/FrontManager/viewProjects','refresh');
                    } 
    }
    
      //function for changing  the status of changestatusProject
    public function changestatusProject($param="")
    {
    
          $param = $_POST['statusid'];
        
        $result = $this->FrontModel->changestatusProject($param);
        
        echo $result;
//        $result = $this->FrontModel->changestatusProject($param);
//      if($result == "active")
//      {
//          
//                 $this->session->set_flashdata('permission_message', 'Status active  Successfully');
//                 redirect('superadmin/FrontManager/viewProjects','refresh');
//      }
//      if($result == "inactive")
//      {
//         
//                 $this->session->set_flashdata('permission_message', 'Status Inactive Successfully');
//                 redirect('superadmin/FrontManager/viewProjects','refresh'); 
////      }   
    }
    
    //function for opening the form of client port folio page
    public function clientPortFolio($param="")
    {
         $value['id']  = $param;
        $this->load->view('superadmin/header');
        $this->load->view('superadmin/front/clientFolio',$value);
        $this->load->view('superadmin/footer');      
    }
    
   public function viewclientPortFolio()
   {
        $value['id']  = $param;
        $this->load->view('superadmin/header');
        $this->load->view('superadmin/front/viewClientFolio',$value);
        $this->load->view('superadmin/footer');  
   }
   //function for saving the client Folio page
    public function saveclientFolio($param1="",$param2="")
    {
        $result = $this->FrontModel->saveclientFolio($param1,$param2);
              if($result == "create")
                    {
                         $this->session->set_flashdata('permission_message', 'Data saved  Successfully');
                        redirect('superadmin/FrontManager/viewclientPortFolio','refresh');
                    } 
                if($result == "edit")
                    {
                         $this->session->set_flashdata('permission_message', 'Data updated  Successfully');
                        redirect('superadmin/FrontManager/viewclientPortFolio','refresh');
                    } 
                 if($result == "delete")
                    {
                         $this->session->set_flashdata('permission_message', 'Deleted  Successfully');
                        redirect('superadmin/FrontManager/viewclientPortFolio','refresh');
                    } 
    }
    
     //function for changing  the status of changestatusProject
    public function changestatusFolio($param="")
    {
    
       
        $param = $_POST['statusid'];
        $result = $this->FrontModel->changestatusFolio($param);
        echo  $result;
    } 
    //function for open the form of help and suppoert for manage the backnd
    public function helpsuppoort($param="")
    {
        $value['id']  = $param;
        
     
        $this->load->view('superadmin/header');
        $this->load->view('superadmin/front/helpsupport',$value);
        $this->load->view('superadmin/footer');   
    }
    
    //function for viewing the details of help and support
    public function helpsuppoortview($param="")
    {
      
        
        $value['id']  = $param;
        $this->load->view('superadmin/header');
        $this->load->view('superadmin/front/helpsupportview',$value);
        $this->load->view('superadmin/footer');   
    }
    
    
    
    
    //function for saving the savehelpSupport in table ' tblhelpSupport'
    public function savehelpSupport($param1="",$param2="")
    {
         $result = $this->FrontModel->savehelpSupport($param1,$param2);
              if($result == "create")
                    {
                         $this->session->set_flashdata('permission_message', 'Data saved  Successfully');
                        redirect('superadmin/FrontManager/helpsuppoortview','refresh');
                    } 
                if($result == "edit")
                    {
                         $this->session->set_flashdata('permission_message', 'Data updated  Successfully');
                        redirect('superadmin/FrontManager/helpsuppoortview','refresh');
                    } 
                 if($result == "delete")
                    {
                         $this->session->set_flashdata('permission_message', 'Deleted  Successfully');
                        redirect('superadmin/FrontManager/helpsuppoortview','refresh');
                    } 
    }
    
      //function for changing  the status of changestatusProject
    public function changestatussupport($param="")
    {
    
        
    
        $param = $_POST['statusid'];
        $result = $this->FrontModel->changestatussupport($param);
        
        echo $result;
    }
    
    
     //function for opening  the form of upfate faq image
    
      //function for changing  the status of changestatusProject
    public function faqImage($param="")
    {
     
        $value['id']  = $param;
        $this->load->view('superadmin/header');
        $this->load->view('superadmin/front/faqimage',$value);
        $this->load->view('superadmin/footer');   
       
     
    }
    
        public function updatefaqImage($param="")
    {
    
                                   
       $result = $this->FrontModel->updatefaqImage($param);
       if($result == "true"){
           
            $this->session->set_flashdata('permission_message', 'Updated Successfully');
                        redirect('superadmin/FrontManager/faqImage','refresh');
       }
    
    
    
    }
    
    
    
    
    
    
    
   }

