<?php
//
//<!--Company Name :- Lazlo Software Solution
//    Creation Date :-6/6/2018
//    Controller Name :- SuperAdmin
//    Description :- This is the super admin controller  where superadmin update his profile 
//managing the settings ,rental company.,manages the plan and packages
//    Database Name:- carrental
//    Table Used :- 'tblsupadmprofile','tblsitesetting','tblplan','tblclientcompany'. -->
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class User extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->driver("session");
        $this->load->database();
        $this->load->model('superadmin/Usermodel');
        if ($this->session->userdata['id'] == "") {
            redirect('Home', 'refresh');
        }
       
    }

    
    //Function for open the view superadmin dashboard
    public function index() 
    {
      
        $this->dashboard();
    }
    
    //Function for opening the view page of dashboard
    public function dashboard()
    {
        $this->load->view('superadmin/header');
        $this->load->view('superadmin/dashboard');
        $this->load->view('superadmin/footer');
   }
   
   //function for opening the view page of the profile of superadmin 
    public function profileView($param="")
    {
    
       
        
        $value['data'] = $param;
        $this->load->view('superadmin/header');
        $this->load->view('superadmin/userprofile',$value);
        $this->load->view('superadmin/footer');
    }
    
    //Function for opening the view page of registered client
    public function registeredClient()
    {
        $this->load->view('superadmin/header');
        $this->load->view('superadmin/registeredClient');
        $this->load->view('superadmin/footer');
    }
    
    
    
    
    
    
    //Function for updation the profile of superadmin  in the table ' tblsupadmprofile'
    
    public function profileUpdate()
    {
       $parameter = $this->input->post('parameter');
       $result = $this->Usermodel->profileUpdate();
        $this->session->set_flashdata('permission_message', 'Profile Updated  Successfully');
        redirect('superadmin/User/profileView/'.$parameter,'refresh');
    }
    //Function for check the old password of super admin during updating the change password
    public function checkPassword()
    {
        $parameter = $this->input->post('parameter');
        $result = $this->Usermodel->checkPassword();
       if($result == "match")
            {
                 $this->session->set_flashdata('permission_message', 'Password Updated  Successfully');
                 redirect('superadmin/User/profileView/'.$parameter,'refresh');
            }
        if($result == "notmatch")
            {
                 $this->session->set_flashdata('flash_message', 'Old  Password is  Wrong');
                 redirect('superadmin/User/profileView/'.$parameter,'refresh');
            }
     
    }
  
    

   }

