<?php
//
//<!--Company Name :- Lazlo Software Solution
//    Creation Date :-6/28/2018
//    Controller Name :- Clientuser
//    Description :- This is the clientuser controller  where clientuser update his profile 
//   Used Model :-ClientuserModel'

//    Database Name:- carrental
//    Table Used :- 'tblclientuser'. -->
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Clientuser extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->driver("session");
        $this->load->database();
        $this->load->model('client/ClientuserModel');
        if ($this->session->userdata['companyid'] == "") {
            redirect('Home', 'refresh');
        }
       
    }

    
    //Function for open the view superadmin dashboard
    public function index() 
    {
      
        $this->dashboard();
    }
    
    //Function for opening the view page of dashboard
    public function dashboard()
    {
        $this->load->view('superadmin/header');
        $this->load->view('superadmin/dashboard');
        $this->load->view('superadmin/footer');
   }
   
   //function for opening the view page of the profile of superadmin 
    public function profileView($param="")
    {
    
        $value['data'] = $param;
        $this->load->view('client/header');
        $this->load->view('client/userprofile',$value);
        $this->load->view('client/footer');
    }
    
    //Function for opening the view page of registered client
    public function registeredClient()
    {
        $this->load->view('superadmin/header');
        $this->load->view('superadmin/registeredClient');
        $this->load->view('superadmin/footer');
    }
    
    
    //Function for updation the profile of superadmin  in the table ' tblclientuser'
    
    public function profileUpdate()
    {
       
        $parameter = $this->input->post('parameter');
        $result = $this->ClientuserModel->profileUpdate();
        $this->session->set_flashdata('permission_message', 'Profile Updated  Successfully');
        redirect('client/Clientuser/profileView/'.$parameter,'refresh');
    }
    //Function for check the old password of super admin during updating the change password in table tblclientuser
    public function checkPassword()
    {
        $parameter = $this->input->post('parameter');
        $result = $this->ClientuserModel->checkPassword();
       if($result == "match")
            {
                 $this->session->set_flashdata('permission_message', 'Password Updated  Successfully');
                 redirect('client/Clientuser/profileView/'.$parameter,'refresh');
            }
        if($result == "notmatch")
            {
                 $this->session->set_flashdata('flash_message', 'Old  Password is  Wrong');
                 redirect('client/Clientuser/profileView/'.$parameter,'refresh');
            }
     
    }
  
    

   }

