<?php
//
//<!--Company Name :- Lazlo Software Solution
//    Creation Date :-6/6/2018
//    Controller Name :- SuperAdmin
//    Description :- This is the super admin controller  where superadmin update his profile 
//managing the settings ,rental company.,manages the plan and packages
//    Database Name:- carrental
//    Table Used :- 'tblsupadmprofile','tblsitesetting','tblplan','tblclientcompany'. -->
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Client  extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->driver("session");
        $this->load->database();
        $this->load->model('client/Client_model');
        if ($this->session->userdata['userid'] == "") {
            redirect('Frontuser/loginpage', 'refresh');
        }
       
    }

    
    //Function for open the view superadmin dashboard
    public function index() 
    {
       $this->dashboard();
    }
    
    //Function for opening the view page of dashboard
    public function dashboard()
    {
      
        
        
        $this->load->view('client/header');
        $this->load->view('client/dashboard');
        $this->load->view('client/footer');
   }
   
   //function for opening the view page of the profile of superadmin 
    public function profileView($param="")
    {
    
        $value['data'] = $param;
        $this->load->view('client/header');
        $this->load->view('client/clientprofile',$value);
        $this->load->view('client/footer');
    }
    //Function for updation the profile of superadmin  in the table ' tblsupadmprofile'
    
    public function profileUpdate()
    {
      
       $parameter = $this->input->post('parameter');
       $result = $this->Client_model->profileUpdate();
       
       
        $this->session->set_flashdata('permission_message', 'Profile Updated  Successfully');
        redirect('client/Client/profileView/'.$parameter,'refresh');
    }
    //Function for check the old password of super admin during updating the change password
    public function checkPassword()
    {
       
        $parameter = $this->input->post('parameter');
        $result = $this->Client_model->checkPassword();
       if($result == "match")
            {
                 $this->session->set_flashdata('permission_message', 'Password Updated  Successfully');
                 redirect('client/Client/profileView/'.$parameter,'refresh');
            }
        if($result == "notmatch")
            {
                 $this->session->set_flashdata('flash_message', 'Old  Password is  Wrong');
                 redirect('client/Client/profileView/'.$parameter,'refresh');
            }
     
    }
    //Function for open the view page of form setting where admin can perform settings  
    public function formsettings($parameter="")
    {
      
        
        $value['data'] = $parameter;
        $this->load->view('client/header');
        $this->load->view('client/formsetting',$value);
        $this->load->view('client/footer');
    }
    //Function for saving the site setting table tblsitesetting
    public function updateSitesSettings()
    {
        $parameter = $this->input->post('parameter');
        $result = $this->Client_model->updateSitesSettings(); 
         if($result == "true")
            {
                 $this->session->set_flashdata('permission_message', ' Updated  Successfully');
                 redirect('client/Client/formsettings/'.$parameter,'refresh');
            }
        else
            {
                 $this->session->set_flashdata('flash_message', 'Error');
                 redirect('client/Client/formsettings','refresh');
            }
        
    }
    // Function for updating th eremaining settings
    public function updateSettings()
    {
          
        $parameter = $this->input->post('parameter');
      
       
       $result = $this->Client_model->updateSettings(); 
          if($result == "true")
            {
                 $this->session->set_flashdata('permission_message', ' Updated  Successfully');
                 redirect('client/Client/formsettings/'.$parameter,'refresh');
            }
        else
            {
                 $this->session->set_flashdata('flash_message', 'Error');
                 redirect('client/Client/formsettings','refresh');
            }
    }
    //Function for updating the emailsettings in table 'tblemailsett
    public function updateEmailSettings()
    {
                $parameter = $this->input->post('parameter');

               $result = $this->Client_model->updateEmailSettings(); 
                  if($result == "true")
            {
                 $this->session->set_flashdata('permission_message', ' Updated  Successfully');
                 redirect('client/Client/formsettings/'.$parameter,'refresh');
            }
        else
            {
                 $this->session->set_flashdata('flash_message', 'Error');
                 redirect('superadmin/SuperAdmin/formsettings','refresh');
            }
    }
    //function for open the form of email template
    public function templateview()
    {
      
        
        $this->load->view('client/header');
        $this->load->view('client/templateView',$value);
        $this->load->view('client/footer'); 
    }
    //function for opening the form of email template
    public function updateTemplate($param="")
    {
        $value['param'] = $param;
        $this->load->view('client/header');
        $this->load->view('client/emailTemplate',$value);
        $this->load->view('client/footer'); 
    }
    // function for updating the template in table ' tbltemplate'
    public function templateUpdate()
    {
        
        $result = $this->Client_model->templateUpdate(); 
        
         if($result == "true")
            {
             
                 $this->session->set_flashdata('permission_message', ' Updated  Successfully');
                 redirect('client/Client/templateview','refresh');
            }
        else
            {
                 $this->session->set_flashdata('flash_message', 'Error');
                 redirect('client/Client/templateview','refresh');
            }

    }
    //function to open the form of plan and package
    public function  formPlan($param="")
    {
        $value['id'] = $param;
        $this->load->view('superadmin/header');
        $this->load->view('superadmin/planPackage',$value);
        $this->load->view('superadmin/footer'); 
        
    }
    //pubclic function save plan for saving th eplan features  and plan in table 'tblplan'
    public function addCarDetials($param="")
    {
        $value['id'] = $param;
        
        
        $this->load->view('client/header');
       $value['title'] = "Manage Car Details";
        $this->load->view('client/managcarDetail',$value);
        
       
        $this->load->view('client/footer'); 
  
    }
    
     //pubclic function save plan for saving th eplan features  and plan in table 'tblplan'
    public function viewcarDetail()
    {
        //$value['id'] = $param;
        $this->load->view('client/header');
       $value['title'] = "Manage Car Details";
        $this->load->view('client/viewcardetail',$value);
        
       
        $this->load->view('client/footer'); 
  
    }
    
    
    //function for saving the car details
    public function savecarDetails($param1="",$param2="")
    {
          
       $result = $this->Client_model->savecarDetails($param1,$param2); 
    // echo $result;
    // echo $parameter;
    // die;
          if($result == "create")
            {
                 $this->session->set_flashdata('permission_message', 'Data Saved  Successfully');
                 redirect('client/Client/viewcarDetail/'.$parameter,'refresh');
            }
            if($result == "edit")
            {
                 $this->session->set_flashdata('permission_message', 'Updated  Successfully');
                 redirect('client/Client/viewcarDetail/'.$parameter,'refresh');
            }
            if($result == "delete")
            {
              
                 $this->session->set_flashdata('permission_message', 'Deleted Successfully');
                 redirect('client/Client/viewcarDetail/'.$parameter,'refresh');
            }
//            
//        else
//            {
//                 $this->session->set_flashdata('flash_message', 'Error');
//                 redirect('superadmin/SuperAdmin/viewPlan','refresh');
//            }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    //function for updating the plan and package in table tblplan
//     public function updatePlan()
//     {
//             $result = $this->SuperAdmin_model->updatePlan(); 
//               if($result == "true")
//                {
//                     $this->session->set_flashdata('permission_message', ' Updated  Successfully');
//                     redirect('superadmin/SuperAdmin/viewPlan/'.$parameter,'refresh');
//                }
//            else
//                {
//                     $this->session->set_flashdata('flash_message', 'Error');
//                     redirect('superadmin/SuperAdmin/viewPlan','refresh');
//                }
// 
//     }
//    
     
    //function for view the plan and package details from table ' tblplan'
    public function  viewPlan()
    {
        $this->load->view('superadmin/header');
        $this->load->view('superadmin/planView',$value);
        $this->load->view('superadmin/footer');  
    }
    //function for deleting the plan in table 'tblplan'
     public function  deletePlan($param="")
    {
       $result = $this->SuperAdmin_model->deletePlan($param); 
      if($result == "true")
                {
                     $this->session->set_flashdata('permission_message', ' Deleted Successfully');
                     redirect('superadmin/SuperAdmin/viewPlan/'.$parameter,'refresh');
                }
            else
                {
                     $this->session->set_flashdata('flash_message', 'Error');
                     redirect('superadmin/SuperAdmin/viewPlan','refresh');
                }
    }
    //Function for opening the view page of creating rental company by superadmin 
    public function rentalcompany($param="",$param1="")
    {     
        
        
        
        $value['parameter'] = $param1;
          $value['id'] = $param;
        $this->load->view('superadmin/header');
        $this->load->view('superadmin/createCompany',$value);
        $this->load->view('superadmin/footer');  
    }
    //Public function saveCompany for saving the rental company in table  'tblclientcompany'
    public function saveCompany($param="",$param1="",$param3="")
    {
       
     $parameter = $this->input->post('parameter');
     if($param3 == "dissapproved")
     {
         $parameter = "dissapproved";
     }
     if($param3 == "approved")
     {
         $parameter = "approved";
     }
  
        
        
       $result = $this->SuperAdmin_model->saveCompany($param,$param1); 
    // echo $result;
    // echo $parameter;
    // die;
          if($result == "true")
            {
                 $this->session->set_flashdata('permission_message', 'Data Saved  Successfully');
                 redirect('superadmin/SuperAdmin/viewCompany/'.$parameter,'refresh');
            }
            if($result == "updated")
            {
                 $this->session->set_flashdata('permission_message', 'Updated  Successfully');
                 redirect('superadmin/SuperAdmin/viewCompany/'.$parameter,'refresh');
            }
            if($result == "delete")
            {
              
                 $this->session->set_flashdata('permission_message', 'Deleted Successfully');
                 redirect('superadmin/SuperAdmin/viewCompany/'.$parameter,'refresh');
            }
            
        else
            {
                 $this->session->set_flashdata('flash_message', 'Error');
                 redirect('superadmin/SuperAdmin/viewPlan','refresh');
            }
        
        
    }
    //Public function to view the list of rental company
    public function viewCompany($param="")
    {
     //   echo $param;
        
        
        $value['id'] = $param;
        $this->load->view('superadmin/header');
        $this->load->view('superadmin/newCompanylist',$value);
        $this->load->view('superadmin/footer');    
    }
    //Function for changing the status of approved or dispproved of rental company in table ' tblclientcompany'
    public function statusCompany($param="")
    {
        $result = $this->SuperAdmin_model->statusCompany($param,$param1); 
        if($result == "updated")
            {
                 $this->session->set_flashdata('permission_message', 'Approved Successfully');
                 redirect('superadmin/SuperAdmin/viewCompany/'.$parameter,'refresh');
            }
        
    }
    //function fpor diapproving the company 
    
    public function disapproveCompany($param="")
    {
       $result = $this->SuperAdmin_model->disapproveCompany($param,$param1); 
        if($result == "updated")
            {
                 $this->session->set_flashdata('permission_message', 'Disapproved Successfully');
                 redirect('superadmin/SuperAdmin/viewCompany/'.$parameter,'refresh');
            } 
    }
    
    
    
    
    //public function for printing the csv
    public function printCsv($param="")
    {
         $this->SuperAdmin_model->CSVprint($param); 

    }
    
    //public function exportpdf forn car rental company
    public function printPdf($param="")
    {
         $this->SuperAdmin_model->printPdf($param); 
    }
    //Function for creating the users of super admin  in th etable  ' tblsupadmuser'
    public function  createUser($param="")
    {
      $value['id'] = $param;
      
      
        $this->load->view('client/header');
        $this->load->view('client/createUser',$value);
        $this->load->view('client/footer');     
    }
    //Function for saving the users in table ' tblsupadmuser'
    
      public function  saveUser($param="",$param1="")
    {
           $result = $this->Client_model->saveUser($param,$param1); 
            if($result == "insert")
                {
                     $this->session->set_flashdata('permission_message', 'Data Saved  Successfully');
                     redirect('client/Client/viewUser/'.$parameter,'refresh');
                }
            if($result == "update")
                {
                     $this->session->set_flashdata('permission_message', 'Updated  Successfully');
                     redirect('client/Client/viewUser/'.$parameter,'refresh');
                }
                 if($result == "delete")
                {
                     $this->session->set_flashdata('permission_message', 'Deleted Successfully');
                     redirect('client/Client/viewUser/'.$parameter,'refresh');
                }
                 if($result == "exist")
                {
                     $this->session->set_flashdata('flash_message', 'Email already exist  ');
                     redirect('client/Client/createUser/'.$parameter,'refresh');
                }
    }
    ///function to see the list of all user created by superadmin
    public function viewUser($param="")
    {
      
        $this->load->view('client/header');
        $this->load->view('client/userview',$value);
        $this->load->view('client/footer');     
    }
    //public function role master for making th emaster of roles in table   'tblmasterrole'
     public function roleMaster($param="")
    {
         $value['id'] = $param;
      
        $this->load->view('client/header');
        $this->load->view('client/clientrole',$value);
        $this->load->view('client/footer');     
    }
    //function for saving the role in table ' tblmasterrole'
     public function saveRole($param="",$param1="")
    {
       
         $result = $this->Client_model->saveRole($param,$param1); 
           if($result == "insert")
                {
                     $this->session->set_flashdata('permission_message', 'Data Saved  Successfully');
                     redirect('client/Client/viewRole/'.$parameter,'refresh');
                }
            if($result == "update")
                {
                     $this->session->set_flashdata('permission_message', 'Updated  Successfully');
                     redirect('client/Client/viewRole/'.$parameter,'refresh');
                }
                 if($result == "delete")
                {
                     $this->session->set_flashdata('permission_message', 'Deleted  Successfully');
                     redirect('client/Client/viewRole/'.$parameter,'refresh');
                }
                 if($result == "exist")
                {
                     $this->session->set_flashdata('flash_message', 'Role already exist  ');
                     redirect('client/Client/roleMaster/'.$parameter,'refresh');
                }
     }
      //function for saving the role in table ' tblmasterrole'
     public function viewRole($param="")
    {
         $this->load->view('client/header');
        $this->load->view('client/viewRole',$value);
        $this->load->view('client/footer'); 
    }
    //function for open the view page of assigning the permission ro a particular role  in table
     public function rolePermission($param="")
    {
         $this->load->view('client/header');
        $this->load->view('client/permission',$value);
        $this->load->view('client/footer'); 
    }
    //function for saving the permission to a particular role in table 'tblsupadmpermission'
    public function savePermission()
    {
        $result = $this->Client_model->savePermission($param,$param1);
        if($result == "insert")
            {
                 $this->session->set_flashdata('permission_message', 'Data Saved  Successfully');
                 redirect('client/Client/rolePermission/'.$parameter,'refresh');
            }
          if($result == "update")
            {
                 $this->session->set_flashdata('permission_message', 'Updated  Successfully');
                 redirect('client/Client/rolePermission/'.$parameter,'refresh');
            }

    }
    //function for getting permission by ajax in file permission view.php from table 'tblsupadmpermission'
    public function ajaxGetPermission()
    { 
       // print_r($_POST);die;
         $result = $this->Client_model->ajaxGetPermission($param,$param1); 
        // print_r($result);
        
    }
    
    //function for active the status of  role on or off
    public function changestatusRole($param="")
    {   
        $param = $_POST['statusid'];
         $result = $this->Client_model->changestatusRole($param); 
         echo $result;
         

    }
    
     //function for active the status of  role on or off
    public function changestatusUser($param="")
    {
        
        
        $param = $_POST['statusid'];
        
         $result = $this->Client_model->changestatusUser($param); 
         
         echo $result;
         
         

    }
    
     //function for active the status of  plan on or off
    public function changestatusPlan($param="")
    {
        
        
         $result = $this->SuperAdmin_model->changestatusPlan($param); 
         
         
          if($result == "active")
            {
                 $this->session->set_flashdata('permission_message', 'Status active  Successfully');
                 redirect('superadmin/SuperAdmin/viewPlan/'.$parameter,'refresh');
            }
          if($result == "inactive")
            {
                 $this->session->set_flashdata('flash_message', 'Status Inactive Successfully');
                 redirect('superadmin/SuperAdmin/viewPlan/'.$parameter,'refresh');
            }
         

    }
    
    
    ///function for creating the sales team in table  'tblsaleteam'

    public function createTeam($param="")
    {
      
        
        $value['id'] = $param;
        $this->load->view('client/header');
        $this->load->view('client/salesTeam',$value);
        $this->load->view('client/footer'); 
    }
     ///function for creating the sales team in table  'tblsaleteam'

    public function viewTeam($param="")
    {
         $this->load->view('client/header');
        $this->load->view('client/salesTeamview',$value);
        $this->load->view('client/footer'); 
    }
    
    //function for saving the sales team in table tblsaleteam
    public function  saveTeam($param1="",$param2="")
    {
        
        $result = $this->Client_model->saveTeam($param1,$param2); 
   
         if($result == "insert")
                {
                     $this->session->set_flashdata('permission_message', 'Data Saved  Successfully');
                     redirect('client/Client/viewTeam/'.$parameter,'refresh');
                }
            if($result == "update")
                {
                     $this->session->set_flashdata('permission_message', 'Updated  Successfully');
                     redirect('client/Client/viewTeam/'.$parameter,'refresh');
                }
                 if($result == "delete")
                {
                     $this->session->set_flashdata('permission_message', 'Deleted  Successfully');
                     redirect('client/Client/viewTeam/'.$parameter,'refresh');
                }
        
        
    }
    
     //function for open the form of meeting
     ///function for creating the sales team in table  'tblsaleteam'

    public function createMeeting($param="")
    {
      
        
        $value['id'] = $param;
        $this->load->view('client/header');
        $this->load->view('client/meeting',$value);
        $this->load->view('client/footer'); 
    }
    
     ///function for creating the sales team in table  'tblsaleteam'

    public function viewMeeting($param="")
    {
      
        
        $value['id'] = $param;
        $this->load->view('client/header');
        $this->load->view('client/meetingView',$value);
        $this->load->view('client/footer'); 
    }
    //  function for saving the meeting in table  tblmeeting
    
    public function  saveMeeting($param1="",$param2="")
    {
      
        
        $result = $this->Client_model->createmeet($param1,$param2); 
 if($result == "insert")
                {
                     $this->session->set_flashdata('permission_message', 'Data Saved  Successfully');
                     redirect('client/Client/viewMeeting/'.$parameter,'refresh');
                }
            if($result == "update")
                {
                     $this->session->set_flashdata('permission_message', 'Updated  Successfully');
                     redirect('client/Client/viewMeeting/'.$parameter,'refresh');
                }
                 if($result == "delete")
                {
                     $this->session->set_flashdata('permission_message', 'Deleted  Successfully');
                     redirect('client/Client/viewMeeting/'.$parameter,'refresh');
                }
        
    }
    
    
    //function for creating the leads in table 'tblleads'
    
    public function createLead($param="",$param2="")
    {
      $value['id'] = $param;
      $value['parameter'] = $param2;
        $this->load->view('client/header');
        $this->load->view('client/createlead',$value);
        $this->load->view('client/footer');  
    }
    
    //function for creating the leads in table 'tblleads'
    
    public function viewLead($param="")
    {
        $value['parameter'] = $param;
      
        $this->load->view('client/header');
        $this->load->view('client/viewlead',$value);
        $this->load->view('client/footer');  
    }
    
    //function for saving the leads
    public function saveLead($param1="",$param2="",$param3="")
    {
        $parameter = $this->input->post('parameter');
       
        if($parameter == ""){
            $parameter = $param3;
        }
       
         $result = $this->Client_model->saveLead($param1,$param2); 
       if($result == "insert")
                {
                     $this->session->set_flashdata('permission_message', 'Data Saved  Successfully');
                     redirect('client/Client/viewLead/'.$parameter,'refresh');
                }
            if($result == "update")
                {
                     $this->session->set_flashdata('permission_message', 'Updated  Successfully');
                     redirect('client/Client/viewLead/'.$parameter,'refresh');
                }
                 if($result == "delete")
                {
                     $this->session->set_flashdata('permission_message', 'Deleted  Successfully');
                     redirect('client/Client/viewLead/'.$parameter,'refresh');
                }
        
    }
    // public function creating the lead calls
    
//    public function createCall($param="")
//    {
//        $value['id'] = $param;
//        $this->load->view('client/header');
//        $this->load->view('client/leadCall',$value);
//        $this->load->view('client/footer');  
//    }
    
     // public function creating the lead calls
    
//    public function viewCall($param="")
//    {
//        
//        
//        $value['id'] = $param;
//        $this->load->view('client/header');
//        $this->load->view('client/leadcallView',$value);
//        $this->load->view('client/footer');  
//    }
    
    
    
    
    
    
//    //function for saving the lead calls i  table ' tblleadcall'
//    public function saveleadCall($param1="",$param2="")
//    {
//    
//        $result = $this->Client_model->saveleadCall($param1,$param2); 
//       if($result == "insert")
//                {
//                     $this->session->set_flashdata('permission_message', 'Data Saved  Successfully');
//                     redirect('client/Client/viewCall/'.$parameter,'refresh');
//                }
//            if($result == "update")
//                {
//                     $this->session->set_flashdata('permission_message', 'Updated  Successfully');
//                     redirect('client/Client/viewCall/'.$parameter,'refresh');
//                }
//                 if($result == "delete")
//                {
//                     $this->session->set_flashdata('permission_message', 'Deleted  Successfully');
//                     redirect('client/Client/viewCall/'.$parameter,'refresh');
//                } 
//        
//        
//        
//    }
    // public function for managing the tags in for leads in table 'tblmanagetags'
    public function manageLead()
    {
        $value['id'] = $param;
        $this->load->view('client/header');
        $this->load->view('client/leadtag',$value);
        $this->load->view('client/footer');  
    }
    //public function  manage lead tags in table '
    public function updateLeads($param1="",$param2="")
    {
       $result = $this->Client_model->updateLeads($param1,$param2); 
       if($result == "true"){
            $this->session->set_flashdata('permission_message', 'Tags Updated  Successfully');
            redirect('client/Client/manageLead/'.$parameter,'refresh');
       }

        
    }
    
    
    
    
    
    //function for changing the status of lead to customer and failed in tbllead
    
    public function statusleadCustomer($param1="",$param2="")
    {
        $data['leadstatus'] = "customer";
        $query=  $this->db->update('tblleads', $data, array('leadid' =>$param1));
        
           $query = $this->db->get_where('tblleads',array('leadid'=>$param1))->row_array();
        
        $data1['opportunity'] = $query['leadoppurtunity'];
        $data1['opportunitysalesperson'] = $query['leadsalespersonid'];

        $data1['opportunityteam'] = $query['leadteamid'];
        $data1['opportunitycustomer'] = $query['leadcustomerid'];
         $data1['clientcompid'] = $this->session->userdata['companyid'];
                $data1['opportunitystatus'] = "pending";

        
        $this->db->insert('tblopportunity',$data1);
        
        
        
        
         $this->session->set_flashdata('permission_message', 'Status Updated  Successfully');
        redirect('client/Client/viewLead/'.$parameter,'refresh');
          
    }
    
     //function for changing the status of lead to customer and failed in tbllead
    
    public function statusleadFailed($param1="",$param2="")
    {
        $data['leadstatus'] = "failed";
        $query=  $this->db->update('tblleads', $data, array('leadid' =>$param1));
         $this->session->set_flashdata('permission_message', 'Status Updated  Successfully');
        redirect('client/Client/viewLead/'.$parameter,'refresh');
          
    }
    
      //function of manging the stages of  opportunities in table 'tblopporstages'
    public function manageStages()
    {
          
        $value['id'] = $param;
        $this->load->view('client/header');
        $this->load->view('client/opportunitystages',$value);
        $this->load->view('client/footer'); 
        
    }
    //function for updating the opportunity stages in tablr o 'tblopporstages'
    
    public function opportunityStages()
    {
                $result = $this->Client_model->opportunityStages($param1,$param2); 
                if($result == "true"){
                   $this->session->set_flashdata('permission_message', 'Stages Updated  Successfully');
                 redirect('client/Client/manageStages/'.$parameter,'refresh');
             
               }

    }
    //function for creating the opportunity in table 'tblopportunity'
    public function createOpportunity($param="",$parameter ="")
    {
          $value['id'] = $param;
          $value['parameter'] = $parameter;
          
         
        $this->load->view('client/header');
        
       
        $this->load->view('client/createopportunity',$value);
        $this->load->view('client/footer'); 
    }
    
     //function for creating the opportunity in table 'tblopportunity'
    public function viewOpportunity($parameter ="")
    {
           $value['parameter'] = $parameter;
          $value['id'] = $param;
        $this->load->view('client/header');
        $this->load->view('client/viewopportunity',$value);
        $this->load->view('client/footer'); 
    }
    
    
    
    //function for saving the opportunoty
    public function saveOpportunity($param1="",$param2="",$param3="")
    {
        $parameter = $_POST['parameter'];
      
         $result = $this->Client_model->saveOpportunity($param1,$param2); 
       if($result == "insert")
                {
                     $this->session->set_flashdata('permission_message', 'Data Saved  Successfully');
                     redirect('client/Client/viewOpportunity/'.$parameter,'refresh');
                }
            if($result == "update")
                {
                     $this->session->set_flashdata('permission_message', 'Updated  Successfully');
                     redirect('client/Client/viewOpportunity/'.$parameter,'refresh');
                }
                 if($result == "delete")
                {
                      if($param3 !=""){
                         $parameter = "qualify";
                     }
                     $this->session->set_flashdata('permission_message', 'Deleted  Successfully');
                     redirect('client/Client/viewOpportunity/'.$parameter,'refresh');
                } 
        
    }
    
    
    //function for changing the status in tabvle tblopportuity
    public function   statusOpportunities($param="")
    {
     
        
        $data['opportunitystatus']  = "qualify";
        $query=  $this->db->update('tblopportunity', $data, array('opportunityid' =>$param));
        $this->session->set_flashdata('permission_message', 'Status Change to Qualify Successfully');
        redirect('client/Client/viewOpportunity/'.$parameter,'refresh');
 }
   
    
    
    
    
//    function for saving the savemeeting
    public function saveopportMeeting()
    {
        $data['meetingsubject'] = $this->input->post('subject');
        $data['meetingattendies'] = implode(",",$_POST['attendies']);
        $data['meetingstartat'] = $this->input->post('starting_date');
        $data['meetingendat'] = $this->input->post('ending_date');
        $data['meetinglocation'] = $this->input->post('location');
        $data['meetingdescription'] = $this->input->post('meeting_description');
        $data['clientcompid'] = $this->session->userdata['companyid'];
        $data['opportunityid'] = $this->input->post('updateid');
         
        $id =$this->input->post('updateid');
         $this->db->insert('tblmeeting',$data);
         
      // $leadquery=  $this->db->update('tblmeeting', $data, array('meetingid' =>$id));

         ?>
         <table class="table table-striped table-hover" id="datatable1" >
                                 <thead>
                                    <tr>
                                        <th>S.No</th>
                                       <th>Subject</th>
                                       <th>Location</th>
                                       <th>Attendies</th>
                                       <th>Starting at</th>
                                        <th>Ending at</th>
                                        <th>Description</th>

                                      
                                      
                                       <th>Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                     <?php
                                     
                                     $comp = $this->session->userdata['companyid'];
                                      $this->db->order_by("meetingid", "desc");
                                      $this->db->where('clientcompid',$comp);
                                      $this->db->where('opportunityid',$id);
                                      $meetquery = $this->db->get('tblmeeting');
                                    
                                     
                                     
                                     
                                    // $query = $this->db->get_where('tblclientuser',array('clientcompid'=>$compid));
                                     $i=1;
                                    // echo $this->db->last_query();
                                     foreach($meetquery->result() as $k=>$vl)
                                     {?>
                                    <tr class="gradeX  hide<?php echo $vl->meetingid;?> ">
                                        <td><?php echo $i;?></td>
                                        
                                        
                                        <td> <?php echo $vl->meetingsubject;?>
                                        </td>
                                     
                                       
                                     
                                       
                                       
                                       
                                       
                                       <td><?php echo $vl->meetinglocation;?> </td>
                                       
                                          
                                       <?php
                                       $k2="";
                                       
                                       $member = explode(",",$vl->meetingattendies);
                                       
                                       foreach($member as $t=>$r) {
                                   
                                       $query123 = $this->db->get_where('tblclientuser', array('clientuserid' => $r))->row_array();
                                          
                                       $name = $query123['clientusername'];
                                       
                                       
                                     $k2 = $k2.','."$name";         
                                       }
                                       ?>
                                        
                                       
                                         <td><?php echo $str = ltrim($k2, ',');?></td>
                                     
                                         
                                         
                                   <td><?php echo $vl->meetingstartat;?></td>
                                   <td><?php echo $vl->meetingendat;?></td>
                                       
                                       
                                   <td> <?php echo $vl->meetingdescription;?>  </td>  
                                       
                                     
                    <td><a onclick="editmeeting('<?php echo $vl->meetingid;?>');" ><i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i></a>
                     <a  onclick="return confirmirmation('<?php echo $vl->meetingid;?>');" ><i class="fa fa-trash-o fa-2x" aria-hidden="true" style="color:#f31a04;"></i></a></td>

                                    </tr>
                                  
                                     <?php $i++; } 
                                     
                                     
                                     
                                     
                                     
                                     ?>
                                  
                              
                                 
                                 </tbody>
                              </table>
         <?php
        
//        $data['meetingsubject'] = $this->input->post('subject');
//        $data['meetingattendies'] = implode(",",$_POST['attendies']);
//        $data['meetingstartat'] = $this->input->post('starting_date');
//        $data['meetingendat'] = $this->input->post('ending_date');
//        $data['meetinglocation'] = $this->input->post('location');
//        $data['meetingdescription'] = $this->input->post('meeting_description');
//        $data['clientcompid'] = $this->session->userdata['companyid'];
//        $data['opportunityid'] = $this->input->post('updateid');;
//        
//       // print_r($data);
//        
//         $this->db->insert('tblmeeting',$data);
//         
//             echo "insert"; 
   }
    
   //function for taking the ajax response from table 'tblmeeting'
   public function getopportunityTable()
   {
        $query = $this->db->get_where('tblmeeting',array('opportunityid'=>$_POST['opportinityId']))->row_array();
        print_r($query);

   }
   //function for deleting the meeting in table 'tblmeeting' 
   public function deletemeeting()
   {
        $query=  $this->db->delete('tblmeeting', array('meetingid' => $_POST['data']));
       echo "delete"; 
   }
   
  public function   editopportMeeting()
  {
    
    //  print_r($_POST);
        $data['meetingsubject'] = $this->input->post('subject');
        $data['meetingattendies'] = implode(",",$_POST['attendies']);
        $data['meetingstartat'] = $this->input->post('starting_date');
        $data['meetingendat'] = $this->input->post('ending_date');
        $data['meetinglocation'] = $this->input->post('location');
        $data['meetingdescription'] = $this->input->post('meeting_description');
        $data['clientcompid'] = $this->session->userdata['companyid'];
        
     
         $id  = $this->input->post('editid');
         
         
           $query=  $this->db->update('tblmeeting', $data, array('meetingid' =>$id)); ?>
              
 <table class="table table-striped table-hover" id="datatable1" >
                                 <thead>
                                    <tr>
                                        <th>S.No</th>
                                       <th>Subject</th>
                                       <th>Location</th>
                                       <th>Attendies</th>
                                       <th>Starting at</th>
                                        <th>Ending at</th>
                                        <th>Description</th>

                                      
                                      
                                       <th>Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                     <?php
                                     
                                     $comp = $this->session->userdata['companyid'];
                                      $this->db->order_by("meetingid", "desc");
                                      $this->db->where('clientcompid',$comp);
                                      $this->db->where('meetingid',$id);
                                      $meetquery = $this->db->get('tblmeeting');
                                    
                                   //  echo $this->db->last_query();
                                     
                                     
                                    // $query = $this->db->get_where('tblclientuser',array('clientcompid'=>$compid));
                                     $i=1;
                                    // echo $this->db->last_query();
                                     foreach($meetquery->result() as $k=>$vl)
                                     {?>
                                    <tr class="gradeX  hide<?php echo $vl->meetingid;?> ">
                                        <td><?php echo $i;?></td>
                                        
                                        
                                        <td> <?php echo $vl->meetingsubject;?>
                                        </td>
                                     
                                       
                                     
                                       
                                       
                                       
                                       
                                       <td><?php echo $vl->meetinglocation;?> </td>
                                       
                                          
                                       <?php
                                       $k2="";
                                       
                                       $member = explode(",",$vl->meetingattendies);
                                       
                                       foreach($member as $t=>$r) {
                                   
                                       $query123 = $this->db->get_where('tblclientuser', array('clientuserid' => $r))->row_array();
                                          
                                       $name = $query123['clientusername'];
                                       
                                       
                                     $k2 = $k2.','."$name";         
                                       }
                                       ?>
                                        
                                       
                                         <td><?php echo $str = ltrim($k2, ',');?></td>
                                     
                                         
                                         
                                   <td><?php echo $vl->meetingstartat;?></td>
                                   <td><?php echo $vl->meetingendat;?></td>
                                       
                                       
                                   <td> <?php echo $vl->meetingdescription;?>  </td>  
                                       
                                     
                    <td><a onclick="editmeeting('<?php echo $vl->meetingid;?>');" ><i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i></a>
                     <a  onclick="return confirmirmation('<?php echo $vl->meetingid;?>');" ><i class="fa fa-trash-o fa-2x" aria-hidden="true" style="color:#f31a04;"></i></a></td>

                                    </tr>
                                  
                                     <?php $i++; } 
                                     
                                     
                                     
                                     
                                     
                                     ?>
                                  
                              
                                 
                                 </tbody>
                              </table>



           
 <?php 
      
//      $data['meetingsubject'] = $this->input->post('subject');
//        $data['meetingattendies'] = implode(",",$_POST['attendies']);
//        $data['meetingstartat'] = $this->input->post('starting_date');
//        $data['meetingendat'] = $this->input->post('ending_date');
//        $data['meetinglocation'] = $this->input->post('location');
//        $data['meetingdescription'] = $this->input->post('meeting_description');
//        $data['clientcompid'] = $this->session->userdata['companyid'];
//        $data['opportunityid'] = $this->input->post('opportunityid');;
//        
//    
//      
//       
//              $query=  $this->db->update('tblmeeting', $data, array('meetingid' =>$_POST['updateid']));
//              
//            echo  $this->db->last_query();
//            return "update";
  }
   
   
   
   
   
   
   
   //function for edit the the meeting
   public function editmeeting()
   {
      
     
       
       
       $query = $this->db->get_where('tblmeeting', array('meetingid' =>$_POST['editid']))->row_array();

      
       
       ?>
<link href="<?php echo base_url();?>multiple-select-master/multiple-select.css" rel="stylesheet"/>
  <!-- Trigger the modal with a button -->
  <button type="button" class="btn btn-info btn-lg abcdefg" data-toggle="modal" data-target="#myeditModal" style="display:none;">Open Modal</button>


      <div id="myeditModal"  >
 <div class="modal-dialog modal-confirm">
  <div class="modal-content" style="margin-left: -7px;">
                      <button type="button"  style="margin-right: 14px; margin-top: 17px;" class="close qwerty" data-dismiss="modal" aria-hidden="true">&times;</button>

   <div class="modal-header">
    <div class="icon-box">
     <!--<i class="material-icons">&#xE5CD;</i>-->
        
        
<!--        <div class="alert alert-block alert-success  modelpop123" id="asdfgh" style="background-color:#3ec0e8 ;display:none;">
                                <a class="close456" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p> Updated Successfully</p>
                        </div>	-->
    </div>    
   </div>
   
      
      
      
       
<!--        <div class="alert alert-block alert-success  modelpop123" style="background-color:#3ec0e8 ;display:none;">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p> Updated Successfully</p>
                        </div>	-->
    <form id="edit_meeting" name="add_meeting" class="form-validation" accept-charset="utf-8" enctype="multipart/form-data" method="post">
               	 <input type="hidden" name="meeting_type_id" value="4">
               	 <input type="hidden" name="meeting_type" value="opportunities">	                        			 
               	 <div class="modal-body">
                   
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="field-2" class="control-label">Meeting Subject</label>
                        <input type="text" class="form-control" name="subject" onblur="hideValue();" value="<?php echo $query['meetingsubject'];?>" id="meeting_subject12" placeholder="">
                                                <span id="meeting12" style="color:red"> </span>

                      </div>
                    </div>
                    
                   
             <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="field-2" class="control-label">Attendies</label>
                        
                        <br>
                        
  <select multiple="multiple"   name="attendies[]" style="width:200px" id="ert1">

                                <?php 
                                     $comp = $this->session->userdata['companyid'];  
                                     $this->db->where('clientuserstatus',1);
                                    $this->db->where('clientcompid', $comp);
                                 
                                    $member = $this->db->get('tblclientuser');

                                
                            
        
        
        
       $mem = explode(",",$query['meetingattendies']) ;
             
     // print_r($mem);
      
       // print_r($member->result());                     
                                foreach($member->result() as $k1=>$v1)
                                    
                                    {
                                    
                                           $m =  $v1->clientuserid;
                                    
                                         if (in_array($m, $mem))
                                                                { ?>
        
                   <option value="<?php echo $v1->clientuserid;?>" <?php echo "selected";?> ><?php echo $v1->clientusername;?></option>

                                                

     <?php           }
                                                              else
                                                                { ?>
                                                               
                                                                  
           <option value="<?php echo $v1->clientuserid;?>"  ><?php echo $v1->clientusername;?></option>

                                                         <?php       }

                                              
                                    }
                                    
                             
                                    ?>

                    
                               
        
        
        
        
        
        
        
        
                                                                       </select>                     
                      
                      
                      
                      </div>
                    </div>

			<input type="hidden"	 value="<?php echo $query['meetingid'];?>" name="editid">
                    <li class="active"><a href="#tab1_1" data-toggle="tab" style="margin-left:40px;"></a></li>
                   
              
				 <div class="tab-content">
						                        
						                        <div class="tab-pane fade active in" id="tab1_1">
							                           <div class="panel-body bg-white">
						                 			  		   											 				  							  <div class="row">
                          							   <div class="col-md-6">
									                      <div class="form-group">
									                        <label for="field-1" class="control-label">Starting at</label>
									                        <!--<input type="text" class="date-picker form-control" name="date" id="date" placeholder="" value="">-->
									                        <input type="text" name="starting_date" value="<?php echo $query['meetingstartat'];?>"  onblur="hideValue();   compareDate();" id="datetimepicker5" class="datetimepicker form-control hasDatepicker  starting12" placeholder="Choose a date...">
									                             <span id="start12" style="color:red"></span>
 
									                      </div>
									                    </div> 
	                          							 <div class="col-sm-6">
							                            <div class="form-group">
							                              <label class="control-label">Location</label>
							                              <div class="append-icon">
							                                <input type="text" name="location" value="<?php echo $query['meetinglocation'];?>"  onblur="hideValue(); " id="location12" class="form-control"> 
							                                                                                                                       <span id="locate12" style="color:red"></span>

							                              </div>
							                            </div>
							                          </div>
                          					 	
					                    			     </div>
	 													 <div class="row">
                          							     	 <div class="col-sm-6">
									                           <div class="form-group">
									                              <label class="control-label">Ending at</label>
									                              <div class="append-icon">
									                                <input type="text" name="ending_date" value="<?php echo $query['meetingendat'];?>"  onblur="hideValue();  compareDate();" id="datetimepicker4" class="datetimepicker form-control hasDatepicker ending_date12" placeholder="Choose a date...">
									                                                                                                                                       <span id="end12" style="color:red"></span>
<input type="hidden" value="<?php echo $query['opportunityid'];?>" name="opportunityid">
									                              </div>
									                            </div>
					                         				 </div>
                          								     <div class="col-sm-6">
				                            
				                         					 </div>
                          					 	
					                    			      </div>
													
														<div class="row">
														<div class="col-sm-12">
					                            		<div class="form-group">
					                               <label class="control-label">Description</label>
					                              <div class="append-icon">
					                                
					                                <textarea name="meeting_description"  value="<?php echo $query['meetingdescription'];?>" onblur="hideValue();"  rows="5" class="form-control desc12312" placeholder="describe the product characteristics..."><?php echo $query['meetingdescription'];?></textarea>   
					                                                                                                 <span id="desc12" style="color:red"></span>

                                                                      </div
					                            </div>
					                         			 </div>
														</div>
							               			   </div>
						                        </div>
											    
<!--											    <div class="tab-pane fade" id="tab1_2">
												<div class="panel-body bg-white">	
													 <div class="row">
													 	 <div class="col-sm-6">
							                            
					                          			</div>-->
														 <div class="col-sm-6">
<!--							                            <div class="form-group">
							                              <label class="control-label">Show Time as</label>
							                              <div class="append-icon">
							                                 
							                                <div class="select2-container form-control" id="s2id_autogen22"><a href="javascript:void(0)" class="select2-choice select2-default" tabindex="-1">   <span class="select2-chosen" id="select2-chosen-23"></span><abbr class="select2-search-choice-close"></abbr>   <span class="select2-arrow" role="presentation"><b role="presentation"></b></span></a><label for="s2id_autogen23" class="select2-offscreen"></label><input class="select2-focusser select2-offscreen" type="text" aria-haspopup="true" role="button" aria-labelledby="select2-chosen-23" id="s2id_autogen23"><div class="select2-drop select2-display-none">   <div class="select2-search select2-search-hidden select2-offscreen">       <label for="s2id_autogen23_search" class="select2-offscreen"></label>       <input type="text" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" class="select2-input" role="combobox" aria-expanded="true" aria-autocomplete="list" aria-owns="select2-results-23" id="s2id_autogen23_search" placeholder="">   </div>   <ul class="select2-results" role="listbox" id="select2-results-23">   </ul></div></div><select name="show_time_as" class="form-control" tabindex="-1" title="" style="display: none;">
<option value="" selected="selected"></option>
<option value="Free">Free</option>
<option value="Busy">Busy</option>
</select>	
							                              </div>
							                            </div>
					                          			</div>	-->
													 </div>
												</div>
					                         </div> 
					                          
					                           
					                            </div>
                </div>
                 
                  <div id="meeting_submitbutton" class="modal-footer text-center"><button  class="btn btn-primary btn-embossed bnt-square createbutton12" onclick="editformSubmit();">Update</button></div>
                 
                </form>
  </div>
 </div>
</div>
  
  
  
 
  
</div>

<script>    
            
            function compareDate1()
            {
               var start = $(".starting12").val();
               var end = $(".ending_date12").val();
             
             var x = new Date(start);
                 var y = new Date(end);
             
             alert(x);
          //   alert(y);
                 if(x>y){
                     
                     $("#start12").html("Start date is not greater than End date");
                     
                      $('.createbutton12'). attr('disabled', 'disabled'); 
                   }else{
                       
                         $("#start12").html("  ");
                       
                         $('.createbutton12').prop("disabled", true);
                         $('.createbutton12').removeAttr("disabled");
                   }
             
            }
            
            
            
            
            </script>



           <script src="<?php echo base_url();?>multiple-select-master/multiple-select.js"> </script>

           <script>
  $('#ert1').multipleSelect({
          //  isOpen: true,
                //filter: true
                // selectAll: false
        });
  </script>
    
  
  <script src="<?php echo base_url();?>assets/js/demo/demo-search.js"></script>
    <script src="<?php echo base_url();?>assets/bootstrap-datetimepicker.min.js"></script>
  
  <script>
$('#datetimepicker4').datetimepicker({
      icons: {
          time: 'fa fa-clock-o',
          date: 'fa fa-calendar',
          up: 'fa fa-chevron-up',
          down: 'fa fa-chevron-down',
          previous: 'fa fa-chevron-left',
          next: 'fa fa-chevron-right',
          today: 'fa fa-crosshairs',
          clear: 'fa fa-trash'
        }
    });
    
      $('#datetimepicker5').datetimepicker({
      icons: {
          time: 'fa fa-clock-o',
          date: 'fa fa-calendar',
          up: 'fa fa-chevron-up',
          down: 'fa fa-chevron-down',
          previous: 'fa fa-chevron-left',
          next: 'fa fa-chevron-right',
          today: 'fa fa-crosshairs',
          clear: 'fa fa-trash'
        }
    });
    
    
    
    
    
    
    
    
    
    
    
</script>
  
        <?php }
    
    
        
//     public function deletetag()
//     {
//       
//                  $result = $this->Client_model->deletetag($param1,$param2); 
//
//     }
//        
      //function for making th eprice master in table tblcarmonths
     
     public function createPriceMaster($param="")
     {
         $value['id'] = $param;
        $this->load->view('client/header');
        $this->load->view('client/priceMaster',$value);
        $this->load->view('client/footer'); 
     }
     //function for saving the all months
     public function savecarMonth($param1="",$param2="")
     {
       $result = $this->Client_model->savecarMonth($param1,$param2); 
     
        if($result == "exist")
                {
                     $this->session->set_flashdata('permission_message', 'Duration already  Saved');
                   
                     redirect('client/Client/carmonthView/'.$parameter,'refresh');
                }
       
       
       
       
        if($result == "insert")
                {
                     $this->session->set_flashdata('permission_message', 'Data Saved  Successfully');
                   
                     redirect('client/Client/carmonthView/'.$parameter,'refresh');
                }
            if($result == "update")
                {
                     $this->session->set_flashdata('permission_message', 'Updated  Successfully');
                     redirect('client/Client/carmonthView/'.$parameter,'refresh');
                }
               if($result == "delete")
                {
                     $this->session->set_flashdata('permission_message', 'Deleted  Successfully');
                     redirect('client/Client/carmonthView/'.$parameter,'refresh');
                }
     }
     //function for view the month master
     public function  carmonthView()
     {
       
        $this->load->view('client/header');
        $this->load->view('client/carmonth',$value);
        $this->load->view('client/footer');  
     }
        
     public function deletetag()
     {
       
                  $result = $this->Client_model->deletetag($param1,$param2); 

     }
     
     
     //      function for saving the lead calls in tablr tbl lead call  
     public function saveleadCall()
    {
    
    $result = $this->Client_model->saveleadCall(); 
    } 
    
    //function for delete the calls fron tbl lead
    public function deletecall()
    {
        
          $result = $this->Client_model->deleteleadcall(); 
    }
     //function for edit lead call
    public function editCall()
    {
         $result = $this->Client_model->editCall(); 
    }
    
    //function for saving the edited form submit
    public function editleadSubmit()
    {
               $result = $this->Client_model->editleadSubmit(); 

    }
     
     //function for updating the opportunity stages in tablr o 'tblopporstages'
    
    public function updateOpportunityStage()
    {
           
        
        $result = $this->Client_model->opportunityStages($param1,$param2); 
                if($result == "true"){
                   $this->session->set_flashdata('permission_message', 'Stages Updated  Successfully');
                 redirect('client/Client/manageStages/'.$parameter,'refresh');
             
               }

    }
     
      //function for deleting the stages from table  'tblopportunitystage;
    
    public function deletestage()
    {
         $result = $this->Client_model->deletestage(); 
    }
     
      // public function creating the lead calls
    
    public function createCall($param="")
    {
     
        
        $value['id'] = $param;
        $this->load->view('client/header');
        $this->load->view('client/logCall',$value);
        $this->load->view('client/footer');  
    }
     // public function creating the lead calls
    
    public function viewCall($param="")
    {
        
        
        $value['id'] = $param;
        $this->load->view('client/header');
        $this->load->view('client/logcallView',$value);
        $this->load->view('client/footer');  
    }
    
    //function for saving the data in  table tbllogcall
   public function savelogCall($param1="",$param2="")
   {
         $result = $this->Client_model->savelogCall($param1,$param2); 

        if($result == "insert")
                {
                     $this->session->set_flashdata('permission_message', 'Data Saved  Successfully');
                   
                     redirect('client/Client/viewCall/'.$parameter,'refresh');
                }
            if($result == "update")
                {
                     $this->session->set_flashdata('permission_message', 'Updated  Successfully');
                     redirect('client/Client/viewCall/'.$parameter,'refresh');
                }
               if($result == "delete")
                {
                     $this->session->set_flashdata('permission_message', 'Deleted  Successfully');
                     redirect('client/Client/viewCall/'.$parameter,'refresh');
                }
   }
     
     
       //function for creating the contract in table 'tblcontract'
   public function createContract($param="")
   {
      $value['id'] = $param;
        $this->load->view('client/header');
        $this->load->view('client/contract',$value);
        $this->load->view('client/footer');  
   }
   
  //fgunction for view the all contract from table tablcontract 
   public function viewContract()
   {
      
        $this->load->view('client/header');
        $this->load->view('client/contractview',$value);
        $this->load->view('client/footer'); 
   }
    //function for saving the contracts in table tblcontract
   
   public function saveContract($param1="",$param2="")
   {
     $result = $this->Client_model->saveContract($param1,$param2); 
      if($result == "insert")
                {
                     $this->session->set_flashdata('permission_message', 'Data Saved  Successfully');
                   
                     redirect('client/Client/viewContract/'.$parameter,'refresh');
                }
            if($result == "update")
                {
                     $this->session->set_flashdata('permission_message', 'Updated  Successfully');
                     redirect('client/Client/viewContract/'.$parameter,'refresh');
                }
               if($result == "delete")
                {
                     $this->session->set_flashdata('permission_message', 'Deleted  Successfully');
                     redirect('client/Client/viewContract/'.$parameter,'refresh');
                }
   }
     
     
     public function changestatuscontract()
    {
         $param = $_POST['statusid'];
        
         $result = $this->Client_model->changestatuscontract($param); 
         echo $result;
    } 
     //function for search the contract betwwen dates in table 'tblconract'
    
    public function searchContract()
    {
        $result = $this->Client_model->searchContract($param); 

    }
    
     //function for seeing all the registred pland from registred clientplan
    public function clientPlans()
    {
          $value['id'] = $param;
        $this->load->view('client/header');
        $this->load->view('client/clientPlan',$value);
        $this->load->view('client/footer');  
    }
    
   //function for updating the plan of client
 public function updatePlan()
 {
////        echo '<pre>';
////        print_r($this->session->all_userdata());
                          redirect('Frontuser/plan');
//
//        //   $this->load->view('Frontuser/plan');
  }
//     
     
   //function for managing the event and appointment
    public function  eventAppointment()
    {
       
        $comp = $this->session->userdata('companyid');
        
        $query = $this->db->get_where("tblmeeting",array("clientcompid"=>$comp));
        
         $value['meeting'] = $query->result();        
         $value['calendar'] = $this->Client_model->geteventCalendar($param); 
//        print_r($value);die;
       $this->load->view('client/header');
        $this->load->view('client/eventAppointment',$value);
      $this->load->view('client/footer');   
        
    }
    
    //p
    
    public function geteventCalendar() 
    {

        
            
                $result = $this->Client_model->geteventCalendar($param); 
  }      
     
   //function test of of validation on managecar rental
function test()
{
    $this->load->view('client/header');
      $this->load->view('client/managcarDetail',$value);
      $this->load->view('client/footer');   
}  
     
     
     
     
     

   }

