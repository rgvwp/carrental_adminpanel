
<!--Company Name :- Lazlo Software Solution
    Creation Date :-6/6/2018
    Controller Name :- Home
    Description :- 1 This is the Home controller  which opens the login page .
                   2 Authorize a user to acces dashboard means login and logout functionality.
     
    Database Name:- carrental
    Table Used :- 'tblsupadmprofile'. --!>

<?php

class Home extends CI_Controller {
    public function __construct() {

        parent::__construct();
       
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->model('CheckLogin_model');
        $this->load->database();
      
         if ($this->session->userdata['id'] !="") {
            redirect('superadmin/SuperAdmin/dashboard', 'refresh');
        }
        
        


                error_reporting(0); 
                ini_set('display_errors', 'Off');



         
    }
 
//  Function for opening the login view page
    public function index() 
    {
        
       
       $this->login();
       
    }
 
//  Function for opening the login page where user can login
    public function login() 
    {
        $this->load->view('login');
    }

//   Function for access of user who have permission to login to dashboard
    public function checkLogin()
    {
        
      $result=$this->CheckLogin_model->checkLogin();
      if($result == "true")
        {
          redirect('superadmin/SuperAdmin');
        }
       if($result == "false")
        {
           $this->session->set_flashdata('flash_message', 'Username or Password is incorrect');
           redirect('Home/login','refresh');
        }
      
    }
 
 // Function for logut of user and go to login page
    public function logout()
    {
        $this->session->set_flashdata('flash_message', 'Logout Successfully');
        
         redirect('Home/login');   
    }
    
    
    
  
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}