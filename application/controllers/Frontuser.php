<?php
/***
 * *Company Name :- Lazlo Software Solution
 **Creation Date :-18/6/2018
 ** Controller Name :- Front
 ** Description :- 1 This is the Front controller  which opens the frontend part .
 **                2 This is website frontend panel where client can sign up .and can tak eplan
 **  
 ** Database Name:- carrental
 ** Table Used :- '
 **/
class Frontuser extends CI_Controller {
    
    public function __construct() {

        parent::__construct();
       
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->library('email');
        $this->load->model('frontmodel/Frontmodel');

        $this->load->database();
      
        error_reporting(0);
        
    }
 
//  Function for opening the login view page
    public function index() 
    {
      $this->homepage();
       
    }
 
//  Function for opening the login page where user can login
    public function homepage() 
    { 
     
       // $this->load->view('frontsite/header');
        $this->load->view('frontsite/index');
        //$this->load->view('frontsite/footer');
       
    }
    
    
       public function signup() 
    {
      
        // $this->load->view('frontsite/header');
         $this->load->view('frontsite/signup');
      //  $this->load->view('frontsite/footer');
         
    }
    
    
    
    
    
    
    
    
    
    
    
    
    //function for opening the view page of about
     public function about() 
    {
          $this->load->view('frontsite/about');
         
    }
//function for opening the view page of plan and package
     public function plan() 
    {
        
         
         $this->load->view('frontsite/plan');
         
    }
    //function for opening the view page of contact
     public function contact() 
    {
          $this->load->view('frontsite/contact');
         
    }
 //function for opening the view page of contact
     public function services() 
    {
          $this->load->view('frontsite/services');
         
    }
    //function for opening the view page of faq
     public function faq() 
    {
          $this->load->view('frontsite/faq');
         
    }
     //function for opening the view page of vision and mission
     public function vision() 
    {
          $this->load->view('frontsite/vision');
         
    }
       //function for opening the view page of enquiry
     public function enquiry() 
    {
          $this->load->view('frontsite/enquiry');
         
    }
     //function for opening the view page of help
     public function help() 
    {
          $this->load->view('frontsite/help');
         
    }
     //function for opening the view page of news
     public function news() 
    {
          $this->load->view('frontsite/news');
         
    }
     //function for opening the view page of client port
     public function clientport() 
    {
          $this->load->view('frontsite/clientport');
         
    }
    
     //function for opening the view page of signup
  
      //function for opening the view page of login
     public function loginpage() 
    {
      
         $this->load->view('frontsite/login');

        
         
    }
    //function for opening the view page of term
     public function term() 
    {
      
         $this->load->view('frontsite/terms');

        
         
    }
  
     //function for opening the view page of privacy
     public function privacy() 
    {
      
         $this->load->view('frontsite/privacy');

        
         
    }
    
//    public function test123()
//    {
//        
//         $from_email = "info@srngroups.com"; 
//    $to_email = 'lss.tm18@livesoftwaresolution.com'; 
//         
//         $config = Array(
//                'charset' => 'utf-8',
//                'wordwrap' => TRUE,
//                'mailtype' => 'html',
//            );
//   
//         //Load email library 
//         $this->load->library('email',$config); 
//   
//         $this->email->from($from_email, 'LSS'); 
//         $this->email->to($to_email);
//         $this->email->subject('Your Demo Pack.'); 
//         $this->email->message('You have registered successfully. We will provide demo trial link within 24 hrs by e-mail.');
//	$r = $this->email->send();
//	
//        echo $r;
//        die;
//        
//    }
//    
    
    
}