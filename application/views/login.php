<!DOCTYPE html>

<?php

error_reporting(0); 
ini_set('display_errors', 'Off');
?>



<html lang="en">

<head>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
   <meta name="description" content="Bootstrap Admin App + jQuery">
   <meta name="keywords" content="app, responsive, jquery, bootstrap, dashboard, admin">
   <title>Carrental</title>
   <!-- =============== VENDOR STYLES ===============-->
   <!-- FONT AWESOME-->
   <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/fontawesome/css/font-awesome.min.css">
   <!-- SIMPLE LINE ICONS-->
   <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/simple-line-icons/css/simple-line-icons.css">
   <!-- =============== BOOTSTRAP STYLES ===============-->
   <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.css" id="bscss">
   <!-- =============== APP STYLES ===============-->
   <link rel="stylesheet" href="<?php echo base_url();?>assets/css/app.css" id="maincss">
</head>

<body>
    
   <div class="wrapper" style="margin-top:100px">
      
      <div class="block-center mt-xl wd-xl">
         <!-- START panel-->
         <div class="panel panel-dark panel-flat">
              <?php if($this->session->flashdata('flash_message'))
	 		{
			?>
					
                        <div class="alert alert-block alert-success fade in" style="background-color:#ff708a">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p ><h4 > Error!</h4> <?php echo $this->session->flashdata('flash_message'); ?></p>
                        </div>						
									
			<?php }         $this->session->sess_destroy();
?>
            <div class="panel-heading text-center" style="height: 50px;">
                 
               <a href="#">
                  <!--<img class="block-center img-rounded" src="<?php echo base_url();?>assets/img/logo.png" alt="Image">-->
               </a>
            </div>
            <div class="panel-body">
               <p class="text-center pv">SIGN IN TO CONTINUE.</p>
               <form  action="<?php echo base_url();?>home/checkLogin" class="mb-lg" role="form" data-parsley-validate="" novalidate=""  method="post">
                  <div class="form-group has-feedback">
                     <input class="form-control" id="exampleInputEmail1" type="text" name="email"  placeholder="Enter email" autocomplete="off" required>
                            
                     <span class="fa fa-envelope form-control-feedback text-muted"></span>
                  </div>
                  <div class="form-group has-feedback">
                     <input class="form-control" id="exampleInputPassword1" type="password" name="password" placeholder="Password" required>
                     <span class="fa fa-lock form-control-feedback text-muted"></span>
                  </div>
                  <div class="clearfix">
                     <div class="checkbox c-checkbox pull-left mt0">
                        <label>
                           <input type="checkbox" value="" name="remember">
                           <span class="fa fa-check"></span>Remember Me</label>
                     </div>
                     <!--<div class="pull-right"><a class="text-muted" href="recover.html">Forgot your password?</a>-->
                     </div>
                  </div>
                  <button class="btn btn-block btn-primary mt-lg" type="submit">Login</button>
               </form>
               <!--<p class="pt-lg text-center">Need to Signup?</p><a class="btn btn-block btn-default" href="register.html">Register Now</a>-->
            </div>
         </div>
         <!-- END panel-->
         <div class="p-lg text-center">
          
            <br>
            <!--<span>Bootstrap Admin Template</span>-->
         </div>
      </div>
   </div>
   <!-- =============== VENDOR SCRIPTS ===============-->
   <!-- MODERNIZR-->
   <script src="<?php echo base_url();?>assets/vendor/modernizr/modernizr.custom.js"></script>
   <!-- JQUERY-->
   <script src="<?php echo base_url();?>assets/vendor/jquery/dist/jquery.js"></script>
   <!-- BOOTSTRAP-->
   <script src="<?php echo base_url();?>assets/vendor/bootstrap/dist/js/bootstrap.js"></script>
   <!-- STORAGE API-->
   <script src="<?php echo base_url();?>assets/vendor/jQuery-Storage-API/jquery.storageapi.js"></script>
   <!-- PARSLEY-->
   <script src="<?php echo base_url();?>assets/vendor/parsleyjs/dist/parsley.min.js"></script>
   <!-- =============== APP SCRIPTS ===============-->
   <script src="<?php echo base_url();?>assets/js/app.js"></script>
</body>

</html>