<?php
//
//<!--Company Name :- Lazlo Software Solution
//    Creation Date :-6/23/2018
//    Controller Name :- client
//    Description :- This is the client controller  where superadmin update his profile 
//managing the settings ,rental company.,manages the plan and packages
//    Database Name:- carrental
//    Table Used :- 'tblsupadmprofile','tblsitesetting','tblplan','tblclientcompany'. -->
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Client  extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->driver("session");
        $this->load->database();
        
        echo "khrgfhrgh";
        die;
//     ..   $this->load->model('client/client_model');
//        if ($this->session->userdata['id'] == "") {
//            redirect('Home', 'refresh');
//        }
       
    }

    
    //Function for open the view superadmin dashboard
    public function index() 
    {
       $this->dashboard();
    }
    
    //Function for opening the view page of dashboard
    public function dashboard()
    {
        
        echo "ref";
        die;
        $this->load->view('client/header');
        $this->load->view('client/dashboard');
        $this->load->view('client/footer');
   }
   
   //function for opening the view page of the profile of superadmin 
    public function profileView($param="")
    {
    
        $value['data'] = $param;
        $this->load->view('client/header');
        $this->load->view('client/clientprofile',$value);
        $this->load->view('client/footer');
    }
    //Function for updation the profile of superadmin  in the table ' tblsupadmprofile'
    
    public function profileUpdate()
    {
      
       $parameter = $this->input->post('parameter');
       $result = $this->client_model->profileUpdate();
       
       
        $this->session->set_flashdata('permission_message', 'Profile Updated  Successfully');
        redirect('client/client/profileView/'.$parameter,'refresh');
    }
    //Function for check the old password of super admin during updating the change password
    public function checkPassword()
    {
        $parameter = $this->input->post('parameter');
        $result = $this->SuperAdmin_model->checkPassword();
       if($result == "match")
            {
                 $this->session->set_flashdata('permission_message', 'Password Updated  Successfully');
                 redirect('superadmin/SuperAdmin/profileView/'.$parameter,'refresh');
            }
        if($result == "notmatch")
            {
                 $this->session->set_flashdata('flash_message', 'Old  Password is  Wrong');
                 redirect('superadmin/SuperAdmin/profileView/'.$parameter,'refresh');
            }
     
    }
    //Function for open the view page of form setting where admin can perform settings  
    public function formsettings($parameter="")
    {
      
        
        $value['data'] = $parameter;
        $this->load->view('superadmin/header');
        $this->load->view('superadmin/formsetting',$value);
        $this->load->view('superadmin/footer');
    }
    //Function for saving the site setting table tblsitesetting
    public function updateSitesSettings()
    {
         $parameter = $this->input->post('parameter');
        $result = $this->SuperAdmin_model->updateSitesSettings(); 
         if($result == "true")
            {
                 $this->session->set_flashdata('permission_message', ' Updated  Successfully');
                 redirect('superadmin/SuperAdmin/formsettings/'.$parameter,'refresh');
            }
        else
            {
                 $this->session->set_flashdata('flash_message', 'Error');
                 redirect('superadmin/SuperAdmin/formsettings','refresh');
            }
        
    }
    // Function for updating th eremaining settings
    public function updateSettings()
    {
        $parameter = $this->input->post('parameter');
      
       
       $result = $this->SuperAdmin_model->updateSettings(); 
          if($result == "true")
            {
                 $this->session->set_flashdata('permission_message', ' Updated  Successfully');
                 redirect('superadmin/SuperAdmin/formsettings/'.$parameter,'refresh');
            }
        else
            {
                 $this->session->set_flashdata('flash_message', 'Error');
                 redirect('superadmin/SuperAdmin/formsettings','refresh');
            }
    }
    //Function for updating the emailsettings in table 'tblemailsett
    public function updateEmailSettings()
    {
                $parameter = $this->input->post('parameter');

               $result = $this->SuperAdmin_model->updateEmailSettings(); 
                  if($result == "true")
            {
                 $this->session->set_flashdata('permission_message', ' Updated  Successfully');
                 redirect('superadmin/SuperAdmin/formsettings/'.$parameter,'refresh');
            }
        else
            {
                 $this->session->set_flashdata('flash_message', 'Error');
                 redirect('superadmin/SuperAdmin/formsettings','refresh');
            }
    }
    //function for open the form of email template
    public function templateview()
    {
        $this->load->view('superadmin/header');
        $this->load->view('template/templateView',$value);
        $this->load->view('superadmin/footer'); 
    }
    //function for opening the form of email template
    public function updateTemplate($param="")
    {
        $value['param'] = $param;
        $this->load->view('superadmin/header');
        $this->load->view('template/emailTemplate',$value);
        $this->load->view('superadmin/footer'); 
    }
    // function for updating the template in table ' tbltemplate'
    public function templateUpdate()
    {
        
        $result = $this->SuperAdmin_model->templateUpdate(); 
        
         if($result == "true")
            {
             
                 $this->session->set_flashdata('permission_message', ' Updated  Successfully');
                 redirect('superadmin/SuperAdmin/templateview','refresh');
            }
        else
            {
                 $this->session->set_flashdata('flash_message', 'Error');
                 redirect('superadmin/SuperAdmin/templateview','refresh');
            }

    }
    //function to open the form of plan and package
    public function  formPlan($param="")
    {
        $value['id'] = $param;
        $this->load->view('superadmin/header');
        $this->load->view('superadmin/planPackage',$value);
        $this->load->view('superadmin/footer'); 
        
    }
    //pubclic function save plan for saving th eplan features  and plan in table 'tblplan'
    public function savePlan()
    {
       
        $result = $this->SuperAdmin_model->savePlan(); 
          if($result == "true")
            {
                 $this->session->set_flashdata('permission_message', 'Data Saved  Successfully');
                 redirect('superadmin/SuperAdmin/viewPlan/'.$parameter,'refresh');
            }
             if($result == "exist")
            {
                 $this->session->set_flashdata('flash_message', 'Demo plan already  Exist');
                 redirect('superadmin/SuperAdmin/formPlan/'.$parameter,'refresh');
            }
        else
            {
                 $this->session->set_flashdata('flash_message', 'Error');
                 redirect('superadmin/SuperAdmin/viewPlan','refresh');
            }
  
    }
    //function for updating the plan and package in table tblplan
     public function updatePlan()
     {
             $result = $this->SuperAdmin_model->updatePlan(); 
               if($result == "true")
                {
                     $this->session->set_flashdata('permission_message', ' Updated  Successfully');
                     redirect('superadmin/SuperAdmin/viewPlan/'.$parameter,'refresh');
                }
            else
                {
                     $this->session->set_flashdata('flash_message', 'Error');
                     redirect('superadmin/SuperAdmin/viewPlan','refresh');
                }
 
     }
    
     
    //function for view the plan and package details from table ' tblplan'
    public function  viewPlan()
    {
        $this->load->view('superadmin/header');
        $this->load->view('superadmin/planView',$value);
        $this->load->view('superadmin/footer');  
    }
    //function for deleting the plan in table 'tblplan'
     public function  deletePlan($param="")
    {
       $result = $this->SuperAdmin_model->deletePlan($param); 
      if($result == "true")
                {
                     $this->session->set_flashdata('permission_message', ' Deleted Successfully');
                     redirect('superadmin/SuperAdmin/viewPlan/'.$parameter,'refresh');
                }
            else
                {
                     $this->session->set_flashdata('flash_message', 'Error');
                     redirect('superadmin/SuperAdmin/viewPlan','refresh');
                }
    }
    //Function for opening the view page of creating rental company by superadmin 
    public function rentalcompany($param="",$param1="")
    {     
        
        
        
        $value['parameter'] = $param1;
          $value['id'] = $param;
        $this->load->view('superadmin/header');
        $this->load->view('superadmin/createCompany',$value);
        $this->load->view('superadmin/footer');  
    }
    //Public function saveCompany for saving the rental company in table  'tblclientcompany'
    public function saveCompany($param="",$param1="",$param3="")
    {
       
     $parameter = $this->input->post('parameter');
     if($param3 == "dissapproved")
     {
         $parameter = "dissapproved";
     }
     if($param3 == "approved")
     {
         $parameter = "approved";
     }
  
        
        
       $result = $this->SuperAdmin_model->saveCompany($param,$param1); 
    // echo $result;
    // echo $parameter;
    // die;
          if($result == "true")
            {
                 $this->session->set_flashdata('permission_message', 'Data Saved  Successfully');
                 redirect('superadmin/SuperAdmin/viewCompany/'.$parameter,'refresh');
            }
            if($result == "updated")
            {
                 $this->session->set_flashdata('permission_message', 'Updated  Successfully');
                 redirect('superadmin/SuperAdmin/viewCompany/'.$parameter,'refresh');
            }
            if($result == "delete")
            {
              
                 $this->session->set_flashdata('permission_message', 'Deleted Successfully');
                 redirect('superadmin/SuperAdmin/viewCompany/'.$parameter,'refresh');
            }
            
        else
            {
                 $this->session->set_flashdata('flash_message', 'Error');
                 redirect('superadmin/SuperAdmin/viewPlan','refresh');
            }
        
        
    }
    //Public function to view the list of rental company
    public function viewCompany($param="")
    {
     //   echo $param;
        
        
        $value['id'] = $param;
        $this->load->view('superadmin/header');
        $this->load->view('superadmin/newCompanylist',$value);
        $this->load->view('superadmin/footer');    
    }
    //Function for changing the status of approved or dispproved of rental company in table ' tblclientcompany'
    public function statusCompany($param="")
    {
        $result = $this->SuperAdmin_model->statusCompany($param,$param1); 
        if($result == "updated")
            {
                 $this->session->set_flashdata('permission_message', 'Approved Successfully');
                 redirect('superadmin/SuperAdmin/viewCompany/'.$parameter,'refresh');
            }
        
    }
    //function fpor diapproving the company 
    
    public function disapproveCompany($param="")
    {
       $result = $this->SuperAdmin_model->disapproveCompany($param,$param1); 
        if($result == "updated")
            {
                 $this->session->set_flashdata('permission_message', 'Disapproved Successfully');
                 redirect('superadmin/SuperAdmin/viewCompany/'.$parameter,'refresh');
            } 
    }
    
    
    
    
    //public function for printing the csv
    public function printCsv($param="")
    {
         $this->SuperAdmin_model->CSVprint($param); 

    }
    
    //public function exportpdf forn car rental company
    public function printPdf($param="")
    {
         $this->SuperAdmin_model->printPdf($param); 
    }
    //Function for creating the users of super admin  in th etable  ' tblsupadmuser'
    public function  createUser($param="")
    {
      $value['id'] = $param;
      
      
        $this->load->view('superadmin/header');
        $this->load->view('superadmin/createUser',$value);
        $this->load->view('superadmin/footer');     
    }
    //Function for saving the users in table ' tblsupadmuser'
    
      public function  saveUser($param="",$param1="")
    {
           $result = $this->SuperAdmin_model->saveUser($param,$param1); 
            if($result == "insert")
                {
                     $this->session->set_flashdata('permission_message', 'Data Saved  Successfully');
                     redirect('superadmin/SuperAdmin/viewUser/'.$parameter,'refresh');
                }
            if($result == "update")
                {
                     $this->session->set_flashdata('permission_message', 'Updated  Successfully');
                     redirect('superadmin/SuperAdmin/viewUser/'.$parameter,'refresh');
                }
                 if($result == "delete")
                {
                     $this->session->set_flashdata('permission_message', 'Deleted Successfully');
                     redirect('superadmin/SuperAdmin/viewUser/'.$parameter,'refresh');
                }
                 if($result == "exist")
                {
                     $this->session->set_flashdata('flash_message', 'Email already exist  ');
                     redirect('superadmin/SuperAdmin/createUser/'.$parameter,'refresh');
                }
    }
    ///function to see the list of all user created by superadmin
    public function viewUser($param="")
    {
      
        $this->load->view('superadmin/header');
        $this->load->view('superadmin/userview',$value);
        $this->load->view('superadmin/footer');     
    }
    //public function role master for making th emaster of roles in table   'tblmasterrole'
     public function roleMaster($param="")
    {
         $value['id'] = $param;
      
        $this->load->view('superadmin/header');
        $this->load->view('superadmin/superadminrole',$value);
        $this->load->view('superadmin/footer');     
    }
    //function for saving the role in table ' tblmasterrole'
     public function saveRole($param="",$param1="")
    {
       
         $result = $this->SuperAdmin_model->saveRole($param,$param1); 
           if($result == "insert")
                {
                     $this->session->set_flashdata('permission_message', 'Data Saved  Successfully');
                     redirect('superadmin/SuperAdmin/viewRole/'.$parameter,'refresh');
                }
            if($result == "update")
                {
                     $this->session->set_flashdata('permission_message', 'Updated  Successfully');
                     redirect('superadmin/SuperAdmin/viewRole/'.$parameter,'refresh');
                }
                 if($result == "delete")
                {
                     $this->session->set_flashdata('permission_message', 'Deleted  Successfully');
                     redirect('superadmin/SuperAdmin/viewRole/'.$parameter,'refresh');
                }
                 if($result == "exist")
                {
                     $this->session->set_flashdata('flash_message', 'Role already exist  ');
                     redirect('superadmin/SuperAdmin/roleMaster/'.$parameter,'refresh');
                }
     }
      //function for saving the role in table ' tblmasterrole'
     public function viewRole($param="")
    {
         $this->load->view('superadmin/header');
        $this->load->view('superadmin/viewRole',$value);
        $this->load->view('superadmin/footer'); 
    }
    //function for open the view page of assigning the permission ro a particular role  in table
     public function rolePermission($param="")
    {
         $this->load->view('superadmin/header');
        $this->load->view('superadmin/permission',$value);
        $this->load->view('superadmin/footer'); 
    }
    //function for saving the permission to a particular role in table 'tblsupadmpermission'
    public function savePermission()
    {
        $result = $this->SuperAdmin_model->savePermission($param,$param1);
        if($result == "insert")
            {
                 $this->session->set_flashdata('permission_message', 'Data Saved  Successfully');
                 redirect('superadmin/SuperAdmin/rolePermission/'.$parameter,'refresh');
            }
          if($result == "update")
            {
                 $this->session->set_flashdata('permission_message', 'Updated  Successfully');
                 redirect('superadmin/SuperAdmin/rolePermission/'.$parameter,'refresh');
            }

    }
    //function for getting permission by ajax in file permission view.php from table 'tblsupadmpermission'
    public function ajaxGetPermission()
    { 
       // print_r($_POST);die;
         $result = $this->SuperAdmin_model->ajaxGetPermission($param,$param1); 
        // print_r($result);
        
    }
    
    //function for active the status of  role on or off
    public function changestatusRole($param="")
    {
         $result = $this->SuperAdmin_model->changestatusRole($param); 
          if($result == "active")
            {
                 $this->session->set_flashdata('permission_message', 'Status active  Successfully');
                 redirect('superadmin/SuperAdmin/viewRole/'.$parameter,'refresh');
            }
          if($result == "inactive")
            {
                 $this->session->set_flashdata('permission_message', 'Status Inactive Successfully');
                 redirect('superadmin/SuperAdmin/viewRole/'.$parameter,'refresh');
            }
         

    }
    
     //function for active the status of  role on or off
    public function changestatusUser($param="")
    {
        
        
         $result = $this->SuperAdmin_model->changestatusUser($param); 
         
         
          if($result == "active")
            {
                 $this->session->set_flashdata('permission_message', 'Status active  Successfully');
                 redirect('superadmin/SuperAdmin/viewUser/'.$parameter,'refresh');
            }
          if($result == "inactive")
            {
                 $this->session->set_flashdata('flash_message', 'Status Inactive Successfully');
                 redirect('superadmin/SuperAdmin/viewUser/'.$parameter,'refresh');
            }
         

    }
    
     //function for active the status of  plan on or off
    public function changestatusPlan($param="")
    {
        
        
         $result = $this->SuperAdmin_model->changestatusPlan($param); 
         
         
          if($result == "active")
            {
                 $this->session->set_flashdata('permission_message', 'Status active  Successfully');
                 redirect('superadmin/SuperAdmin/viewPlan/'.$parameter,'refresh');
            }
          if($result == "inactive")
            {
                 $this->session->set_flashdata('flash_message', 'Status Inactive Successfully');
                 redirect('superadmin/SuperAdmin/viewPlan/'.$parameter,'refresh');
            }
         

    }
    
    
    
    
    
    
    
    

   }

