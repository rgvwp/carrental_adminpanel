
<?php



if($id)
{
    
   $query = $this->db->get_where('tblmasterrole', array('masterroleid' => $id))->row_array();
  
  $formaction = "saveRole";
  $method="edit";
  $button_name = "Update";
}else
{
  
   $formaction = "savePermission";
   $button_name = "Save";
   $method="create";
}

?>
<style>
    input[type=checkbox] {
    transform: scale(2);
    -ms-transform: scale(2);
    -webkit-transform: scale(2);
    /*padding: 50px;*/
      height: 10px;   
    width: 10px;
    /*display: block;*/
}
</style>

      <section>
          
      <?php $compid = $this->session->userdata['companyid'];?>    
         <!-- Page content-->
         <div class="content-wrapper">
            <h3>Permissions
               <!--<small>Validating forms frontend have never been so powerful and easy.</small>-->
            </h3>
            <!-- START row-->
            
            <!-- END row-->
            <!-- START row-->
            <div class="row">
               <div class="col-md-12">
                   <?php if($this->session->flashdata('permission_message'))
	 		{
                       
			?>
					
                        <div class="alert alert-block alert-success fade in" style="background-color:#3ec0e8">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p ><h4 style="background-color:#3ec0e8"> Successful!</h4> <?php echo $this->session->flashdata('permission_message'); ?></p>
                        </div>						
									
			<?php } ?>
            <?php if($this->session->flashdata('flash_message'))
	 		{
			?>
					
                        <div class="alert alert-block alert-success fade in" style="background-color:#ff708a">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p ><h4 style="background-color:#ff708a"> Error!</h4> <?php echo $this->session->flashdata('flash_message'); ?></p>
                        </div>						
									
			<?php } ?>
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <div class="panel-title">Permissions</div>
                        </div>
                        <div class="panel-body">
<!--                           <h4>Type validation</h4>-->
                                      <form class="form-horizontal"   id="searchForm"       action="<?php echo base_url();?>client/Client/<?php echo $formaction;?>/<?php echo $method;?>/<?php echo $id;?>" method="post" enctype='multipart/form-data'>

     

                                          
                           
                               
                           
                           <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Role Name <span style="color:red">*</span></label>
                                 <div class="col-sm-6">
                            <?php    $rolequery = $this->db->get_where('tblclientrole', array('clientrolestatus' => 1,'clientcompid'=>$compid)); ?>
                                      
                                     
                                     
                                     <?php 
                                     
                                    // echo $this->db->last_query();
                                     $st = $query['clientrolestatus'] ; ?>
                                    <select name="role"  class="form-control" data-validation="length" data-validation-length="min1" data-validation-error-msg="Role is required" onchange="setPermission12(this.value);">
                                    <option value="">Select Role</option>
                                    <?php foreach($rolequery->result() as $k1=>$v2){
                                        
                                       
                                        ?>
                                    <option value="<?php echo $v2->clientroleid;?>"  <?php if ($st == 1 ) echo 'selected' ; ?>><?php echo $v2->clientrolename;?></option>
                                    <?php } ?>
                                  </select>
                                 </div>
                                 
                              </div>
                           </fieldset>
                           
                                          
                               <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Permission <span style="color:red">*</span></label>
                                 <div class="col-sm-6">
                            <input type="checkbox" id="ascardetail" value="cardetail"  name="permission[]"/><label for="special-offers" style="margin-left:20px;">Manage Car Details</label><br>
                            <input type="checkbox" id="ascommission" value="commission"   name="permission[]"/><label for="special-offers" style="margin-left:20px;">Manage Commission</label><br>
                            <input type="checkbox" id="assales" value="sales"  name="permission[]"/><label for="special-offers" style="margin-left:20px;">Manage Sales Team</label><br>
                            <input type="checkbox" id="asleads" value="leads"  name="permission[]"/><label for="special-offers" style="margin-left:20px;">Leads Management</label><br>
                            <input type="checkbox" id="asoppurtanities"  value="oppurtanities" name="permission[]"/><label for="special-offers" style="margin-left:20px;">Opportunities</label><br>
                            <input type="checkbox" id="aslogged"  value="logged" name="permission[]"/><label for="special-offers" style="margin-left:20px;">Logged Calls</label><br>
                            <input type="checkbox" id="asappointment"  value="appointment" name="permission[]"/><label for="special-offers" style="margin-left:20px;">Meeting And Appointment</label><br>
                            <input type="checkbox" id="asinvoice"  value="invoice" name="permission[]"/><label for="special-offers" style="margin-left:20px;">Manage Invoice</label><br>
                            <input type="checkbox" id="ascontract"  value="contract" name="permission[]"/><label for="special-offers" style="margin-left:20px;">Manage Contract</label><br>

                                 </div>
                                 
                              </div>
                                   <span id="spa1" style="color: red; margin-left:130px;"></span>
                           </fieldset>           
                                          
                                          
                                          
                                          
                                          
                                          
                                          
                                          
                                          
                                          
                           
                           
                           
                            <div class="panel-footer text-center">
                           <button class="btn btn-info" type="submit"   onclick="return check_test('searchForm');"                         style="margin-left: -193px;"><?php echo $button_name;?></button>
                        </div>
                          
                                      </form>
                         
                        </div>
                     
                     </div>
                     <!-- END panel-->
                  </form>
               </div>
            </div>
            <!-- END row-->
         </div>
      </section>
    <script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js" type="text/javascript"></script>
<!--        <script src="<?php echo base_url();?>assets/datepicker/datepicker1/bootstrap-datetimepicker.css" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/datepicker/datepicker1/bootstrap-datetimepicker.min.css" type="text/javascript"></script>

     <script src="<?php echo base_url();?>assets/datepicker/datepicker1/js/bootstrap-datetimepicker.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/datepicker/datepicker1/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>-->

    
    
    
    
    
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"> </script>
                


<script>
    
    
    
    
    
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah')
                    .attr('src', e.target.result)
                    .width(80)
                    .height(80);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    
    </script>
    
     <script>
                    
                      function isNumber(evt) {
            var iKeyCode = (evt.which) ? evt.which : evt.keyCode
            if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
                return false;

            return true;
        
                    }
                    
                    
                </script>
                
                <script>
                    function passSubmit()
                    {
                    alert();
         $('#passForm').submit();
                  
                        
                    }
                    </script>
                    
                    
                    <script>
    
    function setPermission12(val)
    {
//         //e.preventDefault();
//              $("#spa1").html(" ");
//
//         
//         
          $("#ascardetail").prop("checked", false); 
          $("#ascommission").prop("checked", false); 
         
          $("#assales").prop("checked", false); 
         
          $("#asleads").prop("checked", false); 
         
          $("#asoppurtanities").prop("checked", false); 
            $("#aslogged").prop("checked", false);
            
             $("#asleads").prop("checked", false); 
         
          $("#ascontract").prop("checked", false); 
            $("#asinvoice").prop("checked", false);
            
            $("#asappointment").prop("checked", false);
            


          $.ajax({
          url: '<?php echo site_url(); ?>client/Client/ajaxGetPermission',
          type : "POST",
         
          data : {"roleid" :val},
          success : function(data) {
            // alert(data);
             
             console.log(data);
             var abc = jQuery.parseJSON(data);
           
           var f = abc.length;
      //     alert(f);
              var i =0;
          
       
            for(i=0;i<f;i++)
            { 
            
            $("#as"+abc[i]).prop('checked',true);
                
            }
             
             
//             
          }
//          error : function(data) {
//              // do something
          
      });
    }
    
    
    </script>
    
    
    <script>
        function check_test(e)
        {
           var formname = e;

                   var anyBoxesChecked = false;
                    $('#' + formname + ' input[type="checkbox"]').each(function() {
                        if ($(this).is(":checked")) {
                            anyBoxesChecked = true;
                        }
                    });
 
                    if (anyBoxesChecked == false) {
                     //   alert("select checkboxes");
                        $("#spa1").html("Please select permission");
                                       return false;
                    } 

            var t =$('#selected1').val();
            if(t == 0)
                        {
                                    $("#spa").html("Role is required");
                                   return false;
                       }

        }
        
        </script>