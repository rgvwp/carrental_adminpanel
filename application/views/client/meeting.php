

<link href="<?php echo base_url();?>multiple-select-master/multiple-select.css" rel="stylesheet"/>


 <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/chosen_v1.2.0/chosen.min.css">
   <!-- DATETIMEPICKER-->
   <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">
   
<?php

$comp = $this->session->userdata['companyid'];

if($id)
{
    
   $query = $this->db->get_where('tblmeeting', array('meetingid' => $id))->row_array();
  
  $formaction = "saveMeeting";
  $method="edit";
  $button_name = "Update";
  $title = "Edit Meeting";
}else
{
  
   $formaction = "saveMeeting";
   $button_name = "Save";
   $method="create";
    $title = "Create Meeting";
}

?>

      <section>
         <!-- Page content-->
         <div class="content-wrapper">
            <h3><?php echo $title;?>
               <!--<small>Validating forms frontend have never been so powerful and easy.</small>-->
            </h3>
            <!-- START row-->
            
            <!-- END row-->
            <!-- START row-->
            <div class="row">
               <div class="col-md-12">
                   <?php if($this->session->flashdata('permission_message'))
	 		{
                       
			?>
					
                        <div class="alert alert-block alert-success fade in" style="background-color:#3ec0e8">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p ><h4 style="background-color:#3ec0e8"> Successful!</h4> <?php echo $this->session->flashdata('permission_message'); ?></p>
                        </div>						
									
			<?php } ?>
            <?php if($this->session->flashdata('flash_message'))
	 		{
			?>
					
                        <div class="alert alert-block alert-success fade in" style="background-color:#ff708a">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p ><h4 style="background-color:#ff708a"> Error!</h4> <?php echo $this->session->flashdata('flash_message'); ?></p>
                        </div>						
									
			<?php } ?>
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <div class="panel-title"><?php echo $title;?></div>
                        </div>
                        <div class="panel-body">
<!--                           <h4>Type validation</h4>-->
                                      <form class="form-horizontal" action="<?php echo base_url();?>client/Client/<?php echo $formaction;?>/<?php echo $method;?>/<?php echo $id;?>" method="post" enctype='multipart/form-data'>

        
<fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Meeting Subject <span style="color:red">*</span></label>
                                 <div class="col-sm-6">
                                     <input style="width: 100%" class="form-control" placeholder="Enter Meeting Subject" type="text" value="<?php echo $query['meetingsubject'];?>" name="subject" data-validation="length" data-validation-length="min1" data-validation-error-msg="Meeting Subject is required"  >
                                 </div>
                                 <!--<input type="text" name="parameter" value="<?php echo $parameter;?>">-->
                               
                              </div>
                           </fieldset>
                                          <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Location <span style="color:red">*</span></label>
                                 <div class="col-sm-6">
                                     <input style="width: 100%" class="form-control" placeholder="Enter Location" type="text" value="<?php echo $query['meetinglocation'];?>" name="location" data-validation="length" data-validation-length="min1" data-validation-error-msg="Location is required"  >
                                 </div>
                                 <!--<input type="text" name="parameter" value="<?php echo $parameter;?>">-->
                               
                              </div>
                           </fieldset>
                                          
                                          <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Attendies</label>
                                 <div class="col-sm-6">

     <select multiple="multiple"   name="attendies[]" style="width: 90%" id="ert">

                                <?php 
                                       
                                     $this->db->where('clientuserstatus',1);
                                    $this->db->where('clientcompid', $comp);
                                 
                                    $member = $this->db->get('tblclientuser');

                                
                                
                             if($method == "create")
                             {
                                foreach($member->result() as $k1=>$v1){?>

        <option value="<?php echo $v1->clientuserid;?>"  ><?php echo $v1->clientusername;?></option>
      
   
                                <?php } 
        
        } ?>
        
        
        
        
        <?php    $mem = explode(",",$query['meetingattendies']) ;
             
        //print_r($mem);
                             if($method == "edit")
                             {
                                foreach($member->result() as $k1=>$v1)
                                    
                                    {
                                    
                                           $m =  $v1->clientuserid;
                                    
                                         if (in_array($m, $mem))
                                                                { ?>
                                                                
                   <option value="<?php echo $v1->clientuserid;?>" <?php echo "selected";?> ><?php echo $v1->clientusername;?></option>

                                                

     <?php           }
                                                              else
                                                                { ?>
                                                               
                                                                  
           <option value="<?php echo $v1->clientuserid;?>"  ><?php echo $v1->clientusername;?></option>

                                                         <?php       }

                                           
                                           
                                           
                                   

                                    
                                     
                                       
                                        
                                    }
                                    
                                    
                                    ?>

                    
                                <?php } 
        
        ?>
        
        
        
        
        
        
        
        
                                                                       </select>






                                 </div>
                               
                              </div>
                           </fieldset>
                                          
                                          <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Starting at <span style="color:red">*</span></label>
                                 <div class="col-sm-6">
                                     <input style="width: 100%" class="form-control" placeholder="Select Starting at" type="text" id="datetimepicker" value="<?php echo $query['meetingstartat'];?>" name="startat"  data-validation="length" data-validation-length="min1" data-validation-error-msg=" Start at required" >
                                 </div>
                                
                              </div>
                           </fieldset>
                                                                  
                                          
                                          
                                          
                                          <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Ending at <span style="color:red">*</span></label>
                                 <div class="col-sm-6">
                                     <input style="width: 100%" class="form-control" placeholder="Select Ending at" type="text" id="datetimepicker1" value="<?php echo $query['meetingendat'];?>" name="endat" data-validation="length" data-validation-length="min1" data-validation-error-msg=" Ending at required">
                                 </div>
                               
                              </div>
                           </fieldset>
                                          <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Description</label>
                                 <div class="col-sm-6">
                                     <textarea style="width: 100%" class="form-control" rows="4" cols="80" name="description" placeholder="Enter Description"><?php echo $query['meetingdescription'];?></textarea>

                                    <!--<input  style="background-color: #ECEDEE;" class="form-control" type="password" name="pass_confirmation" value="<?php echo $query['clientuserpass'];?>"  data-validation="strength"  data-validation-strength="2" >-->
                                 </div>
                                
                              </div>
                           </fieldset>
                           
                                        
                                          

                      
                                                   
                                          
                           

                           
                           
                            <div class="panel-footer text-center">
                           <button class="btn btn-info" type="submit" style="margin-left: -193px;"><?php echo $button_name;?></button>
                        </div>
                          
                                      </form>
                         
                        </div>
                     
                     </div>
                     <!-- END panel-->
                  </form>
               </div>
            </div>
            <!-- END row-->
         </div>
      </section>



<!--<script src="<?php echo base_url();?>assets/vendor/moment/min/moment-with-locales.min.js"></script>-->
   <!--<script type="text/javascript" src=".<?php echo base_url();?>assets/vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>-->
   
    <script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js" type="text/javascript"></script>
       
<!--     <script src="<?php echo base_url();?>assets/datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
 

    <script>
        
        $('.datepicker').datepicker();
        </script>
    -->
    
    
    
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"> </script>
                


<script>
    
    
    
    
    
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah')
                    .attr('src', e.target.result)
                    .width(80)
                    .height(80);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    
    </script>
    
     <script>
                    
                      function isNumber1(evt) {
            var iKeyCode = (evt.which) ? evt.which : evt.keyCode
            if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
                return false;

            return true;
        
                    }
                    
                    
                </script>
                
                <script>
                    function passSubmit()
                    {
                    alert();
         $('#passForm').submit();
                  
                        
                    }
                    </script>
                    
                    <script src="<?php echo base_url();?>multiple-select-master/multiple-select.js"> </script>

    
    <script>
        $('#ert').multipleSelect({
          //  isOpen: true,
                //filter: true
                 selectAll: false
        });
    </script>