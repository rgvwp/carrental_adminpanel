<link href="<?php echo base_url();?>multiple-select-master/multiple-select.css" rel="stylesheet"/>


<?php $comp  = $this->session->userdata['companyid'];




?>

<style>
   
    .btn-group, .multiselect{width:100%}
    .multiselect-container
    {
     max-height: 300px;
     overflow-y: auto;
     overflow-x: hidden;
    }
</style>


<?php
if($id)
{
    
   $query = $this->db->get_where('tblopportunity', array('opportunityid' => $id))->row_array();
  
  $formaction = "saveOpportunity";
  $method="edit";
  $button_name = "Update";
}else
{
  
   $formaction = "saveOpportunity";
   $button_name = "Save";
   $method="create";
}

?>

      <section>
         <!-- Page content-->
         <div class="content-wrapper">
            <h3>Create Opportunity
               <!--<small>Validating forms frontend have never been so powerful and easy.</small>-->
            </h3>
            <!-- START row-->
            
            <!-- END row-->
            <!-- START row-->
            <div class="row">
               <div class="col-md-12">
                   <?php if($this->session->flashdata('permission_message'))
	 		{
                       
			?>
					
                        <div class="alert alert-block alert-success fade in" style="background-color:#3ec0e8">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p ><h4 style="background-color:#3ec0e8"> Successful!</h4> <?php echo $this->session->flashdata('permission_message'); ?></p>
                        </div>						
									
			<?php } ?>
            <?php if($this->session->flashdata('flash_message'))
	 		{
			?>
					
                        <div class="alert alert-block alert-success fade in" style="background-color:#ff708a">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p ><h4 style="background-color:#ff708a"> Error!</h4> <?php echo $this->session->flashdata('flash_message'); ?></p>
                        </div>						
									
			<?php } ?>
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <div class="panel-title">Create Opportunity</div>
                        </div>
                        <div class="panel-body">
<!--                           <h4>Type validation</h4>-->
                                      <form class="form-horizontal" action="<?php echo base_url();?>client/Client/<?php echo $formaction;?>/<?php echo $method;?>/<?php echo $id;?>" method="post" enctype='multipart/form-data'>

                                          <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Opportunity</label>
                                 <div class="col-sm-6">
                                     <input class="form-control" type="text" value="<?php echo $query['opportunity'];?>" name="opportunity" data-validation="length" data-validation-length="min1" data-validation-error-msg="Opportunityis requirred"  >
                                 </div>
                                 <input type="hidden" name="parameter" value="<?php echo $parameter;?>">
                               
                              </div>
                           </fieldset>

<fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Stages</label>
                                 <div class="col-sm-6">
                               
                                 
                                      <select name="stage" class="form-control"   style="width:500px"  data-validation="length" data-validation-length="min1" data-validation-error-msg="Stage is requirred" >
                                 
                                     <option value="">Select Stage </option>
                            <?php 
                                          $tg =  $query['opportunitystage']      ;
                            $this->db->where('clientcompid', $comp);
                                  $opporquery = $this->db->get('tblopportunitystage');
                                    $result = $opporquery->row_array();
                                     $stages = explode(",",$result['opportunitystages']);
                                     foreach($stages  as $k=>$v){
                                   ?>
                                  <option value="<?php echo $v; ?>"  <?php if ($v == $tg ) echo 'selected' ; ?>><?php echo $v;?></option>
                                   <?php } ?>
                                     </select>     
                                 
                                 
                                 
                                 
                                 
                              </div>
                           </fieldset>
<!--                                          <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Company Code</label>
                                 <div class="col-sm-6">
                                     <input class="form-control" type="text"  name="companycode" value="<?php echo $query['clientcompcode'];?>" data-validation="length" data-validation-length="min1" data-validation-error-msg="Company Code is requirred" > 
                                 </div>
                                
                              </div>
                           </fieldset>-->
<!--                                          <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label"> Address</label>
                                 <div class="col-sm-6">
                                     <input class="form-control" type="text"  name="address" value="<?php echo $query['clientcompaddress'];?>" data-validation="length" data-validation-length="min1" data-validation-error-msg="Company Address is requirred" >
                                 </div>
                               
                              </div>
                           </fieldset>-->
<?php if($method == "create"){?>
                                          
                                          <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Sales Person</label>
                                 <div class="col-sm-6">
                                     
                                <select multiple="multiple"   name="saleperson[]" style="width:500px" id="ert" >  
                                 <!--<option value="">Select Sales Person </option>-->

                                  <?php  
                                
                                  
                                  
                                    $this->db->where('clientcompid', $comp);
                                 
                                    $teamquery1 = $this->db->get('tblsaleteam');

                                   foreach($teamquery1->result() as $k=>$v1){
                                
                                       $x =$v1->salesteamusersid;
                                       $arr = $arr.','.$x;
                                    }   
                                      $teammembers = trim($arr,",");
                                      $y = explode(",",$teammembers);
                                      $teamarr = array_unique($y);
                                      
                                     foreach($teamarr as $row){
                                         
                                         $usename = $this->db->get_where('tblclientuser',array('clientuserid'=>$row));
                                         
                                       
                                         
                                          foreach($usename->result() as $h=>$r)  {
                                              
                                              $y12 = $r->clientuserid;?>
                                 
                                 <option value="<?php echo $r->clientuserid;?>"    <?php if ($ap == $y12 ) echo 'selected' ; ?>><?php echo $r->clientusername;?></option>

                                        <?php  }
                                     } 
                                      
                                  ?>   
                              
                                   
                                </select> 
                                 </div>
                                
                              </div>
                           </fieldset>


<?php } ?>

<?php if($method == "edit"){
    
    $personarr = explode(",",$query['opportunitysalesperson']);
    
    
    ?>

     <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Sales Person</label>
                                 <div class="col-sm-6">
                                     
                                <select multiple="multiple"   name="saleperson[]" style="width:500px" id="ert" >  
                                 <!--<option value="">Select Sales Person </option>-->

                                  <?php  
                                   
                                  
                                  
                                    $this->db->where('clientcompid', $comp);
                                 
                                    $teamquery1 = $this->db->get('tblsaleteam');

                                   foreach($teamquery1->result() as $k=>$v1){
                                
                                       $x =$v1->salesteamusersid;
                                       $arr = $arr.','.$x;
                                    }   
                                      $teammembers = trim($arr,",");
                                      $y = explode(",",$teammembers);
                                      $teamarr = array_unique($y);
                                      
                                     foreach($teamarr as $row){
                                         
                                         $usename = $this->db->get_where('tblclientuser',array('clientuserid'=>$row));
                                         
                                       
                                         
                                          foreach($usename->result() as $h=>$r) {
                                              
                                              $y12 = $r->clientuserid;
                                              
                                             if (in_array($y12, $personarr))
                                                        { ?>
                                 
         <option value="<?php echo $r->clientuserid;?>"    <?php  echo 'selected' ; ?>><?php echo $r->clientusername;?></option>

                                                     <?php   }
                                                      else
                                                        { ?>
         
         
                                          <option value="<?php echo $r->clientuserid;?>"  ><?php echo $r->clientusername;?></option>

                                                       <?php }

                                              ?>
                                 
                                 
                                 

                                        <?php  }
                                     } 
                                      
                                  ?>   
                              
                                   
                                </select> 
                                 </div>
                                
                              </div>
                           </fieldset>
    
<?php } ?>

















  <fieldset>   
 <div class="form-group">
                                 <label class="col-sm-2 control-label">Sales Team</label>
                                 <div class="col-sm-6">
                                     
                                     <?php  $ap =   $query['opportunityteam']    ;                         ?>
                                    <select name="teamid" class="form-control"   style="width:500px"  data-validation="length" data-validation-length="min1" data-validation-error-msg="Sales Team is requirred" >
                                 
                                     <option value="">Select Team </option>
                                    <?php  
                                   $this->db->where('clientcompid', $comp);
                                   $teamquery = $this->db->get('tblsaleteam');
                                   foreach($teamquery->result() as $k=>$v){
                                   $y12 = $v->salesteamid;      
                                   
                                   ?>
                                    <option value="<?php echo $v->salesteamid; ?>"  <?php if ($ap == $y12 ) echo 'selected' ; ?>><?php echo $v->saleteamname;?></option>
                                   <?php } ?>

                                  </select>   
                                   </div>
                               
                              </div>
                           </fieldset>  


                                          
                            <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Customer</label>
                                 <div class="col-sm-6">
                                     <input class="form-control " type="text"   value="<?php echo $query['opportunitycustomer'];?>" name="customer"  data-validation="length" data-validation-length="min1" data-validation-error-msg="Customer is requirred">
                                 </div>
                               
                              </div>
                           </fieldset>   
 <div class="form-group">
                                 <label class="col-sm-2 control-label">Customer Address</label>
                                 <div class="col-sm-6">
                                     <input class="form-control " type="text"   value="<?php echo $query['opportunitycustomeraddress'];?>" name="address" data-validation="length" data-validation-length="min1" data-validation-error-msg="Address is requirred">
                                 </div>
                               
                              </div>
                           </fieldset> 
                           
                            <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Email</label>
                                 <div class="col-sm-6">
                                     <input class="form-control " type="email"   value="<?php echo $query['opportunityemail'];?>" name="emailaddress" >
                                 </div>
                               
                              </div>
                           </fieldset>  
                                          
                                    </fieldset>
                                      <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Phone</label>
                                 <div class="col-sm-6">
                                     <input class="form-control " type="text" onkeypress="return isNumber1(event)"  value="<?php echo $query['opportunityphone'];?>" name="phone" >
                                 </div>
                               
                              </div>
                           </fieldset>  
                                    
                                    
                         <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Lead Tag</label>
                                 <div class="col-sm-6">
                                     <?php  $tg =  $query['opportunitytag'];?>
                                   <select name="leadtag" class="form-control"   style="width:500px"  data-validation="length" data-validation-length="min1" data-validation-error-msg="Lead tag is requirred" >
                                 
                                     <option value="">Select Tag </option>
                            <?php   $this->db->where('clientcompid', $comp);
                                  $leadquery = $this->db->get('tblleadtag');
                                    $result = $leadquery->row_array();
                                     $tags = explode(",",$result['leadtags']);
                                     foreach($tags  as $k=>$v){
                                   ?>
                                  <option value="<?php echo $v; ?>"  <?php if ($v == $tg ) echo 'selected' ; ?>><?php echo $v;?></option>
                                   <?php } ?>
                                     </select>     
                                 </div>
                              </div>
                           </fieldset>             
                                
                          <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Probability</label>
                                 <div class="col-sm-6">
                                     <input class="form-control " type="text"   value="<?php echo $query['opportunityprobability'];?>" name="probability" >
                                 </div>
                               
                              </div>
                           </fieldset>  
                                    
                               <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Next Action</label>
                                 <div class="col-sm-6">
                                     <input class="form-control " type="text"   value="<?php echo $query['opportunitynextaction'];?>" name="nextaction" >
                                 </div>
                               
                              </div>
                           </fieldset>        
                                    
                                    
                                    
                                    
                                    <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Next Action Date</label>
                                 <div class="col-sm-6">
                                     <input class="form-control " type="text"  id="datetimepicker" value="<?php echo $query['opportunitynextactiondate'];?>" name="nextactiondate" >
                                 </div>
                               
                              </div>
                           </fieldset>  
                           
                          
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                            </fieldset>   
 <div class="form-group">
                                 <label class="col-sm-2 control-label">Notes</label>
                                 <div class="col-sm-6">
<textarea name="notes" rows="4" class="form-control"><?php echo $query['opportunitynotes'];?></textarea>                                 </div>
                               
                              </div>
                           </fieldset>  
                           
                           
                            <div class="panel-footer text-center">
                           <button class="btn btn-info" type="submit" style="margin-left: -193px;"><?php echo $button_name;?></button>
                        </div>
                          
                                      </form>
                         
                        </div>
                     
                     </div>
                     <!-- END panel-->
                  </form>
               </div>
            </div>
            <!-- END row-->
         </div>
      </section>
    <script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js" type="text/javascript"></script>
<!--        <script src="<?php echo base_url();?>assets/datepicker/datepicker1/bootstrap-datetimepicker.css" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/datepicker/datepicker1/bootstrap-datetimepicker.min.css" type="text/javascript"></script>

     <script src="<?php echo base_url();?>assets/datepicker/datepicker1/js/bootstrap-datetimepicker.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/datepicker/datepicker1/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>-->

    
    
    <script src="<?php echo base_url();?>assets/datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
 

    
    
    
    
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"> </script>
                


<script>
                    
                      function isNumber(evt) {
            var iKeyCode = (evt.which) ? evt.which : evt.keyCode
            if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
                return false;

            return true;
        
                    }
                    
                    
                      function isNumber1(evt) {
            var iKeyCode = (evt.which) ? evt.which : evt.keyCode
            if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
                return false;

            return true;
        
                    }
                    
                    
                </script>

                        <script src="<?php echo base_url();?>multiple-select-master/multiple-select.js"> </script>

    
    <script>
        $('#ert').multipleSelect({
          //  isOpen: true,
                //filter: true
                // selectAll: false
        });
    </script>
    
    
