<?php



$query = $this->db->get('tblsupadmprofile')->row_array();

$comp = $this->session->userdata['companyid'];


?>
 
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/datatables/media/css/dataTables.bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/datatables-colvis/css/dataTables.colVis.css">
   <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/dataTables.fontAwesome/index.css">


<style>
    .panel-heading
    {
        display:inline-block;
    }
</style>



<section>
         <!-- Page content-->
         <div class="content-wrapper">
            
            <!-- START row-->
            
            <!-- END row-->
            <!-- START row-->
            <?php if($this->session->flashdata('permission_message'))
	 		{
			?>
					
                        <div class="alert alert-block alert-success fade in" style="background-color:#3ec0e8">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p ><h4 style="background-color:#3ec0e8"> Successful!</h4> <?php echo $this->session->flashdata('permission_message'); ?></p>
                        </div>						
									
			<?php } ?>
            <?php if($this->session->flashdata('flash_message'))
	 		{
			?>
					
                        <div class="alert alert-block alert-success fade in" style="background-color:#ff708a">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p ><h4 style="background-color:#ff708a"> Error!</h4> <?php echo $this->session->flashdata('flash_message'); ?></p>
                        </div>						
									
			<?php } ?>
            <div class="row">
               <div class="col-md-12">
                     <!-- START panel-->
                     <div class="panel panel-default">
                        <div class="panel-heading">
                             <h4>Opportunity
                             </h4>
                        </div>
                         
                        <ul class="nav nav-tabs">
                            
                      <?php
                       if($parameter == "qualify"){
                          $d = "active";
                      }
                    
                      if($parameter == ""){
                          $t = "active";
                      }
                      
                      
                    
                      
                      ?> 
                            
                            
     
                       
    <li class="<?php echo $t;?> "><a data-toggle="tab" href="#pending">Opportunity</a></li>
   
    <li class="<?php echo $d;?>"><a data-toggle="tab" href="#failed" onclick="changeId();">Qualify Opportunity</a></li>

  </ul>
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         

  <div class="tab-content">
  
      
      
      
 <!--*********************************************-->     
      
        
      
    <div id="pending" class="tab-pane fade in <?php echo $t;?> ">
      <div class="panel-body">
          <div class="container-fluid">
               <!-- START DATATABLE 1-->
               <div class="row">
                  <div class="col-lg-12">
                     <div class="panel panel-default">
<!--                        <div class="panel-heading">Data Tables |
                           <small>Zero Configuration + Export Buttons</small>
                        </div>-->
                        <div class="panel-body">
                           <div class="table-responsive">
                              <table class="table table-striped table-hover" id="datatable1">
                                 <thead>
                                    <tr>
                                        <th>S.No</th>
                                       <th>Opportunity</th>
                                       <th>Customer</th>
                                       <th>Next Action Date</th>
                                       <th>Next Action</th>
                                       <th>Stages</th>
                                       <th>Probability</th>
                                        <th>Sales Team</th>
                                    
                                      
                                       <th>Status</th>
                                       <th>Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                     <?php 
                                     
                                     $comp = $this->session->userdata('companyid');
                                     
                                     $this->db->order_by('opportunityid', 'desc');
                                     $this->db->where('clientcompid',$comp);

                                     $this->db->where('opportunitystatus',"pending");
                                     $query = $this->db->get('tblopportunity');
                                     
                                  
                                     $i=1;
                                     foreach($query->result() as $k=>$vl)
                                     {?>
                                    <tr class="gradeX">
                                        <td><?php echo $i;?></td>
                                       <td><?php echo $vl->opportunity;?></td>
                                       <td><?php echo $vl->opportunitycustomer;?></td>
                                       <td><?php echo $vl->opportunitynextactiondate;?></td>
                                    <td><?php echo $vl->opportunitynextaction;?></td>
                                    
                                    <?php $stageid = $vl->opportunitystage;
                                       $teamname= $this->db->get_where('tblopportunitystage',array('opportunitystageid'=>$stageid))->row_array() ; 
                                         $stage = $teamname['opportunitystages'];
                                    
                                    ?>
                                    
                                    
                                    <td><?php echo $stage;?></td>
                                    <td><?php echo $vl->opportunityprobability;?></td>

                                 
                                       
                                       <?php 
                                       
                                          $t =$vl->opportunityteam;
                                           $teamname= $this->db->get_where('tblsaleteam',array('salesteamid'=>$t))->row_array() ; 
                                           $name = $teamname['saleteamname'];

                                       ?>
                                       
                                         <td><?php echo $name;?></td>
                                        
                                     
                                
                              <?php  $st = $vl->leadstatus; 
                              if($st == "pending"){
                                  $status = "peding";
                              }?>
                                  

                              
                              
                                       
    <td><button type="button" class="btn btn-success" onclick="return confirm1('<?php echo base_url();?>client/Client/statusOpportunities/<?php echo $vl->opportunityid;?>','<?php echo base_url();?>client/Client/statusleadFailed/<?php echo $vl->leadid;?>');"  >Pending</button></td>
        
                                       
    
    
    
    
    
    
    
                                       
                                       <td><a href="<?php echo base_url();?>client/Client/createOpportunity/<?php echo $vl->opportunityid;?>" ><i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i></a>
                     <a href="<?php echo base_url();?>client/Client/saveOpportunity/delete/<?php echo $vl->opportunityid;?>/"  onclick="return confirm('Are you sure want to delete?');" ><i class="fa fa-trash-o fa-2x" aria-hidden="true" style="color:#f31a04;"></i></a></td>

                                    
                                      

                                    </tr>
                                  
                                     <?php $i++; } 
                                     
                                     
                                     
                                     
                                     
                                     ?>
                                  
                              
                                 
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- END DATATABLE 1-->
               <!-- START DATATABLE 2-->
               
               <!-- END DATATABLE 2-->
               <!-- START DATATABLE 3-->
               
               <!-- END DATATABLE 3-->
               
            </div>
         </div>
    </div>
      
   <!--*****************************************************-->   
      
    <div id="failed" class="tab-pane fade in <?php echo $d;?> ">
      <div class="panel-body">
          <div class="container-fluid">
               <!-- START DATATABLE 1-->
               <div class="row">
                  <div class="col-lg-12">
                     <div class="panel panel-default">
<!--                        <div class="panel-heading">Data Tables |
                           <small>Zero Configuration + Export Buttons</small>
                        </div>-->
                        <div class="panel-body">
                           <div class="table-responsive">
                              <table class="table table-striped table-hover" id="datatable2">
                                 <thead>
                                    <tr>
                                        <th>S.No</th>
                                       <th>Opportunity</th>
                                       <th>Customer</th>
                                       <th>Next Action Date</th>
                                       <th>Next Action</th>
                                       <th>Stages</th>
                                       <th>Probability</th>
                                        <th>Sales Team</th>
                                    
                                      
                                       <th>Status</th>
                                       <th>Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                     <?php 
                                     
                                     
                                     $this->db->order_by('opportunityid', 'desc');
                                       $this->db->where('clientcompid',$comp);
                                     $this->db->where('opportunitystatus',"qualify");
                                     $query = $this->db->get('tblopportunity');
                                     
                                  
                                     $i=1;
                                     foreach($query->result() as $k=>$vl)
                                     {?>
                                    <tr class="gradeX">
                                        <td><?php echo $i;?></td>
                                       <td><?php echo $vl->opportunity;?></td>
                                       <td><?php echo $vl->opportunitycustomer;?></td>
                                       <td><?php echo $vl->opportunitynextactiondate;?></td>
                                    <td><?php echo $vl->opportunitynextaction;?></td>
                                    <td><?php echo $vl->opportunitystage;?></td>
                                    <td><?php echo $vl->opportunityprobability;?></td>

                                 
                                       
                                       <?php 
                                       
                                          $t =$vl->opportunityteam;
                                           $teamname= $this->db->get_where('tblsaleteam',array('salesteamid'=>$t))->row_array() ; 
                                           $name = $teamname['saleteamname'];

                                       ?>
                                       
                                         <td><?php echo $name;?></td>
                                        
                                     
                                
                              <?php  $st = $vl->leadstatus; 
                              if($st == "pending"){
                                  $status = "peding";
                              }?>
                                  

                              
                              
                                       
    <td><button type="button" class="btn btn-success"  ><?php echo "qualify";?></button></td>
        
                                       
    
    
    
    
    
    
    
                                       
                                       <td><a href="<?php echo base_url();?>client/Client/createOpportunity/<?php echo $vl->opportunityid;?>/qualify" ><i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i></a>
                     <a href="<?php echo base_url();?>client/Client/saveOpportunity/delete/<?php echo $vl->opportunityid;?>/qualify"  onclick="return confirm('Are you sure want to delete?');" ><i class="fa fa-trash-o fa-2x" aria-hidden="true" style="color:#f31a04;"></i></a></td>

                                    
                                      

                                    </tr>
                                  
                                     <?php $i++; } 
                                     
                                     
                                     
                                     
                                     
                                     ?>
                                  
                              
                                 
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- END DATATABLE 1-->
               <!-- START DATATABLE 2-->
               
               <!-- END DATATABLE 2-->
               <!-- START DATATABLE 3-->
               
               <!-- END DATATABLE 3-->
               
            </div>
         </div>
    </div>
      
   <!--******************************************************-->   
   
   
   
   

   
   
   
   
   
   
   
   
   
   
   <!--***********************************************-->
      
</div> 
             
                        
                        
                     </div>
                  
                  </form>
               </div>
            </div>
            <!-- END row-->
         </div>
         
         <div class="container">
  <h2></h2>
   <!--Trigger the modal with a button--> 
  <button type="button" class="btn btn-info btn-lg abcde" data-toggle="modal" data-target="#myModal" style="display:none;">Open Modal</button>

   <!--Modal--> 
 <div id="myModal" class="modal fade">
 <div class="modal-dialog modal-confirm">
  <div class="modal-content">
   <div class="modal-header">
    <div class="icon-box">
     <i class="material-icons"></i>
    </div>    
    <h4 class="modal-title">Are you sure?</h4> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
   </div>
   
   <div class="modal-footer">
    <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>  
  <a id="approve_link" class="btn btn-success ">Qualify </a> 
  <!--<a id="delete_link" class="btn btn-danger btn-flat "></a>--> 

   </div>
  </div>
 </div>
</div>
  
  
  
 
  
</div>
             
         
         
         
         
         
         
         
         <div class="container">
  <h2></h2>
  <!-- Trigger the modal with a button -->
  <button type="button" class="btn btn-info btn-lg abcdef" data-toggle="modal" data-target="#myModal2" style="display:none;">Open Modal</button>

  <!-- Modal -->
 <div id="myModal2" class="modal fade">
 <div class="modal-dialog modal-confirm">
  <div class="modal-content">
   <div class="modal-header">
    <div class="icon-box">
     <!--<i class="material-icons">&#xE5CD;</i>-->
    </div>    
    <h4 class="modal-title">Are you sure?</h4> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
   </div>
   
   <div class="modal-footer">
    <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>  
  <!--<a id="approve_link" class="btn btn-success ">Approve</a>--> 
  <a id="delete_link23" class="btn btn-success btn-flat ">Customer</a> 

   </div>
  </div>
 </div>
</div>
  
  
  
 
  
</div>
         
         
         
         
         
         
         
         <div class="container">
  <h2></h2>
  <!-- Trigger the modal with a button -->
  <button type="button" class="btn btn-info btn-lg abcdefg" data-toggle="modal" data-target="#myModal3" style="display:none;">Open Modal</button>

  <!-- Modal -->
 <div id="myModal3" class="modal fade">
 <div class="modal-dialog modal-confirm">
  <div class="modal-content">
   <div class="modal-header">
    <div class="icon-box">
     <!--<i class="material-icons">&#xE5CD;</i>-->
    </div>    
    <h4 class="modal-title">Are you sure?</h4> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
   </div>
   
   <div class="modal-footer">
    <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>  
  <a id="approve_link23" class="btn btn-success ">Customer</a> 
  <!--<a id="delete_link23" class="btn btn-danger btn-flat ">Disapprove</a>--> 

   </div>
  </div>
 </div>
</div>
  
  
  
 
  
</div>
      </section>











<script>
    
    
    
    
    
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah')
                    .attr('src', e.target.result)
                    .width(80)
                    .height(80);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    
    </script>
    
     <script>
                    
                      function isNumber(evt) {
            var iKeyCode = (evt.which) ? evt.which : evt.keyCode
            if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
                return false;

            return true;
        
                    }
                    
                    
                </script>
                
                <script>
                    function passSubmit()
                    {
                    alert();
         $('#passForm').submit();
                  
                        
                    } 
                    </script>
                    
                    
                     
    
   <!-- =============== VENDOR SCRIPTS ===============-->
  
   <script src="<?php echo base_url();?>assets/vendor/matchMedia/matchMedia.js"></script>
    JQUERY
   <script src="<?php echo base_url();?>assets/vendor/jquery/dist/jquery.js"></script>
  
  
   <script src="<?php echo base_url();?>assets/vendor/datatables/media/js/jquery.dataTables.min.js"></script>
   <script src="<?php echo base_url();?>assets/vendor/datatables-colvis/js/dataTables.colVis.js"></script>
   <script src="<?php echo base_url();?>assets/vendor/datatables/media/js/dataTables.bootstrap.js"></script>
   <!--<script src="<?php echo base_url();?>assets/vendor/datatables-buttons/js/dataTables.buttons.js"></script>-->
   <script src="<?php echo base_url();?>assets/vendor/datatables-buttons/js/buttons.bootstrap.js"></script>
   <script src="<?php echo base_url();?>assets/vendor/datatables-buttons/js/buttons.colVis.js"></script>
   <script src="<?php echo base_url();?>assets/vendor/datatables-buttons/js/buttons.flash.js"></script>
   <script src="<?php echo base_url();?>assets/vendor/datatables-buttons/js/buttons.html5.js"></script>
   <script src="<?php echo base_url();?>assets/vendor/datatables-buttons/js/buttons.print.js"></script>
   <script src="<?php echo base_url();?>assets/vendor/datatables-responsive/js/dataTables.responsive.js"></script>
   <script src="<?php echo base_url();?>assets/vendor/datatables-responsive/js/responsive.bootstrap.js"></script>
   <script src="<?php echo base_url();?>assets/js/demo/demo-datatable.js"></script>
   
   <script src="<?php echo base_url();?>assets/js/app.js"></script>

   
   
   

   
   
   
   
   
   
  
   <script>
   function confirm1(x,y)
    {
 
 //alert(x);
 //alert(y);
       // var delete_url = x;
       // $('#myModal1').modal('show');
       document.getElementById('approve_link').setAttribute('href', x);

//        document.getElementById('delete_link').setAttribute('href', y);
        
        $(".abcde").click();
    }
    
    
    function confirm2(x)
    {
 
 //alert(x);
 //alert(y);
       // var delete_url = x;
     //   $('#myModal2').modal('show');
      // document.getElementById('approve_link').setAttribute('href', x);

        document.getElementById('delete_link23').setAttribute('href', x);
        
      $(".abcdef").click();
    }
    
    
     function confirm3(z)
    {
 
// alert(z);
 //alert(y);
       // var delete_url = x;
     //   $('#myModal2').modal('show');
      // document.getElementById('approve_link').setAttribute('href', x);

        document.getElementById('approve_link23').setAttribute('href', z);
        
      $(".abcdefg").click();
    }
    
    
    
    
    
    
</script>

<script>
    
    function changeId()
    {
             document. getElementsByClassName('approve_link23').setAttribute('id', "datatable2");



    }
    
    </script>
    
    
    
    <script>
        
        function formSubmit()
        {
                    $.ajax({
          url: '<?php echo base_url();?>client/Client/changestatusUser',
          type : "POST",
         
          data : $('#submit_form').serialize(),
          success : function(data) {
            
          if(data == "insert")
          {
              
             $("#pmodel").show(); 
        }
            
            
     
        
            
          }
        
          
      });
      
        }
       
        
        
        
        
        
        
        
        
        
        
        
        
        </script>