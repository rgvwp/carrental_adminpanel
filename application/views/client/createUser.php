
<!-- <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/chosen_v1.2.0/chosen.min.css">-->
   <!-- DATETIMEPICKER-->
<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">
   
<?php



if($id)
{
    
   $query = $this->db->get_where('tblclientuser', array('clientuserid' => $id))->row_array();
  
  $formaction = "saveUser";
  $method="edit";
  $button_name = "Update";
  $title = "Edit User";
}else
{
  
   $formaction = "saveUser";
   $button_name = "Save";
   $method="create";
   $title = "Create User";
}

?>

      <section>
         <!-- Page content-->
         <div class="content-wrapper">
            <h3><?php echo $title;?>
               <!--<small>Validating forms frontend have never been so powerful and easy.</small>-->
            </h3>
            <!-- START row-->
            
            <!-- END row-->
            <!-- START row-->
            <div class="row">
               <div class="col-md-12">
                   <?php if($this->session->flashdata('permission_message'))
	 		{
                       
			?>
					
                        <div class="alert alert-block alert-success fade in" style="background-color:#3ec0e8">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p ><h4 style="background-color:#3ec0e8"> Successful!</h4> <?php echo $this->session->flashdata('permission_message'); ?></p>
                        </div>						
									
			<?php } ?>
            <?php if($this->session->flashdata('flash_message'))
	 		{
			?>
					
                        <div class="alert alert-block alert-success fade in" style="background-color:#ff708a">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p ><h4 style="background-color:#ff708a"> Error!</h4> <?php echo $this->session->flashdata('flash_message'); ?></p>
                        </div>						
									
			<?php } ?>
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <div class="panel-title"><?php echo $title;?></div>
                        </div>
                        <div class="panel-body">
<!--                           <h4>Type validation</h4>-->
                                      <form class="form-horizontal" action="<?php echo base_url();?>client/Client/<?php echo $formaction;?>/<?php echo $method;?>/<?php echo $id;?>" method="post" enctype='multipart/form-data'>

                            <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label" style="padding-top: 92px;">Profile Image <span style="color:red">*</span></label>
                                 <div class="col-sm-6">
                                     <?php $pic = $query['clientuserimg'];
                                     if($pic !="") {?>
                                     <img  id="blah" name="image" class=" img-circle " src="<?php echo base_url();?>uploads/images/<?php echo $query['clientuserimg'];?>" alt="Image" style="width: 85px; margin-left: 82px;" >

                                     <?php     }else {?>
                                 <img  id="blah" name="image" class=" img-circle " src="<?php echo base_url();?>/assets/img/user/user.jpg" alt="Image" style="width: 85px; margin-left: 82px;" >
                                <?php } ?>
                                   <input type='file' id="im123" name="image" onchange="readURL(this);" style=" margin-left: 82px;"  />

                                 </div>
                                 
                              </div>
                           </fieldset>
                            <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">User Name <span style="color:red">*</span></label>
                                 <div class="col-sm-6">
                                     <input class="form-control" type="text" placeholder="Enter User Name" value="<?php echo $query['clientusername'];?>" name="username" data-validation="length" data-validation-length="min1" data-validation-error-msg="User Name is requirred">
                                 </div>
                                 <!--<input type="text" name="parameter" value="<?php echo $parameter;?>">-->
                               
                              </div>
                           </fieldset>
                                          <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">User Email <span style="color:red">*</span></label>
                                 <div class="col-sm-6">
                                     <input class="form-control" type="email" placeholder="Enter User Email" value="<?php echo $query['clientuseremail'];?>" name="useremail" data-validation="length" data-validation-length="min1" data-validation-error-msg="User Email is requirred"  >
                                 </div>
                                 <!--<input type="text" name="parameter" value="<?php echo $parameter;?>">-->
                               
                              </div>
                           </fieldset>
                                          
                                          <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">User Address <span style="color:red">*</span></label>
                                 <div class="col-sm-6">
                                     <input class="form-control" type="text" placeholder="Enter User Address" name="address" value="<?php echo $query['clientuseraddress'];?>" data-validation="length" data-validation-length="min1" data-validation-error-msg=" Address is requirred" >
                                 </div>
                               
                              </div>
                           </fieldset>
                                          
                                          <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Contact Number <span style="color:red">*</span></label>
                                 <div class="col-sm-6">
                                     <input class="form-control" type="text" placeholder="Enter Contact Number" onkeypress="return isNumber1(event)" value="<?php echo $query['clientusercontact'];?>" name="contactnum"  data-validation="length" data-validation-length="max10" data-validation-error-msg="Please enter 10 digit contact number" >
                                 </div>
                                
                              </div>
                           </fieldset>
                                                                  
                                          
                                          
                                          
                                          <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Joining Date <span style="color:red">*</span></label>
                                 <div class="col-sm-6">
                                     <input class="form-control" type="text" placeholder="Enter Joining Date" id="datetimepicker" value="<?php echo $query['clientjoiningdate'];?>" name="joiningdate" >
                                 </div>
                               
                              </div>
                           </fieldset>
                                          <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Password <span style="color:red">*</span></label>
                                 <div class="col-sm-6">
                                    <input class="form-control" type="password" name="pass_confirmation" placeholder="Enter Password" value="<?php echo $query['clientuserpass'];?>"  data-validation="strength"  data-validation-strength="2" >
                                 </div>
                                
                              </div>
                           </fieldset>
                           <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Confirm Password <span style="color:red">*</span></label>
                                 <div class="col-sm-6">
                                    <input class="form-control" type="password" name="pass" placeholder="Enter Confirm Password" value="<?php echo $query['clientuserpass'];?>" data-validation="confirmation"  value="">
                                 </div>
                                
                              </div>
                           </fieldset>
                                        
                                          

                                         
                                 <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Role <span style="color:red">*</span></label>
                                 <div class="col-sm-6">
                                     <?php
                                     
                                     $compid = $this->session->userdata['companyid'];
                                     
                                     $rolequery = $this->db->get_where('tblclientrole', array('clientrolestatus' => 1,'clientcompid'=>$compid));

                                     
                                     
                                    $st = $query['clientuserrole'] ; ?>
                                    <select name="role"  class="form-control" data-validation="length" data-validation-length="min1" data-validation-error-msg="Role is requirred" >
                                    <option value=" ">Select Role</option>
                                    <?php foreach($rolequery->result() as $k=>$vl){
                                        
                           $r =$vl->clientroleid;
                                   //   echo $st;
                                        
                                        ?>
                                    <option value="<?php echo $vl->clientroleid;?>"  <?php if ($st == $r ) echo 'selected' ; ?>><?php echo $vl->clientrolename;?></option>
                                    <?php }?>
                                    <!--<option value="0"  <?php if ($st == 0 ) echo 'selected' ; ?>>Inactive</option>-->
                                  </select>
                                 </div>
                                 
                              </div>
                           </fieldset>   
                                                   
                                          
                           
<!--                           <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Status</label>
                                 <div class="col-sm-6">
                                     <?php $st = $query['supadmuserstatus'] ; ?>
                                    <select name="status"  class="form-control" data-validation="length" data-validation-length="min1" data-validation-error-msg="Status is requirred" >
                                    <option value=" ">Select Status</option>
                                    <option value="1"  <?php if ($st == 1 ) echo 'selected' ; ?>>Active</option>
                                    <option value="0"  <?php if ($st == 0 ) echo 'selected' ; ?>>Inactive</option>
                                  </select>
                                 </div>
                                 
                              </div>
                           </fieldset>
                           -->
                           
                           
                           
                            <div class="panel-footer text-center">
                           <button class="btn btn-info" type="submit" style="margin-left: -193px;"><?php echo $button_name;?></button>
                        </div>
                          
                                      </form>
                         
                        </div>
                     
                     </div>
                     <!-- END panel-->
                  </form>
               </div>
            </div>
            <!-- END row-->
         </div>
      </section>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"> </script>
                


    <script>
   
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result)
                        .width(80)
                        .height(80);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    
    </script>
    
     <script>
                    
            function isNumber1(evt) {
            var iKeyCode = (evt.which) ? evt.which : evt.keyCode
            if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
                return false;

            return true;
        
                    }
                    
                    
                </script>
               