
   <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">



<link href="<?php echo base_url();?>multiple-select-master/multiple-select.css" rel="stylesheet"/>
  <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/datatables/media/css/dataTables.bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/datatables-colvis/css/dataTables.colVis.css">
   <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/dataTables.fontAwesome/index.css">


<?php $comp  = $this->session->userdata['companyid'];




?>

<style>
   
    .btn-group, .multiselect{width:100%;}
    .multiselect-container
    {
     max-height: 800px;
     
     overflow-y: auto;
     overflow-x: hidden;
    }
    
    
    
    
    
    #classModal {}

.modal-body {
  overflow-x: auto;
}


.fa-big{
    font-size: 40px;
 }
 .fa-num{
  font-size: 20px;   
 }

</style>


<?php
if($id)
{
    
   $query = $this->db->get_where('tblopportunity', array('opportunityid' => $id))->row_array();
  
  $formaction = "saveOpportunity";
  $method="edit";
  
  $button_name = "Update";
  
  $title = "Edit Opportunity";
}else
{
  
   $formaction = "saveOpportunity";
   $button_name = "Save";
   $method="create";
   
   $title = "Create Opportunity";
}

?>

      <section>
         <!-- Page content-->
         <div class="content-wrapper">
            <h3><?php echo $title;?>
               <!--<small>Validating forms frontend have never been so powerful and easy.</small>-->
            </h3>  
            <!-- START row-->
            
            <!-- END row-->
            <!-- START row-->
            <div class="row">
               <div class="col-md-12">
                   <?php if($this->session->flashdata('permission_message'))
	 		{
                       
			?>
					
                        <div class="alert alert-block alert-success fade in" style="background-color:#3ec0e8">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p ><h4 style="background-color:#3ec0e8"> Successful!</h4> <?php echo $this->session->flashdata('permission_message'); ?></p>
                        </div>						
									
			<?php  
                        } ?>
            <?php if($this->session->flashdata('flash_message'))
	 		{
			?>
					
                        <div class="alert alert-block alert-success fade in" style="background-color:#ff708a">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p ><h4 style="background-color:#ff708a"> Error!</h4> <?php echo $this->session->flashdata('flash_message'); ?></p>
                        </div>						
									
			<?php } ?>
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <div class="panel-title"><?php echo $title;?></div>
                        </div>
                         
                       <?php if($method == "edit") { ?>

                         
                          <?php                   
        $meetingNumber = $this->db->get_where("tblmeeting",array('opportunityid'=>$id)) ;                                       
            $number =  $meetingNumber->num_rows();                                 
                                                ?>
                                                
<div class="infobox" style="margin-left: 780px; margin-top: 1200x" >                 
                                    <div class="left" style="margin-top: 1200x">
                                        
                                        
                                        <i class="icon-user     fa-big"       style="color:#5d9cec;margin-left: 40px;"   onclick= "test123('<?php echo $id;?>');"> 
                  
<!--                  <button type="button"  id="demo" style="margin-left: 4px; margin-bottom: 300x"  onclick= "test123('<?php echo $id;?>');" class="btn btn-primary"> Meetings</button>-->
</i>

                                    </div>
                                    <div class="right" style="margin-left: 40px;">
                                        <div class="clearfix">
                                            <div>
                                                <span class="c-yellow pull-left fa-num"><?php echo $number;?></span>
                                                <br>
                                            </div>
                                            <div class="txt" style="color:#5d9cec">MEETINGS</div>
                                        </div>
                                    </div>
                                </div>
                         
                         
                       <?php } ?>
                         
                         <div class="panel-body">
<!--                           <h4>Type validation</h4>-->
                                      <form class="form-horizontal" action="<?php echo base_url();?>client/Client/<?php echo $formaction;?>/<?php echo $method;?>/<?php echo $id;?>" method="post" enctype='multipart/form-data'>

                                          <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Opportunity <span style="color:red">*</span></label>
                                 <div class="col-sm-6">
                                     <input style="width:100%" placeholder="Enter Opportunity" class="form-control" type="text" value="<?php echo $query['opportunity'];?>" name="opportunity" data-validation="length" data-validation-length="min1" data-validation-error-msg="Opportunity is required"  >
                                 </div>
                                 <input type="hidden" name="parameter" value="<?php echo $parameter;?>">
                               
                              </div>
                           </fieldset>

<fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Stages <span style="color:red">*</span></label>
                                 <div class="col-sm-6">
                               
                                 
                                      <select name="stage" class="form-control"   style="width: 100%;"  data-validation="length" data-validation-length="min1" data-validation-error-msg="Stage is required" >
                                 
                                     <option value="">Select Stage </option>
                            <?php 
                                          $tg =  $query['opportunitystage']      ;
                                         $this->db->where('clientcompid', $comp);
                                         $opporquery = $this->db->get('tblopportunitystage');
                                         //$result = $opporquery->row_array();
                                         // $stages = explode(",",$result['opportunitystages']);
                                     foreach($opporquery->result()  as $k=>$v){
                                            $v1 = $v->opportunitystageid;  ?>
                                  <option value="<?php echo $v->opportunitystageid; ?>"  <?php if ($v1 == $tg ) echo 'selected' ; ?>><?php echo $v->opportunitystages;?></option>
                                   <?php } ?>
                                     </select>     
                                 
                                 
                                 
                                 
                                 
                              </div>
                           </fieldset>
<!--                                          <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Company Code</label>
                                 <div class="col-sm-6">
                                     <input class="form-control" type="text"  name="companycode" value="<?php echo $query['clientcompcode'];?>" data-validation="length" data-validation-length="min1" data-validation-error-msg="Company Code is requirred" > 
                                 </div>
                                
                              </div>
                           </fieldset>-->
<!--                                          <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label"> Address</label>
                                 <div class="col-sm-6">
                                     <input class="form-control" type="text"  name="address" value="<?php echo $query['clientcompaddress'];?>" data-validation="length" data-validation-length="min1" data-validation-error-msg="Company Address is requirred" >
                                 </div>
                               
                              </div>
                           </fieldset>-->
<?php if($method == "create"){?>
                                          
                                          <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Sales Person</label>
                                 <div class="col-sm-6">
                                     
                                <select multiple="multiple"   name="saleperson[]" style="width:90%" id="ert" >  
                                 <!--<option value="">Select Sales Person </option>-->

                                  <?php  
                                
                                  
                                  
                                    $this->db->where('clientcompid', $comp);
                                 
                                    $teamquery1 = $this->db->get('tblsaleteam');

                                   foreach($teamquery1->result() as $k=>$v1){
                                
                                       $x =$v1->salesteamusersid;
                                       $arr = $arr.','.$x;
                                    }   
                                      $teammembers = trim($arr,",");
                                      $y = explode(",",$teammembers);
                                      $teamarr = array_unique($y);
                                      
                                     foreach($teamarr as $row){
                                         
                                         $usename = $this->db->get_where('tblclientuser',array('clientuserid'=>$row));
                                         
                                       
                                         
                                          foreach($usename->result() as $h=>$r)  {
                                              
                                              $y12 = $r->clientuserid;?>
                                 
                                 <option value="<?php echo $r->clientuserid;?>"    <?php if ($ap == $y12 ) echo 'selected' ; ?>><?php echo $r->clientusername;?></option>

                                        <?php  }
                                     } 
                                      
                                  ?>   
                              
                                   
                                </select> 
                                 </div>
                                
                              </div>
                           </fieldset>


<?php } ?>

<?php if($method == "edit"){
    
    $personarr = explode(",",$query['opportunitysalesperson']);
    
    
    ?>

     <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Sales Person</label>
                                 <div class="col-sm-6">
                                     
                                <select multiple="multiple"   name="saleperson[]" style="width:90%" id="ert" >  
                                 <!--<option value="">Select Sales Person </option>-->

                                  <?php  
                                   
                                  
                                  
                                    $this->db->where('clientcompid', $comp);
                                 
                                    $teamquery1 = $this->db->get('tblsaleteam');

                                   foreach($teamquery1->result() as $k=>$v1){
                                
                                       $x =$v1->salesteamusersid;
                                       $arr = $arr.','.$x;
                                    }   
                                      $teammembers = trim($arr,",");
                                      $y = explode(",",$teammembers);
                                      $teamarr = array_unique($y);
                                      
                                     foreach($teamarr as $row){
                                         
                                         $usename = $this->db->get_where('tblclientuser',array('clientuserid'=>$row));
                                         
                                       
                                         
                                          foreach($usename->result() as $h=>$r) {
                                              
                                              $y12 = $r->clientuserid;
                                              
                                             if (in_array($y12, $personarr))
                                                        { ?>
                                 
         <option value="<?php echo $r->clientuserid;?>"    <?php  echo 'selected' ; ?>><?php echo $r->clientusername;?></option>

                                                     <?php   }
                                                      else
                                                        { ?>
         
         
                                          <option value="<?php echo $r->clientuserid;?>"  ><?php echo $r->clientusername;?></option>

                                                       <?php }

                                              ?>
                                 
                                 
                                 

                                        <?php  }
                                     } 
                                      
                                  ?>   
                              
                                   
                                </select> 
                                 </div>
                                
                              </div>
                           </fieldset>
    
<?php } ?>

















  <fieldset>   
 <div class="form-group">
                                 <label class="col-sm-2 control-label">Sales Team <span style="color:red">*</span></label>
                                 <div class="col-sm-6">
                                     
                                     <?php  $ap =   $query['opportunityteam']    ;                         ?>
                                    <select name="teamid" class="form-control"   style="width: 100%;"  data-validation="length" data-validation-length="min1" data-validation-error-msg="Sales Team is required" >
                                 
                                     <option value="">Select Team </option>
                                    <?php  
                                   $this->db->where('clientcompid', $comp);
                                   $teamquery = $this->db->get('tblsaleteam');
                                   foreach($teamquery->result() as $k=>$v){
                                   $y12 = $v->salesteamid;      
                                   
                                   ?>
                                    <option value="<?php echo $v->salesteamid; ?>"  <?php if ($ap == $y12 ) echo 'selected' ; ?>><?php echo $v->saleteamname;?></option>
                                   <?php } ?>

                                  </select>   
                                   </div>
                               
                              </div>
                           </fieldset>  


                                          
                            <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Customer <span style="color:red">*</span></label>
                                 <div class="col-sm-6">
                                     <input class="form-control " placeholder="Enter Customer" type="text"   value="<?php echo $query['opportunitycustomer'];?>" name="customer"  data-validation="length" data-validation-length="min1" data-validation-error-msg="Customer is required">
                                 </div>
                               
                              </div>
                           </fieldset>   
 <div class="form-group">
                                 <label class="col-sm-2 control-label">Customer Address <span style="color:red">*</span></label>
                                 <div class="col-sm-6">
                                     <input class="form-control " type="text" placeholder="Enter Address"  value="<?php echo $query['opportunitycustomeraddress'];?>" name="address" data-validation="length" data-validation-length="min1" data-validation-error-msg="Address is required">
                                 </div>
                               
                              </div>
                           </fieldset> 
                           
                            <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Email</label>
                                 <div class="col-sm-6">
                                     <input class="form-control " type="email" placeholder="Enter Email"  value="<?php echo $query['opportunityemail'];?>" name="emailaddress" >
                                 </div>
                               
                              </div>
                           </fieldset>  
                                          
                                    </fieldset>
                                      <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Phone</label>
                                 <div class="col-sm-6">
                                     <input class="form-control" placeholder="Enter Phone" type="text" onkeypress="return isNumber1(event)"  value="<?php echo $query['opportunityphone'];?>" name="phone" >
                                 </div>
                               
                              </div>
                           </fieldset>  
                                    
                                    
                         <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Lead Tag <span style="color:red">*</span></label>
                                 <div class="col-sm-6">
                                     <?php  $tg =  $query['opportunitytag'];?>
                                   <select name="leadtag" class="form-control"    style="width: 100%;" data-validation="length" data-validation-length="min1" data-validation-error-msg="Lead tag is required" >
                                 
                                     <option value="">Select Tag </option>
                            <?php   $this->db->where('clientcompid', $comp);
                                  $leadquery = $this->db->get('tblleadtag');
//                                    $result = $leadquery->row_array();
//                                     $tags = explode(",",$result['leadtags']);
                                     foreach($leadquery->result()  as $k=>$v){
                                      $v1 = $v->leadtagid;   ?>
                                  <option value="<?php echo $v->leadtagid; ?>"  <?php if ($v1 == $tg ) echo 'selected' ; ?>><?php echo $v->leadtags;?></option>
                                   <?php } ?>
                                     </select>     
                                 </div>
                              </div>
                           </fieldset>             
                                
                          <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Probability</label>
                                 <div class="col-sm-6">
                                     <input class="form-control " type="text" placeholder="Enter Probability"  value="<?php echo $query['opportunityprobability'];?>" name="probability" >
                                 </div>
                               
                              </div>
                           </fieldset>  
                                    
                               <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Next Action</label>
                                 <div class="col-sm-6">
                                     <input class="form-control " type="text"  placeholder="Enter Next Action" value="<?php echo $query['opportunitynextaction'];?>" name="nextaction" >
                                 </div>
                               
                              </div>
                           </fieldset>        
                                    
                                    
                                    
                                    
                                    <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Next Action Date</label>
                                 <div class="col-sm-6">
                                     <input class="form-control " type="text" placeholder="Enter Next Action Date" id="datetimepicker" value="<?php echo $query['opportunitynextactiondate'];?>" name="nextactiondate" >
                                 </div>
                               
                              </div>
                           </fieldset>  
                           
                          
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                            </fieldset>   
 <div class="form-group">
                                 <label class="col-sm-2 control-label">Notes</label>
                                 <div class="col-sm-6">
<textarea name="notes" rows="4" class="form-control" placeholder="Enter Notes" ><?php echo $query['opportunitynotes'];?></textarea>                                 </div>
                               
                              </div>
                           </fieldset>  
                           
                           
                            <div class="panel-footer text-center">
                           <button class="btn btn-info" type="submit" style="margin-left: -193px;"><?php echo $button_name;?></button>
                        </div>
                          
                                      </form>
                         
                        </div>
                     
                     </div>
                     <!-- END panel-->
                  </form>
               </div>
            </div>
            <!-- END row-->
         </div>
            
            
            <div class="container">
<!--  <h2>Modal Example</h2>-->
  <!-- Trigger the modal with a button -->
  <button type="button"  style="display:none;"class="btn btn-info btn-lg  openpopup" data-toggle="modal" data-target="#myoldModal">Open Modal</button>

  <!-- Modal -->
  <div class="modal fade" id="myoldModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
<!--        <div class="modal-header">-->
          <button type="button" class="close  abcde12"  style="margin-right: 14px; margin-top: 20px;" data-dismiss="modal">&times;</button>
          <!--<h4 class="modal-title">Modal Header</h4>-->
        <!--</div>-->
        
        
  
        
        <div class="modal-body">
            
            <p>
        <button type="button"  id="createmeet"   onclick= "createMeeting('<?php echo $id;?>');" class="btn btn-primary">Create Meetings</button>
  
            
            <div id="active" class="tab-pane fade in ">
      <div class="panel-body">
          <div class="container-fluid">
               <!-- START DATATABLE 1-->
               <div class="row">
                  <div class="col-lg-12">
                     <div class="panel panel-default">
<!--                        <div class="panel-heading">Data Tables |
                           <small>Zero Configuration + Export Buttons</small>
                        </div>-->
                        <div class="panel-body">
                           <div class="table-responsive">
                              <table class="table table-striped table-hover" id="datatable1" >
                                 <thead>
                                    <tr>
                                        <th>S.No</th>
                                       <th>Subject</th>
                                       <th>Location</th>
                                       <th>Attendies</th>
                                       <th>Starting at</th>
                                        <th>Ending at</th>
                                        <th>Description</th>

                                      
                                      
                                       <th>Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                     <?php
                                     
                                     
                                      $this->db->order_by("meetingid", "desc");
                                      $this->db->where('clientcompid',$comp);
                                      $this->db->where('opportunityid',$id);
                                      $meetquery = $this->db->get('tblmeeting');
                                    
                                     
                                     
                                     
                                    // $query = $this->db->get_where('tblclientuser',array('clientcompid'=>$compid));
                                     $i=1;
                                    // echo $this->db->last_query();
                                     foreach($meetquery->result() as $k=>$vl)
                                     {?>
                                    <tr class="gradeX  hide<?php echo $vl->meetingid;?> ">
                                        <td><?php echo $i;?></td>
                                        
                                        
                                        <td> <?php echo $vl->meetingsubject;?>
                                        </td>
                                     
                                       
                                     
                                       
                                       
                                       
                                       
                                       <td><?php echo $vl->meetinglocation;?> </td>
                                       
                                          
                                       <?php
                                       $k2="";
                                       
                                       $member = explode(",",$vl->meetingattendies);
                                       
                                       foreach($member as $t=>$r) {
                                   
                                       $query123 = $this->db->get_where('tblclientuser', array('clientuserid' => $r))->row_array();
                                          
                                       $name = $query123['clientusername'];
                                       
                                       
                                     $k2 = $k2.','."$name";         
                                       }
                                       ?>
                                        
                                       
                                         <td><?php echo $str = ltrim($k2, ',');?></td>
                                     
                                         
                                         
                                   <td><?php echo $vl->meetingstartat;?></td>
                                   <td><?php echo $vl->meetingendat;?></td>
                                       
                                       
                                   <td> <?php echo $vl->meetingdescription;?>  </td>  
                                       
                                     
                    <td><a onclick="editmeeting('<?php echo $vl->meetingid;?>');" ><i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i></a>
                     <a  onclick="return confirmirmation('<?php echo $vl->meetingid;?>');" ><i class="fa fa-trash-o fa-2x" aria-hidden="true" style="color:#f31a04;"></i></a></td>

                                    </tr>
                                  
                                     <?php $i++; } 
                                     
                                     
                                     
                                     
                                     
                                     ?>
                                  
                              
                                 
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- END DATATABLE 1-->
               <!-- START DATATABLE 2-->
               
               <!-- END DATATABLE 2-->
               <!-- START DATATABLE 3-->
               
               <!-- END DATATABLE 3-->
               
            </div>
         </div>
            </div>
        
        </p>
            
            
            
            
          <!--<p>Some text in the modal.</p>-->
       
        
        
        
        
        
        
        
        
        
        
        </div>
        
      </div>
      
    </div>
  </div>
  
</div>

</body>
      </section>

<div class="">
  
</div>
<!--***********************Pop up of create meeting*****************************--> 
<div class="container">
  <h2></h2>
  <!-- Trigger the modal with a button -->
  <button type="button" class="btn btn-info btn-lg abcdefg" data-toggle="modal" data-target="#myModal3" style="display:none;">Open Modal</button>

  <!-- Modal -->
 <div id="myModal3" class="modal fade pasteDiv" >
 <div class="modal-dialog modal-confirm">
  <div class="modal-content"  style="margin-left: -7px;">
      
      
           <button type="button" class="close  closeCreate"   style="margin-right: 14px; margin-top: 17px;" data-dismiss="modal" aria-hidden="true">&times;</button>

   <div class="modal-header">
       
    <div class="icon-box">
     <!--<i class="material-icons">&#xE5CD;</i>-->
        

<!--        <div class="alert alert-block alert-success  modelpop" style="background-color:#3ec0e8 ;display:none;">
                                <a class="close"   style="margin-left: 576px;" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p> Submitted Successfully</p>
                        </div>	-->

    </div>    
   </div>
   
      
      
      
      
    <form id="add_meeting" name="add_meeting" class="form-validation" accept-charset="utf-8" enctype="multipart/form-data" method="post">
               	 <input type="hidden" name="meeting_type_id" value="4">
               	 <input type="hidden" name="meeting_type" value="opportunities">	                        			 
               	 <div class="modal-body">
                   
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="field-2" class="control-label">Meeting Subject</label>
                        <input type="text" class="form-control" name="subject" onblur="hideValue();" id="meeting_subject" placeholder="">
                                                <span id="meeting" style="color:red"> </span>

                      </div>
                    </div>
                    
                   
             <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="field-2" class="control-label">Attendies</label>
                        
                        <br>
                        
  <select multiple="multiple"   name="attendies[]" style="width:200px" id="ert12">

                                <?php 
                                       
                                     $this->db->where('clientuserstatus',1);
                                    $this->db->where('clientcompid', $comp);
                                 
                                    $member = $this->db->get('tblclientuser');

                                
                                
                             if($method == "create")
                             {
                                foreach($member->result() as $k1=>$v1){?>

        <option value="<?php echo $v1->clientuserid;?>"  ><?php echo $v1->clientusername;?></option>
      
   
                                <?php } 
        
        } ?>
        
        
        
        
        <?php    $mem = explode(",",$query['meetingattendies']) ;
             
        print_r($mem);
                             if($method == "edit")
                             {
                                foreach($member->result() as $k1=>$v1)
                                    
                                    {
                                    
                                           $m =  $v1->clientuserid;
                                    
                                         if (in_array($m, $mem))
                                                                { ?>
        
                   <option value="<?php echo $v1->clientuserid;?>" <?php echo "selected";?> ><?php echo $v1->clientusername;?></option>

                                                

     <?php           }
                                                              else
                                                                { ?>
                                                               
                                                                  
           <option value="<?php echo $v1->clientuserid;?>"  ><?php echo $v1->clientusername;?></option>

                                                         <?php       }

                                           
                                           
                                           
                                   

                                    
                                     
                                       
                                        
                                    }
                                    
                                    
                                    ?>

                    
                                <?php } 
        
        ?>
        
        
        
        
        
        
        
        
                                                                       </select>                     
                      
                      
                      
                      </div>
                    </div>

			<input type="hidden"	 value="<?php echo $id;?>" name="updateid">
                    <!--<li class="active"><a href="#tab1_1" data-toggle="tab" style="margin-left:40px;"></a></li>-->
                   
              
				 <div class="tab-content">
						                        
						                        <div class="tab-pane fade active in" id="tab1_1">
							                           <div class="panel-body bg-white">
						                 			  		   											 				  							  <div class="row">
                          							   <div class="col-md-6">
									                      <div class="form-group">
									                        <label for="field-1" class="control-label">Starting at</label>
									                        <!--<input type="text" class="date-picker form-control" name="date" id="date" placeholder="" value="">-->
									                        <input type="text" name="starting_date"  onblur="hideValue(); compareDate1();" id="datetimepicker1" class="datetimepicker form-control hasDatepicker  starting" placeholder="Choose a date...">
									                             <span id="start" style="color:red"></span>
 
									                      </div>
									                    </div> 
	                          							 <div class="col-sm-6">
							                            <div class="form-group">
							                              <label class="control-label">Location</label>
							                              <div class="append-icon">
							                                <input type="text" name="location" value="" onblur="hideValue();" id="location" class="form-control"> 
							                                                                                                                       <span id="locate" style="color:red"></span>

							                              </div>
							                            </div>
							                          </div>
                          					 	
					                    			     </div>
	 													 <div class="row">
                          							     	 <div class="col-sm-6">
									                           <div class="form-group">
									                              <label class="control-label">Ending at</label>
									                              <div class="append-icon">
									                                <input type="text" name="ending_date"  onblur="hideValue(); compareDate1();" id="datetimepicker2" class="form-control hasDatepicker   ending_date" placeholder="Choose a date...">
									                                                                                                                                       <span id="end" style="color:red"></span>

									                              </div>
									                            </div>
					                         				 </div>
                          								     <div class="col-sm-6">
				                            
				                         					 </div>
                          					 	
					                    			      </div>
													
														<div class="row">
														<div class="col-sm-12">
					                            		<div class="form-group">
					                               <label class="control-label">Description</label>
					                              <div class="append-icon">
					                                
					                                <textarea name="meeting_description" onblur="hideValue();"  rows="5" class="form-control desc123" placeholder="describe the product characteristics..."></textarea>   
					                                                                                                 <span id="desc" style="color:red"></span>

                                                                      </div
					                            </div>
					                         			 </div>
														</div>
							               			   </div>
						                        </div>
											    
<!--											    <div class="tab-pane fade" id="tab1_2">
												<div class="panel-body bg-white">	
													 <div class="row">
													 	 <div class="col-sm-6">
							                            
					                          			</div>-->
														 <div class="col-sm-6">
<!--							                            <div class="form-group">
							                              <label class="control-label">Show Time as</label>
							                              <div class="append-icon">
							                                 
							                                <div class="select2-container form-control" id="s2id_autogen22"><a href="javascript:void(0)" class="select2-choice select2-default" tabindex="-1">   <span class="select2-chosen" id="select2-chosen-23"></span><abbr class="select2-search-choice-close"></abbr>   <span class="select2-arrow" role="presentation"><b role="presentation"></b></span></a><label for="s2id_autogen23" class="select2-offscreen"></label><input class="select2-focusser select2-offscreen" type="text" aria-haspopup="true" role="button" aria-labelledby="select2-chosen-23" id="s2id_autogen23"><div class="select2-drop select2-display-none">   <div class="select2-search select2-search-hidden select2-offscreen">       <label for="s2id_autogen23_search" class="select2-offscreen"></label>       <input type="text" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" class="select2-input" role="combobox" aria-expanded="true" aria-autocomplete="list" aria-owns="select2-results-23" id="s2id_autogen23_search" placeholder="">   </div>   <ul class="select2-results" role="listbox" id="select2-results-23">   </ul></div></div><select name="show_time_as" class="form-control" tabindex="-1" title="" style="display: none;">
<option value="" selected="selected"></option>
<option value="Free">Free</option>
<option value="Busy">Busy</option>
</select>	
							                              </div>
							                            </div>
					                          			</div>	-->
													 </div>
												</div>
					                         </div> 
					                          
					                           
					                            </div>
                </div>
                 
                  <div id="meeting_submitbutton" class="modal-footer text-center"><button  class="btn btn-primary btn-embossed bnt-square createbutton" onclick="formSubmit();">Create</button></div>
                 
                </form>
  </div>
 </div>
</div>
  
  
  
 
  
</div>



</div>
         <!--*******************************Popup for paste*******************************-->
         
         <div class="container">
<!--  <h2>Modal Example</h2>-->
  <!-- Trigger the modal with a button -->
  <button type="button" class="btn btn-info btn-lg  editedform" data-toggle="modal" data-target="#editedForm"></button>

  <!-- Modal -->
  <div class="modal fade" id="editedForm" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
          <p>Some text in the modal.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>

<!--********************-->
         
      </div>
      









    <script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js" type="text/javascript"></script>
 <script src="<?php echo base_url();?>assets/vendor/matchMedia/matchMedia.js"></script>
    JQUERY
   <script src="<?php echo base_url();?>assets/vendor/jquery/dist/jquery.js"></script>
  
  
   <script src="<?php echo base_url();?>assets/vendor/datatables/media/js/jquery.dataTables.min.js"></script>
   <!--<script src="<?php echo base_url();?>assets/vendor/datatables-colvis/js/dataTables.colVis.js"></script>-->
   <script src="<?php echo base_url();?>assets/vendor/datatables/media/js/dataTables.bootstrap.js"></script>
   <!--<script src="<?php echo base_url();?>assets/vendor/datatables-buttons/js/dataTables.buttons.js"></script>-->
   <script src="<?php echo base_url();?>assets/vendor/datatables-buttons/js/buttons.bootstrap.js"></script>
   <script src="<?php echo base_url();?>assets/vendor/datatables-buttons/js/buttons.colVis.js"></script>
   <!--<script src="<?php echo base_url();?>assets/vendor/datatables-buttons/js/buttons.flash.js"></script>-->
   <script src="<?php echo base_url();?>assets/vendor/datatables-buttons/js/buttons.html5.js"></script>
   <!--<script src="<?php echo base_url();?>assets/vendor/datatables-buttons/js/buttons.print.js"></script>-->
   <script src="<?php echo base_url();?>assets/vendor/datatables-responsive/js/dataTables.responsive.js"></script>
   <script src="<?php echo base_url();?>assets/vendor/datatables-responsive/js/responsive.bootstrap.js"></script>
   <script src="<?php echo base_url();?>assets/js/demo/demo-datatable.js"></script>
   
   <script src="<?php echo base_url();?>assets/js/app.js"></script>

   
       
    
    <script src="<?php echo base_url();?>assets/datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
 

    
    
    
    
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"> </script>
                


<script>
                    
                      function isNumber(evt) {
            var iKeyCode = (evt.which) ? evt.which : evt.keyCode
            if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
                return false;

            return true;
        
                    }
                    
                    
                      function isNumber1(evt) {
            var iKeyCode = (evt.which) ? evt.which : evt.keyCode
            if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
                return false;

            return true;
        
                    }
                    
                    
                </script>

                        <script src="<?php echo base_url();?>multiple-select-master/multiple-select.js"> </script>

    
    <script>
        $('#ert').multipleSelect({
          //  isOpen: true,
                //filter: true
                // selectAll: false
        });
        
        
        
        
        
         $('#ert12').multipleSelect({
          //  isOpen: true,
                //filter: true
                // selectAll: false
        });
    </script>
    
    <script>
        
       function test123()
       {
         
        $(".openpopup").click();
       }
     </script>
        
    
 <script>
        
        function formSubmit()
        { 
          
       
        
        event.preventDefault();
            
             
            var m =  $("#meeting_subject").val();
           var s =  $(".starting").val();
           var l =   $("#location").val();
           var e =   $(".ending_date").val();
             var d = $(".desc123").val();
           
           if(d == ""){
                
              
                $("#desc").html("Description is required");
                return false;
            }
           
           
            if(m == ""){
                
                $("#meeting").html("Meeting is required");
                return false;
            }
            if(s == ""){
               $("#start").html("Start date is required"); 
               return false;
            }
            if(l == ""){
              $("#locate").html("Location is required");  
              return false;
            }
            if(e == ""){
              $("#end").html("End date is required");
              return false;
            }
                $.ajax({
          url: '<?php echo base_url();?>client/Client/saveopportMeeting',
          type : "POST",
         
          data : $('#add_meeting').serialize(),
          success : function(response) {
              
             // alert(response);
              $("#datatable1").html(response);
              
              $(".closeCreate").click();
              
                 $("#meeting_subject").val(" ");
          $(".starting").val(" ");
          $("#location").val(" ");
          $(".ending_date").val(" ");
          $(".desc123").val(" ");
              
              
              
           // console.log(response);
//                  if(data == "insert")
//                    {
//                        $("#meeting_subject").val(" ");
//                        $(".starting").val(" ");
//                        $("#location").val(" ");
//                        $(".ending_date").val(" ");
//
//
//                    //$(".modelpop").show();
//                      }
           }
        
          
      });
      
        }
        function hideValue()
        {
            
        $("#meeting").html(" ");
        $("#start").html(" "); 
        $("#locate").html(" ");  
        $("#end").html(" ");
        $(".desc123").html(" ");
         }
      </script>
        
        
        <script>
            function createMeeting(vala){
         //  $(".abcde12").click();
         $(".abcdefg").click();
            
               }   
        </script>
        
        <script>
            
        function confirmirmation(a){
            
    
        var abc =   confirm('Are you sure want to delete?');
          
         
        
        if(abc == true){
    
             $.ajax({
          url: '<?php echo base_url();?>client/Client/deletemeeting',
          type : "POST",
         
          data : {'data':a},
          success : function(data) {
                
             $(".hide"+a).hide();
           }
        
          
      });
               
           }
        }
            
        </script>
        
        <script>
          function editmeeting(editid){
          
//       alert();
        
      //  event.preventDefault();
            
             $.ajax({
          url: '<?php echo base_url();?>client/Client/editmeeting',
          type : "POST",
          data : {'editid':editid},
          success : function(data) {
              
            //alert(data);
//             console.log(data);
            // $("#createmeet").click();
            
                       //  $('.pasteDiv').html(data);
                       $('.editedform').click();
//                        $('#myModal3').modal('show');
                       $('#editedForm').html(data);
            
//             $('.pasteDiv').html(data);
//             $('#myModal3').modal('show');
//            $('.abcdefg').click();
              
         //$("#createmeet").click();
       
            
           }
        
          
      });
//           
          }  
            
            
            
        </script>
           
        
<!--        <script>
        
        function formSubmit()
        { 
            event.preventDefault();
            
             
            var m =  $("#meeting_subject").val();
           var s =  $(".starting").val();
           var l =   $("#location").val();
           var e =   $("#ending_date").val();
             var d = $(".desc123").val();
           
           if(d == ""){
                
                $("#desc").html("Description is requirred");
                return false;
            }
           
           
            if(m == ""){
                
                $("#meeting").html("Meeting is requirred");
                return false;
            }
            if(s == ""){
               $("#start").html("Start date is requirred"); 
               return false;
            }
            if(l == ""){
              $("#locate").html("Location is requirred");  
              return false;
            }
            if(e == ""){
              $("#end").html("End date is requirred");
              return false;
            }
                $.ajax({
          url: '<?php echo base_url();?>client/Client/saveopportMeeting',
          type : "POST",
         
          data : $('#add_meeting').serialize(),
          success : function(data) {
               if(data == "insert")
          {
              $("#meeting_subject").val(" ");
              $(".starting").val(" ");
              $("#location").val(" ");
              $("#ending_date").val(" ");
              
              
          $(".modelpop").show();
            }
           }
        
          
      });
      
        }
        function hideValue()
        {
            
        $("#meeting").html(" ");
        $("#start").html(" "); 
        $("#locate").html(" ");  
        $("#end").html(" ");
        $(".desc123").html(" ");
         }
      </script>-->
        
      <script>
          
           function editformSubmit()
        { 
           
        
        
        
        event.preventDefault();
            
             
            var m =  $("#meeting_subject12").val();
           var s =  $(".starting12").val();
           var l =   $("#location12").val();
           var e =   $(".ending_date12").val();
             var d = $(".desc12312").val();
           
         
           if(d == ""){
                
                $("#desc12").html("Description is required");
                return false;
            }
          
           
            if(m == ""){
                
                $("#meeting12").html("Meeting is required");
                return false;
            }
            if(s == ""){
               $("#start12").html("Start date is required"); 
               return false;
            }

// alert('abc');
            if(l == ""){
              $("#locate12").html("Location is required");  
              return false;
            }
            if(e == ""){
              $("#end12").html("End date is required");
              return false;
            }
            
           
        
                $.ajax({
          url: '<?php echo base_url();?>client/Client/editopportMeeting',
          type : "POST",
         
          data : $('#edit_meeting').serialize(),
          success : function(data) {
              
            //  $("#asdfgh").css("display", "block");
             // alert(data);
             
             
             console.log(data);
          $("#datatable1").html(data);
          
          $(".qwerty").click();
                  
           }
        
          
      });
      
        } 
          
          
          </script>
       <script>    
            
            function compareDate1()
            {
              
               var start = $(".starting").val();
               var end = $(".ending_date").val();
             
             var x = new Date(start);
                 var y = new Date(end);
             
             
                 if(x>y){
                     
                     $("#start").html("Start date is not greater than End date");
                     
                      $('.createbutton'). attr('disabled', 'disabled'); 
                   }else{
                       
                       
                         $('.createbutton').prop("disabled", true);
                         $('.createbutton').removeAttr("disabled");
                   }
             
            }
            
            
            
            
            </script>
        
        
        <!--***********************Pop up of create meeting*****************************--> 

            <!--********************************-->