<!DOCTYPE html>

<?php


$userid = $this->session->userdata['userid'];

  $usertype = $this->session->userdata['usertype'];
  
 
 
      $role =$this->session->userdata['role'];

      $query = $this->db->get_where('tblclientrolepermis', array('clientroleid' => $role))->row_array();
      $permission = explode(",",$query['clientrolepermision']);

    
?>


<html lang="en">

<head>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
   <meta name="description" content="Bootstrap Admin App + jQuery">
   <meta name="keywords" content="app, responsive, jquery, bootstrap, dashboard, admin">
   <title>Car Rental</title>
   <!-- =============== VENDOR STYLES ===============-->
   <!-- FONT AWESOME-->
   
 
<!--     <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/chosen_v1.2.0/chosen.min.css">
    DATETIMEPICKER
   <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">
   -->
   
   <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/fontawesome/css/font-awesome.min.css">
   <!-- SIMPLE LINE ICONS-->
   <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/simple-line-icons/css/simple-line-icons.css">
   <!-- ANIMATE.CSS-->
   <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/animate.css/animate.min.css">
   <!-- WHIRL (spinners)-->
   <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/whirl/dist/whirl.css">
   <!-- =============== PAGE VENDOR STYLES ===============-->
   
<!--     <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/datatables-colvis/css/dataTables.colVis.css">
   <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/datatables/media/css/dataTables.bootstrap.css">
   <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/dataTables.fontAwesome/index.css">-->
   
   
   <!-- WEATHER ICONS-->
   <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/weather-icons/css/weather-icons.min.css">
   <!-- =============== BOOTSTRAP STYLES ===============-->
   <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.css" id="bscss">
   <!-- =============== APP STYLES ===============-->
   <link rel="stylesheet" href="<?php echo base_url();?>assets/css/app.css" id="maincss">
   
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"> </script>
   
</head>

<body>
   <div class="wrapper">
      <!-- top navbar-->
      <header class="topnavbar-wrapper">
         <!-- START Top Navbar-->
         <nav class="navbar topnavbar" role="navigation">
            <!-- START navbar header-->
            <div class="navbar-header">
               <a class="navbar-brand" href="#/">
                  <div class="brand-logo">
                     <!--<img class="img-responsive" src="<?php echo base_url();?>assets/img/logo.png" alt="App Logo">-->
                  </div>
                  <div class="brand-logo-collapsed">
                     <!--<img class="img-responsive" src="<?php echo base_url();?>assets/img/logo-single.png" alt="App Logo">-->
                  </div>
               </a>
            </div>
            <!-- END navbar header-->
            <!-- START Nav wrapper-->
            <div class="nav-wrapper">
               <!-- START Left navbar-->
               <ul class="nav navbar-nav">
                  <li>
                     <!-- Button used to collapse the left sidebar. Only visible on tablet and desktops-->
                     <a class="hidden-xs" href="#" data-trigger-resize="" data-toggle-state="aside-collapsed">
                        <em class="fa fa-navicon"></em>
                     </a>
                     <!-- Button to show/hide the sidebar on mobile. Visible on mobile only.-->
                     <a class="visible-xs sidebar-toggle" href="#" data-toggle-state="aside-toggled" data-no-persist="true">
                        <em class="fa fa-navicon"></em>
                     </a>
                  </li>
                  <!-- START User avatar toggle-->
                  <li>
                     <!-- Button used to collapse the left sidebar. Only visible on tablet and desktops-->
                     <a id="user-block-toggle" href="#user-block" data-toggle="collapse">
                        <em class="icon-user"></em>
                     </a>
                  </li>
                  <!-- END User avatar toggle-->
                  <!-- START lock screen-->
                  <li>
                     <a href="<?php echo base_url();?>frontcarrental/Signup/clientLogout" title="Logout">
                       
                        <em class="fa fa-power-off top-left-logout "></em>
                     </a>
                  </li>
                  
                  <!-- END lock screen-->
               </ul>
               <!-- END Left navbar-->
               <!-- START Right Navbar-->
               <ul class="nav navbar-nav navbar-right">
                  <!-- Search icon-->
                  <li>
                     <a href="#" data-search-open="">
                        <em class="icon-magnifier"></em>
                     </a>
                  </li>
                  <!-- Fullscreen (only desktops)-->
                  <li class="visible-lg">
                     <a href="#" data-toggle-fullscreen="">
                        <em class="fa fa-expand"></em>
                     </a>
                  </li>
                  <!-- START Alert menu-->
                  <li class="dropdown dropdown-list">
                     <a href="#" data-toggle="dropdown">
                        <em class="icon-bell"></em>
                        <div class="label label-danger">11</div>
                     </a>
                     <!-- START Dropdown menu-->
                     <ul class="dropdown-menu animated flipInX">
                        <li>
                           <!-- START list group-->
                           <div class="list-group">
                              <!-- list item-->
                              <a class="list-group-item" href="#">
                                 <div class="media-box">
                                    <div class="pull-left">
                                       <em class="fa fa-twitter fa-2x text-info"></em>
                                    </div>
                                    <div class="media-box-body clearfix">
                                       <p class="m0">New followers</p>
                                       <p class="m0 text-muted">
                                          <small>1 new follower</small>
                                       </p>
                                    </div>
                                 </div>
                              </a>
                              <!-- list item-->
                              <a class="list-group-item" href="#">
                                 <div class="media-box">
                                    <div class="pull-left">
                                       <em class="fa fa-envelope fa-2x text-warning"></em>
                                    </div>
                                    <div class="media-box-body clearfix">
                                       <p class="m0">New e-mails</p>
                                       <p class="m0 text-muted">
                                          <small>You have 10 new emails</small>
                                       </p>
                                    </div>
                                 </div>
                              </a>
                              <!-- list item-->
                              <a class="list-group-item" href="#">
                                 <div class="media-box">
                                    <div class="pull-left">
                                       <em class="fa fa-tasks fa-2x text-success"></em>
                                    </div>
                                    <div class="media-box-body clearfix">
                                       <p class="m0">Pending Tasks</p>
                                       <p class="m0 text-muted">
                                          <small>11 pending task</small>
                                       </p>
                                    </div>
                                 </div>
                              </a>
                              <!-- last list item-->
                              <a class="list-group-item" href="#">
                                 <small>More notifications</small>
                                 <span class="label label-danger pull-right">14</span>
                              </a>
                           </div>
                           <!-- END list group-->
                        </li>
                     </ul>
                     <!-- END Dropdown menu-->
                  </li>
                  <!-- END Alert menu-->
                  <!-- START Offsidebar button-->
                  <li>
                     <a href="#" data-toggle-state="offsidebar-open" data-no-persist="true">
                        <em class="icon-notebook"></em>
                     </a>
                  </li>
                  <!-- END Offsidebar menu-->
               </ul>
               <!-- END Right Navbar-->
            </div>
            <!-- END Nav wrapper-->
            <!-- START Search form-->
            <form class="navbar-form" role="search" action="search.html">
               <div class="form-group has-feedback">
                  <input class="form-control" type="text" placeholder="Type and hit enter ...">
                  <div class="fa fa-times form-control-feedback" data-search-dismiss=""></div>
               </div>
               <button class="hidden btn btn-default" type="submit">Submit</button>
            </form>
            <!-- END Search form-->
         </nav>
         <!-- END Top Navbar-->
      </header>
      <!-- sidebar-->
          <!-- sidebar-->
      <aside class="aside">
         <!-- START Sidebar (left)-->
         <div class="aside-inner">
            <nav class="sidebar" data-sidebar-anyclick-close="">
               <!-- START sidebar nav-->
               <ul class="nav">
                  <!-- START user info-->
                  <li class="has-user-block">
                     <div class="collapse" id="user-block">
                        <div class="item user-block">
                           <!-- User picture-->
                           <div class="user-block-picture">
                              
                               
                               
                               
                               
                               
                               <?php 
                               if($usertype == "client"){
                                 $query  =$this->db->get_where('tblregisclient',array('regisClientId'=>$userid))->row_array();
                                  $pic =$query['regisclientimg'];
                                  $profilename = $query['regisclientFname'];
                                  $controller = "Client";
                               }
                               
                              
                               if($usertype == "clientuser"){
                                 $query  =$this->db->get_where('tblclientuser',array('clientuserid'=>$userid))->row_array();
                                  $pic =$query['clientuserimg'];
                                  $profilename = $query['clientusername'];
                                  $controller = "Clientuser";

                               }
                                  
                                  ?>
                               
                               
                               
                               
                               <?php //echo $this->db->last_query();?>
                              <div class="user-block-status">
                                  <?php if($pic !=""){?>
                                  <a  href="<?php echo base_url();?>client/<?php echo $controller;?>/profileView"> <img class="img-thumbnail img-circle" src="<?php echo base_url();?>uploads/images/<?php echo $pic;?>" alt="Avatar" width="60" height="60"></a>
                                  <?php }else { ?>
                                  <a  href="<?php echo base_url();?>client/<?php echo $controller;?>/profileView"> <img class="img-thumbnail img-circle" src="<?php echo base_url();?>assets/img/user/02.jpg" alt="Avatar" width="60" height="60"></a>

                                  <?php } ?>
                                  
                                  <div class="circle circle-success circle-lg"></div>
                              </div>
                           </div>
                           <!-- Name and Job-->
                           <div class="user-block-info">
                              <span class="user-block-name"><?php echo $profilename ;?></span>
                              <!--<span class="user-block-role">Designer</span>-->
                           </div>
                        </div>
                     </div>
                  </li>
                  <!-- END user info-->
                  <!-- Iterates over all sidebar items-->
                  <li class="nav-heading ">
                     <span data-localize="sidebar.heading.HEADER">Main Navigation</span>
                  </li>
                  <li class=" ">
                     <a href="<?php echo base_url();?>client/Client/dashboard" title="Dashboard">
                        <!--<div class="pull-right label label-info">3</div>-->
                        <em class="icon-speedometer"></em>
                        <span data-localize="sidebar.nav.DASHBOARD">Dashboard</span>
                     </a>
                     <ul class="nav sidebar-subnav collapse" id="dashboard">
                        <li class="sidebar-subnav-header">Dashboard</li>
<!--                        <li class=" active">
                           <a href="dashboard.html" title="Dashboard v1">
                              <span>Dashboard v1</span>
                           </a>
                        </li>-->
<!--                        <li class=" ">
                           <a href="dashboard_v2.html" title="Dashboard v2">
                              <span>Dashboard v2</span>
                           </a>
                        </li>-->
<!--                        <li class=" ">
                           <a href="dashboard_v3.html" title="Dashboard v3">
                              <span>Dashboard v3</span>
                           </a>
                        </li>-->
                     </ul>
                  </li>
                  


                  
                  
               <li class=" ">
                     <a href="#maps" title="Maps" data-toggle="collapse">
                        <em class="icon-settings"></em>
                        <span data-localize="sidebar.nav.map.MAP">Settings</span>
                     </a>
                     <ul class="nav sidebar-subnav collapse" id="maps">
                        <li class="sidebar-subnav-header">Signup Email Template</li>

                        <li class=" ">
                           <a href="<?php echo base_url();?>client/Client/formsettings" title="Manage Title">
                              <span data-localize="sidebar.nav.map.GOOGLE">Manage Settings </span>
                           </a>
                        </li>

                        <li class=" ">
                           <a href="<?php echo base_url();?>client/Client/clientPlans" title="Manage Title">
                              <span data-localize="sidebar.nav.map.GOOGLE">Current Plan</span>
                           </a>
                        </li>
<!--                        <li class=" ">
                           <a href="<?php echo base_url();?>client/Client/updatePlan" title="Manage Title">
                              <span data-localize="sidebar.nav.map.GOOGLE">Update Plan </span>
                           </a>
                        </li>-->
                        
                        
                        
                        
                        <li class=" ">
                           <a href="<?php echo base_url();?>client/Client/templateview" title="Manage Title">
                              <span data-localize="sidebar.nav.map.GOOGLE">Templates </span>
                           </a>
                        </li>
                        
                     </ul>
                  </li>   
                  
                  
                  
                            
               <li class=" ">
                     <a href="#car" title="Maps" data-toggle="collapse">
                        <em class="fa fa-car"></em>
                        <span data-localize="sidebar.nav.map.MAP">Car Details</span>
                     </a>
                     <ul class="nav sidebar-subnav collapse" id="car">
                        <li class="sidebar-subnav-header">Signup</li>

                        
                           
                        <li class=" ">
                           <a href="<?php echo base_url();?>client/Client/createPriceMaster" title="Manage Title">
                              <span data-localize="sidebar.nav.map.GOOGLE">Duration Master </span>
                           </a>
                        </li>
                        <li class=" ">
                           <a href="<?php echo base_url();?>client/Client/carmonthView" title="Manage Title">
                              <span data-localize="sidebar.nav.map.GOOGLE"> Master View</span>
                           </a>
                        </li>
                        
                        
                        
                        <li class=" ">
                           <a href="<?php echo base_url();?>client/Client/addCarDetials" title="Manage Title">
                              <span data-localize="sidebar.nav.map.GOOGLE">Car Details </span>
                           </a>
                        </li>
                        <li class=" ">
                           <a href="<?php echo base_url();?>client/Client/viewcarDetail" title="Manage Title">
                              <span data-localize="sidebar.nav.map.GOOGLE">view Car details </span>
                           </a>
                        </li>
                        
                     </ul>
                  </li>   
                  
                  

                  <li class=" ">
                     <a href="#layout12" title="Layouts" data-toggle="collapse">
                        <em class="fa fa-key"></em>
                        <span>Permission</span>
                     </a>
                     <ul class="nav sidebar-subnav collapse" id="layout12">
                        <li class="sidebar-subnav-header">Layouts</li>
                        
                        
                         <li class=" ">
                           <a href="#blog" title="Blog" data-toggle="collapse">
                              <em class="fa fa-angle-right"></em>
                              <span>Role</span>
                           </a>
                           <ul class="nav sidebar-subnav collapse" id="blog">
                              <li class="sidebar-subnav-header">Blog</li>
                              <li class=" ">
  <a href="<?php echo base_url();?>client/Client/roleMaster" title="Horizontal">                                    <span>Create Role</span>
                                 </a>
                              </li>
                              <li class=" ">
                           <a href="<?php echo base_url();?>client/Client/viewRole" title="Horizontal">
                                    <span>View Role</span>
                                 </a>
                              </li>
                              
                           </ul>
                        </li>
                        
                        
                        
                        
                        
<!--                        <li class=" ">
                           <a href="<?php echo base_url();?>superadmin/SuperAdmin/viewRole" title="Horizontal">
                              <span>View Role</span>
                           </a>
                        </li>-->
                        <li class=" ">
                           <a href="<?php echo base_url();?>client/Client/rolePermission" title="Horizontal">
                              <span>Assign Permissions</span>
                           </a>
                        </li>
                     </ul>
                       
                       
                       
                  </li>
                  
                  
                    <li class=" ">
                     <a href="#layout1" title="Layouts" data-toggle="collapse">
                        <em class="fa fa-user"></em>
                        <span>Create User</span>
                     </a>
                     <ul class="nav sidebar-subnav collapse" id="layout1">
                        <li class="sidebar-subnav-header">Layouts</li>
                        <li class=" ">
                           <a href="<?php echo base_url();?>client/Client/createUser" title="Horizontal">
                              <span>Create User</span>
                           </a>
                        </li>
                        <li class=" ">
                           <a href="<?php echo base_url();?>client/Client/viewUser" title="Horizontal">
                              <span> List User</span>
                           </a>
                        </li>
                     </ul>
                  </li>      
                  
                     <li class=" ">
                     <a href="#sales" title="Layouts" data-toggle="collapse">
                        <em class="fa fa-user"></em>
                        <span>Sales Team</span>
                     </a>
                     <ul class="nav sidebar-subnav collapse" id="sales">
                        <li class="sidebar-subnav-header">Sales Team</li>
                        <li class=" ">
                           <a href="<?php echo base_url();?>client/Client/createTeam" title="Create Sales Team">
                              <span>Create Sales Team</span>
                           </a>
                        </li>
                        <li class=" ">
                           <a href="<?php echo base_url();?>client/Client/viewTeam" title="List Sales Team">
                              <span> List Sales Team</span>
                           </a>
                        </li>
                     </ul>
                  </li>   
                  
                     <li class=" ">
                     <a href="#salesmeeting" title="Layouts" data-toggle="collapse">
                        <em class="fa fa-meetup"></em>
                        <span>Meetings</span>
                     </a>
                     <ul class="nav sidebar-subnav collapse" id="salesmeeting">
                        <li class="sidebar-subnav-header">Meetings</li>
                        <li class=" ">
                           <a href="<?php echo base_url();?>client/Client/createMeeting" title="Create Meeting">
                              <span>Create Meeting</span>
                           </a>
                        </li>
                        <li class=" ">
                           <a href="<?php echo base_url();?>client/Client/viewMeeting" title="List Meeting">
                              <span> List Meeting</span>
                           </a>
                        </li>
                     </ul>
                  </li>   
                  
                  
                  
                        <li class=" ">
                     <a href="#leads" title="Layouts" data-toggle="collapse">
                        <em class="fa icon-rocket"></em>
                        <span>Leads</span>
                     </a>
                     <ul class="nav sidebar-subnav collapse" id="leads">
                        <li class="sidebar-subnav-header">Leads</li>
                        
                        <li class=" ">
                           <a href="<?php echo base_url();?>client/Client/manageLead" title="Lead Tags">
                              <span>Manage Lead Tags </span>
                           </a>
                        </li>
                        <li class=" ">
                           <a href="<?php echo base_url();?>client/Client/createLead" title="Lead">
                              <span>Create Lead</span>
                           </a>
                        </li>
                        <li class=" ">
                           <a href="<?php echo base_url();?>client/Client/viewLead" title="Lead">
                              <span> List Lead</span>
                           </a>
                        </li>
                        
                        
<!--                        <li class=" ">
                           <a href="<?php echo base_url();?>client/Client/createCall" title="Lead Calls">
                              <span>Lead  Calls</span>
                           </a>
                        </li>-->
<!--                        <li class=" ">
                           <a href="<?php echo base_url();?>client/Client/viewCall" title="Lead Calls">
                              <span> Leads Call View</span>
                           </a>
                        </li>-->
                        
                        
                        
                     </ul>
                  </li>   
                  
                    <li class=" ">
                     <a href="#Opportunities" title="Layouts" data-toggle="collapse">
                        <em class="fa icon-key"></em>
                        <span>Opportunities </span>
                     </a>
                     <ul class="nav sidebar-subnav collapse" id="Opportunities">
                        <li class="sidebar-subnav-header">Leads</li>
                        
                        <li class=" ">
                           <a href="<?php echo base_url();?>client/Client/manageStages" title="Lead Tags">
                              <span>Manage Stages </span>
                           </a>
                        </li>
                        
                        
                        <li class=" ">
                           <a href="<?php echo base_url();?>client/Client/createOpportunity" title="Lead">
                              <span>Create Opportunity</span>
                           </a>
                        </li>
                        <li class=" ">
                           <a href="<?php echo base_url();?>client/Client/viewOpportunity" title="Lead">
                              <span> List Opportunity</span>
                           </a>
                        </li>
                        
                        
                     
                        
                     </ul>
                     <ul class="nav sidebar-subnav collapse" id="Opportunities">
                        <!--<li class="sidebar-subnav-header">Lead Calls</li>-->
                       
                  </li>   
                </ul>  
                      
                      
                  
                  
                  
                  <li class=" ">
                     <a href="#logCall" title="Layouts" data-toggle="collapse">
                        <em class="fa fa-phone"></em>
                        <span>Log Calls</span>
                     </a>
                     <ul class="nav sidebar-subnav collapse" id="logCall">
                        <li class="sidebar-subnav-header">Log Calls</li>
                         <li class=" ">
                           <a href="<?php echo base_url();?>client/Client/createCall" title="Log  Calls">
                              <span>Create  Calls</span>
                           </a>
                        </li>
                        <li class=" ">
                           <a href="<?php echo base_url();?>client/Client/viewCall" title="Log Calls">
                              <span> Call View</span>
                           </a>
                        </li>
                     </ul>
                  </li>  
                  
                  
                    <li class=" ">
                     <a href="#Contract" title="Layouts" data-toggle="collapse">
                        <em class="fa fa-search-plus"></em>
                        <span>Contract</span>
                     </a>
                     <ul class="nav sidebar-subnav collapse" id="Contract">
                        <li class="sidebar-subnav-header">Quotations</li>
                         <li class=" ">
                           <a href="<?php echo base_url();?>client/Client/createContract" title="Contract">
                              <span>Create Contract</span>
                           </a>
                        </li>
                        <li class=" ">
                           <a href="<?php echo base_url();?>client/Client/viewContract" title="Contract">
                              <span>View Contract</span>
                           </a>
                        </li>
                     </ul>
                  </li>  
                  
                      <li class=" ">
                     <a href="<?php echo base_url();?>client/Client/eventAppointment" title="Dashboard">
                        <!--<div class="pull-right label label-info">3</div>-->
                        <em class="icon-speedometer"></em>
                        <span data-localize="sidebar.nav.DASHBOARD">Event And Appointment</span>
                     </a>
                     <ul class="nav sidebar-subnav collapse" id="dashboard">
                        <li class="sidebar-subnav-header">Event and Appointment</li>
<!--                        <li class=" active">
                           <a href="dashboard.html" title="Dashboard v1">
                              <span>Dashboard v1</span>
                           </a>
                        </li>-->
<!--                        <li class=" ">
                           <a href="dashboard_v2.html" title="Dashboard v2">
                              <span>Dashboard v2</span>
                           </a>
                        </li>-->
<!--                        <li class=" ">
                           <a href="dashboard_v3.html" title="Dashboard v3">
                              <span>Dashboard v3</span>
                           </a>
                        </li>-->
                     </ul>
                  </li>
               </ul>
               <!-- END sidebar nav-->
            </nav>
         </div>
         <!-- END Sidebar (left)-->
      </aside>
      <!-- offsidebar-->
      <aside class="offsidebar hide">
         <!-- START Off Sidebar (right)-->
         <nav>
            <div role="tabpanel">
               <!-- Nav tabs-->
               <ul class="nav nav-tabs nav-justified" role="tablist">
                  <li class="active" role="presentation">
                     <a href="#app-settings" aria-controls="app-settings" role="tab" data-toggle="tab">
                        <em class="icon-equalizer fa-lg"></em>
                     </a>
                  </li>
                  <li role="presentation">
                     <a href="#app-chat" aria-controls="app-chat" role="tab" data-toggle="tab">
                        <em class="icon-user fa-lg"></em>
                     </a>
                  </li>
               </ul>
               <!-- Tab panes-->
               <div class="tab-content">
                  <div class="tab-pane fade in active" id="app-settings" role="tabpanel">
                     <h3 class="text-center text-thin">Settings</h3>
                     <div class="p">
                        <h4 class="text-muted text-thin">Themes</h4>
                        <div class="table-grid mb">
                           <div class="col mb">
                              <div class="setting-color">
                                 <label data-load-css="css/theme-a.css">
                                    <input type="radio" name="setting-theme" checked="checked">
                                    <span class="icon-check"></span>
                                    <span class="split">
                                       <span class="color bg-info"></span>
                                       <span class="color bg-info-light"></span>
                                    </span>
                                    <span class="color bg-white"></span>
                                 </label>
                              </div>
                           </div>
                           <div class="col mb">
                              <div class="setting-color">
                                 <label data-load-css="css/theme-b.css">
                                    <input type="radio" name="setting-theme">
                                    <span class="icon-check"></span>
                                    <span class="split">
                                       <span class="color bg-green"></span>
                                       <span class="color bg-green-light"></span>
                                    </span>
                                    <span class="color bg-white"></span>
                                 </label>
                              </div>
                           </div>
                           <div class="col mb">
                              <div class="setting-color">
                                 <label data-load-css="<?php echo base_url();?>assets/css/theme-c.css">
                                    <input type="radio" name="setting-theme">
                                    <span class="icon-check"></span>
                                    <span class="split">
                                       <span class="color bg-purple"></span>
                                       <span class="color bg-purple-light"></span>
                                    </span>
                                    <span class="color bg-white"></span>
                                 </label>
                              </div>
                           </div>
                           <div class="col mb">
                              <div class="setting-color">
                                 <label data-load-css="<?php echo base_url();?>assets/css/theme-d.css">
                                    <input type="radio" name="setting-theme">
                                    <span class="icon-check"></span>
                                    <span class="split">
                                       <span class="color bg-danger"></span>
                                       <span class="color bg-danger-light"></span>
                                    </span>
                                    <span class="color bg-white"></span>
                                 </label>
                              </div>
                           </div>
                        </div>
                        <div class="table-grid mb">
                           <div class="col mb">
                              <div class="setting-color">
                                 <label data-load-css="<?php echo base_url();?>assets/css/theme-e.css">
                                    <input type="radio" name="setting-theme">
                                    <span class="icon-check"></span>
                                    <span class="split">
                                       <span class="color bg-info-dark"></span>
                                       <span class="color bg-info"></span>
                                    </span>
                                    <span class="color bg-gray-dark"></span>
                                 </label>
                              </div>
                           </div>
                           <div class="col mb">
                              <div class="setting-color">
                                 <label data-load-css="<?php echo base_url();?>assets/css/theme-f.css">
                                    <input type="radio" name="setting-theme">
                                    <span class="icon-check"></span>
                                    <span class="split">
                                       <span class="color bg-green-dark"></span>
                                       <span class="color bg-green"></span>
                                    </span>
                                    <span class="color bg-gray-dark"></span>
                                 </label>
                              </div>
                           </div>
                           <div class="col mb">
                              <div class="setting-color">
                                 <label data-load-css="<?php echo base_url();?>assets/css/theme-g.css">
                                    <input type="radio" name="setting-theme">
                                    <span class="icon-check"></span>
                                    <span class="split">
                                       <span class="color bg-purple-dark"></span>
                                       <span class="color bg-purple"></span>
                                    </span>
                                    <span class="color bg-gray-dark"></span>
                                 </label>
                              </div>
                           </div>
                           <div class="col mb">
                              <div class="setting-color">
                                 <label data-load-css="<?php echo base_url();?>assets/css/theme-h.css">
                                    <input type="radio" name="setting-theme">
                                    <span class="icon-check"></span>
                                    <span class="split">
                                       <span class="color bg-danger-dark"></span>
                                       <span class="color bg-danger"></span>
                                    </span>
                                    <span class="color bg-gray-dark"></span>
                                 </label>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="p">
                        <h4 class="text-muted text-thin">Layout</h4>
                        <div class="clearfix">
                           <p class="pull-left">Fixed</p>
                           <div class="pull-right">
                              <label class="switch">
                                 <input id="chk-fixed" type="checkbox" data-toggle-state="layout-fixed">
                                 <span></span>
                              </label>
                           </div>
                        </div>
                        <div class="clearfix">
                           <p class="pull-left">Boxed</p>
                           <div class="pull-right">
                              <label class="switch">
                                 <input id="chk-boxed" type="checkbox" data-toggle-state="layout-boxed">
                                 <span></span>
                              </label>
                           </div>
                        </div>
                        <div class="clearfix">
                           <p class="pull-left">RTL</p>
                           <div class="pull-right">
                              <label class="switch">
                                 <input id="chk-rtl" type="checkbox">
                                 <span></span>
                              </label>
                           </div>
                        </div>
                     </div>
                     <div class="p">
                        <h4 class="text-muted text-thin">Aside</h4>
                        <div class="clearfix">
                           <p class="pull-left">Collapsed</p>
                           <div class="pull-right">
                              <label class="switch">
                                 <input id="chk-collapsed" type="checkbox" data-toggle-state="aside-collapsed">
                                 <span></span>
                              </label>
                           </div>
                        </div>
                        <div class="clearfix">
                           <p class="pull-left">Collapsed Text</p>
                           <div class="pull-right">
                              <label class="switch">
                                 <input id="chk-collapsed-text" type="checkbox" data-toggle-state="aside-collapsed-text">
                                 <span></span>
                              </label>
                           </div>
                        </div>
                        <div class="clearfix">
                           <p class="pull-left">Float</p>
                           <div class="pull-right">
                              <label class="switch">
                                 <input id="chk-float" type="checkbox" data-toggle-state="aside-float">
                                 <span></span>
                              </label>
                           </div>
                        </div>
                        <div class="clearfix">
                           <p class="pull-left">Hover</p>
                           <div class="pull-right">
                              <label class="switch">
                                 <input id="chk-hover" type="checkbox" data-toggle-state="aside-hover">
                                 <span></span>
                              </label>
                           </div>
                        </div>
                        <div class="clearfix">
                           <p class="pull-left">Show Scrollbar</p>
                           <div class="pull-right">
                              <label class="switch">
                                 <input id="chk-hover" type="checkbox" data-toggle-state="show-scrollbar" data-target=".sidebar">
                                 <span></span>
                              </label>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="tab-pane fade" id="app-chat" role="tabpanel">
                     <h3 class="text-center text-thin">Connections</h3>
                     <ul class="nav">
                        <!-- START list title-->
                        <li class="p">
                           <small class="text-muted">ONLINE</small>
                        </li>
                        <!-- END list title-->
                        <li>
                           <!-- START User status-->
                           <a class="media-box p mt0" href="#">
                              <span class="pull-right">
                                 <span class="circle circle-success circle-lg"></span>
                              </span>
                              <span class="pull-left">
                                 <!-- Contact avatar-->
                                 <img class="media-box-object img-circle thumb48" src="<?php echo base_url();?>assets/img/user/05.jpg" alt="Image">
                              </span>
                              <!-- Contact info-->
                              <span class="media-box-body">
                                 <span class="media-box-heading">
                                    <strong>Juan Sims</strong>
                                    <br>
                                    <small class="text-muted">Designeer</small>
                                 </span>
                              </span>
                           </a>
                           <!-- END User status-->
                           <!-- START User status-->
                           <a class="media-box p mt0" href="#">
                              <span class="pull-right">
                                 <span class="circle circle-success circle-lg"></span>
                              </span>
                              <span class="pull-left">
                                 <!-- Contact avatar-->
                                 <img class="media-box-object img-circle thumb48" src="<?php echo base_url();?>assets/img/user/06.jpg" alt="Image">
                              </span>
                              <!-- Contact info-->
                              <span class="media-box-body">
                                 <span class="media-box-heading">
                                    <strong>Maureen Jenkins</strong>
                                    <br>
                                    <small class="text-muted">Designeer</small>
                                 </span>
                              </span>
                           </a>
                           <!-- END User status-->
                           <!-- START User status-->
                           <a class="media-box p mt0" href="#">
                              <span class="pull-right">
                                 <span class="circle circle-danger circle-lg"></span>
                              </span>
                              <span class="pull-left">
                                 <!-- Contact avatar-->
                                 <img class="media-box-object img-circle thumb48" src="<?php echo base_url();?>assets/img/user/07.jpg" alt="Image">
                              </span>
                              <!-- Contact info-->
                              <span class="media-box-body">
                                 <span class="media-box-heading">
                                    <strong>Billie Dunn</strong>
                                    <br>
                                    <small class="text-muted">Designeer</small>
                                 </span>
                              </span>
                           </a>
                           <!-- END User status-->
                           <!-- START User status-->
                           <a class="media-box p mt0" href="#">
                              <span class="pull-right">
                                 <span class="circle circle-warning circle-lg"></span>
                              </span>
                              <span class="pull-left">
                                 <!-- Contact avatar-->
                                 <img class="media-box-object img-circle thumb48" src="<?php echo base_url();?>assets/img/user/08.jpg" alt="Image">
                              </span>
                              <!-- Contact info-->
                              <span class="media-box-body">
                                 <span class="media-box-heading">
                                    <strong>Tomothy Roberts</strong>
                                    <br>
                                    <small class="text-muted">Designer</small>
                                 </span>
                              </span>
                           </a>
                           <!-- END User status-->
                        </li>
                        <!-- START list title-->
                        <li class="p">
                           <small class="text-muted">OFFLINE</small>
                        </li>
                        <!-- END list title-->
                        <li>
                           <!-- START User status-->
                           <a class="media-box p mt0" href="#">
                              <span class="pull-right">
                                 <span class="circle circle-lg"></span>
                              </span>
                              <span class="pull-left">
                                 <!-- Contact avatar-->
                                 <img class="media-box-object img-circle thumb48" src="<?php echo base_url();?>assets/img/user/09.jpg" alt="Image">
                              </span>
                              <!-- Contact info-->
                              <span class="media-box-body">
                                 <span class="media-box-heading">
                                    <strong>Lawrence Robinson</strong>
                                    <br>
                                    <small class="text-muted">Developer</small>
                                 </span>
                              </span>
                           </a>
                           <!-- END User status-->
                           <!-- START User status-->
                           <a class="media-box p mt0" href="#">
                              <span class="pull-right">
                                 <span class="circle circle-lg"></span>
                              </span>
                              <span class="pull-left">
                                 <!-- Contact avatar-->
                                 <img class="media-box-object img-circle thumb48" src="<?php echo base_url();?>assets/img/user/10.jpg" alt="Image">
                              </span>
                              <!-- Contact info-->
                              <span class="media-box-body">
                                 <span class="media-box-heading">
                                    <strong>Tyrone Owens</strong>
                                    <br>
                                    <small class="text-muted">Designer</small>
                                 </span>
                              </span>
                           </a>
                           <!-- END User status-->
                        </li>
                        <li>
                           <div class="p-lg text-center">
                              <!-- Optional link to list more users-->
                              <a class="btn btn-purple btn-sm" href="#" title="See more contacts">
                                 <strong>Load more..</strong>
                              </a>
                           </div>
                        </li>
                     </ul>
                     <!-- Extra items-->
                     <div class="p">
                        <p>
                           <small class="text-muted">Tasks completion</small>
                        </p>
                        <div class="progress progress-xs m0">
                           <div class="progress-bar progress-bar-success progress-80" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100">
                              <span class="sr-only">80% Complete</span>
                           </div>
                        </div>
                     </div>
                     <div class="p">
                        <p>
                           <small class="text-muted">Upload quota</small>
                        </p>
                        <div class="progress progress-xs m0">
                           <div class="progress-bar progress-bar-warning progress-40" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100">
                              <span class="sr-only">40% Complete</span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </nav>
         <!-- END Off Sidebar (right)-->
      </aside>
      
     