<link href="<?php echo base_url();?>multiple-select-master/multiple-select.css" rel="stylesheet"/>

   <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">

<?php $comp  = $this->session->userdata['companyid'];




?>

<style>
   
    .btn-group, .multiselect{width:100%}
    .multiselect-container
    {
     max-height: 300px;
     overflow-y: auto;
     overflow-x: hidden;
    }
</style>


<?php
if($id)
{
    
 
    
    
    
    $query = $this->db->get_where('tbllogcall', array('logcallid' => $id))->row_array();
  
  $formaction = "savelogCall";
  $method="edit";
  $button_name = "Update";
  $title = "Edit Calls";
}else
{
  
   $formaction = "savelogCall";
   $button_name = "Save";
   $method="create";
   
   $title = "Create Calls";
}

?>

      <section>
         <!-- Page content-->
         <div class="content-wrapper">
            <h3><?php echo $title;?>
               <!--<small>Validating forms frontend have never been so powerful and easy.</small>-->
            </h3>
            <!-- START row-->
            
            <!-- END row-->
            <!-- START row-->
            <div class="row">
               <div class="col-md-12">
                   <?php if($this->session->flashdata('permission_message'))
	 		{
                       
			?>
					
                        <div class="alert alert-block alert-success fade in" style="background-color:#3ec0e8">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p ><h4 style="background-color:#3ec0e8"> Successful!</h4> <?php echo $this->session->flashdata('permission_message'); ?></p>
                        </div>						
									
			<?php } ?>
            <?php if($this->session->flashdata('flash_message'))
	 		{
			?>
					
                        <div class="alert alert-block alert-success fade in" style="background-color:#ff708a">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p ><h4 style="background-color:#ff708a"> </h4> <?php echo $this->session->flashdata('flash_message'); ?></p>
                        </div>						
									
			<?php } ?>
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <div class="panel-title"><?php echo $title;?></div>
                        </div>
                        <div class="panel-body">
<!--                           <h4>Type validation</h4>-->
                                      <form class="form-horizontal" action="<?php echo base_url();?>client/Client/<?php echo $formaction;?>/<?php echo $method;?>/<?php echo $id;?>" method="post" enctype='multipart/form-data'>


<fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Date <span style="color:red">*</span></label>
                                 <div class="col-sm-6">
                                     <input class="form-control" placeholder="Select Date" type="text"  id="datetimepicker"  value="<?php echo $query['logcalldate'];?>" name="leadcall" data-validation="length" data-validation-length="min1" data-validation-error-msg="Date is required"  >
                                 </div>
                                 <input type="hidden" name="parameter" value="<?php echo $query['leadcallid'];?>">
                               
                              </div>
                           </fieldset>
                                          <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Call Summary <span style="color:red">*</span></label>
                                 <div class="col-sm-6">
                                     <input class="form-control" placeholder="Enter Call Summary" type="text"  name="summary" value="<?php echo $query['logcallsummary'];?>" data-validation="length" data-validation-length="min1" data-validation-error-msg="Call Summary is required" > 
                                 </div>
                                
                              </div>
                           </fieldset>
                                          <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Contact Number <span style="color:red">*</span></label>
                                 <div class="col-sm-6">
                                     <input class="form-control" placeholder="Enter Contact Number" type="text" onkeypress="return isNumber1(event)" name="contact" value="<?php echo $query['logcallcontact'];?>" data-validation="number" data-validation-length="min1" data-validation-error-msg="Valid contact number required" maxlength="10" minlength="10">
                                 </div>
                               
                              </div>
                           </fieldset>
                                  <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Responsible Person</label>
                                 <div class="col-sm-6">
                                     
                                     
                                 <select multiple="multiple"   name="member[]" style="width:90%" id="ert" >

                                <?php 
                                       
                                     $this->db->where('clientuserstatus',1);
                                    $this->db->where('clientcompid', $comp);
                                 
                                    $member = $this->db->get('tblclientuser');

                                
                                
                             if($method == "create")
                             {
                                foreach($member->result() as $k1=>$v1){?>

        <option value="<?php echo $v1->clientuserid;?>"  ><?php echo $v1->clientusername;?></option>
      
   
                                <?php } 
        
        } ?>
        
        
        
        
        <?php    $mem = explode(",",$query['logcallresponsible']) ;
             
                             if($method == "edit")
                             {
                                foreach($member->result() as $k1=>$v1)
                                    
                                    {
                                    
                                           $m =  $v1->clientuserid;
                                    
                                         if (in_array($m, $mem))
                                                                { ?>
        
                   <option value="<?php echo $v1->clientuserid;?>" <?php echo "selected";?> ><?php echo $v1->clientusername;?></option>

                                                

     <?php           }
                                                              else
                                                                { ?>
                                                               
                                                                  
           <option value="<?php echo $v1->clientuserid;?>"  ><?php echo $v1->clientusername;?></option>

                                                         <?php       }

                                           
                                           
                                           
                                   

                                    
                                     
                                       
                                        
                                    }
                                    
                                    
                                    ?>

                    
                                <?php } 
        
        ?>
        
        
        
        
        
        
        
        
                                                                       </select>
                                 </div>
                               
                              </div>
                           </fieldset>        
                                          
                                        
     
                           
                           
                           
                            <div class="panel-footer text-center">
                           <button class="btn btn-info" type="submit" style="margin-left: -193px;"><?php echo $button_name;?></button>
                        </div>
                          
                                      </form>
                         
                        </div>
                     
                     </div>
                     <!-- END panel-->
                  </form>
               </div>
            </div>
            <!-- END row-->
         </div>
      </section>
    <script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/datepicker/datepicker1/bootstrap-datetimepicker.css" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/datepicker/datepicker1/bootstrap-datetimepicker.min.css" type="text/javascript"></script>

     <script src="<?php echo base_url();?>assets/datepicker/datepicker1/js/bootstrap-datetimepicker.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/datepicker/datepicker1/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>

    
    
    
    
    
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"> </script>
                


<script>
                    
                      function isNumber(evt) {
            var iKeyCode = (evt.which) ? evt.which : evt.keyCode
            if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
                return false;

            return true;
        
                    }
                    
                    
                      function isNumber1(evt) {
            var iKeyCode = (evt.which) ? evt.which : evt.keyCode
            if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
                return false;

            return true;
        
                    }
                    
                    
                </script>
<script type="text/javascript">
    $(".datepicker").datetimepicker({format: 'yyyy-mm-dd hh:ii'});
</script>
<link href="<?php echo base_url();?>multiple-select-master/multiple-select.css" rel="stylesheet"/>


 <script src="<?php echo base_url();?>multiple-select-master/multiple-select.js"> </script>

    
    <script>
        $('#ert').multipleSelect({
          //  isOpen: true,
                //filter: true
                 selectAll: false
        });
    </script>