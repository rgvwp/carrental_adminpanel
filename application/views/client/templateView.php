<style>
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input {display:none;}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}


.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>


<?php

$compid = $this->session->userdata['companyid'];

?>



      <!-- Main section-->
      <section>
         <!-- Page content-->
         <div class="content-wrapper">
            <h3>Templates
             
            </h3>
            <div class="container-fluid">
             <?php if($this->session->flashdata('permission_message'))
	 		{
                       
			?>
					
                        <div class="alert alert-block alert-success fade in" style="background-color:#3ec0e8">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p ><h4 style="background-color:#3ec0e8"> Successful!</h4> <?php echo $this->session->flashdata('permission_message'); ?></p>
                        </div>						
									
			<?php } ?>
            <?php if($this->session->flashdata('flash_message'))
	 		{
			?>
					
                        <div class="alert alert-block alert-success fade in" style="background-color:#ff708a">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p ><h4 style="background-color:#ff708a"> Error!</h4> <?php echo $this->session->flashdata('flash_message'); ?></p>
                        </div>						
									
			<?php } ?>
               <div class="row">
                  <div class="col-lg-12">
                     <div class="panel panel-default">
                        <div class="panel-heading">Template List |
                        
                        </div>
                        <div class="panel-body">
                           <div class="table-responsive">
                              <table class="table table-striped table-hover" id="datatable1">
                                 <thead>
                                    <tr>
                                        <th>SNo.</th>
                                       <th>Template Name</th>
                                       <!--<th>Template Format</th>-->
                                       <!--<th>Status</th>-->
                                       <th>Action</th>
                                       
                                    </tr>
                                 </thead>
                                 <tbody>
                                 <?php  $i =1;
                                 $query = $this->db->get('tblcomptemplate');
                              
                               //  echo $this->db->last_query();
                                     foreach($query->result() as $k=>$vl)       {   ?>
                                    <tr class="gradeC">
                                        <td><?php echo $i;?></td>
                                       <td><?php echo $vl->comptemplatename;?></td>
                                       <!--<td><?php echo $vl->templatetitle;?></td>-->
                                      <?php $st =$vl->templatestatus;
                                      if($st == "1")
                                      { ?>
                                          <!--<td>Active</td>-->
                                 <?php     }
                                 else{?>
                                           <!--<td> In Active</td>-->
                                 <?php } ?>
<!-- <td><label class="switch"><input type="checkbox"<?php if($st == 1){echo checked;}?>>
                           <a href="<?php echo base_url();?>superadmin/SuperAdmin/changestatusUser/<?php echo $vl->comptemplateid;?>" <span class="slider round"></a></span></label></td>            
                             
                                       -->
                                       <th ><a href="<?php echo base_url();?>client/Client/updateTemplate/<?php echo $vl->comptemplateid;?>" ><i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i></a></th>
                                     </tr>
                                     <?php $i++; } ?>
                                 
                                
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
              