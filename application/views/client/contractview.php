   <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">


<style>
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input {display:none;}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}


.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style><!--<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
   <meta name="description" content="Bootstrap Admin App + jQuery">
   <meta name="keywords" content="app, responsive, jquery, bootstrap, dashboard, admin">
   <title>Angle - Bootstrap Admin Template</title>-->
   <!-- =============== VENDOR STYLES ===============-->
   <!-- FONT AWESOME-->
<!--   <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/fontawesome/css/font-awesome.min.css">
    SIMPLE LINE ICONS
   <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/simple-line-icons/css/simple-line-icons.css">
    ANIMATE.CSS
   <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/animate.css/animate.min.css">
    WHIRL (spinners)
   <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/whirl/dist/whirl.css">
    =============== PAGE VENDOR STYLES ===============-->
   <!-- DATATABLES-->
   <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/datatables-colvis/css/dataTables.colVis.css">
   <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/datatables/media/css/dataTables.bootstrap.css">
   <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/dataTables.fontAwesome/index.css">
   <!-- =============== BOOTSTRAP STYLES ===============-->
   <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.css" id="bscss">
   <!-- =============== APP STYLES ===============-->
   <link rel="stylesheet" href="<?php echo base_url();?>assets/css/app.css" id="maincss">
</head>

<body>
   <div class="wrapper">
      <!-- top navbar-->
      
      <!-- sidebar-->
      
      <!-- offsidebar-->
      
      <!-- Main section-->
      
      <section>
         <!-- Page content-->
         <div class="content-wrapper">
                
             <div class="alert alert-block alert-success fade in  messagestatus" style="background-color:#3ec0e8;display:none" >
                                <a class="close"  aria-hidden="true">&times;</a>
                                <h4>  <p id="message"></p> </h4>
                        </div>	
             
             
             
             
               <?php if($this->session->flashdata('permission_message'))
	 		{
			?>
					
                        <div class="alert alert-block alert-success fade in" style="background-color:#3ec0e8">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p ><h4 style="background-color:#3ec0e8"> Successful!</h4> <?php echo $this->session->flashdata('permission_message'); ?></p>
                        </div>						
									
			<?php } ?>
            <?php if($this->session->flashdata('flash_message'))
	 		{
			?>
					
                        <div class="alert alert-block alert-success fade in" style="background-color:#ff708a">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p ><h4 style="background-color:#ff708a"> Error!</h4> <?php echo $this->session->flashdata('flash_message'); ?></p>
                        </div>						
									
			<?php } ?>
            <h3>View Contract
               
            </h3>
             <div class="row">
					<div class="col-sm-3">
						<div class="form-group">
						  <label class="control-label">Start Date</label>
						  <div class="append-icon">
						    <input type="text" id="datetimepicker"  onblur="findResult();" name="min" class="date-picker   startdate form-control hasDatepicker">
						    <!--<i class="icon-calendar"></i>-->
						  </div>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="form-group">
						  <label class="control-label">End Date</label>
						  <div class="append-icon">
						    <input type="text" id="datetimepicker1"   onblur="findResult();"  name="max" class="date-picker enddate       form-control hasDatepicker">
						    <!--<i class="icon-calendar"></i>-->
						  </div>
						</div>
					</div>
				</div>
            <div class="container-fluid">
               <!-- START DATATABLE 1-->
               <div class="row">
                  <div class="col-lg-12">
                     <div class="panel panel-default">
<!--                        <div class="panel-heading">Data Tables |
                           <small>Zero Configuration + Export Buttons</small>
                        </div>-->

                        <div class="panel-body">
<!--                                                    <a href="<?php echo base_url();?>superadmin/SuperAdmin/roleMaster" title="Horizontal">
                                                        <button type="button" class="btn btn-primary">Create Role</button></a>-->
                           <div class="table-responsive">
                              <table class="table table-striped table-hover" id="datatable1">
                                 <thead>
                                    <tr>
                                        <th style ="margin-right:58px">S.No</th>
                                       <th>Start Date</th>
                                        <th>End Date</th>
                                        <th> Contact</th>
                                         <th>Responsible</th>
                                         <th>Document</th>
                                         <th>Status</th>
                                       <th>Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                     
                                     
                                     <?php
                                     $comp = $this->session->userdata('companyid');
                                     
                                     $query = $this->db->get_where('tblcontract',array('clientcompid'=>$comp));
                                     $i=1;
                                     foreach($query->result() as $k=>$vl)
                                     {?>
                                    <tr class="gradeX">
                                        <td style ="margin-right:58px;"><?php echo $i;?></td>
                                        <td><?php echo $vl->contractstartdate;?></td>
                                        <td><?php echo $vl->contractenddate;?>	</td>
                                        <td><?php echo $vl->contractcontact;?></td>
                                          
                                              
                                       <?php
                                       $k2="";
                                       
                                       $member = explode(",",$vl->contractresponsible);
                                       
                                       foreach($member as $t=>$r) {
                                   
                                       $query123 = $this->db->get_where('tblclientuser', array('clientuserid' => $r))->row_array();
                                          
                                       $name = $query123['clientusername'];
                                       
                                       
                                     $k2 = $k2.','."$name";         
                                       }
                                       ?>
                                        
                                       
                                         <td><?php echo $str = ltrim($k2, ',');?></td>
                                        
                                        
                                        
                                         <td>
                                           <?php $doc = $vl->contractdocument;
                                           
                                             if($doc !=""){?>
                                           <a href="<?php echo base_url();?>uploads/document/<?php echo $vl->contractdocument;?>" download><h6 style="color: blue; font-size: 15px;">Document </h6></a></td>
                                             <?php } else {?>
                                       
                                  <a  ><h6 style="color: red; font-size: 15px;">Not Uploaded </h6></a></td>

                                             <?php } ?>
                                        
                                        <?php $st = $vl->contractstatus ; ?>
                                        
                             <td><label class="switch"><input type="checkbox"<?php if($st == 1){echo checked;}?>>
                           <a  onclick="changeStatus('<?php echo $vl->contractid;?>');" <span class="slider round"></a></span></label></td>            
                           
                                        
                             <td ><a href="<?php echo base_url();?>client/Client/createContract/<?php echo $vl->contractid;?>" ><i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i></a>
                  
                                 
                           <a href="<?php echo base_url();?>client/Client/saveContract/delete/<?php echo $vl->contractid?>"   onclick="return confirm('Are you sure want to delete?');"><i class="fa fa-trash-o fa-2x" aria-hidden="true" style="color:#f31a04;"></i></a></td>

                                    </tr>
                                  
                                     <?php $i++; } 
                                     
                                     
                                     
                                     
                                     
                                     ?>
                                  
                              
                                 
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- END DATATABLE 1-->
               <!-- START DATATABLE 2-->
               
               <!-- END DATATABLE 2-->
               <!-- START DATATABLE 3-->
               
               <!-- END DATATABLE 3-->
               
            </div>
         </div>
      </section>
      <!-- Page footer-->
      
   </div>
   <!-- =============== VENDOR SCRIPTS ===============-->
   <!-- MODERNIZR-->
   <script src="<?php echo base_url();?>assets/vendor/modernizr/modernizr.custom.js"></script>
   <!-- MATCHMEDIA POLYFILL-->
   <script src="<?php echo base_url();?>assets/vendor/matchMedia/matchMedia.js"></script>
   <!-- JQUERY-->
   <script src="<?php echo base_url();?>assets/vendor/jquery/dist/jquery.js"></script>
   <!-- BOOTSTRAP-->
   <script src="<?php echo base_url();?>assets/vendor/bootstrap/dist/js/bootstrap.js"></script>
   <!-- STORAGE API-->
   <script src="<?php echo base_url();?>assets/vendor/jQuery-Storage-API/jquery.storageapi.js"></script>
   <!-- JQUERY EASING-->
   <script src="<?php echo base_url();?>assets/vendor/jquery.easing/js/jquery.easing.js"></script>
   <!-- ANIMO-->
   <script src="<?php echo base_url();?>assets/vendor/animo.js/animo.js"></script>
   <!-- SLIMSCROLL-->
   <script src="<?php echo base_url();?>assets/vendor/slimScroll/jquery.slimscroll.min.js"></script>
   <!-- SCREENFULL-->
   <script src="<?php echo base_url();?>assets/vendor/screenfull/dist/screenfull.js"></script>
   <!-- LOCALIZE-->
   <script src="<?php echo base_url();?>assets/vendor/jquery-localize-i18n/dist/jquery.localize.js"></script>
   <!-- RTL demo-->
   <script src="<?php echo base_url();?>assets/js/demo/demo-rtl.js"></script>
   <!-- =============== PAGE VENDOR SCRIPTS ===============-->
   <!-- DATATABLES-->
   <script src="<?php echo base_url();?>assets/vendor/datatables/media/js/jquery.dataTables.min.js"></script>
   <script src="<?php echo base_url();?>assets/vendor/datatables-colvis/js/dataTables.colVis.js"></script>
   <script src="<?php echo base_url();?>assets/vendor/datatables/media/js/dataTables.bootstrap.js"></script>
<!--   <script src="<?php echo base_url();?>assets/vendor/datatables-buttons/js/dataTables.buttons.js"></script>-->
   <!--<script src="<?php echo base_url();?>assets/vendor/datatables-buttons/js/buttons.bootstrap.js"></script>-->
   <script src="<?php echo base_url();?>assets/vendor/datatables-buttons/js/buttons.colVis.js"></script>
   <!--<script src="<?php echo base_url();?>assets/vendor/datatables-buttons/js/buttons.flash.js"></script>-->
   <script src="<?php echo base_url();?>assets/vendor/datatables-buttons/js/buttons.html5.js"></script>
   <script src="<?php echo base_url();?>assets/vendor/datatables-buttons/js/buttons.print.js"></script>
   <script src="<?php echo base_url();?>assets/vendor/datatables-responsive/js/dataTables.responsive.js"></script>
   <script src="<?php echo base_url();?>assets/vendor/datatables-responsive/js/responsive.bootstrap.js"></script>
   <script src="<?php echo base_url();?>assets/js/demo/demo-datatable.js"></script>
   <!-- =============== APP SCRIPTS ===============-->
   <script src="<?php echo base_url();?>assets/js/app.js"></script>
</body>

</html>


<script>
    
    $(".close").click(function(){
   // alert("The paragraph was clicked.");
    $(".messagestatus").hide();
});
    
  function changeStatus(val)
    {
        
       
        
      
        
                 // $(".close").setAttribute("aria-hidden", "true");

        
     //alert(val);
        
          $.ajax({
          url: '<?php echo base_url();?>client/Client/searchContract',
          type : "POST",
         
          data : {"statusid" :val},
          success : function(data) {
            
           alert(data);
           // console.log(data);
            if(data == "active"){
               //   alert('active');
                $(".messagestatus").show();
             $("#message").html("Status active Successfully");
            }
        
            if(data == "inactive"){
                // alert('inactive');
                $(".messagestatus").show();
             $("#message").html("Status inactive Successfully");
            }
        
        
            
          }
        
          
      });
      
      
    }
    
    
    </script>
    <script>
        
        function findResult()
        {
           var s = $(".startdate").val();
           var e = $(".enddate").val();
            $.ajax({
          url: '<?php echo base_url();?>client/Client/searchContract',
          type : "POST",
         
          data : {"startdate" :s,"enddate":e},
          success : function(data) {
            
           //alert(data);
           console.log(data);
          
          $("#datatable1").html(data);
         
        
            
          }
        
          
      });
            
        }
        
        
        
        </script>