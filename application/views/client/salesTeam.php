<link href="<?php echo base_url();?>multiple-select-master/multiple-select.css" rel="stylesheet"/>


<?php $comp  = $this->session->userdata['companyid'];

?>

<style>
   
    .btn-group, .multiselect{width:100%}
    .multiselect-container
    {
     max-height: 300px;
     overflow-y: auto;
     overflow-x: hidden;
    }
</style>


 <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/chosen_v1.2.0/chosen.min.css">
   <!-- DATETIMEPICKER-->
   <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">
   
<?php



if($id)
{
    
   $query = $this->db->get_where('tblsaleteam', array('salesteamid' => $id))->row_array();
  
  $formaction = "saveTeam";
  $method="edit";
  $button_name = "Update";
  $title = "Edit User";
}else
{
  
   $formaction = "saveTeam";
   $button_name = "Save";
   $method="create";
    $title = "Sales Team";
}

?>

      <section>
         <!-- Page content-->
         <div class="content-wrapper">
            <h3><?php echo $title;?>
               <!--<small>Validating forms frontend have never been so powerful and easy.</small>-->
            </h3>
            <!-- START row-->
            
            <!-- END row-->
            <!-- START row-->
            <div class="row">
               <div class="col-md-12">
                   <?php if($this->session->flashdata('permission_message'))
	 		{
                       
			?>
					
                        <div class="alert alert-block alert-success fade in" style="background-color:#3ec0e8">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p ><h4 style="background-color:#3ec0e8"> Successful!</h4> <?php echo $this->session->flashdata('permission_message'); ?></p>
                        </div>						
									
			<?php } ?>
            <?php if($this->session->flashdata('flash_message'))
	 		{
			?>
					
                        <div class="alert alert-block alert-success fade in" style="background-color:#ff708a">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p ><h4 style="background-color:#ff708a"> Error!</h4> <?php echo $this->session->flashdata('flash_message'); ?></p>
                        </div>						
									
			<?php } ?>
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <div class="panel-title"><?php echo $title;?></div>
                        </div>
                        <div class="panel-body">
<!--                           <h4>Type validation</h4>-->
                                      <form class="form-horizontal" action="<?php echo base_url();?>client/Client/<?php echo $formaction;?>/<?php echo $method;?>/<?php echo $id;?>" method="post" enctype='multipart/form-data'>

        
<fieldset>
                              <div class="form-group">
                                 <label class="col-sm-3 control-label">Sales Team Name <span style="color:red">*</span></label>
                                 <div class="col-sm-6">
                                     
                                     <input style="width:100%" class="form-control" type="text" value="<?php echo $query['saleteamname'];?>" placeholder="Enter Sales Team Name" name="salesteam" data-validation="length" data-validation-length="min1" data-validation-error-msg="Team Name is required"  >
 
                                     
                                 </div>
                               
                              </div>
                           </fieldset>
                                          <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-3 control-label">Team Leader <span style="color:red">*</span></label>
                                 <div class="col-sm-6">
                                <?php   $ap = $query['saleteamleader']; ?>
                                <select style="width:100%" name="leader" class="form-control"   style="width:300px"  data-validation="length" data-validation-length="min1" data-validation-error-msg="Team Leader is required" >
                                 
                                     <option value="">Select Team Leader</option>
                                   <?php  
                                   
                                   
                                   $this->db->where('clientuserstatus',1);
                                    $this->db->where('clientcompid', $comp);
                                 
                                    $teamquery = $this->db->get('tblclientuser');

                                   
                                   
                                   
                                   
                                   
                                   
                                   
//                                   $teamquery = $this->db->get('tblclientuser',array("clientuserstatus"=>1)); 
                                 
                                   foreach($teamquery->result() as $k=>$v){
                                   $y12 = $v->clientuserid;      
                                   
                                  ?>
                                      
                                      
                                    <option value="<?php echo $v->clientuserid; ?>"  <?php if ($ap == $y12 ) echo 'selected' ; ?>><?php echo $v->clientusername;?></option>
                                   <?php } ?>

                                  </select>   
                                 
                                 </div>
                               
                              </div>
                           </fieldset>
                                          
                                          <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-3 control-label">Team Members</label>
                                 <div class="col-sm-6">
                                     
                                     
                               <select multiple="multiple"   name="member[]" style="width:90%" id="ert">

                                <?php 
                                       
                                     $this->db->where('clientuserstatus',1);
                                    $this->db->where('clientcompid', $comp);
                                 
                                    $member = $this->db->get('tblclientuser');

                                
                                
                             if($method == "create")
                             {
                                foreach($member->result() as $k1=>$v1){?>

        <option value="<?php echo $v1->clientuserid;?>"  ><?php echo $v1->clientusername;?></option>
      
   
                                <?php } 
        
        } ?>
        
        
        
        
        <?php    $mem = explode(",",$query['salesteamusersid']) ;
             
                             if($method == "edit")
                             {
                                foreach($member->result() as $k1=>$v1)
                                    
                                    {
                                    
                                           $m =  $v1->clientuserid;
                                    
                                         if (in_array($m, $mem))
                                                                { ?>
        
                   <option value="<?php echo $v1->clientuserid;?>" <?php echo "selected";?> ><?php echo $v1->clientusername;?></option>

                                                

     <?php           }
                                                              else
                                                                { ?>
                                                               
                                                                  
           <option value="<?php echo $v1->clientuserid;?>"  ><?php echo $v1->clientusername;?></option>

                                                         <?php       }

                                           
                                           
                                           
                                   

                                    
                                     
                                       
                                        
                                    }
                                    
                                    
                                    ?>

                    
                                <?php } 
        
        ?>
        
        
        
        
        
        
        
        
                                                                       </select>
                                 </div>
                               
                              </div>
                           </fieldset>
                                          
                                          <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-3 control-label">Notes</label>
                                 <div class="col-sm-6">

                                     <textarea style="width:100%"  rows="5" cols="80" name="note" placeholder="Enter Notes"><?php echo $query['salesteamnotes'];?></textarea>

                                 </div>
                                
                              </div>
                           </fieldset>
                                                                  
                                          
                                          
                                          
                                          
                                          
                                        
                                          
<!--                                          <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Approval</label>
                                 <div class="col-sm-6">
                                  <select name="aprstatus"  class="form-control" data-validation="length" data-validation-length="min1" data-validation-error-msg="Status is requirred" >
                                  <?php $ap =  $query['clientcompapproval'];?>
                                      <option value=" ">Select Status</option>
                                    <option value="1"  <?php if ($ap == 1 ) echo 'selected' ; ?>>Approved</option>
                                    <option value="0"  <?php if ($st == 0 ) echo 'selected' ; ?>>Pending</option>
                                    <option value="0"  <?php if ($ap == 0 ) echo 'selected' ; ?>>Not Approved</option>

                                  </select>    
                                 </div>
                               
                              </div>
                           </fieldset>-->
                                         
<!--                                 <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Role</label>
                                 <div class="col-sm-6">
                                     <?php
                                     
                                     $rolequery = $this->db->get_where('tblmasterrole', array('masterrolestatus' => 1));

                                     
                                     
                                    $st = $query['supadmuserrole'] ; ?>
                                    <select name="role"  class="form-control" data-validation="length" data-validation-length="min1" data-validation-error-msg="Role is requirred" >
                                    <option value=" ">Select Role</option>
                                    <?php foreach($rolequery->result() as $k=>$vl){
                                        
                                        $r =$vl->masterroleid;
                                        
                                        ?>
                                    <option value="<?php echo $vl->masterroleid;?>"  <?php if ($st == $r ) echo 'selected' ; ?>><?php echo $vl->masterrolename;?></option>
                                    <?php }?>
                                    <option value="0"  <?php if ($st == 0 ) echo 'selected' ; ?>>Inactive</option>
                                  </select>
                                 </div>
                                 
                              </div>
                           </fieldset>   
                                                   -->
                                          
                           
<!--                           <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Status</label>
                                 <div class="col-sm-6">
                                     <?php $st = $query['supadmuserstatus'] ; ?>
                                    <select name="status"  class="form-control" data-validation="length" data-validation-length="min1" data-validation-error-msg="Status is requirred" >
                                    <option value=" ">Select Status</option>
                                    <option value="1"  <?php if ($st == 1 ) echo 'selected' ; ?>>Active</option>
                                    <option value="0"  <?php if ($st == 0 ) echo 'selected' ; ?>>Inactive</option>
                                  </select>
                                 </div>
                                 
                              </div>
                           </fieldset>
                           -->
                           
                           
                           
                            <div class="panel-footer text-center">
                           <button class="btn btn-info" type="submit" style="margin-left: -193px;"><?php echo $button_name;?></button>
                        </div>
                          
                                      </form>
                         
                        </div>
                     
                     </div>
                     <!-- END panel-->
                  </form>
               </div>
            </div>
            <!-- END row-->
         </div>
      </section>



<!--<script src="<?php echo base_url();?>assets/vendor/moment/min/moment-with-locales.min.js"></script>-->
   <!--<script type="text/javascript" src=".<?php echo base_url();?>assets/vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>-->
   
    <script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js" type="text/javascript"></script>
       
<!--     <script src="<?php echo base_url();?>assets/datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
 

    <script>
        
        $('.datepicker').datepicker();
        </script>
    -->
                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"> </script>

                        <script src="<?php echo base_url();?>multiple-select-master/multiple-select.js"> </script>

    
    <script>
        $('#ert').multipleSelect({
          //  isOpen: true,
                //filter: true
                 selectAll: false
        });
    </script>
    
    
    
    
    
    
                


<script>
    
    
    
    
    
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah')
                    .attr('src', e.target.result)
                    .width(80)
                    .height(80);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    
    </script>
    
     <script>
                    
                      function isNumber1(evt) {
            var iKeyCode = (evt.which) ? evt.which : evt.keyCode
            if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
                return false;

            return true;
        
                    }
                    
                    
                </script>
                
                <script>
                    function passSubmit()
                    {
                    alert();
         $('#passForm').submit();
                  
                        
                    }
                    </script>
                    
                    
                    
<!--                    <script type="text/javascript">
        $(function () {
            $('#e2').multiselect({
                includeSelectAllOption: true
            });
             $('#e3').multiselect({
                includeSelectAllOption: true
            });
            
           
        });
    </script>-->