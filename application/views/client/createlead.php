   <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">

<link href="<?php echo base_url();?>multiple-select-master/multiple-select.css" rel="stylesheet"/>
  <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/datatables/media/css/dataTables.bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/datatables-colvis/css/dataTables.colVis.css">
   <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/dataTables.fontAwesome/index.css">


<link href="<?php echo base_url();?>multiple-select-master/multiple-select.css" rel="stylesheet"/>


<?php $comp  = $this->session->userdata['companyid'];




?>

<style>
   .fileinline
   {
       display:inline-block !important;
   }
   .addbutton
   {
   display: inline-block;
    padding: 6px 15px;
    font-size: 13px;
    vertical-align: middle;
    margin-left: 20px;
    background: #46c3e9;
    color: #fff;
   }
   .addbutton:hover,.addbutton:focus,.addbutton:active:focus{
       text-decoration: none;
       color:#000;
       background: #ddd;
   }
   
   
   
   
   
   .imagemenu
   {
       margin:0;
       padding:0;
   }
   .imagemenu ul
   {
       margin:0;
       padding:0;
   }
   .imagemenu ul li
   {
     display: inline-block;
    margin-left: 23px;
    border: 1px solid #ddd;
    padding: 10px;
   }
   .imagemenu ul li img
   {
       width:150px;
       
   }
    .imagemenu ul li a
   {
    display: inline-block;
    padding: 5px 10px;
    font-size: 13px;
    vertical-align: top;
    margin-left: 10px;
    background: #46c3e9;
    color: #fff;
    line-height: 1;
   }
    .imagemenu ul li a:hover
    {
        text-decoration: none;
    }
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
    .btn-group, .multiselect{width:100%}
    .multiselect-container
    {
     max-height: 300px;
     overflow-y: auto;
     overflow-x: hidden;
    }
</style>


<?php
if($id)
{
    
   $query = $this->db->get_where('tblleads', array('leadid' => $id))->row_array();
   
  
  
  $formaction = "saveLead";
  $method="edit";
  $button_name = "Update";
}else
{
  
   $formaction = "saveLead";
   $button_name = "Save";
   $method="create";
}

?>

      <section>
         <!-- Page content-->
         <div class="content-wrapper">
            <h3>Create Lead
               <!--<small>Validating forms frontend have never been so powerful and easy.</small>-->
            </h3>
            <!-- START row-->
            
            <!-- END row-->
            <!-- START row-->
            
         
            
            <div class="row">
               <div class="col-md-12">
                   
                 
                   <?php if($this->session->flashdata('permission_message'))
	 		{
                       
			?>
					
                        <div class="alert alert-block alert-success fade in" style="background-color:#3ec0e8">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p ><h4 style="background-color:#3ec0e8"> Successful!</h4> <?php echo $this->session->flashdata('permission_message'); ?></p>
                        </div>						
									
			<?php } ?>
            <?php if($this->session->flashdata('flash_message'))
	 		{
			?>
					
                        <div class="alert alert-block alert-success fade in" style="background-color:#ff708a">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p ><h4 style="background-color:#ff708a"> Error!</h4> <?php echo $this->session->flashdata('flash_message'); ?></p>
                        </div>						
									
			<?php } ?>
                   
                   
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <div class="panel-title">Create Lead</div>
                        </div>
                         
                         
          <?php if($method == "edit") { ?>
                         <button type="button"  id="demo" style="margin-left: 897px;"  onclick= "test123('<?php echo $id;?>');" class="btn btn-primary"> Create Call</button>
                       
                       <?php } ?>                    
                         
                         
                         
                        <div class="panel-body">
<!--                           <h4>Type validation</h4>-->
                                      <form class="form-horizontal" action="<?php echo base_url();?>client/Client/<?php echo $formaction;?>/<?php echo $method;?>/<?php echo $id;?>" method="post" enctype='multipart/form-data'>

                                          <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Opportunity</label>
                                 <div class="col-sm-6">
                                     <input class="form-control" type="text" value="<?php echo $query['leadoppurtunity'];?>" name="opportunity" data-validation="length" data-validation-length="min1" data-validation-error-msg="Opportunityis requirred"  >
                                 </div>
                                 <input type="hidden" name="parameter" value="<?php echo $parameter;?>">
                               
                              </div>
                           </fieldset>

<!--<fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Company Name</label>
                                 <div class="col-sm-6">
                                     <input class="form-control" type="text" value="<?php echo $query['clientcompname'];?>" name="companyname" data-validation="length" data-validation-length="min1" data-validation-error-msg="Company Name is requirred"  >
                                 </div>
                                 <input type="hidden" name="parameter" value="<?php echo $parameter;?>">
                               
                              </div>
                           </fieldset>-->
<!--                                          <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Company Code</label>
                                 <div class="col-sm-6">
                                     <input class="form-control" type="text"  name="companycode" value="<?php echo $query['clientcompcode'];?>" data-validation="length" data-validation-length="min1" data-validation-error-msg="Company Code is requirred" > 
                                 </div>
                                
                              </div>
                           </fieldset>-->
<!--                                          <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label"> Address</label>
                                 <div class="col-sm-6">
                                     <input class="form-control" type="text"  name="address" value="<?php echo $query['clientcompaddress'];?>" data-validation="length" data-validation-length="min1" data-validation-error-msg="Company Address is requirred" >
                                 </div>
                               
                              </div>
                           </fieldset>-->
<?php if($method == "create"){?>
                                          
                                          <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Sales Person</label>
                                 <div class="col-sm-6">
                                     
                                <select multiple="multiple"   name="saleperson[]" style="width:500px" id="ert" >  
                                 <!--<option value="">Select Sales Person </option>-->

                                  <?php  
                                   
                                  
                                  
                                    $this->db->where('clientcompid', $comp);
                                 
                                    $teamquery1 = $this->db->get('tblsaleteam');

                                   foreach($teamquery1->result() as $k=>$v1){
                                
                                       $x =$v1->salesteamusersid;
                                       $arr = $arr.','.$x;
                                    }   
                                      $teammembers = trim($arr,",");
                                      $y = explode(",",$teammembers);
                                      $teamarr = array_unique($y);
                                      
                                     foreach($teamarr as $row){
                                         
                                         $usename = $this->db->get_where('tblclientuser',array('clientuserid'=>$row));
                                         
                                       
                                         
                                          foreach($usename->result() as $h=>$r)  {
                                              
                                              $y12 = $r->clientuserid;?>
                                 
                                 <option value="<?php echo $r->clientuserid;?>"    <?php if ($ap == $y12 ) echo 'selected' ; ?>><?php echo $r->clientusername;?></option>

                                        <?php  }
                                     } 
                                      
                                  ?>   
                              
                                   
                                </select> 
                                 </div>
                                
                              </div>
                           </fieldset>


<?php } ?>

<?php if($method == "edit"){
    
    $personarr = explode(",",$query['leadsalespersonid']);
    
    
    ?>

     <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Sales Person</label>
                                 <div class="col-sm-6">
                                     
                                <select multiple="multiple"   name="saleperson[]" style="width:518px" id="ert" >  
                                 <!--<option value="">Select Sales Person </option>-->

                                  <?php  
                                   
                                  
                                  
                                    $this->db->where('clientcompid', $comp);
                                 
                                    $teamquery1 = $this->db->get('tblsaleteam');

                                   foreach($teamquery1->result() as $k=>$v1){
                                
                                       $x =$v1->salesteamusersid;
                                       $arr = $arr.','.$x;
                                    }   
                                      $teammembers = trim($arr,",");
                                      $y = explode(",",$teammembers);
                                      $teamarr = array_unique($y);
                                      
                                     foreach($teamarr as $row){
                                         
                                         $usename = $this->db->get_where('tblclientuser',array('clientuserid'=>$row));
                                         
                                       
                                         
                                          foreach($usename->result() as $h=>$r) {
                                              
                                              $y12 = $r->clientuserid;
                                              
                                             if (in_array($y12, $personarr))
                                                        { ?>
                                 
         <option value="<?php echo $r->clientuserid;?>"    <?php  echo 'selected' ; ?>><?php echo $r->clientusername;?></option>

                                                     <?php   }
                                                      else
                                                        { ?>
         
         
                                          <option value="<?php echo $r->clientuserid;?>"  ><?php echo $r->clientusername;?></option>

                                                       <?php }

                                              ?>
                                 
                                 
                                 

                                        <?php  }
                                     } 
                                      
                                  ?>   
                              
                                   
                                </select> 
                                 </div>
                                
                              </div>
                           </fieldset>
    
<?php } ?>

















  <fieldset>   
 <div class="form-group">
                                 <label class="col-sm-2 control-label">Sales Team</label>
                                 <div class="col-sm-6">
                                     
                                     <?php  $ap =   $query['leadteamid']    ;                         ?>
                                    <select name="teamid" class="form-control"   style="width:518px"  data-validation="length" data-validation-length="min1" data-validation-error-msg="Sales Team is requirred" >
                                 
                                     <option value="">Select Team </option>
                                    <?php  
                                   $this->db->where('clientcompid', $comp);
                                   $teamquery = $this->db->get('tblsaleteam');
                                   foreach($teamquery->result() as $k=>$v){
                                   $y12 = $v->salesteamid;      
                                   
                                   ?>
                                    <option value="<?php echo $v->salesteamid; ?>"  <?php if ($ap == $y12 ) echo 'selected' ; ?>><?php echo $v->saleteamname;?></option>
                                   <?php } ?>

                                  </select>   
                                   </div>
                               
                              </div>
                           </fieldset>  


                                 

<fieldset>   
 <div class="form-group">
                                 <label class="col-sm-2 control-label">Sales Brand</label>
                                 <div class="col-sm-6">
                                     
                                     <?php                          ?>
                                    <select name="brand" class="form-control"   style="width:518px"  data-validation="length" data-validation-length="min1" data-validation-error-msg="Brand Name is requirred" >
                                 
                                     <option value="">Select Brand </option>
                                    <?php  
                                   $editbrandid = $query['leadbrand'];
                                   $brandquery = $this->db->get('tblbrand');
                                   foreach($brandquery->result() as $k=>$v){
                                   $y12 = $v->brandid;      
                                   
                                   ?>
                                    <option value="<?php echo $v->brandid; ?>"  <?php if ($editbrandid == $y12 ) echo 'selected' ; ?>><?php echo $v->brandname;?></option>
                                   <?php } ?>

                                  </select>   
                                   </div>
                               
                              </div>
                           </fieldset>  






































                            <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Customer</label>
                                 <div class="col-sm-6">
                                     <input class="form-control " type="text"   value="<?php echo $query['leadcustomerid'];?>" name="customer"  data-validation="length" data-validation-length="min1" data-validation-error-msg="Customer is requirred">
                                 </div>
                               
                              </div>
                           </fieldset>   

<fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Km Requested</label>
                                 <div class="col-sm-6">
                                     <input class="form-control " type="text"   value="<?php echo $query['leadkmrequested'];?>" name="kmrequested"  data-validation="length" data-validation-length="min1" data-validation-error-msg="Kilometer requested is requirred">
                                 </div>
                               
                              </div>
                           </fieldset>   

<fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Car Model</label>
                                 <div class="col-sm-6">
                                     <input class="form-control " type="text"   value="<?php echo $query['leadmodel'];?>" name="model"  data-validation="length" data-validation-length="min1" data-validation-error-msg="Model is requirred">
                                 </div>
                               
                              </div>
                           </fieldset> 




 <div class="form-group">
                                 <label class="col-sm-2 control-label">Rental Duration</label>
                                 <div class="col-sm-6">
                                     <input class="form-control " type="text"   value="<?php echo $query['leadrentduration'];?>" name="rentalDuration" data-validation="length" data-validation-length="min1" data-validation-error-msg="Rental Duration is requirred">
                                 </div>
                               
                              </div>
                           </fieldset> 
                           
                            <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Email</label>
                                 <div class="col-sm-6">
                                     <input class="form-control " type="email"   value="<?php echo $query['leadcustomeremail'];?>" name="emailaddress" >
                                 </div>
                               
                              </div>
                           </fieldset>  
                                          
                                    </fieldset>
                                      <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Phone</label>
                                 <div class="col-sm-6">
                                     <input class="form-control " type="text" onkeypress="return isNumber1(event)"  value="<?php echo $query['leadcustomerphone'];?>" name="phone" >
                                 </div>
                               
                              </div>
                           </fieldset>  
                                    
                                    
                         <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Lead Tag</label>
                                 <div class="col-sm-6">
                                     <?php   $tg =  $query['leadtags'];?>
                                   <select name="leadtag" class="form-control"   style="width:510px"  data-validation="length" data-validation-length="min1" data-validation-error-msg="Lead tag is requirred" >
                                 
                                     <option value="">Select Tag </option>
                            <?php   $this->db->where('clientcompid', $comp);
                                  $leadquery = $this->db->get('tblleadtag');
                                   
                                     foreach($leadquery->result()  as $k=>$v){
                                        $m = $v->leadtagid;  ?>
                                  <option value="<?php echo $v->leadtagid; ?>"  <?php if ($m == $tg ) echo 'selected' ; ?>><?php echo $v->leadtags;?></option>
                                   <?php } ?>
                                     </select>     
                                 </div>
                              </div>
                           </fieldset>             
                                
                          
                                    <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Next FollowUp Date</label>
                                 <div class="col-sm-6">
                                     <input class="form-control " type="text"  id="datetimepicker" value="<?php echo $query['leadsnextfollowdate'];?>" name="followupdate" >
                                 </div>
                               
                              </div>
                           </fieldset>  
                                    
                                    <fieldset>
 <div class="form-group">
                                 <label class="col-sm-2 control-label">Notes</label>
                                 <div class="col-sm-6">
<textarea name="notes" rows="4" class="form-control"><?php echo $query['leadnotes'];?></textarea>                                 </div>
                               
                              </div>
                           </fieldset>  
                           
<!--                            <fieldset>
                              <div class="form-group">
                                 
                                 <div class="col-sm-6">
   <label class="col-sm-2 control-label" style=" margin-left: 50px;"   >Lead Document</label> 
  <?php     if($method == "edit"){
      
  }            ?>
   <input type='file'  name="document"  accept=".xlsx,.xls,.doc, .docx,.ppt, .pptx,.txt,.pdf"  style=" margin-left: 192px;"  />

                                 </div>
                                 
                              </div>
                           </fieldset>  -->
                                    
                                    <?php if($method == "create"){?>
                               <div class="container1" >
   <div class="form-group ">
                                    <label class="col-sm-3 col-lg-3 control-label">Lead Document</label>  
                                    <button type="button"  style="margin-left: -183px;"  class="btn pricebutton  addbutton  add_form_field">+</button>

                                      
                                      <div class="col-sm-4 col-lg-4 controls">
     
          <!--<img  id="blah" name="image" class=" img-circle " accept="image/*"  alt="Image" style="width: 65px; margin-left: 6px;" >-->
     
   <input type='file' <?php echo $r;?> onchange="readURL(this);" name="document[]"    />                           
 </div> <div class="col-sm-1 col-lg-1">
                                      </div>
                                    </div>
                                   <br>
                                    </div>     <?php } ?>



                        
                                    
                                    
                                    
                            </fieldset>  
                                      
                           <?php if($method == "edit"){?> 
                    <div class="row">
                        <div class="col-md-3"><label class="col-sm-3 col-lg-3 control-label"    style="margin-left: 82px;">Lead Document</label>  </div>
                        <div class="col-md-9">
                            <div class="imagemenu">
                                <ul>
                                    
                                    <?php  $ap =   $query['leaddocument'];  
 $document = explode(",",$ap);
 ?> 
                                    
     <?php 
     $x = 0;
     foreach($document as $row){ 
         
          $path = $row;
$ext = pathinfo($path, PATHINFO_EXTENSION);  
         $extensions = array('jpg', 'JPG', 'png' ,'PNG' ,'jpeg' ,'JPEG');
 if (in_array($ext, $extensions))
  { ?>
                                    <span class="remove<?php echo $x;?>">
        <li> <img src="<?php echo base_url();?>uploads/document/<?php echo $row;?>">
        <a href="#"  onclick="removetext('<?php echo $x;?>','<?php echo $id;?>');"> -  </a></li>
        <input type="hidden" value="<?php echo $row;?>" name="editdocument[]">
                                    </span>                      
                                    
  <?php } else {?>
                                <span class="remove<?php echo $x;?>">            
       
        <li><input type='hidden' class="removeimage<?php echo $x;?>"  name="editdocument[]" value="<?php echo $row;?>"> <?php echo $row;?>
 <a href="#"  onclick="removetext('<?php echo $x;?>','<?php echo $id;?>');"> -  </a></li>
<!--                              <input type="hidden" value="<?php echo $row;?>" name="editdocument[]">-->
                                </span>
  <?php } ?>
     <?php  $x++; } ?>
                                    
                                </ul>  
                                
                                
                                
                            </div>
                            
                            
                            
                            
                            
                        </div>     
                        
                        
                    </div> 
                             <br>
                             
                             
                              <div class="container1">
                     <div class="row" style="margin-bottom :10px;">
                                        <div class="col-md-3"></div>
                                              <div class="col-md-9">
                                                <input type='file' class="fileinline" onchange="readURL(this,'<?php echo $x;?>');" name="document[]" /> 
                                       <button type="button"    class="btn pricebutton addbutton add_form_field">+</button>
                                              </div>  
                     </div><br>
                              </div>
                             
                             <?php } ?>
                             
                           
                            <div class="panel-footer text-center">
                           <button class="btn btn-info" type="submit" style="margin-left: -193px;"><?php echo $button_name;?></button>
                        </div>
                          
                                      </form>
                         
                        </div>
                     
                     </div>
                     <!-- END panel-->
                  </form>
               </div>
            </div>
            <!-- END row-->
         </div>
         
         
        <div class="container">
<!--  <h2>Modal Example</h2>-->
  <!-- Trigger the modal with a button -->
  <button type="button"  style="display:none;"class="btn btn-info btn-lg  openpopup" data-toggle="modal" data-target="#myoldModal">Open Modal</button>

  <!-- Modal -->
  <div class="modal fade" id="myoldModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
<!--        <div class="modal-header">-->
          <!--<h4 class="modal-title">Modal Header</h4>-->
        <!--</div>-->
        
        
  
        
        <div class="modal-body   hideDiv12">
            
            <p>
        <button type="button"  id="createmeet"   onclick= "createMeeting('<?php echo $id;?>');" class="btn btn-primary">Create Call</button>
  
                      <button type="button" class="close" onclick="hideDiv1();"  style="margin-right: 14px; margin-top: 17px;" data-dismiss="modal">&times;</button>

            <div id="active" class="tab-pane fade in ">
      <div class="panel-body">
          <div class="container-fluid">
               <!-- START DATATABLE 1-->
               <div class="row">
                  <div class="col-lg-12">
                     <div class="panel panel-default">
<!--                        <div class="panel-heading">Data Tables |
                           <small>Zero Configuration + Export Buttons</small>
                        </div>-->
                        <div class="panel-body">
                           <div class="table-responsive">
                              <table class="table table-striped table-hover" id="datatable1" >
                                 <thead>
                                    <tr>
                                        <th>S.No</th>
                                       <th> Date</th>
                                       <th>Summary</th>
                                       <th>Contact</th>
                                        <th>Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                     
                                      <?php 
                                       
                                    $comp = $this->session->userdata['companyid'];
                                    $this->db->where('clientcompid', $comp);
                                   $this->db->where('leadcalltype', $id);

                                 
                                    $leadcall = $this->db->get('tblleadcall');
$i = 1;
                                foreach($leadcall->result() as $k=>$v)     {                                  ?>
                                    <tr class="gradeX  hide<?php echo $v->leadcallid;?>">
                                        <td><?php echo $i;?></td>
                                        <td><?php echo $v->leadcalldate;?></td>
                                         <td><?php echo $v->leadcalltime;?> </td>
                                        <td><?php echo $v->leadcallcontact;?> </td>

        <td><a onclick="editmeeting('<?php echo $v->leadcallid;?>');" ><i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i></a>
        <a  onclick="return confirmirmation('<?php echo $v->leadcallid;?>');" ><i class="fa fa-trash-o fa-2x" aria-hidden="true" style="color:#f31a04;"></i></a></td>

                                    </tr>
                                  
                                    
                                     
                                     
                                     
                                     
                                <?php $i++ ;} ?>
                                    
                                  
                              
                                 
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- END DATATABLE 1-->
               <!-- START DATATABLE 2-->
               
               <!-- END DATATABLE 2-->
               <!-- START DATATABLE 3-->
               
               <!-- END DATATABLE 3-->
               
            </div>
         </div>
            </div>
        
        </p>
            
            
            
            
          <!--<p>Some text in the modal.</p>-->
       
        
        
        
        
        
        
        
        
        
        
        </div>
        
      </div>
      
    </div>
  </div>
  
</div> 
         
         
         
         <!--**********************************-->
         
         <div class="container">
  <h2></h2>
  <!-- Trigger the modal with a button -->
  <button type="button" class="btn btn-info btn-lg abcdefg" data-toggle="modal" data-target="#myModal3" style="display:none;">Open Modal</button>

  <!-- Modal -->
 <div id="myModal3" class="modal fade pasteDiv" style="margin-right:12px;" >
 <div class="modal-dialog modal-confirm">
  <div class="modal-content">
      
      
           <button type="button" class="close   asdf123"   style="margin-right: 14px; margin-top: 17px;" data-dismiss="modal" aria-hidden="true">&times;</button>

   <div class="modal-header">
       
    <div class="icon-box">
     <!--<i class="material-icons">&#xE5CD;</i>-->
        <?php if($method == "edit") { ?>
                         <button type="button"  id="demo"   onclick= "test123('<?php echo $id;?>');" class="btn btn-primary"> Create Call</button>
                       
                       <?php } ?>

<!--        <div class="alert alert-block alert-success  modelpop" style="background-color:#3ec0e8 ;display:none;">
                                <a class="close"   style="margin-left: 576px;" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p> Deleted Successfully</p>
                        </div>	-->

    </div>    
   </div>
   
       <form id="add_call" name="add_call" class="form-validation" accept-charset="utf-8" enctype="multipart/form-data" method="post">
             

 
<!--           <input type="hidden" name="call_type_id" value="1">
               	 <input type="hidden" name="call_type" value="leads">	                        	-->
               	 <div class="modal-body">
                    
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="field-1" class="control-label">Date</label>
                        <input type="texts"  class="form-control  callDate"  id="datetimepicker1" name="date"  placeholder="" >
                        <span id="span1" style="color:red"></span>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="field-2" class="control-label">Call	Summary</label>
                        <input type="text" class="form-control" name="summary" id="call_summary" placeholder="">
                       <span id="span2" style="color:red"></span>
                      </div>
                    </div>
                  </div>
                   <input type="hidden" name="updateid" value="<?php echo $id;?>" >
                   
                    <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                       <label for="field-2" class="control-label">Contact</label>
                        <input type="text" onkeypress="return isNumber(event)"            class="form-control" name="contact" id="contact" placeholder="">
                       <span id="span3" style="color:red"></span>
                      </div>
                    </div>
                        
                        
                        
                        
                    <div class="col-md-6">
                      <div class="form-group">
                        
                         <label for="field-2" class="control-label">Responsible</label>
                         <br>
                        <select multiple="multiple"   name="attendies[]" style="width:250px" id="ert12">

                                <?php 
                                       
                                     $this->db->where('clientuserstatus',1);
                                    $this->db->where('clientcompid', $comp);
                                 
                                    $member = $this->db->get('tblclientuser');

                                
                                
                             if($method == "create")
                             {
                                foreach($member->result() as $k1=>$v1){?>

        <option value="<?php echo $v1->clientuserid;?>"  ><?php echo $v1->clientusername;?></option>
      
   
                                <?php } 
        
        } ?>
        
        
        
        
        <?php    $mem = explode(",",$query['meetingattendies']) ;
             
      
                             if($method == "edit")
                             {
                                foreach($member->result() as $k1=>$v1)
                                    
                                    {
                                    
                                           $m =  $v1->clientuserid;
                                    
                                         if (in_array($m, $mem))
                                                                { ?>
        
                   <option value="<?php echo $v1->clientuserid;?>" <?php echo "selected";?> ><?php echo $v1->clientusername;?></option>

                                                

     <?php           }
                                                              else
                                                                { ?>
                                                               
                                                                  
           <option value="<?php echo $v1->clientuserid;?>"  ><?php echo $v1->clientusername;?></option>

                                                         <?php       }

                                           
                                           
                                           
                                   

                                    
                                     
                                       
                                        
                                    }
                                    
                                    
                                    ?>

                    
                                <?php } 
        
        ?>
        
        
        
        
        
        
        
        
                                                                       </select>   
                        
                        

                        
                        
                        
                        <span id="span3" style="color:red"></span> 
                          
                      </div>
                    </div>
                  </div>
                   
                   
                   
                   
                   
                   
                   
<!--                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="field-2" class="control-label">Contact</label>
                        <input type="text" onkeypress="return isNumber(event)"            class="form-control" name="contact" id="contact" placeholder="">
                       <span id="span3" style="color:red"></span>
                      </div>
                    </div>
                    
                      
                      
                      <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="field-2" class="control-label">Responsible</label>
                        
                        <select multiple="multiple"   name="attendies[]" style="width:180px" id="ert12">

                                <?php 
                                       
                                     $this->db->where('clientuserstatus',1);
                                    $this->db->where('clientcompid', $comp);
                                 
                                    $member = $this->db->get('tblclientuser');

                                
                                
                             if($method == "create")
                             {
                                foreach($member->result() as $k1=>$v1){?>

        <option value="<?php echo $v1->clientuserid;?>"  ><?php echo $v1->clientusername;?></option>
      
   
                                <?php } 
        
        } ?>
        
        
        
        
        <?php    $mem = explode(",",$query['meetingattendies']) ;
             
      
                             if($method == "edit")
                             {
                                foreach($member->result() as $k1=>$v1)
                                    
                                    {
                                    
                                           $m =  $v1->clientuserid;
                                    
                                         if (in_array($m, $mem))
                                                                { ?>
        
                   <option value="<?php echo $v1->clientuserid;?>" <?php echo "selected";?> ><?php echo $v1->clientusername;?></option>

                                                

     <?php           }
                                                              else
                                                                { ?>
                                                               
                                                                  
           <option value="<?php echo $v1->clientuserid;?>"  ><?php echo $v1->clientusername;?></option>

                                                         <?php       }

                                           
                                           
                                           
                                   

                                    
                                     
                                       
                                        
                                    }
                                    
                                    
                                    ?>

                    
                                <?php } 
        
        ?>
        
        
        
        
        
        
        
        
                                                                       </select>   
                        
                        

                        
                        
                        
                        <span id="span3" style="color:red"></span>
                      </div>
                    </div>
                    
                     
                  </div>
                     
              
                     
                     
                     
                     
                     
                     
                </div>-->
                 
                  <div id="call_submitbutton" class="modal-footer text-center"><button type="submit" class="btn btn-primary btn-embossed bnt-square"  onclick="formSubmit();">Create</button></div>
                 
                </form>
      
      
      
    
  </div>
 </div>
</div>
  
  
  
 
  
</div>

<!--*******************************Popup for paste*******************************-->
         
         <div class="container" >
<!--  <h2>Modal Example</h2>-->
  <!-- Trigger the modal with a button -->
  <button type="button"    style="margin-left: -400px;" class="btn btn-info btn-lg  editedform" data-toggle="modal" data-target="#editedForm"></button>

  <!-- Modal -->
  <div class="modal fade" id="editedForm" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
          <p>Some text in the modal.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>

<!--********************-->

</div>
         
         
         
         
         
         <!--****************************************-->
         
         
         
         
         
         
         
      </section>
   <script src="<?php echo base_url();?>assets/vendor/jquery/dist/jquery.js"></script>


    <script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js" type="text/javascript"></script>
 <script src="<?php echo base_url();?>assets/vendor/datatables/media/js/jquery.dataTables.min.js"></script>
   <!--<script src="<?php echo base_url();?>assets/vendor/datatables-colvis/js/dataTables.colVis.js"></script>-->
   <script src="<?php echo base_url();?>assets/vendor/datatables/media/js/dataTables.bootstrap.js"></script>
   <!--<script src="<?php echo base_url();?>assets/vendor/datatables-buttons/js/dataTables.buttons.js"></script>-->
   <!--<script src="<?php echo base_url();?>assets/vendor/datatables-buttons/js/buttons.bootstrap.js"></script>-->
   <script src="<?php echo base_url();?>assets/vendor/datatables-buttons/js/buttons.colVis.js"></script>
   <!--<script src="<?php echo base_url();?>assets/vendor/datatables-buttons/js/buttons.flash.js"></script>-->
   <script src="<?php echo base_url();?>assets/vendor/datatables-buttons/js/buttons.html5.js"></script>
   <!--<script src="<?php echo base_url();?>assets/vendor/datatables-buttons/js/buttons.print.js"></script>-->
   <script src="<?php echo base_url();?>assets/vendor/datatables-responsive/js/dataTables.responsive.js"></script>
   <script src="<?php echo base_url();?>assets/vendor/datatables-responsive/js/responsive.bootstrap.js"></script>
   <script src="<?php echo base_url();?>assets/js/demo/demo-datatable.js"></script>
    
    <script src="<?php echo base_url();?>assets/datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
 

    
    
    
    
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"> </script>
                


<script>
                    
                      function isNumber(evt) {
            var iKeyCode = (evt.which) ? evt.which : evt.keyCode
            if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
                return false;

            return true;
        
                    }
                    
                    
                      function isNumber1(evt) {
            var iKeyCode = (evt.which) ? evt.which : evt.keyCode
            if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
                return false;

            return true;
        
                    }
                    
                    
                </script>

                        <script src="<?php echo base_url();?>multiple-select-master/multiple-select.js"> </script>

    
    <script>
        $('#ert').multipleSelect({
          //  isOpen: true,
                //filter: true
                // selectAll: false
        });
        
         $('#ert12').multipleSelect({
          //  isOpen: true,
                //filter: true
                // selectAll: false
        });
    </script>
    
    
 <script>
        
       function test123()
       {
         
        $(".openpopup").click();
       }
     </script>
        
     <script>
            function createMeeting(vala){
         //  $(".close12").click();
         $(".abcdefg").click();
            
               }   
        </script>
        
        <script>
          function formSubmit()
        { 
            
//            $("#date").val(" ");
//            $("#call_summary").val(" ");
//            $("#contact").val(" ");
            
           
            event.preventDefault();
            
             
            var m =  $(".callDate").val();
           var s =  $("#call_summary").val();
           var l =   $("#contact").val();
           if(m == ""){
                
                $("#span1").html("Date is requirred");
                return false;
            }
             if(s == ""){
                
                $("#span2").html("Summary is requirred");
                return false;
            }
            if(l == ""){
               $("#span3").html("Contact is requirred"); 
               return false;
            }
            
           // alert();
                 $.ajax({
          url: '<?php echo base_url();?>client/Client/saveleadCall',
          type : "POST",
         
          data : $('#add_call').serialize(),
          success : function(data) {
                 $('#datatable1').html(data);
                 $(".asdf123").click();
                 
                   $(".callDate").val(" ");
                   $("#call_summary").val(" ");
                  $("#contact").val(" ");
                 
                 
           }
        
          
      });
      
        }
        </script>
        
        
        <script>
            
        function confirmirmation(a){
            
    
        var abc =   confirm('Are you sure want to delete?');
          
         
        
        if(abc == true){
    
             $.ajax({
          url: '<?php echo base_url();?>client/Client/deletecall',
          type : "POST",
         
          data : {'data':a},
          success : function(response) {
        
        
     //   alert(response);
//                          
             $(".hide"+a).hide();
              $(".modelpop").show();
           }
        
          
      });
               
           }
        }
            
        </script>
        
        
        <script>
          function editmeeting(editid){
          //  $("#createmeet").click();
            
         // alert(editid);
        
        $.ajax({
          url: '<?php echo base_url();?>client/Client/editCall',
          type : "POST",
         
          data : {'editid':editid},
          success : function(data) {
              
          
            console.log(data);
//             $('.pasteDiv').html(data);
//             $('.abcdefg').click();
           
           $('.editedform').click();

                  $('#editedForm').html(data);
            
            
           }
        
          
      });
//           
          }  
            
            
            
            
            
            
     function editformSubmit()
        { 
            event.preventDefault();
            
          
          var m =  $(".caldateedit").val();
           var s =  $("#call_summary12").val();
           var l =   $("#contact12").val();
           if(m == ""){
                
                $("#span12").html("Date is requirred");
                return false;
            }
             if(s == ""){
                
                $("#span22").html("Summary is requirred");
                return false;
            }
            if(l == ""){
               $("#span32").html("Contact is requirred"); 
               return false;
            }
            
        
       
         
                $.ajax({
          url: '<?php echo base_url();?>client/Client/editleadSubmit',
          type : "POST",
         
          data : $('#edit_call').serialize(),
          success : function(data) {
              
          
           $('#datatable1').html(data);
$(".asdf123").click();
             

           
           }
        
          
      });
      
        }          
            
            
//        function hideDiv1()
//        {
//           
//        alert();
//        //$("#myModal3").hide(); 
//    }
            
            
            
            
            
            
            
            
            
            
            
        </script>
        
        
        <script>
        $(document).ready(function() {
    var max_fields      = 1000;
    var wrapper         = $(".container1"); 
    var add_button      = $(".add_form_field"); 
    
    var x = 1; 
    $(add_button).click(function(e){ 
        e.preventDefault();
        if(x < max_fields){ 
            x++; 
            //alert(x);
            $(wrapper).append("<div class='row  removealreadyDiv"+x+"' style='margin-bottom :10px;' ><div class='col-md-3'></div><div class='col-md-9'><input type='file' class='fileinline' onchange='readURL(this,'<?php echo $x;?>');' name='document[]' /><button type='button'  onclick =removeDiv("+x+") class='btn pricebutton addbutton'>-</button></div></div><br id='rowBr"+x+"'> "); //add input box
        }
    else
    {
    alert('You Reached the limits')
    }
    });
    
    $(wrapper).on("click",".delete", function(e){ 
    
   // alert(x);
     //   e.preventDefault(); $(this).parent('div').parent().remove(); x--;
    })
});
</script>


<script>
        
       function removeDiv(q)
       {
           $(".removealreadyDiv"+q).remove();
           $("#rowBr"+q).remove();
       }
        
        
        </script>
        <script>
    
    
    
    
    
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah')
                    .attr('src', e.target.result)
                    .width(80)
                    .height(80);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    
    </script>
    
    
    <script>
    
    function removetext(key,id)
    {
      
       
          $.ajax({
          url: '<?php echo base_url();?>client/Client/removedocument',
          type : "POST",
         
          data : {"id" :id,"key":key},
          success : function(data) {
            
                 
                // alert(data);
         console.log(data);
      
        $(".remove"+key).remove();
            
          }
        
          
      });
      
      
    }
    
    </script>