
<style>
    .link_ul{
    padding:0;
    list-style:none;
}
.link_ul li a
{
    color: #A2A2A2;
    color: rgba(255, 255, 255, .8);
    text-decoration: none;
}
</style>
<!-- ======== @Region: #footer ======== -->
  <footer id="footer" class="p-0">
    <div class="container pt-6 pb-5">
      <div class="row">
        <div class="col-md-4">
          <!--@todo: replace with company contact details-->
          <h4 class="text-uppercase text-white">
            Contact Us
          </h4>
          <address>
              <?php $cquery = $this->db->get('tblcmscontact')->row_array(); ?>
              <ul class="list-unstyled">
                <li>
                  <abbr title="Phone"><i class="fa fa-phone fa-fw"></i></abbr>
                  <?php echo $cquery['cmscontactno'];?>
                </li>
                <li>
                  <abbr title="Email"><i class="fa fa-envelope fa-fw"></i></abbr>
                  <?php echo $cquery['cmscontactemail'];?>
                </li>
                <li>
                  <abbr title="Address"><i class="fa fa-home fa-fw"></i></abbr>
                <?php echo $cquery['cmscontactaddr'];?>
                </li>
              </ul>
            </address>
        </div>
          
         
          
        <div class="col-md-3">
          <h4 class="text-uppercase text-white">
            Quick Links
          </h4>
            <ul class="link_ul">
                <li><a href="<?php echo base_url();?>Frontuser/enquiry">Enquiry Page</a></li>
                <li><a href="<?php echo base_url();?>Frontuser/faq">FAQ</a></li>
                <li><a href="<?php echo base_url();?>Frontuser/vision">Vision & Mission</a></li>
                <li><a href="<?php echo base_url();?>Frontuser/help">Help & Support</a></li>
                <li><a href="<?php echo base_url();?>Frontuser/help">User Guide</a></li>
                <li><a href="<?php echo base_url();?>Frontuser/news">News & Event</a></li>
                <li><a href="<?php echo base_url();?>Frontuser/clientport">Client Portfolio</a></li>
            </ul>
        </div>
        
<div class="col-md-5">
    
    
    
    
    
    
          <h4 class="text-uppercase text-white">
            Newsletter
          </h4>
          <p>Stay up to date with our latest news and product releases by signing up to our newsletter.</p>
        
           <div class="alert alert-block alert-success abc123" style="background-color:#64aea2 ;height:50px;width:400px; margin-left: 10px ;display:none">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p > Subscribed Successfully</p>
                        </div>	
          
          
          
          
          <form action="<?php echo base_url();?>superadmin/FrontManager/saveNewspaper" id="formId" method="post">
            <div class="input-group">
              <label class="sr-only" for="email-field">Email</label>
              <input type="email" class="form-control email" onblur="abc();"       name="email" id="get123" placeholder="Email" required>
                           

              <span class="input-group-append">
                  <button class="btn btn-primary" type="button" onclick="return MyTest();">Go!</button>
              
              </span>
            </div>
                             <span id="spa12" style="color:red"></span>

          </form>
        </div>
        
      </div>

    </div>
    <hr class="my-0 hr-blank op-2" />
    <!--@todo: replace with company copyright details-->
    <div class="bg-inverse-dark text-sm py-3">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
       <?php       $copyquery = $this->db->get('tblmanagerurl')->row_array();?>
            <p class="mb-0"><?php echo $copyquery['managecopyright'];?></p>
          </div>
          <div class="col-md-6">
            <ul class="list-inline footer-links float-md-right mb-0">
                <li class="list-inline-item"><a href="<?php echo base_url();?>Frontuser/term">Terms</a></li>
              <li class="list-inline-item"><a href="<?php echo base_url();?>Frontuser/privacy">Privacy</a></li>
              <li class="list-inline-item"><a href="<?php echo base_url();?>Frontuser/contact">Contact Us</a></li>
            </ul>
          </div>
        </div>
        <a href="#top" class="btn btn-icon btn-dark pos-fixed pos-b pos-r mr-3 mb-3 scroll-state-hidden" title="Back to top" data-scroll="scroll-state"><i class="fa fa-chevron-up"></i></a>
      </div>
    </div>
  </footer>

  <!--Off canvas region/sidebar (legacy .header-hidden region) -->
  <aside id="offcanvas-sidebar" class="bg-dark text-white js-offcanvas">
    <a class="js-offcanvas-close c-button c-button--close-right btn btn-sm btn-icon btn-link text-white pos-absolute pos-t pos-r mt-2 mr-2 op-7 pos-zindex-1 pos-zindex-10"> <i class="la la-close"></i> </a>
    <div class="py-6 px-4">
      <h5 class="text-uppercase text-letter-spacing">
        About Us
      </h5>
      <p class="text-sm">Making the web a prettier place one template at a time! We make beautiful, quality, responsive Drupal & web templates!</p>
      <a href="about.php" class="btn btn-sm btn-primary">Find out more</a>
      <hr class="hr-light op-2 my-4" />
      <!--@todo: replace with company contact details-->
      <h5 class="text-uppercase text-letter-spacing">
        Contact Us
      </h5>
      <address>
          <p class="mb-1 text-sm">
            <abbr title="Phone"><i class="fa fa-phone"></i></abbr>
            +91-522-4043950
          </p>
          <p class="mb-1 text-sm">
            <abbr title="Email"><i class="fa fa-envelope"></i></abbr>
            info@livesoftwaresolution.com
          </p>
          <p class="mb-0 text-sm">
            <abbr title="Address"><i class="fa fa-home"></i></abbr>
            330 Lekhraj Khajana, Faizabad Road, LUCKNOW
          </p>
        </address>
    </div>
  </aside>
  <!-- Style switcher - demo only - @todo: remove in production -->
  <div class="colour-switcher">
    <a href="#" class="colour-switcher-toggle" data-toggle="class" data-target=".colour-switcher"> <i class="fa fa-paint-brush"></i> </a>
    <h5 class="text-uppercase my-0">
      Theme Colours
    </h5>
    <hr />
    <div class="theme-colours"> <a href="#green" class="green active" data-toggle="tooltip" data-placement="right" data-original-title="Green (Default)">Green</a> <a href="#red" class="red" data-toggle="tooltip" data-placement="right" data-original-title="Red">Red</a> <a href="#blue"
        class="blue" data-toggle="tooltip" data-placement="right" data-original-title="Blue">Blue</a> <a href="#purple" class="purple" data-toggle="tooltip" data-placement="right" data-original-title="Purple">Purple</a> <a href="#pink" class="pink" data-toggle="tooltip"
        data-placement="right" data-original-title="Pink">Pink</a> <a href="#orange" class="orange" data-toggle="tooltip" data-placement="right" data-original-title="Orange">Orange</a> <a href="#lime" class="lime" data-toggle="tooltip" data-placement="right"
        data-original-title="Lime">Lime</a> <a href="#blue-dark" class="blue-dark" data-toggle="tooltip" data-placement="right" data-original-title="Blue-dark">Blue-dark</a> <a href="#red-dark" class="red-dark" data-toggle="tooltip" data-placement="right"
        data-original-title="Red-dark">Red-dark</a> <a href="#brown" class="brown" data-toggle="tooltip" data-placement="right" data-original-title="Brown">Brown</a> <a href="#cyan-dark" class="cyan-dark" data-toggle="tooltip" data-placement="right" data-original-title="Cyan-dark">Cyan-dark</a>      <a href="#yellow" class="yellow" data-toggle="tooltip" data-placement="right" data-original-title="Yellow">Yellow</a> <a href="#slate" class="slate" data-toggle="tooltip" data-placement="right" data-original-title="Slate">Slate</a> <a href="#olive"
        class="olive" data-toggle="tooltip" data-placement="right" data-original-title="Olive">Olive</a> <a href="#teal" class="teal" data-toggle="tooltip" data-placement="right" data-original-title="Teal">Teal</a> <a href="#green-bright" class="green-bright"
        data-toggle="tooltip" data-placement="right" data-original-title="Green-bright">Green-bright</a> </div>
    <hr />
    <p class="text-xs text-muted">Cookies are NOT enabled so colour selection is not persistent.</p>
    <p class="text-xs my-0"><a href="index.php">Back to main homepage</a></p>
    <p class="text-xs my-0"><a href="#">Back to intro page</a></p>
  </div>
  <!--Hidden elements - excluded from jPanel Menu on mobile-->
  <div class="hidden-elements js-off-canvas-exclude">
    <!--@modal - signup modal-->
    <div class="modal fade" id="signup-modal" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <form action="signup.php">
          <div class="modal-content">
            <div class="modal-header bg-light">
              <h4 class="modal-title">
                Sign Up
              </h4>
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
              <div class="form-group">
                <h6 class="op-8">
                  Price Plan
                </h6>
                <select class="form-control">
                    <option>Basic</option>
                    <option>Pro</option>
                    <option>Pro +</option>
                  </select>
              </div>
              <hr />

              <h6 class="op-8">
                Account Information
              </h6>
              <div class="form-group">
                <label class="sr-only" for="signup-first-name">First Name</label>
                <input type="text" class="form-control" id="signup-first-name" placeholder="First name">
              </div>
              <div class="form-group">
                <label class="sr-only" for="signup-last-name">Last Name</label>
                <input type="text" class="form-control" id="signup-last-name" placeholder="Last name">
              </div>
              <div class="form-group">
                <label class="sr-only" for="signup-username">Userame</label>
                <input type="text" class="form-control" id="signup-username" placeholder="Username">
              </div>
              <div class="form-group">
                <label class="sr-only" for="signup-email">Email address</label>
                <input type="email" class="form-control" id="signup-email" placeholder="Email address">
              </div>
              <div class="form-group">
                <label class="sr-only" for="signup-password">Password</label>
                <input type="password" class="form-control" id="signup-password" placeholder="Password">
              </div>
              <div class="form-check text-xs">
                <label class="form-check-label op-8">
                    <input type="checkbox" value="term" class="form-check-input mt-1">
                    I agree with the Terms and Conditions. 
                  </label>
              </div>
            </div>
            <div class="modal-footer bg-light py-3">
              <div class="d-flex align-items-center">
                <button type="button" class="btn btn-primary">Sign Up</button>
                <button type="button" class="btn btn-link ml-1" data-dismiss="modal" aria-hidden="true">Cancel</button>
              </div>
              <p class="text-xs text-right text-lh-tight op-8 my-0 ml-auto">Already signed up? <a href="login.php">Login here</a></p>
            </div>
          </div>
          <!-- /.modal-content -->
        </form>
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!--@modal - login modal-->
    <div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
          <form action="login.php">
            <div class="modal-header bg-light">
              <h4 class="modal-title">
                Login
              </h4>
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
              <div class="form-group">
                <label class="sr-only" for="login-email">Email</label>
                <input type="email" id="login-email" class="form-control email" placeholder="Email">
              </div>
              <div class="form-group mb-0">
                <label class="sr-only" for="login-password">Password</label>
                <input type="password" id="login-password" class="form-control password mb-1" placeholder="Password">
              </div>
            </div>
            <div class="modal-footer bg-light py-3">
              <div class="d-flex align-items-center">
                <button type="button" class="btn btn-primary">Login</button>
                <button type="button" class="btn btn-link ml-1" data-dismiss="modal" aria-hidden="true">Cancel</button>
              </div>
              <p class="text-xs text-right text-lh-tight op-8 my-0 ml-auto">
                Not a member? <a href="#" class="signup">Sign up now!</a>
                <br />
                <a href="#">Forgotten password?</a>
              </p>
            </div>
        </div>
        <!-- /.modal-content -->
        </form>
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
  </div>


  <!--jQuery 3.3.1 via CDN -->
<!--  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>-->
  <!-- Popper 1.14.3 via CDN, needed for Bootstrap Tooltips & Popovers -->
<!--  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>-->

  <!-- Bootstrap v4.1.1 JS via CDN -->
<!--  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>-->
     <script src="<?php echo base_url();?>front/assets/js/jquery.min.js"></script>
   <script src="<?php echo base_url();?>front/assets/js/popper.min.js"></script>
   <script src="<?php echo base_url();?>front/assets/js/bootstrap.min.js"></script>

  

  <!--Custom scripts & Lazlo API integration -->
  <script src="<?php echo base_url();?>front/assets/js/custom-script.js"></script>
  <!--Lazlo scripts mainly used to trigger libraries/plugins -->
  <script src="<?php echo base_url();?>front/assets/js/script.min.js"></script>
  <script>
   function toggleIcon(e) {
    $(e.target)
        .prev('.panel-heading')
        .find(".more-less")
        .toggleClass('fa-plus fa-minus');
}
$('.panel-group').on('hidden.bs.collapse', toggleIcon);
$('.panel-group').on('shown.bs.collapse', toggleIcon);
  </script>
</body>

</html>

<script>
    
    function MyTest()
    {
     $(".abc123").hide();
        var e = $("#get123").val();
        
      //  alert(e);
        if(e == "")
        {
            $("#spa12").html("Email is require");
            return false;
        }
        
        $.ajax({
          url: '<?php echo base_url();?>superadmin/FrontManager/saveNewspaper',
          type : "POST",
         
          data : {"email" :e},
          success : function(data) {
           
        //   alert(data);
        if(data == "create"){
            
            $(".abc123").show();
        }
             
                                     }
       
      });
        
        
    }
    
    
    
    function abc()
    {
           $("#spa12").html("  ");
    }
    
    
    
    
    
    
    </script>