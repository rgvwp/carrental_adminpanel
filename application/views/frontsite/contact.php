<?php include 'header.php' ?>

  <!-- ======== @Region: #content ======== -->
  <div id="content">
    <div class="container">
      <div class="row">
        <!-- sidebar -->
        <div class="col-md-3">

          <!-- Sections Menu-->
          
        </div>
        <!--main content-->
        <div class="col-md-12">
          <h2 class="title-divider">
                <?php if($this->session->flashdata('permission_message'))
	 		{
			?>
					
                        <div class="alert alert-block alert-success " style="background-color:#64aea2 ;height:50px;
width:400px;">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                <p>Message Sended Succesfully</p>
                        </div>						
									
			<?php } ?>
            <?php if($this->session->flashdata('flash_message'))
	 		{
			?>
					
                        <div class="alert alert-block alert-success" style="background-color:#ff708a">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p ><h4 style="background-color:#ff708a"> </h4> <?php echo $this->session->flashdata('flash_message'); ?></p>
                        </div>						
									
			<?php } ?>
            <span>Contact <span class="font-weight-normal text-muted">Us</span></span>
            <small>Ways To Get In Touch</small>
          </h2>
          <div class="row">
            <div class="col-md-6">
              <form  action="<?php echo base_url();?>superadmin/FrontManager/savefrontContact"  method="post" id="FormId">
                <div class="form-group">
                    
                  <label class="sr-only" for="contact-name">Name</label>
                  <input type="text" class="form-control name" name="name" id="contact-name" placeholder="Name">
                  <small id="contact-name-help" class="form-text text-muted">First and last names.</small>
                                      <span id="name" style="color:red"></span>

                </div>
                <div class="form-group">
                  <label class="sr-only" for="contact-email">Email</label>
                  <input type="email" class="form-control  email"  name="email" id="contact-email" placeholder="Email" required>
                  <small id="contact-email-help" class="form-text text-muted">We'll never share your email with anyone else.</small>
                                   <span id="email" style="color:red"></span>

                </div>
                <div class="form-group">
                  <label class="sr-only" for="contact-message">Message</label>
                  <textarea rows="12" class="form-control   message" name="message" id="contact-message" placeholder="Message"></textarea>
                  <small id="contact-message-help" class="form-text text-muted">Your message and details.</small>
                                  <span id="message" style="color:red"></span>

                </div>
                <input type="button" class="btn btn-primary" value="Send Message" onclick="return myMessage();">
              </form>
            </div>
            <div class="col-md-6">
              <p>              <?php $cquery = $this->db->get('tblcmscontact')->row_array(); ?>

                <abbr title="Phone"><i class="fa fa-phone"></i></abbr>    <?php echo $cquery['cmscontactno'];?>
              </p>
              <p>
                <abbr title="Email"><i class="fa fa-envelope"></i></abbr>   <?php echo $cquery['cmscontactemail'];?>
              </p>
              <p>
                <abbr title="Address"><i class="fa fa-home"></i></abbr>   <?php echo $cquery['cmscontactaddr'];?>
              </p>
              <p>
                <a href="#">
                    <img src="<?php echo base_url();?>front/assets/img/misc/map.png" alt="Location map" class="img-thumbnail" style="width:100%"/>
                  </a>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- ======== @Region: #content-below ======== -->
  <div id="content-below">
    <!-- Awesome features call to action -->
    <div class="bg-primary bg-op-9 text-white py-4">
      <div class="container">
        <div class="row text-center text-lg-left align-items-lg-center">
          <div class="col-12 col-lg-7 text-white">
            <h3 class="font-weight-bold my-0 text-uppercase">
              Awesome Features
            </h3>
            <p class="font-weight-normal op-9 my-0"> <i class="la la-check-circle-o"></i> 99.9% Uptime <i class="la la-check-circle-o ml-lg-3"></i> Free Upgrades <i class="la la-check-circle-o ml-lg-3"></i> Fully Responsive <i class="la la-check-circle-o ml-lg-3"></i>              Bug Free </p>
          </div>
          <div class="col-12 col-lg-5 py-2 text-lg-right">
            <a href="#" class="btn btn-xlg btn-white btn-rounded shadow-lg bg-light bg-op-8 bg-hover-white">Get AppStrap<i class="fa fa-arrow-right ml-2 mt-1"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>

 <?php include 'footer.php' ?>

  <script>
      
   $('#contact-email').on('change', function () 
   {
        var re = /([A-Z0-9a-z_-][^@])+?@[^$#<>?]+?\.[\w]{2,4}/.test(this.value);
        if (!re) {
            $("#email").text("Email address is not correct");

        } else {
            setTimeout(function () {
                $("#email").text("");
            }, 02);
        }
    })   
      
  </script>
  
  <script>
      
      function myMessage()
      {
          var name = $(".name").val();
          var email = $(".email").val();
          var message = $(".message").val();
          if(name == "")
          {
              $("#name").html("Name is required");
              return false ;
          }
           if(email == "")
          {
              $("#email").html("Email is required");
              return false ;
          }
           if(message == "")
          {
              $("#message").html("Message is require");
              return false ;
          }
          
       //   alert();
         $("#FormId").submit();
          
      }
      
      
      
     
      
      </script>