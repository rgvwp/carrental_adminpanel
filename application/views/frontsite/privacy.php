<?php include 'header.php' ?>


  <!-- ======== @Region: #content ======== -->
  <div id="content">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-9 col-xs-12">
                <div>
                    <h2 class="title-divider">
            <span>Privacy <span class="font-weight-normal text-muted">Policy</span></span>
            <small>What &amp; who makes us tick!</small>
          </h2>

           <p><strong>GENERAL INFORMATION</strong></p> 
            
            <p>One of our main priorities is the privacy of our visitors. This Privacy Policy document contains types of information that is collected and recorded by KeyDesign and how we use it.</p>
            <p>If you have additional questions or require more information about our Privacy Policy, do not hesitate to contact us through email at Incubator. An example of a privacy policy can be found at buyproxies.io.</p>
      
            <p><strong>LOG FILES</strong></p>
            <p>KeyDesign follows a standard procedure of using log files. These files log visitors when they visit websites. All hosting companies do this and a part of hosting services analytics. The information collected by log files include:</p>
            
            <ul class="wbp_content">
                <li>internet protocol (IP) addresses</li>
                <li>browser type, Internet Service Provider (ISP)</li>
                <li>date and time stamp, referring/exit pages</li>
                <li>possibly the number of clicks</li>
            </ul>
            
            <p>&nbsp;</p>
            
            <p><strong>PRIVACY POLICIES</strong></p>
            
            <p>You may consult this list to find the Privacy Policy for each of the advertising partners of Incubator.</p>
            
            <p>Third-party ad servers or ad networks uses technologies like cookies, JavaScript, or Web Beacons that are used in their respective advertisements and links that appear on Incubator, which are sent directly to users browser. They automatically receive your IP address when this occurs. These technologies are used to measure the effectiveness of their advertising campaigns and/or to personalize the advertising content that you see on websites that you visit.</p>
            
            
             
             <p><strong>ONLINE PRIVACY</strong></p>
             
             <p>This privacy policy applies only to our online activities and is valid for visitors to our website with regards to the information that they shared and/or collect in KeyDesign. This policy is not applicable to any information collected offline or via channels other than this website.</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
                <div>
                    <div class="mb-4">
            <h4 class="title-divider">
              <span>Our Services</span>
            </h4>
            <ul class="list-unstyled list-sm tags">
              <li><i class="fa fa-angle-right fa-fw"></i> <a href="#">Breakdown assistance</a></li>
              <li><i class="fa fa-angle-right fa-fw"></i> <a href="#">Pickup and delivery</a></li>
              <li><i class="fa fa-angle-right fa-fw"></i> <a href="#">Personal driver</a></li>
              <li><i class="fa fa-angle-right fa-fw"></i> <a href="#">Car navigation</a></li>
              <li><i class="fa fa-angle-right fa-fw"></i> <a href="#">Fuel plans</a></li>
            </ul>
          </div>
                    
                    <div class="mb-4">
            <h4 class="title-divider">
              <span>Recent News</span>
            </h4>
            <ul class="list-unstyled list-sm tags">
              <li><i class="fa fa-angle-right fa-fw"></i> <a href="#">Land lights let be divided</a></li>
              <li><i class="fa fa-angle-right fa-fw"></i> <a href="#">Seasons over bearing air</a></li>
              <li><i class="fa fa-angle-right fa-fw"></i> <a href="#">Signs likeness for may</a></li>
              <li><i class="fa fa-angle-right fa-fw"></i> <a href="#">Greater open after grass</a></li>
              <li><i class="fa fa-angle-right fa-fw"></i> <a href="#">Gathered was divide second</a></li>
            </ul>
          </div>
                    <div class="mb-4">
            <h4 class="title-divider">
              <span>Get a quote</span>
            </h4>
            <p>An duo lorem altera gloriatur. No imperdiet adver sarium pro. No sit sumo lorem. Mei ea eius elitr consequ unturimperdiet.</p>
            <a href="#" class="btn btn-lg btn-primary border-bottom">GET QUOTE</a>
          </div>
                    
                </div>
            </div>
        </div>
      
    </div>
  </div>

  <!-- ======== @Region: #content-below ======== -->
  <div id="content-below">
    <!-- Awesome features call to action -->
    <div class="bg-primary bg-op-9 text-white py-4">
      <div class="container">
        <div class="row text-center text-lg-left align-items-lg-center">
          <div class="col-12 col-lg-7 text-white">
            <h3 class="font-weight-bold my-0 text-uppercase">
              Awesome Features
            </h3>
            <p class="font-weight-normal op-9 my-0"> <i class="la la-check-circle-o"></i> 99.9% Uptime <i class="la la-check-circle-o ml-lg-3"></i> Free Upgrades <i class="la la-check-circle-o ml-lg-3"></i> Fully Responsive <i class="la la-check-circle-o ml-lg-3"></i>              Bug Free </p>
          </div>
          <div class="col-12 col-lg-5 py-2 text-lg-right">
            <a href="#" class="btn btn-xlg btn-white btn-rounded shadow-lg bg-light bg-op-8 bg-hover-white">Get AppStrap<i class="fa fa-arrow-right ml-2 mt-1"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>

 <?php include 'footer.php' ?>