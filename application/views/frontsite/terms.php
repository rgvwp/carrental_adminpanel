<?php include 'header.php' ?>


  <!-- ======== @Region: #content ======== -->
  <div id="content">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-9 col-xs-12">
                <div>
                    <h2 class="title-divider">
            <span>Terms <span class="font-weight-normal text-muted">& Condition</span></span>
            <small>What &amp; who makes us tick!</small>
          </h2>

           <p><strong>GENERAL INFORMATION</strong></p> 
            
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. </p>
            <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo.</p>
      
            
            <ul class="wbp_content">
                <li>internet protocol (IP) addresses</li>
                <li>browser type, Internet Service Provider (ISP)</li>
                <li>date and time stamp, referring/exit pages</li>
                <li>possibly the number of clicks</li>
            </ul>
            
            <p>&nbsp;</p>
            
            <p><strong>TERM & CONDITION</strong></p>
            
            <p>You may consult this list to find the Privacy Policy for each of the advertising partners of Incubator.</p>
            
            <p>Morbi mattis ullamcorper velit. Phasellus gravida semper nisi. Nullam vel sem. Pellentesque libero tortor, tincidunt et, tincidunt eget, semper nec, quam. Sed hendrerit. Morbi ac felis. Nunc egestas, augue at pellentesque laoreet, felis eros vehicula leo, at malesuada velit leo quis pede. Donec interdum, metus et hendrerit aliquet, dolor diam sagittis ligula, eget egestas libero turpis vel mi.</p>
            
            
             <p><strong>THIRD PARTY CONDITIONS</strong></p> 
             <p>You can choose to disable cookies through your individual browser options. To know more detailed information about cookie management with specific web browsers, it can be found at the browsers respective websites. What Are Cookies?</p>
             
               </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
                <div>
                    <div class="mb-4">
            <h4 class="title-divider">
              <span>Product categories</span>
            </h4>
            <ul class="list-unstyled list-sm tags">
              <li><i class="fa fa-angle-right fa-fw"></i> <a href="#">Breakdown assistance</a></li>
              <li><i class="fa fa-angle-right fa-fw"></i> <a href="#">Pickup and delivery</a></li>
              <li><i class="fa fa-angle-right fa-fw"></i> <a href="#">Personal driver</a></li>
              <li><i class="fa fa-angle-right fa-fw"></i> <a href="#">Car navigation</a></li>
              <li><i class="fa fa-angle-right fa-fw"></i> <a href="#">Fuel plans</a></li>
            </ul>
          </div>
                    
                    
                    <div class="mb-4">
                        <h4 class="title-divider">
              <span>Recent News</span>
            </h4>
            <ul class="nav nav-tabs">
              <li class="nav-item"><a href="#popular" class="nav-link active show" data-toggle="tab">Popular</a></li>
              <li class="nav-item"><a href="#latest" class="nav-link" data-toggle="tab">Latest</a></li>
            </ul>
            <div class="tab-content tab-content-bordered">
              <!-- Popular tab content -->
              <div class="tab-pane fade blog-roll-mini active show" id="popular">
                <!-- Popular blog post 1 -->
                <div class="row blog-post">
                  <div class="col-4">
                    <div class="blog-media">
                      <a href="#">
                          <img src="assets/img/blog/frog.jpg" alt="Picture of frog by Ben Fredericson" class="img-fluid">
                        </a>
                    </div>
                  </div>
                  <div class="col-8">
                    <h6>
                      <a href="#">Euismod Macto Obruo Premo</a>
                    </h6>
                  </div>
                </div>
                <!-- Popular blog post 2 -->
                <div class="row blog-post">
                  <div class="col-4">
                    <div class="blog-media">
                      <a href="#">
                          <img src="assets/img/blog/ladybird.jpg" alt="Picture of frog by Ben Fredericson" class="img-fluid">
                        </a>
                    </div>
                  </div>
                  <div class="col-8">
                    <h6>
                      <a href="#">Capto Conventio Nobis Te</a>
                    </h6>
                  </div>
                </div>
                <!-- Popular blog post 3 -->
                <div class="row blog-post">
                  <div class="col-4">
                    <div class="blog-media">
                      <a href="#">
                          <img src="assets/img/blog/bee.jpg" alt="Picture of frog by Ben Fredericson" class="img-fluid">
                        </a>
                    </div>
                  </div>
                  <div class="col-8">
                    <h6>
                      <a href="#">Et Refoveo Suscipere Tego</a>
                    </h6>
                  </div>
                </div>
                <!-- Popular blog post 4 -->
                <div class="row blog-post">
                  <div class="col-4">
                    <div class="blog-media">
                      <a href="#">
                          <img src="assets/img/blog/butterfly.jpg" alt="Picture of frog by Ben Fredericson" class="img-fluid">
                        </a>
                    </div>
                  </div>
                  <div class="col-8">
                    <h6>
                      <a href="#">Accumsan Gemino Modo Persto</a>
                    </h6>
                  </div>
                </div>
              </div>
              <!-- Latest tab content -->
              <div class="tab-pane fade blog-roll-mini" id="latest">
                <!-- Latest blog post 1 -->
                <div class="row blog-post">
                  <div class="col-4">
                    <div class="blog-media">
                      <a href="#">
                          <img src="assets/img/blog/ladybird.jpg" alt="Picture of frog by Ben Fredericson" class="img-fluid">
                        </a>
                    </div>
                  </div>
                  <div class="col-8">
                    <h6>
                      <a href="#">Elit Ulciscor Vulputate Zelus</a>
                    </h6>
                  </div>
                </div>
                <!-- Latest blog post 2 -->
                <div class="row blog-post">
                  <div class="col-4">
                    <div class="blog-media">
                      <a href="#">
                          <img src="assets/img/blog/ladybird.jpg" alt="Picture of frog by Ben Fredericson" class="img-fluid">
                        </a>
                    </div>
                  </div>
                  <div class="col-8">
                    <h6>
                      <a href="#">Abluo Fere Sagaciter Vicis</a>
                    </h6>
                  </div>
                </div>
                <!-- Latest blog post 3 -->
                <div class="row blog-post">
                  <div class="col-4">
                    <div class="blog-media">
                      <a href="#">
                          <img src="assets/img/blog/bee.jpg" alt="Picture of frog by Ben Fredericson" class="img-fluid">
                        </a>
                    </div>
                  </div>
                  <div class="col-8">
                    <h6>
                      <a href="#">Dolus Pagus Veniam Vicis</a>
                    </h6>
                  </div>
                </div>
                <!-- Latest blog post 4 -->
                <div class="row blog-post">
                  <div class="col-4">
                    <div class="blog-media">
                      <a href="#">
                          <img src="assets/img/blog/water-pump.jpg" alt="Picture of frog by Ben Fredericson" class="img-fluid">
                        </a>
                    </div>
                  </div>
                  <div class="col-8">
                    <h6>
                      <a href="#">Ea Proprius Ullamcorper Valde</a>
                    </h6>
                  </div>
                </div>
              </div>
            </div>
          </div>
                    
                </div>
            </div>
        </div>
      
    </div>
  </div>

  <!-- ======== @Region: #content-below ======== -->
  <div id="content-below">
    <!-- Awesome features call to action -->
    <div class="bg-primary bg-op-9 text-white py-4">
      <div class="container">
        <div class="row text-center text-lg-left align-items-lg-center">
          <div class="col-12 col-lg-7 text-white">
            <h3 class="font-weight-bold my-0 text-uppercase">
              Awesome Features
            </h3>
            <p class="font-weight-normal op-9 my-0"> <i class="la la-check-circle-o"></i> 99.9% Uptime <i class="la la-check-circle-o ml-lg-3"></i> Free Upgrades <i class="la la-check-circle-o ml-lg-3"></i> Fully Responsive <i class="la la-check-circle-o ml-lg-3"></i>              Bug Free </p>
          </div>
          <div class="col-12 col-lg-5 py-2 text-lg-right">
            <a href="#" class="btn btn-xlg btn-white btn-rounded shadow-lg bg-light bg-op-8 bg-hover-white">Get AppStrap<i class="fa fa-arrow-right ml-2 mt-1"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>

 <?php include 'footer.php' ?>