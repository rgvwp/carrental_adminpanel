<!DOCTYPE html>
<html lang="en">

<head>
  <title>Homepage</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />

  <!-- @todo: fill with your company info or remove -->
  <meta name="description" content="">
  <meta name="author" content="Themelize.me">

  <!-- Bootstrap v4.1.1 CSS via CDN -->
<!--  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">-->
<link href="<?php echo base_url();?>front/assets/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>front/assets/css/font-awesome.css" rel="stylesheet">
<link href="<?php echo base_url();?>front/assets/css/font-awesome.min.css" rel="stylesheet">


  <!-- Theme style -->
  <link href="<?php echo base_url();?>front/assets/css/theme-style.min.css" rel="stylesheet">

  <!--Your custom colour override-->
  <link href="#" id="colour-scheme" rel="stylesheet">

  <!-- Your custom override -->
  <link href="<?php echo base_url();?>front/assets/css/custom-style.css" rel="stylesheet">



  <!-- Optional: ICON SETS -->
  <!-- Iconset: Font Awesome 5.0.13 via CDN -->
<!--    <link href="assets/css/all.css" rel="stylesheet">-->
  <!-- Iconset: flag icons - http://lipis.github.io/flag-icon-css/ -->
  <link href="<?php echo base_url();?>front/assets/plugins/flag-icon-css/css/flag-icon.min.css" rel="stylesheet">
  <!-- Iconset: ionicons - http://ionicons.com/ -->
    <link href="<?php echo base_url();?>front/assets/css/ionicons.min.css" rel="stylesheet">
  <!-- Iconset: Linearicons - https://linearicons.com/free -->
    <link href="<?php echo base_url();?>front/assets/css/icon-font.min.css" rel="stylesheet">
  <!-- Iconset: Lineawesome - https://icons8.com/articles/line-awesome -->
    <link href="<?php echo base_url();?>front/assets/css/line-awesome.min.css" rel="stylesheet">


  <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url();?>front/assets/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url();?>front/assets/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>front/assets/favicons/favicon-16x16.png">
  <link rel="manifest" href="<?php echo base_url();?>front/assets/favicons/manifest.json">
  <link rel="shortcut icon" href="<?php echo base_url();?>front/assets/favicons/favicon.ico">
  <meta name="msapplication-config" content="<?php echo base_url();?>front/assets/favicons/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">


  <!-- Google fonts -->
    <link href="<?php echo base_url();?>front/assets/css/font-family.css" rel="stylesheet">

  <!--Plugin: Retina.js (high def image replacement) - @see: http://retinajs.com/-->
  <script src="<?php echo base_url();?>front/assets/js/retina.min.js"></script>
</head>

<!-- ======== @Region: body ======== -->

<body class="page page-index navbar-layout-default">

  <!-- @plugin: page loading indicator, delete to remove loader -->
  <div class="page-loader" data-toggle="page-loader"></div>

  <a id="#top" href="#content" class="sr-only">Skip to content</a>

  <!-- ======== @Region: #header ======== -->
  <div id="header">

    <!--Header upper region-->
    <div class="header-upper">
    
      <div class="header-inner container">
        <!--user menu-->
        <div class="header-block-flex order-1 mr-auto">
          <nav class="nav nav-sm header-block-flex">
            <a class="nav-link d-md-none" href="<?php echo base_url();?>Frontuser/loginpage"><i class="fa fa-user"></i></a>
            <a class="nav-link text-xs text-uppercase d-none d-md-block" href="<?php echo base_url();?>Frontuser/signup">Sign Up</a> <a class="nav-link text-xs text-uppercase d-none d-md-block" href="<?php echo base_url();?>Frontuser/loginpage">Login</a>
          </nav>
          <div class="header-divider header-divider-sm"></div>
          <!--language menu-->
          <div class="dropdown dropdowns-no-carets">
            <a href="#en" class="nav-link text-xs p-0 dropdown-toggle ml-1" data-toggle="dropdown"><i class="flag-icon flag-icon-gb"></i></a>
            <div class="dropdown-menu dropdown-menu-sm rounded dropdown-menu-arrow border-w-2 ml-2-neg">
              <a href="#es" class="dropdown-item lang-es"><i class="flag-icon flag-icon-es"></i> Spanish</a>
              <a href="#pt" class="dropdown-item lang-pt"><i class="flag-icon flag-icon-pt"></i> Portguese</a>
              <a href="#cn" class="dropdown-item lang-cn"><i class="flag-icon flag-icon-cn"></i> Chinese</a>
              <a href="#se" class="dropdown-item lang-se"><i class="flag-icon flag-icon-se"></i> Swedish</a>
            </div>
          </div>
        </div>
        <!--social media icons-->
        <div class="nav nav-icons header-block order-12">
          <!--@todo: replace with company social media details-->
            <?php       $urlquery = $this->db->get('tblmanagerurl')->row_array();?>
          <a href="http://<?php echo $urlquery['manageUrltwitter'];?> " class="nav-link"> <i class="fa fa-twitter-square icon-1x"></i> <span class="sr-only">Twitter</span> </a>
          <a href="http://<?php echo $urlquery['manageUrlfb'];?>" class="nav-link"> <i class="fa fa-facebook-square icon-1x"></i> <span class="sr-only">Facebook</span> </a>
          <a href="http://<?php echo $urlquery['manageUrlin'];?>" class="nav-link"> <i class="fa fa-linkedin-square icon-1x"></i> <span class="sr-only">Linkedin</span> </a>
          <a href="http://<?php echo $urlquery['manageUrlgplus'];?>" class="nav-link"> <i class="fa fa-google-plus-square icon-1x"></i> <span class="sr-only">Google plus</span> </a>
        </div>
      </div>
    </div>
    <div data-toggle="sticky">

      <!--Header search region - hidden by default -->
      <div class="header-search collapse" id="search">
        <form class="search-form container">
          <input type="text" name="search" class="form-control search" value="" placeholder="Search">
          <button type="button" class="btn btn-link"><span class="sr-only">Search </span><i class="fa fa-search fa-flip-horizontal search-icon"></i></button>
          <button type="button" class="btn btn-link close-btn" data-toggle="search-form-close"><span class="sr-only">Close </span><i class="fa fa-times search-icon"></i></button>
        </form>
      </div>

      <!--Header & Branding region-->











      <div class="header">
       
        <div class="header-inner container">
          <!--branding/logo -->
          <div class="header-brand">
            <a class="header-brand-text" href="<?php echo base_url();?>Frontuser/homepage" title="Home">
              <h1 class="h2">
<!--                <span class="header-brand-text-alt">App</span>Strap<span class="header-brand-text-alt">.</span>-->
                <img src="<?php echo base_url();?>front/assets/img/demo_logo.png" class="img-responsive" style="width: 130px;">
              </h1>
            </a>
<!--            <div class="header-divider d-none d-lg-block"></div>-->
<!--            <div class="header-slogan d-none d-lg-block">Bootstrap 4 Theme</div>-->
          </div>
          <!-- other header content -->
          <div class="header-block order-12">

            <!--Search trigger -->
            <a href="#search" class="btn btn-icon btn-link header-btn float-right order-11" data-toggle="search-form" data-target=".header-search"><i class="fa fa-search fa-flip-horizontal search-icon"></i></a>

            <!-- mobile collapse menu button - data-toggle="collapse" = default BS menu - data-toggle="off-canvas" = Off-cavnas Menu - data-toggle="overlay" = Overlay Menu -->
            <a href="#top" class="btn btn-link btn-icon header-btn float-right d-lg-none" data-toggle="off-canvas" data-target=".navbar-main" data-settings='{"cloneTarget":true, "targetClassExtras": "navbar-offcanvas"}'> <i class="fa fa-bars"></i> </a>
            <!--Show/hide trigger for #offcanvas-sidebar -->
            <a href="#" title="Click me you'll get a surprise" class="btn btn-icon btn-link header-btn float-right order-last" data-toggle="off-canvas" data-target="#offcanvas-sidebar" data-settings='{"cloneTarget":false}'> <i class="fa fa-ellipsis-v"></i> </a>
          </div>

          <div class="navbar navbar-expand-md navbar-static-top">
            <!--everything within this div is collapsed on mobile-->
            <div class="navbar-main collapse">
              <!--main navigation-->
              <ul class="nav navbar-nav navbar-nav-stretch float-lg-right dropdown-effect-fade">
                <!-- Homepages -->
                <li class="nav-item dropdown dropdown-mega-menu active">
                  <a href="<?php echo base_url();?>Frontuser/homepage" class="nav-link" id="indexs-drop"> 
                    <span>Home</span> </a>
                </li>
                <!-- Pages -->
                <li class="nav-item">
                  <a href="<?php echo base_url();?>Frontuser/about" class="nav-link " id="more-drop">About</a>
                </li>
                <li class="nav-item">
                  <a href="<?php echo base_url();?>Frontuser/services" class="nav-link " id="more-drop">Services</a>
                </li>
                <li class="nav-item">
                  <a href="<?php echo base_url();?>Frontuser/contact" class="nav-link " id="more-drop">Contact</a>
                </li>
                <li class="nav-item">
                    <a href="<?php echo base_url();?>Frontuser/plan" class="nav-link " id="more-drop">Plans</a>
                </li>
<!--                <li class="nav-item dropdown">
                  <a href="#" class="nav-link dropdown-toggle" id="pages-drop" data-toggle="dropdown" data-hover="dropdown">Pages</a>
                 
                  <div class="dropdown-menu">
                      <a href="about.php" class="dropdown-item" id="about-drop">About</a>
                     
                    <div class="dropdown dropdown-submenu">
                      <a href="blog.html" class="dropdown-item dropdown-toggle" id="blog-drop" data-toggle="dropdown" data-hover="dropdown" data-close-others="false">Blog</a>
                     
                      <div class="dropdown-menu">
                       <a href="blog-grid.php" class="dropdown-item">Blog List</a> 
                       <a href="blog-post.php" class="dropdown-item">Blog Post</a>
                       </div>
                    </div>
                    
                      <a href="pricing-tables.php" class="dropdown-item" id="pricing-drop" >Pricing</a>
                    
                   
                      <a href="timeline-stacked.php" class="dropdown-item " id="timeline-drop">Timeline</a>
                  
                  
                    <a href="customers.php" class="dropdown-item">Customers</a>
                     <a href="features.php" class="dropdown-item">Features/Services</a>
                      <a href="login.php" class="dropdown-item">Login</a>
                       <a href="signup.php" class="dropdown-item">Sign Up</a>           
                                <a href="starter.php" class="dropdown-item">Starter Snippets</a>
                                 <a href="404.php" class="dropdown-item">404 Error</a>
                  </div>
                </li>-->

                <!-- Shop megamenu -->
<!--                <li class="nav-item dropdown dropdown-mega-menu dropdown-mega-menu-75">
                  <a href="#" class="nav-link dropdown-toggle" id="shop-drop" data-toggle="dropdown" data-hover="dropdown">Shop</a>
                   Dropdown Menu - mega menu
                  <div class="dropdown-menu dropdown-menu-right">
                    <div class="row">
                      <div class="col-lg-6">
                        <h3 class="dropdown-header mt-0 pt-0">
                          Pages
                        </h3>
                        <a href="#" class="dropdown-item">Shop Homepage</a>
                <a href="#" class="dropdown-item">Cart</a>
                 <a href="#" class="dropdown-item">Checkout</a> 
                        <a href="#" class="dropdown-item">Confirmation</a>                       
                         <a href="#" class="dropdown-item">Products Grid</a>
                          <a href="#" class="dropdown-item">Products List</a> 
                          <a href="#" class="dropdown-item">Product View</a>
                      </div>
                      <div class="col-lg-6 d-none d-lg-block">
                        <h3 class="dropdown-header mt-0 pt-0">
                          Widget <span class="badge badge-primary text-uppercase">Hot</span>
                        </h3>
                        <div class="dropdown-widget">
                           Shop product carousel Uses Owl Carousel plugin All options here are customisable from the data-owl-carousel-settings="{OBJECT}" item via data- attributes: http://www.owlgraphic.com/owlcarousel/#customizing ie. data-settings="{"items": "4", "lazyLoad":"true", "navigation":"true"}" 
                          <div class="products-carousel owl-nav-over" data-toggle="owl-carousel" data-owl-carousel-settings='{"items": 1,"responsive":{"0":{"items":1,"nav":true, "dots":false}}}'>
                            <a href="#">
                                <img src="assets/img/shop/jacket-1.jpg" alt="Item 1 image" class="lazyOwl img-fluid" />
                              </a>
                            <a href="#">
                                <img src="assets/img/shop/jacket-2.jpg" alt="Item 2 image" class="lazyOwl img-fluid" />
                              </a>
                            <a href="#">
                                <img src="assets/img/shop/jacket-3.jpg" alt="Item 3 image" class="lazyOwl img-fluid" />
                              </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>-->
                <!-- Mega menu example -->
<!--                <li class="nav-item dropdown dropdown-persist dropdown-mega-menu dropdown-mega-menu-50">
                  <a href="#" class="nav-link dropdown-toggle" id="megamenu-drop" data-toggle="dropdown" data-hover="dropdown">Mega Menu</a>
                   Dropdown Menu - Mega Menu 
                  <div class="dropdown-menu dropdown-menu-right">
                     Nav tabs 
                    <ul class="nav nav-pills nav-pills-border-bottom-inside flex-column flex-lg-row" role="tablist">
                      <li class="nav-item"> <a class="nav-link p-3 active text-center font-weight-bold text-uppercase" data-toggle="tab" data-target=".menu-tab-1" role="tab">Mega Menu Tab 1</a> </li>
                      <li class="nav-item"> <a class="nav-link p-3 text-center font-weight-bold text-uppercase" data-toggle="tab" data-target=".menu-tab-2" role="tab">Mega Menu Tab 2</a> </li>
                    </ul>
                     Tab panes 
                    <div class="tab-content py-3">
                      <div class="tab-pane active show menu-tab-1" role="tabpanel">
                        <div class="row text-center">
                          <div class="col-lg-4 py-2">
                            <i class="fa fa-tachometer-alt icon-3x op-6"></i>
                            <h5 class="mt-2">
                              Fully Optimized
                            </h5>
                            <p class="text-sm mb-0">Abdo ad aliquam humo interdico meus sagaciter.</p>
                          </div>
                          <div class="col-lg-4 py-2">
                            <i class="fa fa-wrench icon-3x op-6"></i>
                            <h5 class="mt-2">
                              Free Support
                            </h5>
                            <p class="text-sm mb-0">Abbas aliquam esse exputo nobis praesent saluto sino valde volutpat.</p>
                          </div>
                          <div class="col-lg-4 py-2">
                            <i class="fa fa-rocket icon-3x op-6"></i>
                            <h5 class="mt-2">
                              Free Upgrades
                            </h5>
                            <p class="text-sm mb-0">Abico cui gravis iaceo interdico plaga valetudo.</p>
                          </div>
                        </div>
                      </div>
                      <div class="tab-pane menu-tab-2" role="tabpanel">
                        <div class="row text-center">
                          <div class="col-lg-4 py-2">
                            <i class="fa fa-chart-line icon-3x op-6"></i>
                            <h5 class="mt-2">
                              99.9% Uptime
                            </h5>
                            <p class="text-sm mb-0">Comis elit meus os roto tamen tum validus.</p>
                          </div>
                          <div class="col-lg-4 py-2">
                            <i class="fa fa-users icon-3x op-6"></i>
                            <h5 class="mt-2">
                              Multiuser
                            </h5>
                            <p class="text-sm mb-0">Antehabeo immitto importunus praesent proprius singularis valetudo vereor.</p>
                          </div>
                          <div class="col-lg-4 py-2">
                            <i class="fa fa-plug icon-3x op-6"></i>
                            <h5 class="mt-2">
                              Plug n play
                            </h5>
                            <p class="text-sm mb-0">In modo odio proprius.</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>-->
              </ul>
            </div>
            <!--/.navbar-collapse -->
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- ======== @Region: #highlighted ======== -->
