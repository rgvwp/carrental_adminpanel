<?php include 'header.php' ?>
<style>
    .card
    {
        background-color: #fff;
        border: 1px solid #e5e7f2;
    }
    .card:hover {
    box-shadow: 0 5px 40px rgba(0,0,0,.05);
    }
    .card-header 
    {
        border-bottom: 0;
        background:#fff;
    }
    .card-header .card-link
    {
        font-size: 15px;
        cursor: pointer;
    }
    .card-body
    {
        font-size: 14px;
        padding-top: 0;
    }
    #green
    {
        background: #f1f1f1;
    }
    .justify-content-center
    {
        margin:0;
    }
    .justify-content-center .nav-item a
    {
        max-width: 150px;
        text-align: center;
        font-size: 14px;
        color:#999;
        font-weight: 600;
        padding: 15px 26px;
    }
    .faq-tab-content .tab-pane
    {
        padding: 70px 0;
    }
    .fa.pull-right
    {
        margin-top: 6px;
    }
    </style>
  <!-- ======== @Region: #content ======== -->
  <div id="content">
    <div class="container">
      <h2 class="title-divider">
        FAQs
      </h2>

</div>
    </div>
  
  

        <div class="container">
            <div class="row">
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class=" demo">

    <div class="panel-group" id="accordion">

        <?php 
        $i=1;
        $faq = $this->db->get_where(' tblcmsfaq',array('cmsfaqstatus'=>1));
        foreach($faq->result() as $key=>$value){?>
        
        
        
        <div class="card">
            <div class="card-header" >
                <h4 class="card-link" data-toggle="collapse" href="#collapse<?php echo $i;?>">
                    <a>
                        <i class="pull-right  fa fa-plus"></i>
                        <?php echo $i;?>. <?php echo $value->cmsfaqquestions;?>
                    </a>
                </h4>
            </div>
            <div id="collapse<?php echo $i;?>" class="collapse show" data-parent="#accordion">
                <div class="card-body">
<?php echo $value->cmsfaqanswers;?>                </div>
            </div>
        </div>

        <?php   $i++;  } ?>
<!--        <div class="card">
            <div class="card-header" >
                <h4 class="card-link" data-toggle="collapse" href="#collapseTwo">
                    <a>
                        <i class="pull-right  fa fa-plus"></i>
                       2. Versatile hosting plans and pricing
                    </a>
                </h4>
            </div>
            <div id="collapseTwo" class="collapse" data-parent="#accordion">
                <div class="card-body">
                     Tincidunt elit magnis nulla facilisis. Dolor sagittis maecenas. Sapien nunc amet ultrices, dolores sit ipsum velit purus aliquet, massa fringilla leo orci.
                </div>
            </div>
        </div>-->

<!--        <div class="card">
            <div class="card-header" >
                <h4 class="card-link" data-toggle="collapse" href="#collapseThree">
                    <a>
                        <i class="pull-right  fa fa-plus"></i>
                        3. Compatibility with premium plugins
                    </a>
                </h4>
            </div>
            <div id="collapseThree" class="collapse" data-parent="#accordion">
                <div class="card-body">
                      Tincidunt elit magnis nulla facilisis. Dolor sagittis maecenas. Sapien nunc amet ultrices, dolores sit ipsum velit purus aliquet, massa fringilla leo orci.
                </div>
            </div>
        </div>-->
<!--        <div class="card">
            <div class="card-header" >
                <h4 class="card-link" data-toggle="collapse" href="#collapseFour">
                    <a>
                        <i class="pull-right  fa fa-plus"></i>
                        4. Install theme demo contents
                    </a>
                </h4>
            </div>
            <div id="collapseFour" class="collapse" data-parent="#accordion">
                <div class="card-body">
                      Tincidunt elit magnis nulla facilisis. Dolor sagittis maecenas. Sapien nunc amet ultrices, dolores sit ipsum velit purus aliquet, massa fringilla leo orci.
                </div>
            </div>
        </div>-->
<!--        <div class="card">
            <div class="card-header" >
                <h4 class="card-link" data-toggle="collapse" href="#collapseFive">
                    <a>
                        <i class="pull-right  fa fa-plus"></i>
                        5. Layout and design options
                    </a>
                </h4>
            </div>
            <div id="collapseFive" class="collapse" data-parent="#accordion">
                <div class="card-body">
                      Tincidunt elit magnis nulla facilisis. Dolor sagittis maecenas. Sapien nunc amet ultrices, dolores sit ipsum velit purus aliquet, massa fringilla leo orci.
                </div>
            </div>
        </div>-->

    </div><!-- panel-group -->
    
    
</div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div>
                  <?php   $faqimage = $this->db->get_where('tblcmsfaq',array('cmsfaqstatus'=>"image"))->row_array(); ?>

            
            
            <img class="img-responsive" src="<?php echo base_url();?>uploads/images/<?php echo $faqimage['cmsfaqimage'];?>" style="width:100%">
        </div>
    </div>
</div>
        </div>
    
      

  </div>

  <!-- ======== @Region: #content-below ======== -->
  <div id="content-below">
    <!-- Awesome features call to action -->
    <div class="bg-primary bg-op-9 text-white py-4">
      <div class="container">
        <div class="row text-center text-lg-left align-items-lg-center">
          <div class="col-12 col-lg-7 text-white">
            <h3 class="font-weight-bold my-0 text-uppercase">
              Awesome Features
            </h3>
            <p class="font-weight-normal op-9 my-0"> <i class="la la-check-circle-o"></i> 99.9% Uptime <i class="la la-check-circle-o ml-lg-3"></i> Free Upgrades <i class="la la-check-circle-o ml-lg-3"></i> Fully Responsive <i class="la la-check-circle-o ml-lg-3"></i>              Bug Free </p>
          </div>
          <div class="col-12 col-lg-5 py-2 text-lg-right">
            <a href="#" class="btn btn-xlg btn-white btn-rounded shadow-lg bg-light bg-op-8 bg-hover-white">Get AppStrap<i class="fa fa-arrow-right ml-2 mt-1"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>

 <?php include 'footer.php' ?>