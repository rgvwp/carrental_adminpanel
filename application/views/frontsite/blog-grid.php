<?php include 'header.php' ?>

  <!-- ======== @Region: #content ======== -->
  <div id="content">
    <div class="container">
      <h2 class="title-divider">
        <span>Company <span class="font-weight-normal text-muted">Blog</span></span>
        <small>We love to talk!</small>
      </h2>

      <div class="row">
        <!--Blog Grid -->
        <div class="col-md-9">
          <div class="blog-roll blog-grid" data-toggle="isotope-grid" data-isotope-options='{"itemSelector": ".grid-item"}'>
            <div class="row">

              <!--Timeline item 1-->
              <div class="col-sm-6 col-md-4 grid-item">
                <div class="blog-post">
                  <div class="blog-media">
                    <a href="#">
                        <img src="assets/img/blog/ladybird.jpg" alt="Picture of frog by Ben Fredericson" class="img-fluid" />
                      </a>
                  </div>
                  <div class="mt-4">
                    <div class="date-wrapper date-wrapper-horizontal">
                      <span class="date-m bg-primary">May</span>
                      <span class="date-d">30</span> </div>
                    <div class="tags"><a href="#" class="text-primary">coding</a> / <a href="#" class="type">image</a></div>
                    <h4 class="timeline-item-title">
                      <a href="#">Duis Enim Fere Hos Velit</a>
                    </h4>
                    <p class="timeline-item-description">Amet conventio distineo exputo nulla sed tego uxor. Abigo consectetuer dignissim illum patria tincidunt turpis.</p>
                    <a href="blog-post.html" class="btn btn-link"><i class="fa fa-arrow-circle-right"></i> Read more</a>
                  </div>
                </div>
              </div>

              <!--Timeline item 2-->
              <div class="col-sm-6 col-md-4 grid-item">
                <div class="blog-post">
                  <div class="blog-media">
                    <a href="#">
                        <img src="assets/img/blog/fly.jpg" alt="Picture of frog by Ben Fredericson" class="img-fluid" />
                      </a>
                  </div>
                  <div class="mt-4">
                    <div class="date-wrapper date-wrapper-horizontal">
                      <span class="date-m bg-primary">May</span>
                      <span class="date-d">28</span> </div>
                    <div class="tags"><a href="#" class="text-primary">jobs</a> / <a href="#" class="type">image</a></div>
                    <h4 class="timeline-item-title">
                      <a href="#">Antehabeo Causa Meus Quae Voco</a>
                    </h4>
                    <p class="timeline-item-description">Blandit consequat facilisi. Adipiscing amet distineo facilisis metuo quae sed usitas velit.</p>
                    <a href="blog-post.html" class="btn btn-link"><i class="fa fa-arrow-circle-right"></i> Read more</a>
                  </div>
                </div>
              </div>

              <!--Timeline item 3-->
              <div class="col-sm-6 col-md-4 grid-item">
                <div class="blog-post">
                  <div class="blog-media">
                    <a href="#">
                        <img src="assets/img/blog/bee.jpg" alt="Picture of frog by Ben Fredericson" class="img-fluid" />
                      </a>
                  </div>
                  <div class="mt-4">
                    <div class="date-wrapper date-wrapper-horizontal">
                      <span class="date-m bg-primary">May</span>
                      <span class="date-d">27</span> </div>
                    <div class="tags"><a href="#" class="text-primary">design</a> / <a href="#" class="type">image</a></div>
                    <h4 class="timeline-item-title">
                      <a href="#">Causa Hos Nunc Quadrum Tincidunt</a>
                    </h4>
                    <p class="timeline-item-description">In laoreet saluto secundum sino. Abluo acsi ea erat hos ratis rusticus.</p>
                    <a href="blog-post.html" class="btn btn-link"><i class="fa fa-arrow-circle-right"></i> Read more</a>
                  </div>
                </div>
              </div>

              <!--Timeline item 4-->
              <div class="col-sm-6 col-md-4 grid-item">
                <div class="blog-post">
                  <div class="blog-media">
                    <div class="slider-wrapper">
                      <div class="flexslider slider-mini-nav" data-slidernav="auto" data-transition="fade">
                        <div class="slides">
                          <div class="slide">
                            <img src="assets/img/blog/ladybird.jpg" alt="Picture of frog by Ben Fredericson" class="img-fluid" />
                          </div>
                          <div class="slide">
                            <img src="assets/img/blog/ape.jpg" alt="Picture of frog by Ben Fredericson" class="img-fluid" />
                          </div>
                          <div class="slide">
                            <img src="assets/img/blog/butterfly.jpg" alt="Picture of frog by Ben Fredericson" class="img-fluid" />
                          </div>
                          <div class="slide">
                            <img src="assets/img/blog/frog.jpg" alt="Picture of frog by Ben Fredericson" class="img-fluid" />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="mt-4">
                    <div class="date-wrapper date-wrapper-horizontal">
                      <span class="date-m bg-primary">May</span>
                      <span class="date-d">26</span> </div>
                    <div class="tags"><a href="#" class="text-primary">coding</a> / <a href="#" class="type">slideshow</a></div>
                    <h4 class="timeline-item-title">
                      <a href="#">Caecus Ideo Illum Immitto Nutus</a>
                    </h4>
                    <p class="timeline-item-description">Dolus quadrum saepius si. Ad decet eros eu iriure patria ratis si validus.</p>
                    <a href="blog-post.html" class="btn btn-link"><i class="fa fa-arrow-circle-right"></i> Read more</a>
                  </div>
                </div>
              </div>

              <!--Timeline item 5-->
              <div class="col-sm-6 col-md-4 grid-item">
                <div class="blog-post">
                  <div class="blog-media">
                    <a href="#">
                        <img src="assets/img/blog/frog.jpg" alt="Picture of frog by Ben Fredericson" class="img-fluid" />
                      </a>
                  </div>
                  <div class="mt-4">
                    <div class="date-wrapper date-wrapper-horizontal">
                      <span class="date-m bg-primary">May</span>
                      <span class="date-d">24</span> </div>
                    <div class="tags"><a href="#" class="text-primary">health</a> / <a href="#" class="type">image</a></div>
                    <h4 class="timeline-item-title">
                      <a href="#">Abigo Consectetuer Luptatum Si Ymo</a>
                    </h4>
                    <p class="timeline-item-description">Abluo at enim esca facilisis nobis pecus pneum. Abico acsi autem camur eu tum.</p>
                    <a href="blog-post.html" class="btn btn-link"><i class="fa fa-arrow-circle-right"></i> Read more</a>
                  </div>
                </div>
              </div>

              <!--Timeline item 6-->
              <div class="col-sm-6 col-md-4 grid-item">
                <div class="blog-post">
                  <div class="blog-media">
                    <object width="560" height="315">
                        <param name="movie" value="//www.youtube.com/v/YXVoqJEwqoQ?version=3&amp;hl=en_US&amp;rel=0"></param>
                        <param name="allowFullScreen" value="true"></param>
                        <param name="allowscriptaccess" value="always"></param>
                        <embed src="//www.youtube.com/v/YXVoqJEwqoQ?version=3&amp;hl=en_US&amp;rel=0" type="application/x-shockwave-flash" width="560" height="315" allowscriptaccess="always" allowfullscreen="true"></embed>
                      </object>
                  </div>
                  <div class="mt-4">
                    <div class="date-wrapper date-wrapper-horizontal">
                      <span class="date-m bg-primary">May</span>
                      <span class="date-d">23</span> </div>
                    <div class="tags"><a href="#" class="text-primary">health</a> / <a href="#" class="type">video</a></div>
                    <h4 class="timeline-item-title">
                      <a href="#">At Damnum Facilisi Vereor Zelus</a>
                    </h4>
                    <p class="timeline-item-description">Erat huic iaceo macto quis sit tincidunt. Aliquam brevitas capto dolus facilisi iusto mos nunc sagaciter voco.</p>
                    <a href="blog-post.html" class="btn btn-link"><i class="fa fa-arrow-circle-right"></i> Read more</a>
                  </div>
                </div>
              </div>

              <!--Timeline item 7-->
              <div class="col-sm-6 col-md-4 grid-item">
                <div class="blog-post">
                  <div class="blog-media">
                    <a href="#">
                        <img src="assets/img/blog/butterfly.jpg" alt="Picture of frog by Ben Fredericson" class="img-fluid" />
                      </a>
                  </div>
                  <div class="mt-4">
                    <div class="date-wrapper date-wrapper-horizontal">
                      <span class="date-m bg-primary">May</span>
                      <span class="date-d">22</span> </div>
                    <div class="tags"><a href="#" class="text-primary">jobs</a> / <a href="#" class="type">image</a></div>
                    <h4 class="timeline-item-title">
                      <a href="#">Illum Persto Sit Tamen Typicus</a>
                    </h4>
                    <p class="timeline-item-description">Acsi blandit lenis nisl nostrud olim quis si venio. Cui damnum euismod interdico obruo olim similis.</p>
                    <a href="blog-post.html" class="btn btn-link"><i class="fa fa-arrow-circle-right"></i> Read more</a>
                  </div>
                </div>
              </div>

              <!--Timeline item 8-->
              <div class="col-sm-6 col-md-4 grid-item">
                <div class="blog-post">
                  <div class="blog-media">
                    <iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/113479203&color=ff5500&auto_play=false&hide_related=false&show_artwork=true"></iframe>
                  </div>
                  <div class="mt-4">
                    <div class="date-wrapper date-wrapper-horizontal">
                      <span class="date-m bg-primary">May</span>
                      <span class="date-d">21</span> </div>
                    <div class="tags"><a href="#" class="text-primary">jobs</a> / <a href="#" class="type">audio</a></div>
                    <h4 class="timeline-item-title">
                      <a href="#">Aliquam Gilvus Iustum Quibus Vulputate</a>
                    </h4>
                    <p class="timeline-item-description">Et fere huic quia. Augue iusto roto utrum. Antehabeo eu exputo iusto proprius suscipere.</p>
                    <a href="blog-post.html" class="btn btn-link"><i class="fa fa-arrow-circle-right"></i> Read more</a>
                  </div>
                </div>
              </div>

              <!--Timeline item 9-->
              <div class="col-sm-6 col-md-4 grid-item">
                <div class="blog-post">
                  <div class="blog-media">
                    <a href="#">
                        <img src="assets/img/blog/frog.jpg" alt="Picture of frog by Ben Fredericson" class="img-fluid" />
                      </a>
                  </div>
                  <div class="mt-4">
                    <div class="date-wrapper date-wrapper-horizontal">
                      <span class="date-m bg-primary">May</span>
                      <span class="date-d">19</span> </div>
                    <div class="tags"><a href="#" class="text-primary">culture</a> / <a href="#" class="type">image</a></div>
                    <h4 class="timeline-item-title">
                      <a href="#">Antehabeo Diam Letalis Pecus Vulputate</a>
                    </h4>
                    <p class="timeline-item-description">Appellatio et facilisi melior saepius scisco turpis. Antehabeo facilisis fere genitus gilvus magna utinam validus volutpat.</p>
                    <a href="blog-post.html" class="btn btn-link"><i class="fa fa-arrow-circle-right"></i> Read more</a>
                  </div>
                </div>
              </div>

              <!--Timeline item 10-->
              <div class="col-sm-6 col-md-4 grid-item">
                <div class="blog-post">
                  <div class="blog-media">
                    <a href="#">
                        <img src="assets/img/blog/water-pump.jpg" alt="Picture of frog by Ben Fredericson" class="img-fluid" />
                      </a>
                  </div>
                  <div class="mt-4">
                    <div class="date-wrapper date-wrapper-horizontal">
                      <span class="date-m bg-primary">May</span>
                      <span class="date-d">18</span> </div>
                    <div class="tags"><a href="#" class="text-primary">jobs</a> / <a href="#" class="type">image</a></div>
                    <h4 class="timeline-item-title">
                      <a href="#">Causa Dolor Hendrerit Imputo Quidem</a>
                    </h4>
                    <p class="timeline-item-description">Consectetuer et interdico meus nulla quibus saepius utrum. Damnum ea molior nutus quidne sino ullamcorper.</p>
                    <a href="blog-post.html" class="btn btn-link"><i class="fa fa-arrow-circle-right"></i> Read more</a>
                  </div>
                </div>
              </div>

              <!--Timeline item 11-->
              <div class="col-sm-6 col-md-4 grid-item">
                <div class="blog-post">
                  <div class="blog-media">
                    <a href="#">
                        <img src="assets/img/blog/bee.jpg" alt="Picture of frog by Ben Fredericson" class="img-fluid" />
                      </a>
                  </div>
                  <div class="mt-4">
                    <div class="date-wrapper date-wrapper-horizontal">
                      <span class="date-m bg-primary">May</span>
                      <span class="date-d">17</span> </div>
                    <div class="tags"><a href="#" class="text-primary">jobs</a> / <a href="#" class="type">image</a></div>
                    <h4 class="timeline-item-title">
                      <a href="#">Adipiscing Elit Esse Loquor Utinam</a>
                    </h4>
                    <p class="timeline-item-description">Gemino gilvus iustum os pecus qui sit vulpes. Abbas diam eros ille occuro pecus praemitto roto si.</p>
                    <a href="blog-post.html" class="btn btn-link"><i class="fa fa-arrow-circle-right"></i> Read more</a>
                  </div>
                </div>
              </div>

              <!--Timeline item 12-->
              <div class="col-sm-6 col-md-4 grid-item">
                <div class="blog-post">
                  <div class="blog-media">
                    <a href="#">
                        <img src="assets/img/blog/ape.jpg" alt="Picture of frog by Ben Fredericson" class="img-fluid" />
                      </a>
                  </div>
                  <div class="mt-4">
                    <div class="date-wrapper date-wrapper-horizontal">
                      <span class="date-m bg-primary">May</span>
                      <span class="date-d">15</span> </div>
                    <div class="tags"><a href="#" class="text-primary">weather</a> / <a href="#" class="type">image</a></div>
                    <h4 class="timeline-item-title">
                      <a href="#">Exputo Humo Lenis Metuo Quibus</a>
                    </h4>
                    <p class="timeline-item-description">Esca melior neo nutus oppeto scisco te velit. Commodo humo luptatum macto plaga secundum.</p>
                    <a href="blog-post.html" class="btn btn-link"><i class="fa fa-arrow-circle-right"></i> Read more</a>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="pagination d-block">
            <button type="button" class="btn btn-secondary btn-lg btn-block">Load More</button>
          </div>
        </div>

        <!--Sidebar-->
        <div class="col-md-3 sidebar-right">

          <!-- @Element: Search form -->
          <div class="mb-4">
            <form role="form">
              <div class="input-group">
                <label class="sr-only" for="search-field">Search</label>
                <input type="search" class="form-control" id="search-field" placeholder="Search">
                <span class="input-group-append">
                    <button class="btn btn-secondary" type="button">Go!</button>
                  </span>
              </div>
            </form>
          </div>

          <!-- @Element: badge cloud -->
          <div class="mb-4">
            <h4 class="title-divider">
              <span>Tags</span>
            </h4>
            <div class="tag-cloud">
              <span class="badge"><a href="#">culture</a> (70)</span>
              <span class="badge"><a href="#">general</a> (67)</span>
              <span class="badge"><a href="#">coding</a> (68)</span>
              <span class="badge"><a href="#">design</a> (0)</span>
              <span class="badge"><a href="#">weather</a> (76)</span>
              <span class="badge"><a href="#">jobs</a> (99)</span>
              <span class="badge"><a href="#">health</a> (58)</span>
            </div>
          </div>

          <!-- @Element: Archive -->
          <div class="mb-4">
            <h4 class="title-divider">
              <span>Archive</span>
            </h4>
            <ul class="list-unstyled list-lg tags">
              <li><i class="fa fa-angle-right fa-fw"></i> <a href="#">May 2018</a> (18)</li>
              <li><i class="fa fa-angle-right fa-fw"></i> <a href="#">May 2018</a> (91)</li>
              <li><i class="fa fa-angle-right fa-fw"></i> <a href="#">April 2018</a> (72)</li>
              <li><i class="fa fa-angle-right fa-fw"></i> <a href="#">March 2018</a> (38)</li>
              <li><i class="fa fa-angle-right fa-fw"></i> <a href="#">January 2018</a> (19)</li>
              <li><i class="fa fa-angle-right fa-fw"></i> <a href="#">January 2018</a> (52)</li>
              <li><i class="fa fa-angle-right fa-fw"></i> <a href="#">December 2017</a> (94)</li>
            </ul>
          </div>

          <!-- @Element: Popular/recent tabs -->
          <div class="mb-4">
            <ul class="nav nav-tabs">
              <li class="nav-item"><a href="#popular" class="nav-link active" data-toggle="tab">Popular</a></li>
              <li class="nav-item"><a href="#latest" class="nav-link" data-toggle="tab">Latest</a></li>
            </ul>
            <div class="tab-content tab-content-bordered">
              <!-- Popular tab content -->
              <div class="tab-pane fade active show blog-roll-mini" id="popular">
                <!-- Popular blog post 1 -->
                <div class="row blog-post">
                  <div class="col-4">
                    <div class="blog-media">
                      <a href="#">
                          <img src="assets/img/blog/frog.jpg" alt="Picture of frog by Ben Fredericson" class="img-fluid" />
                        </a>
                    </div>
                  </div>
                  <div class="col-8">
                    <h5>
                      <a href="#">Euismod Macto Obruo Premo</a>
                    </h5>
                  </div>
                </div>
                <!-- Popular blog post 2 -->
                <div class="row blog-post">
                  <div class="col-4">
                    <div class="blog-media">
                      <a href="#">
                          <img src="assets/img/blog/ladybird.jpg" alt="Picture of frog by Ben Fredericson" class="img-fluid" />
                        </a>
                    </div>
                  </div>
                  <div class="col-8">
                    <h5>
                      <a href="#">Capto Conventio Nobis Te</a>
                    </h5>
                  </div>
                </div>
                <!-- Popular blog post 3 -->
                <div class="row blog-post">
                  <div class="col-4">
                    <div class="blog-media">
                      <a href="#">
                          <img src="assets/img/blog/bee.jpg" alt="Picture of frog by Ben Fredericson" class="img-fluid" />
                        </a>
                    </div>
                  </div>
                  <div class="col-8">
                    <h5>
                      <a href="#">Et Refoveo Suscipere Tego</a>
                    </h5>
                  </div>
                </div>
                <!-- Popular blog post 4 -->
                <div class="row blog-post">
                  <div class="col-4">
                    <div class="blog-media">
                      <a href="#">
                          <img src="assets/img/blog/butterfly.jpg" alt="Picture of frog by Ben Fredericson" class="img-fluid" />
                        </a>
                    </div>
                  </div>
                  <div class="col-8">
                    <h5>
                      <a href="#">Accumsan Gemino Modo Persto</a>
                    </h5>
                  </div>
                </div>
              </div>
              <!-- Latest tab content -->
              <div class="tab-pane fade blog-roll-mini" id="latest">
                <!-- Latest blog post 1 -->
                <div class="row blog-post">
                  <div class="col-4">
                    <div class="blog-media">
                      <a href="#">
                          <img src="assets/img/blog/ladybird.jpg" alt="Picture of frog by Ben Fredericson" class="img-fluid" />
                        </a>
                    </div>
                  </div>
                  <div class="col-8">
                    <h5>
                      <a href="#">Elit Ulciscor Vulputate Zelus</a>
                    </h5>
                  </div>
                </div>
                <!-- Latest blog post 2 -->
                <div class="row blog-post">
                  <div class="col-4">
                    <div class="blog-media">
                      <a href="#">
                          <img src="assets/img/blog/ladybird.jpg" alt="Picture of frog by Ben Fredericson" class="img-fluid" />
                        </a>
                    </div>
                  </div>
                  <div class="col-8">
                    <h5>
                      <a href="#">Abluo Fere Sagaciter Vicis</a>
                    </h5>
                  </div>
                </div>
                <!-- Latest blog post 3 -->
                <div class="row blog-post">
                  <div class="col-4">
                    <div class="blog-media">
                      <a href="#">
                          <img src="assets/img/blog/bee.jpg" alt="Picture of frog by Ben Fredericson" class="img-fluid" />
                        </a>
                    </div>
                  </div>
                  <div class="col-8">
                    <h5>
                      <a href="#">Dolus Pagus Veniam Vicis</a>
                    </h5>
                  </div>
                </div>
                <!-- Latest blog post 4 -->
                <div class="row blog-post">
                  <div class="col-4">
                    <div class="blog-media">
                      <a href="#">
                          <img src="assets/img/blog/water-pump.jpg" alt="Picture of frog by Ben Fredericson" class="img-fluid" />
                        </a>
                    </div>
                  </div>
                  <div class="col-8">
                    <h5>
                      <a href="#">Ea Proprius Ullamcorper Valde</a>
                    </h5>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <!-- @Element: Subscrive button -->
          <div class="mb-4">
            <a href="#" class="btn btn-warning text-white"><i class="fa fa-rss"></i> Subscribe via RSS feed</a>
          </div>

          <!-- Follow Widget -->
          <div class="mb-4">
            <h4 class="title-divider">
              <span>Follow Us On</span>
            </h4>
            <!--@todo: replace with real social media links -->
            <ul class="list-unstyled">
              <li>
                <a href="#" class="text-hover-brand-twitter d-flex align-items-center py-1 text-hover-no-underline"><i class="fa fa-twitter-square fa-fw icon-2x"></i> Twitter</a>
              </li>
              <li>
                <a href="#" class="text-hover-brand-facebook d-flex align-items-center py-1 text-hover-no-underline"><i class="fa fa-facebook-square fa-fw icon-2x"></i> Facebook</a>
              </li>
              <li>
                <a href="#" class="text-hover-brand-linkedin d-flex align-items-center py-1 text-hover-no-underline"><i class="fa fa-linkedin fa-fw icon-2x"></i> LinkedIn</a>
              </li>
              <li>
                <a href="#" class="text-hover-brand-google-plus d-flex align-items-center py-1 text-hover-no-underline"><i class="fa fa-google-plus-square fa-fw icon-2x"></i> Google+</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!--.container-->
  </div>
  <!--#content-->

  <!-- ======== @Region: #content-below ======== -->
  <div id="content-below">
    <!-- Awesome features call to action -->
    <div class="bg-primary bg-op-9 text-white py-4">
      <div class="container">
        <div class="row text-center text-lg-left align-items-lg-center">
          <div class="col-12 col-lg-7 text-white">
            <h3 class="font-weight-bold my-0 text-uppercase">
              Awesome Features
            </h3>
            <p class="font-weight-normal op-9 my-0"> <i class="la la-check-circle-o"></i> 99.9% Uptime <i class="la la-check-circle-o ml-lg-3"></i> Free Upgrades <i class="la la-check-circle-o ml-lg-3"></i> Fully Responsive <i class="la la-check-circle-o ml-lg-3"></i>              Bug Free </p>
          </div>
          <div class="col-12 col-lg-5 py-2 text-lg-right">
            <a href="#" class="btn btn-xlg btn-white btn-rounded shadow-lg bg-light bg-op-8 bg-hover-white">Get Started<i class="fa fa-arrow-right ml-2 mt-1"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php include 'footer.php' ?>