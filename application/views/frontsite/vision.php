<?php include 'header.php' ?>
                    <?php    $visionquery = $this->db->get('tblvisionmission')->row_array();?>


  <!-- ======== @Region: #content ======== -->
  <div id="content" class="pt-7 pb-7" data-bg-img="<?php echo base_url();?>uploads/images/<?php echo  $visionquery['visionimage'];?>" data-animate="fadeIn">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="vission_box">
                    <h2>OUR VISION</h2>
                    <p><?php echo $visionquery['visiondesc'];?> </p>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="vission_box">
                    <h2>OUR MISSION</h2>
                                        <p><?php echo $visionquery['missiondesc'];?> </p>

                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="vission_box">
                    <h2>OUR VALUES</h2>
              <p><?php echo $visionquery['valuedesc'];?> </p>

                </div>
            </div>
        </div>
    </div>
  </div>

  <!-- ======== @Region: #content-below ======== -->
  <div id="content-below">
    <!-- Awesome features call to action -->
    <div class="bg-primary bg-op-9 text-white py-4">
      <div class="container">
        <div class="row text-center text-lg-left align-items-lg-center">
          <div class="col-12 col-lg-7 text-white">
            <h3 class="font-weight-bold my-0 text-uppercase">
              Awesome Features
            </h3>
            <p class="font-weight-normal op-9 my-0"> <i class="la la-check-circle-o"></i> 99.9% Uptime <i class="la la-check-circle-o ml-lg-3"></i> Free Upgrades <i class="la la-check-circle-o ml-lg-3"></i> Fully Responsive <i class="la la-check-circle-o ml-lg-3"></i>              Bug Free </p>
          </div>
          <div class="col-12 col-lg-5 py-2 text-lg-right">
            <a href="#" class="btn btn-xlg btn-white btn-rounded shadow-lg bg-light bg-op-8 bg-hover-white">Get AppStrap<i class="fa fa-arrow-right ml-2 mt-1"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>

 <?php include 'footer.php' ?>