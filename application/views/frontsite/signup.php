<?php include 'header.php' ?>

  <!-- ======== @Region: #content ======== -->
  <div id="content">
      
      
      
      
    <div class="container">
      <!-- Sign Up form -->
      <form class="form-login form-wrapper form-medium" action="<?php echo base_url();?>frontcarrental/Signup/submitSignup"  method="post" id="FormId">
        <h3 class="title-divider">
             <?php if($this->session->flashdata('permission_message'))
	 		{
			?>
					
                        <div class="alert alert-block alert-success " style="background-color:#64aea2 ;height:50px;width:400px; margin-left: 10px">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p > <?php echo $this->session->flashdata('permission_message'); ?></p>
                        </div>						
									
			<?php   $this->session->sess_destroy();                } ?>
            <?php if($this->session->flashdata('flash_message'))
	 		{
			?>
					
                        <div class="alert alert-block alert-success" style="background-color:#64aea2 ;height:50px;width:400px; margin-left: 10px">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p> <?php echo $this->session->flashdata('flash_message'); ?></p>
                        </div>						
									
			<?php   $this->session->sess_destroy();               }  ?>
          <span>Sign Up</span>
          <small class="mt-4">Already signed up? <a href="<?php echo base_url();?>/Frontuser/loginpage">Login here</a>.</small>
        </h3>
<!--        <h5>
          Price Plan
        </h5>-->
<!--        <div class="form-group">
          <select class="form-control">
              <option>Basic</option>
              <option>Pro</option>
              <option>Pro +</option>
            </select>
        </div>-->
        <hr />
        <h5>
          Account Information
        </h5>
        <div class="form-group">
          <label class="sr-only" for="signup-first-name-page">First Name</label>
          <input type="text" class="form-control first" name="firstname" id="signup-first-name-page" placeholder="First name">
             <span id="first" style="color:red"></span>
        </div>
        <div class="form-group">
          <label class="sr-only" for="signup-last-name-page">Last Name</label>
          <input type="text" class="form-control  last" name="lastname" id="signup-last-name-page" placeholder="Last name">
                    <span id="last" style="color:red"></span>

        </div>
        <div class="form-group">
          <label class="sr-only" for="signup-username-page">Userame</label>
          <input type="text" class="form-control  user"  name="username" id="signup-username-page" placeholder="Username">
                    <span id="user" style="color:red"></span>

        </div>
        <div class="form-group">
          <label class="sr-only" for="signup-email-page">Email address</label>
          <input type="email" class="form-control  email" name="emailaddress"  id="signup-email-page" placeholder="Email address">
                    <span id="email" style="color:red"></span>

        </div>
        <div class="form-group">
          <label class="sr-only" for="signup-password-page">Password</label>
          <input type="password" class="form-control   pass"  name="password" id="signup-password-page" placeholder="Password">
                   <span id="pass" style="color:red"></span>

        </div>
        <div class="form-check">
          <label class="form-check-label">
              <input type="checkbox" value="term" class="form-check-input termscondition">
              I agree with the Terms and Conditions. 
              
            </label>
            
        </div>
        <span id="chkcondition" style="color:red"></span>
        <hr />
        <button class="btn btn-primary" id="chkbtn" type="submit" onclick="return MYtest();">Sign up</button>
      </form>
    </div>
  </div>

  <!-- ======== @Region: #content-below ======== -->
  <div id="content-below">
    <!-- Awesome features call to action -->
    <div class="bg-primary bg-op-9 text-white py-4">
      <div class="container">
        <div class="row text-center text-lg-left align-items-lg-center">
          <div class="col-12 col-lg-7 text-white">
            <h3 class="font-weight-bold my-0 text-uppercase">
              Awesome Features
            </h3>
            <p class="font-weight-normal op-9 my-0"> <i class="la la-check-circle-o"></i> 99.9% Uptime <i class="la la-check-circle-o ml-lg-3"></i> Free Upgrades <i class="la la-check-circle-o ml-lg-3"></i> Fully Responsive <i class="la la-check-circle-o ml-lg-3"></i>              Bug Free </p>
          </div>
          <div class="col-12 col-lg-5 py-2 text-lg-right">
            <a href="#" class="btn btn-xlg btn-white btn-rounded shadow-lg bg-light bg-op-8 bg-hover-white">Get Started<i class="fa fa-arrow-right ml-2 mt-1"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php include 'footer.php' ?>

  <script>
      
   $('#signup-email-page').on('keypress', function () 
   {
        var re = /([A-Z0-9a-z_-][^@])+?@[^$#<>?]+?\.[\w]{2,4}/.test(this.value);
        if (!re) {
            $("#email").text("Email address is not correct");
           
            $('#chkbtn').prop('disabled', true);
            
        } else {
            setTimeout(function () {
                $("#email").text("");
            }, 02);
            $('#chkbtn').prop('disabled', false);
        }
    })   
      
  </script>
  
  <script>
      
      function MYtest()
      {
       
          var first = $(".first").val();
          var last = $(".last").val();
          var user = $(".user").val();
          var email = $(".email").val();
          var pass = $(".pass").val();
         // var chkbox = $(".termscondition").val();
          
          if(first == "")
          {
              
              $("#first").html("First name is require");
              return false;
          }
          if(last == "")
          {
              $("#last").html("Last name is require");
              return false;
          }
           if(user == "")
          {
              $("#user").html("User name is require");
              return false;
          }
             
          if(email == "")
          {
              $("#email").html("Email is require");
              return false;
          }
          if(pass == "")
          {
              $("#pass").html("Password is require");
              return false;
          }
          
          if($('.termscondition:checkbox:checked').length == 0)
          {
              $("#chkcondition").html("Terms and conditions is require");
              return false;
          }
          
          $("#FormId").submit();
          
    }
          </script>