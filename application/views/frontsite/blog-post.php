


<?php include 'header.php' ?>

  <!-- ======== @Region: #content ======== -->
  <div id="content">
    <div class="container">
      <h2 class="title-divider">
        <span>Blog Post: <span class="font-weight-normal text-muted">Default</span></span>
        <small>
            <a href="blog.html"><i class="fa fa-arrow-left"></i> Back to Blog</a>
          </small>
      </h2>
      <div class="row">
        <!--Main Content-->
        <div class="col-md-9">
          <!-- Blog post -->
          <div class="row blog-post">
            <div class="col-md-1 date-md">
              <!-- Date desktop -->
              <div class="date-wrapper"> <span class="date-m bg-primary">Jan</span> <span class="date-d">07</span>
              </div>
              <!-- Meta details desktop -->
              <p class="text-muted text-xs"> <i class="fa fa-user"></i> <a href="#">Erin</a>
              </p>
            </div>
            <div class="col-md-11">
              <div class="media-body">
                <div class="text-xs text-uppercase"><a href="#" class="text-primary">coding</a> / <a href="#" class="text-dark">image</a></div>
                <h3 class="title media-heading">
                  Abbas Accumsan Nobis Ut Velit
                </h3>

                <!-- Meta details mobile -->
                <ul class="list-inline meta text-muted">
                  <li class="list-inline-item"><i class="fa fa-calendar"></i> Mon 7th Jan 2013</li>
                  <li class="list-inline-item"><i class="fa fa-user"></i> <a href="#">Erin</a></li>
                </ul>

                <!--Main content of post-->
                <div class="blog-content">
                  <div class="blog-media">
                    <img src="assets/img/blog/ape.jpg" alt="Picture of frog by Ben Fredericson" class="img-fluid" />
                  </div>
                  <p class="lead">Appellatio luctus magna sit. Capto commoveo nutus proprius validus. Amet pecus sagaciter. Abico blandit cui dignissim mauris oppeto singularis. Abigo acsi incassum lobortis mos nunc nutus obruo paratus vulpes.</p>
                  <p>Et interdico iriure luptatum nisl quidne sed vulpes. Exputo ibidem importunus singularis. Ad ludus nostrud. Bene gravis lenis luptatum mos ratis. Abico aliquip damnum distineo iriure lobortis pecus persto valde. Genitus in lobortis occuro.
                    Aliquip lenis quibus. Ad damnum inhibeo luctus nutus singularis sit vulputate zelus.</p>
                  <p>Aliquip augue caecus capto diam elit jus quia te tum. Ideo iustum nunc. Abdo huic nibh quidem. Ideo obruo venio. Abbas bene illum molior pneum refoveo saluto tum. Aliquam antehabeo at ex haero ludus nunc praemitto quia.</p>

                  <!--Used to highlight key points of post-->
                  <div class="focus-box float-md-right col-md-6 p-3 m-3">
                    <h5>
                      Key Points
                    </h5>
                    <ul class="list-unstyled list-lg">
                      <li><i class="fa fa-check text-primary list-item-icon"></i> Conventio duis letalis pala populus.</li>
                      <li><i class="fa fa-check text-primary list-item-icon"></i> Acsi eligo esca exputo in pertineo sed utrum vero.</li>
                      <li><i class="fa fa-check text-primary list-item-icon"></i> Acsi bene saluto.</li>
                    </ul>
                    <p>Ad exputo facilisi gilvus obruo occuro probo scisco validus. Facilisis immitto loquor pagus.</p>
                  </div>
                  <p>Abdo abluo caecus euismod importunus refoveo valde venio. Gemino quidne tation tego uxor valde valetudo vel veniam. Gemino te vel. Cogo cui dignissim iusto ludus luptatum meus pagus pecus. Eros facilisis iriure ullamcorper valde. Abdo
                    aliquam bene cui dignissim interdico laoreet tum voco volutpat.</p>
                  <p>Decet dignissim ille. Meus mos vereor vero. Dignissim facilisis genitus nibh pagus pneum roto. Ea eu feugiat gravis minim oppeto refoveo veniam venio verto. Exerci interdico pneum sudo. Erat facilisis gravis importunus magna pagus valde.</p>
                  <h5 class="text-uppercase mt-4 mb-1">
                    Ex Torqueo Utrum Vel
                  </h5>
                  <p>Abluo accumsan exerci immitto luctus olim refoveo rusticus sit zelus. Acsi bene consectetuer hos neo nostrud. Eu exputo ille luptatum molior pneum populus. Defui hos ille iusto probo singularis ut. Magna tum veniam ymo. Abigo adipiscing
                    elit immitto neo quibus sino. Gravis lucidus melior obruo proprius tum utinam. Consequat laoreet nobis pagus utrum.</p>
                  <p>Cogo hendrerit patria quadrum utinam. Cogo diam dolus exputo facilisis iaceo ibidem. Camur distineo elit mos pala velit. Dolore ea feugiat jumentum plaga ratis saluto scisco sudo utrum. Ad autem fere hos lobortis metuo obruo olim quadrum
                    valetudo.</p>

                  <!--Alertnative blockquote style-->
                  <blockquote class="blockquote float-left">
                    <p>Commodo consequat dolor exerci gilvus huic interdico suscipere. Caecus diam ea ex ibidem imputo incassum utinam validus veniam.</p>
                    <footer class="blockquote-footer">Refoveo Similis</footer>
                  </blockquote>
                  <p>Aliquam conventio illum loquor oppeto probo tation. Abdo consequat enim immitto odio te venio vulputate. Aptent gilvus haero modo tincidunt vero. Dolus hendrerit paratus ymo. Amet euismod illum importunus iusto paratus quae similis.
                    Eu incassum luptatum mauris natu occuro tamen te.</p>
                  <p>Aliquip jugis pneum ulciscor. Accumsan commodo exerci roto similis sudo usitas. Capto cogo decet eros modo oppeto quibus quidem vulpes. Cogo haero ideo metuo meus nutus plaga typicus. Duis eligo exputo hos laoreet pagus pertineo sed
                    singularis velit. Camur et gravis neque olim rusticus saluto. Amet cogo hendrerit iaceo jumentum secundum similis tincidunt tum veniam.</p>
                </div>

                <!-- Post tags -->
                <div class="tag-cloud post-tag-cloud">
                  <h4>
                    tags
                  </h4>
                  <a href="#" class="badge badge-secondary">general</a> <a href="#" class="badge badge-secondary">coding</a> <a href="#" class="badge badge-secondary">weather</a> <a href="#" class="badge badge-secondary">video</a> <a href="#" class="badge badge-secondary">podcast</a>
                </div>

                <div class="block bg-light p-3 post-block">
                  <!-- About The Author -->
                  <div class="post-author">
                    <h4>
                      About The Author: <span class="font-weight-normal">Jim Jones</span>
                    </h4>
                    <p>Aptent brevitas causa ibidem nisl praesent qui similis vicis. Lucidus tum vereor zelus. Amet comis nisl nostrud. Persto similis tincidunt validus. Mauris te tum. Cogo consectetuer defui eu jugis nutus obruo pertineo tincidunt.</p>
                  </div>

                  <!-- Share Widget -->
                  <div class="post-share social-media-branding">
                    <h5>
                      Share it:
                    </h5>
                    <!--@todo: replace with real social share links -->
                    <a href="#" class="text-brand-twitter"><i class="fa fa-twitter-square icon-2x"></i></a>
                    <a href="#" class="text-brand-facebook"><i class="fa fa-facebook-square icon-2x"></i></a>
                    <a href="#" class="text-brand-linkedin"><i class="fa fa-linkedin icon-2x"></i></a>
                    <a href="#" class="text-brand-google-plus"><i class="fa fa-google-plus-square icon-2x"></i></a>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <!-- Related Content -->
          <div class="post-related-content">
            <h4>
              Related Content
            </h4>
            <div class="row">
              <div class="col-md-3 blog-post">
                <div class="blog-media">
                  <a href="#">
                      <img src="assets/img/blog/ape.jpg" alt="Picture of frog by Ben Fredericson" class="img-fluid" />
                    </a>
                </div>
                <h5>
                  <a href="#">Accumsan Camur Paulatim Vel</a>
                </h5>
              </div>
              <div class="col-md-3 blog-post">
                <div class="blog-media">
                  <a href="#">
                      <img src="assets/img/blog/butterfly.jpg" alt="Picture of frog by Ben Fredericson" class="img-fluid" />
                    </a>
                </div>
                <h5>
                  <a href="#">Defui Metuo Scisco Tamen</a>
                </h5>
              </div>
              <div class="col-md-3 blog-post">
                <div class="blog-media">
                  <a href="#">
                      <img src="assets/img/blog/ape.jpg" alt="Picture of frog by Ben Fredericson" class="img-fluid" />
                    </a>
                </div>
                <h5>
                  <a href="#">Abico Brevitas Illum Os</a>
                </h5>
              </div>
              <div class="col-md-3 blog-post">
                <div class="blog-media">
                  <a href="#">
                      <img src="assets/img/blog/fly.jpg" alt="Picture of frog by Ben Fredericson" class="img-fluid" />
                    </a>
                </div>
                <h5>
                  <a href="#">Elit Eu Si Voco</a>
                </h5>
              </div>
            </div>
          </div>

          <!-- Post To Post Pager -->
          <div class="post-to-post-pager"> <a href="#" class="btn btn-lg btn-primary previous">&larr; Previous Post</a> <a href="#" class="btn btn-lg btn-primary next">Next Post &rarr;</a> </div>

          <!--Comments-->
          <div class="comments" id="comments">
            <h3>
              Comments (10)
            </h3>
            <hr />
            <ul class="list-unstyled">
              <li class="media mb-3 pos-relative">
                <a href="#">
                    <img src="assets/img/team/steve.jpg" alt="Picture of Tim Rice" class="d-flex mr-3 img-thumbnail img-fluid" />
                  </a>
                <div class="media-body">
                  <ul class="list-inline blog-meta text-muted">
                    <li class="list-inline-item"><i class="fa fa-calendar"></i> Fri 25th May 2018</li>
                    <li class="list-inline-item"><i class="fa fa-user"></i> <a href="#">Mike Thompson</a></li>
                  </ul>
                  <h5 class="mt-0 mb-2">
                    Nisi rhoncus nisi porttitor risus ridiculus tristique, quis.
                  </h5>
                  <p class="mb-1">Nisi rhoncus nisi porttitor risus ridiculus tristique, quis.Nisi rhoncus nisi porttitor risus ridiculus tristique, quis.Nisi rhoncus nisi porttitor risus ridiculus tristique, quis.Nisi rhoncus nisi porttitor risus ridiculus tristique,
                    quis.</p>
                </div>
              </li>
              <li class="media mb-3 pos-relative">
                <a href="#">
                    <img src="assets/img/team/jimi.jpg" alt="Picture of Alex Jones" class="d-flex mr-3 img-thumbnail img-fluid" />
                  </a>
                <div class="media-body">
                  <ul class="list-inline blog-meta text-muted">
                    <li class="list-inline-item"><i class="fa fa-calendar"></i> Tue 22nd May 2018</li>
                    <li class="list-inline-item"><i class="fa fa-user"></i> <a href="#">Sara Mason</a></li>
                  </ul>
                  <h5 class="mt-0 mb-2">
                    Magna aliquet diam mauris tortor turpis vel porta
                  </h5>
                  <p class="mb-1">Magna aliquet diam mauris tortor turpis vel portaMagna aliquet diam mauris tortor turpis vel portaMagna aliquet diam mauris tortor turpis vel portaMagna aliquet diam mauris tortor turpis vel porta</p>
                </div>
              </li>
              <li class="media mb-3 pos-relative">
                <a href="#">
                    <img src="assets/img/team/bono.jpg" alt="Picture of Kylie Michaels" class="d-flex mr-3 img-thumbnail img-fluid" />
                  </a>
                <div class="media-body">
                  <ul class="list-inline blog-meta text-muted">
                    <li class="list-inline-item"><i class="fa fa-calendar"></i> Fri 18th May 2018</li>
                    <li class="list-inline-item"><i class="fa fa-user"></i> <a href="#">Alex Jones</a></li>
                  </ul>
                  <h5 class="mt-0 mb-2">
                    Enim augue elit adipiscing placerat natoque
                  </h5>
                  <p class="mb-1">Enim augue elit adipiscing placerat natoqueEnim augue elit adipiscing placerat natoqueEnim augue elit adipiscing placerat natoqueEnim augue elit adipiscing placerat natoque</p>
                </div>
              </li>
              <li class="media mb-3 pos-relative">
                <a href="#">
                    <img src="assets/img/team/bono.jpg" alt="Picture of Alex Jones" class="d-flex mr-3 img-thumbnail img-fluid" />
                  </a>
                <div class="media-body">
                  <ul class="list-inline blog-meta text-muted">
                    <li class="list-inline-item"><i class="fa fa-calendar"></i> Wed 16th May 2018</li>
                    <li class="list-inline-item"><i class="fa fa-user"></i> <a href="#">Mike Thompson</a></li>
                  </ul>
                  <h5 class="mt-0 mb-2">
                    Odio, nunc platea mattis, mid et
                  </h5>
                  <p class="mb-1">Odio, nunc platea mattis, mid etOdio, nunc platea mattis, mid etOdio, nunc platea mattis, mid etOdio, nunc platea mattis, mid et</p>
                </div>
              </li>
              <li class="media mb-3 pos-relative">
                <a href="#">
                    <img src="assets/img/team/obama.jpg" alt="Picture of Sara Mason" class="d-flex mr-3 img-thumbnail img-fluid" />
                  </a>
                <div class="media-body">
                  <ul class="list-inline blog-meta text-muted">
                    <li class="list-inline-item"><i class="fa fa-calendar"></i> Tue 15th May 2018</li>
                    <li class="list-inline-item"><i class="fa fa-user"></i> <a href="#">Kylie Michaels</a></li>
                  </ul>
                  <h5 class="mt-0 mb-2">
                    Urna natoque in phasellus rhoncus aliquet penatibus
                  </h5>
                  <p class="mb-1">Urna natoque in phasellus rhoncus aliquet penatibusUrna natoque in phasellus rhoncus aliquet penatibusUrna natoque in phasellus rhoncus aliquet penatibusUrna natoque in phasellus rhoncus aliquet penatibus</p>
                </div>
              </li>
              <li class="media mb-3 pos-relative">
                <a href="#">
                    <img src="assets/img/team/robert.jpg" alt="Picture of Tim Rice" class="d-flex mr-3 img-thumbnail img-fluid" />
                  </a>
                <div class="media-body">
                  <ul class="list-inline blog-meta text-muted">
                    <li class="list-inline-item"><i class="fa fa-calendar"></i> Sun 13th May 2018</li>
                    <li class="list-inline-item"><i class="fa fa-user"></i> <a href="#">Sara Mason</a></li>
                  </ul>
                  <h5 class="mt-0 mb-2">
                    Porta risus porttitor facilisis sit dapibus
                  </h5>
                  <p class="mb-1">Porta risus porttitor facilisis sit dapibusPorta risus porttitor facilisis sit dapibusPorta risus porttitor facilisis sit dapibusPorta risus porttitor facilisis sit dapibus</p>
                </div>
              </li>
              <li class="media mb-3 pos-relative">
                <a href="#">
                    <img src="assets/img/team/obama.jpg" alt="Picture of Sara Mason" class="d-flex mr-3 img-thumbnail img-fluid" />
                  </a>
                <div class="media-body">
                  <ul class="list-inline blog-meta text-muted">
                    <li class="list-inline-item"><i class="fa fa-calendar"></i> Sat 12th May 2018</li>
                    <li class="list-inline-item"><i class="fa fa-user"></i> <a href="#">Mike Thompson</a></li>
                  </ul>
                  <h5 class="mt-0 mb-2">
                    Magna aliquet diam mauris tortor turpis vel porta
                  </h5>
                  <p class="mb-1">Magna aliquet diam mauris tortor turpis vel portaMagna aliquet diam mauris tortor turpis vel portaMagna aliquet diam mauris tortor turpis vel portaMagna aliquet diam mauris tortor turpis vel porta</p>
                  <!-- Nested media object -->
                  <div class="media mt-4 mb-2 pos-relative">
                    <a href="#">
                        <img src="assets/img/team/adele.jpg" alt="Picture of Kylie Michaels" class="d-flex mr-3 img-thumbnail img-fluid" />
                      </a>
                    <div class="media-body">
                      <ul class="list-inline blog-meta text-muted">
                        <li class="list-inline-item"><i class="fa fa-calendar"></i> Sat 12th May 2018</li>
                        <li class="list-inline-item"><i class="fa fa-user"></i> <a href="#">Alex Jones</a></li>
                      </ul>
                      <h5 class="mt-0 mb-2">
                        Magna aliquet diam mauris tortor turpis vel porta
                      </h5>
                      <p class="mb-1">Magna aliquet diam mauris tortor turpis vel portaMagna aliquet diam mauris tortor turpis vel portaMagna aliquet diam mauris tortor turpis vel portaMagna aliquet diam mauris tortor turpis vel porta</p>
                    </div>
                  </div>
                </div>
              </li>
              <li class="media mb-3 pos-relative">
                <a href="#">
                    <img src="assets/img/team/robert.jpg" alt="Picture of Mike Thompson" class="d-flex mr-3 img-thumbnail img-fluid" />
                  </a>
                <div class="media-body">
                  <ul class="list-inline blog-meta text-muted">
                    <li class="list-inline-item"><i class="fa fa-calendar"></i> Fri 11th May 2018</li>
                    <li class="list-inline-item"><i class="fa fa-user"></i> <a href="#">Alex Jones</a></li>
                  </ul>
                  <h5 class="mt-0 mb-2">
                    a nec in sed hac ultrices cursus
                  </h5>
                  <p class="mb-1">a nec in sed hac ultrices cursusa nec in sed hac ultrices cursusa nec in sed hac ultrices cursusa nec in sed hac ultrices cursus</p>
                </div>
              </li>
              <li class="media mb-3 pos-relative">
                <a href="#">
                    <img src="assets/img/team/bono.jpg" alt="Picture of Alexander Vanjuvic" class="d-flex mr-3 img-thumbnail img-fluid" />
                  </a>
                <div class="media-body">
                  <ul class="list-inline blog-meta text-muted">
                    <li class="list-inline-item"><i class="fa fa-calendar"></i> Tue 8th May 2018</li>
                    <li class="list-inline-item"><i class="fa fa-user"></i> <a href="#">Mike Thompson</a></li>
                  </ul>
                  <h5 class="mt-0 mb-2">
                    a nec in sed hac ultrices cursus
                  </h5>
                  <p class="mb-1">a nec in sed hac ultrices cursusa nec in sed hac ultrices cursusa nec in sed hac ultrices cursusa nec in sed hac ultrices cursus</p>
                </div>
              </li>
              <li class="media mb-3 pos-relative">
                <a href="#">
                    <img src="assets/img/team/robert.jpg" alt="Picture of Sara Mason" class="d-flex mr-3 img-thumbnail img-fluid" />
                  </a>
                <div class="media-body">
                  <ul class="list-inline blog-meta text-muted">
                    <li class="list-inline-item"><i class="fa fa-calendar"></i> Sat 5th May 2018</li>
                    <li class="list-inline-item"><i class="fa fa-user"></i> <a href="#">Alexander Vanjuvic</a></li>
                  </ul>
                  <h5 class="mt-0 mb-2">
                    amet urna integer urna enim, sit arcu pid in nec?
                  </h5>
                  <p class="mb-1">amet urna integer urna enim, sit arcu pid in nec?amet urna integer urna enim, sit arcu pid in nec?amet urna integer urna enim, sit arcu pid in nec?amet urna integer urna enim, sit arcu pid in nec?</p>
                </div>
              </li>
              <li class="media mb-3 pos-relative">
                <a href="#">
                    <img src="assets/img/team/obama.jpg" alt="Picture of Alexander Vanjuvic" class="d-flex mr-3 img-thumbnail img-fluid" />
                  </a>
                <div class="media-body">
                  <ul class="list-inline blog-meta text-muted">
                    <li class="list-inline-item"><i class="fa fa-calendar"></i> Thu 3rd May 2018</li>
                    <li class="list-inline-item"><i class="fa fa-user"></i> <a href="#">Sara Mason</a></li>
                  </ul>
                  <h5 class="mt-0 mb-2">
                    Odio, nunc platea mattis, mid et
                  </h5>
                  <p class="mb-1">Odio, nunc platea mattis, mid etOdio, nunc platea mattis, mid etOdio, nunc platea mattis, mid etOdio, nunc platea mattis, mid et</p>
                  <!-- Nested media object -->
                  <div class="media mt-4 mb-2 pos-relative">
                    <a href="#">
                        <img src="assets/img/team/jimi.jpg" alt="Picture of Alexander Vanjuvic" class="d-flex mr-3 img-thumbnail img-fluid" />
                      </a>
                    <div class="media-body">
                      <ul class="list-inline blog-meta text-muted">
                        <li class="list-inline-item"><i class="fa fa-calendar"></i> Thu 3rd May 2018</li>
                        <li class="list-inline-item"><i class="fa fa-user"></i> <a href="#">Mike Thompson</a></li>
                      </ul>
                      <h5 class="mt-0 mb-2">
                        Odio, nunc platea mattis, mid et
                      </h5>
                      <p class="mb-1">Odio, nunc platea mattis, mid etOdio, nunc platea mattis, mid etOdio, nunc platea mattis, mid etOdio, nunc platea mattis, mid et</p>
                    </div>
                  </div>
                </div>
              </li>
            </ul>
            <form id="comment-form" class="comment-form" role="form">
              <h3>
                Add Comment
              </h3>
              <hr />
              <div class="form-group">
                <label class="sr-only" for="comment-name">Name</label>
                <input type="text" class="form-control" id="comment-name" placeholder="Name">
              </div>
              <div class="form-group">
                <label class="sr-only" for="comment-name">Email</label>
                <input type="email" class="form-control" id="comment-email" placeholder="Email">
              </div>
              <div class="form-group">
                <label class="sr-only" for="comment-comment">Comment</label>
                <textarea rows="8" class="form-control" id="comment-comment" placeholder="Comment"></textarea>
              </div>
              <div class="form-check">
                <label class="form-check-label">
                    <input type="checkbox" class="form-check-input">
                    Subscribe to updates 
                  </label>
              </div>
              <button type="submit" class="btn btn-primary">Submit</button>
            </form>
          </div>
        </div>
        <!--Sidebar-->
        <div class="col-md-3 sidebar-right">

          <!-- @Element: Search form -->
          <div class="mb-4">
            <form role="form">
              <div class="input-group">
                <label class="sr-only" for="search-field">Search</label>
                <input type="search" class="form-control" id="search-field" placeholder="Search">
                <span class="input-group-append">
                    <button class="btn btn-secondary" type="button">Go!</button>
                  </span>
              </div>
            </form>
          </div>

          <!-- @Element: badge cloud -->
          <div class="mb-4">
            <h4 class="title-divider">
              <span>Tags</span>
            </h4>
            <div class="tag-cloud">
              <span class="badge"><a href="#">culture</a> (100)</span>
              <span class="badge"><a href="#">general</a> (90)</span>
              <span class="badge"><a href="#">coding</a> (78)</span>
              <span class="badge"><a href="#">design</a> (70)</span>
              <span class="badge"><a href="#">weather</a> (81)</span>
              <span class="badge"><a href="#">jobs</a> (72)</span>
              <span class="badge"><a href="#">health</a> (76)</span>
            </div>
          </div>

          <!-- @Element: Archive -->
          <div class="mb-4">
            <h4 class="title-divider">
              <span>Archive</span>
            </h4>
            <ul class="list-unstyled list-lg tags">
              <li><i class="fa fa-angle-right fa-fw"></i> <a href="#">May 2018</a> (54)</li>
              <li><i class="fa fa-angle-right fa-fw"></i> <a href="#">May 2018</a> (42)</li>
              <li><i class="fa fa-angle-right fa-fw"></i> <a href="#">April 2018</a> (18)</li>
              <li><i class="fa fa-angle-right fa-fw"></i> <a href="#">March 2018</a> (83)</li>
              <li><i class="fa fa-angle-right fa-fw"></i> <a href="#">January 2018</a> (78)</li>
              <li><i class="fa fa-angle-right fa-fw"></i> <a href="#">January 2018</a> (26)</li>
              <li><i class="fa fa-angle-right fa-fw"></i> <a href="#">December 2017</a> (5)</li>
            </ul>
          </div>

          <!-- @Element: Popular/recent tabs -->
          <div class="mb-4">
            <ul class="nav nav-tabs">
              <li class="nav-item"><a href="#popular" class="nav-link active" data-toggle="tab">Popular</a></li>
              <li class="nav-item"><a href="#latest" class="nav-link" data-toggle="tab">Latest</a></li>
            </ul>
            <div class="tab-content tab-content-bordered">
              <!-- Popular tab content -->
              <div class="tab-pane fade active show blog-roll-mini" id="popular">
                <!-- Popular blog post 1 -->
                <div class="row blog-post">
                  <div class="col-4">
                    <div class="blog-media">
                      <a href="#">
                          <img src="assets/img/blog/water-pump.jpg" alt="Picture of frog by Ben Fredericson" class="img-fluid" />
                        </a>
                    </div>
                  </div>
                  <div class="col-8">
                    <h5>
                      <a href="#">Blandit Et Nisl Probo</a>
                    </h5>
                  </div>
                </div>
                <!-- Popular blog post 2 -->
                <div class="row blog-post">
                  <div class="col-4">
                    <div class="blog-media">
                      <a href="#">
                          <img src="assets/img/blog/fly.jpg" alt="Picture of frog by Ben Fredericson" class="img-fluid" />
                        </a>
                    </div>
                  </div>
                  <div class="col-8">
                    <h5>
                      <a href="#">Dolor Facilisi Gemino Vulputate</a>
                    </h5>
                  </div>
                </div>
                <!-- Popular blog post 3 -->
                <div class="row blog-post">
                  <div class="col-4">
                    <div class="blog-media">
                      <a href="#">
                          <img src="assets/img/blog/fly.jpg" alt="Picture of frog by Ben Fredericson" class="img-fluid" />
                        </a>
                    </div>
                  </div>
                  <div class="col-8">
                    <h5>
                      <a href="#">Autem Capto Validus Venio</a>
                    </h5>
                  </div>
                </div>
                <!-- Popular blog post 4 -->
                <div class="row blog-post">
                  <div class="col-4">
                    <div class="blog-media">
                      <a href="#">
                          <img src="assets/img/blog/ape.jpg" alt="Picture of frog by Ben Fredericson" class="img-fluid" />
                        </a>
                    </div>
                  </div>
                  <div class="col-8">
                    <h5>
                      <a href="#">Erat Quia Turpis Wisi</a>
                    </h5>
                  </div>
                </div>
              </div>
              <!-- Latest tab content -->
              <div class="tab-pane fade blog-roll-mini" id="latest">
                <!-- Latest blog post 1 -->
                <div class="row blog-post">
                  <div class="col-4">
                    <div class="blog-media">
                      <a href="#">
                          <img src="assets/img/blog/ape.jpg" alt="Picture of frog by Ben Fredericson" class="img-fluid" />
                        </a>
                    </div>
                  </div>
                  <div class="col-8">
                    <h5>
                      <a href="#">Capto Luctus Nulla Ulciscor</a>
                    </h5>
                  </div>
                </div>
                <!-- Latest blog post 2 -->
                <div class="row blog-post">
                  <div class="col-4">
                    <div class="blog-media">
                      <a href="#">
                          <img src="assets/img/blog/frog.jpg" alt="Picture of frog by Ben Fredericson" class="img-fluid" />
                        </a>
                    </div>
                  </div>
                  <div class="col-8">
                    <h5>
                      <a href="#">Aptent Enim Incassum Vereor</a>
                    </h5>
                  </div>
                </div>
                <!-- Latest blog post 3 -->
                <div class="row blog-post">
                  <div class="col-4">
                    <div class="blog-media">
                      <a href="#">
                          <img src="assets/img/blog/butterfly.jpg" alt="Picture of frog by Ben Fredericson" class="img-fluid" />
                        </a>
                    </div>
                  </div>
                  <div class="col-8">
                    <h5>
                      <a href="#">Mos Paratus Scisco Ymo</a>
                    </h5>
                  </div>
                </div>
                <!-- Latest blog post 4 -->
                <div class="row blog-post">
                  <div class="col-4">
                    <div class="blog-media">
                      <a href="#">
                          <img src="assets/img/blog/frog.jpg" alt="Picture of frog by Ben Fredericson" class="img-fluid" />
                        </a>
                    </div>
                  </div>
                  <div class="col-8">
                    <h5>
                      <a href="#">Modo Patria Praemitto Virtus</a>
                    </h5>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <!-- @Element: Subscrive button -->
          <div class="mb-4">
            <a href="#" class="btn btn-warning text-white"><i class="fa fa-rss"></i> Subscribe via RSS feed</a>
          </div>

          <!-- Follow Widget -->
          <div class="mb-4">
            <h4 class="title-divider">
              <span>Follow Us On</span>
            </h4>
            <!--@todo: replace with real social media links -->
            <ul class="list-unstyled">
              <li>
                <a href="#" class="text-hover-brand-twitter d-flex align-items-center py-1 text-hover-no-underline"><i class="fa fa-twitter-square fa-fw icon-2x"></i> Twitter</a>
              </li>
              <li>
                <a href="#" class="text-hover-brand-facebook d-flex align-items-center py-1 text-hover-no-underline"><i class="fa fa-facebook-square fa-fw icon-2x"></i> Facebook</a>
              </li>
              <li>
                <a href="#" class="text-hover-brand-linkedin d-flex align-items-center py-1 text-hover-no-underline"><i class="fa fa-linkedin fa-fw icon-2x"></i> LinkedIn</a>
              </li>
              <li>
                <a href="#" class="text-hover-brand-google-plus d-flex align-items-center py-1 text-hover-no-underline"><i class="fa fa-google-plus-square fa-fw icon-2x"></i> Google+</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- ======== @Region: #content-below ======== -->
  <div id="content-below">
    <!-- Awesome features call to action -->
    <div class="bg-primary bg-op-9 text-white py-4">
      <div class="container">
        <div class="row text-center text-lg-left align-items-lg-center">
          <div class="col-12 col-lg-7 text-white">
            <h3 class="font-weight-bold my-0 text-uppercase">
              Awesome Features
            </h3>
            <p class="font-weight-normal op-9 my-0"> <i class="la la-check-circle-o"></i> 99.9% Uptime <i class="la la-check-circle-o ml-lg-3"></i> Free Upgrades <i class="la la-check-circle-o ml-lg-3"></i> Fully Responsive <i class="la la-check-circle-o ml-lg-3"></i>              Bug Free </p>
          </div>
          <div class="col-12 col-lg-5 py-2 text-lg-right">
            <a href="#" class="btn btn-xlg btn-white btn-rounded shadow-lg bg-light bg-op-8 bg-hover-white">Get Started<i class="fa fa-arrow-right ml-2 mt-1"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>

 <?php include 'footer.php' ?>