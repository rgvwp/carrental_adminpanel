<?php include 'header.php' ?>

  <!-- ======== @Region: #highlighted ======== -->
  <div id="highlighted">
    <div class="inner">
      <!--Page Hero Region-->
      <div class="container">
        <!--Static Image-->
        <div class="row text-center">
          <img src="assets/img/slides/slide2.png" alt="Slide 2" />
        </div>
        <!--@see index.htm for slideshow example-->
      </div>
    </div>
  </div>

  <!-- ======== @Region: #content ======== -->
  <div id="content" class="demos">
    <div class="container">
      <h2 class="title-divider">
        <span>Starter Snippets</span>
        <small>Page starter snippets you can copy & paste to make new pages & layouts</small>
      </h2>
      <!--@EXAMPLE: Content Region With No Sidebars-->
      <h3 class="title-divider">
        <span>Example 1: <span class="font-weight-normal text-muted">Content Region With No Sidebars</span></span>
      </h3>
      <div class="row">
        <div class="col-md-12">Content Region With No Sidebars</div>
      </div>
      <!--@EXAMPLE: End of Content Region With No Sidebars-->
      <hr />
      <!--@EXAMPLE: Content Region With Left Sidebar-->
      <h3 class="title-divider">
        <span>Example 2: <span class="font-weight-normal text-muted">Content Region With Left Sidebar</span></span>
      </h3>
      <div class="row">
        <!--Content Area-->
        <div class="col-md-9 order-md-2"> Content Region With Left Sidebar </div>
        <!--Sidebar-->
        <div class="col-md-3 order-md-1 sidebar sidebar-left">
          <div class="inner">
            Sidebar Left
            <small>(use .inner wrapper to add border and padding)</small>
          </div>
        </div>
      </div>
      <!--@EXAMPLE: Content Region With Left Sidebar-->
      <hr />
      <!--@EXAMPLE: Content Region With Right Sidebar-->
      <h3 class="title-divider">
        <span>Example 3: <span class="font-weight-normal text-muted">Content Region With Right Sidebar</span></span>
      </h3>
      <div class="row">
        <!--Content Area-->
        <div class="col-md-9"> Content Region With Right Sidebar </div>
        <!--Sidebar-->
        <div class="col-md-3 sidebar-right">
          <div class="inner"> Sidebar right </div>
        </div>
      </div>
      <!--@EXAMPLE: Content Region With Right Sidebar-->
      <hr />
      <!--@EXAMPLE: Content Region With Both Sidebars-->
      <h3 class="title-divider">
        <span>Example 4: <span class="font-weight-normal text-muted">Content Region With Both Sidebars (left -> content -> Right)</span></span>
      </h3>
      <div class="row">
        <!--Content Area-->
        <div class="col-md-6 push-md-3"> Content Region With Both Sidebars </div>
        <!--Sidebar Left-->
        <div class="col-md-3 pull-md-6 sidebar sidebar-left">
          <div class="inner"> Sidebar Left </div>
        </div>
        <!--Sidebar Right-->
        <div class="col-md-3 sidebar-right">
          <div class="inner"> Sidebar Right </div>
        </div>
      </div>
      <!--@EXAMPLE: End of Content Region With Both Sidebars-->
      <hr />
      <!--@EXAMPLE: Content Region With Both Sidebars Stacked-->
      <h3 class="title-divider">
        <span>Example 5: <span class="font-weight-normal text-muted">Content Region With Both Sidebars Stacked (content -> sidebar 1 -> sidebar 2)</span></span>
      </h3>
      <div class="row">
        <!--Content Area-->
        <div class="col-md-6"> Content Region With Both Sidebars Stacked </div>
        <!--Sidebar Left-->
        <div class="col-md-3 sidebar-right">
          <div class="inner"> Sidebar 1 </div>
        </div>
        <!--Sidebar Right-->
        <div class="col-md-3 sidebar-right">
          <div class="inner"> Sidebar 2 </div>
        </div>
      </div>
      <!--@EXAMPLE: End of Content Region With Both Sidebars Stacked-->
    </div>
  </div>

  <!-- ======== @Region: #content-below ======== -->
  <div id="content-below">
    <!-- Awesome features call to action -->
    <div class="bg-primary bg-op-9 text-white py-4">
      <div class="container">
        <div class="row text-center text-lg-left align-items-lg-center">
          <div class="col-12 col-lg-7 text-white">
            <h3 class="font-weight-bold my-0 text-uppercase">
              Awesome Features
            </h3>
            <p class="font-weight-normal op-9 my-0"> <i class="la la-check-circle-o"></i> 99.9% Uptime <i class="la la-check-circle-o ml-lg-3"></i> Free Upgrades <i class="la la-check-circle-o ml-lg-3"></i> Fully Responsive <i class="la la-check-circle-o ml-lg-3"></i>              Bug Free </p>
          </div>
          <div class="col-12 col-lg-5 py-2 text-lg-right">
            <a href="#" class="btn btn-xlg btn-white btn-rounded shadow-lg bg-light bg-op-8 bg-hover-white">Get Started<i class="fa fa-arrow-right ml-2 mt-1"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>

 <?php include 'footer.php' ?>