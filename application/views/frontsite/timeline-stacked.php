<?php include 'header.php' ?>

  <!-- ======== @Region: #content ======== -->
  <div id="content">
    <div class="container" id="about">
      <h2 class="title-divider">
        <span>Timelines</span>
        <small>5 New Timeline Options</small>
      </h2>
      <!-- Timeline 1: timeline-stacked -->
      <h3>
        Timeline 4: .timeline-stacked
      </h3>
      <div class="timeline timeline-stacked">
        <div class="timeline-breaker">2014</div>
        <!--Timeline item 0-->
        <div class="timeline-item animated fadeIn de-02">
          <div class="row">
            <div class="col-md-4">
              <img src="assets/img/blog/frog.jpg" alt="Picture of frog by Ben Fredericson" class="img-fluid" />
            </div>
            <div class="col-md-8">
              <div class="mt-4 timeline-item-date">Thu 31st May</div>
              <h4 class="timeline-item-title">
                <a href="#">Aptent Camur Iriure Meus Veniam</a>
              </h4>
              <p class="timeline-item-description">Eros meus roto rusticus saluto suscipit. Bene cui iriure paratus sudo utrum. Mauris patria saluto tincidunt velit vero volutpat. Erat nulla roto sit. Elit eu gilvus hendrerit importunus imputo tum typicus. Abigo exerci haero illum melior
                singularis. Ad causa haero hendrerit lenis minim nibh suscipit tego.</p>
              <p class="timeline-item-description">Luptatum paulatim veniam verto. Accumsan autem immitto praemitto sino tamen zelus. Dolus ille modo neque populus quidne scisco sino validus. At cogo roto virtus. Abico comis damnum esse humo ideo obruo pecus saluto verto. Blandit dignissim
                erat facilisi jus. Acsi ideo nibh paulatim scisco torqueo ullamcorper valetudo vulputate.</p>
            </div>
          </div>
        </div>
        <!--Timeline item 1-->
        <div class="timeline-item animated right fadeIn de-02">
          <div class="row">
            <div class="col-md-4">
              <img src="assets/img/blog/ladybird.jpg" alt="Picture of frog by Ben Fredericson" class="img-fluid" />
            </div>
            <div class="col-md-8">
              <div class="mt-4 timeline-item-date">Wed 30th May</div>
              <h4 class="timeline-item-title">
                <a href="#">Conventio Molior Oppeto Pala Veniam</a>
              </h4>
              <p class="timeline-item-description">Hos metuo mos neo nimis occuro premo ratis venio vereor. Bene eros magna proprius qui quibus sudo uxor ymo. Brevitas esse mauris voco. Decet dignissim eros esca iriure neo occuro quadrum tego venio. Accumsan causa vel. Bene cogo distineo
                molior quidne saepius typicus.</p>
              <p class="timeline-item-description">Et eum letalis ludus occuro patria praemitto sagaciter ullamcorper wisi. Dolor exerci nimis pertineo sino. Ad aptent meus nibh occuro praemitto sit sudo vulpes ymo. Et nimis pneum premo proprius suscipere. Adipiscing brevitas nostrud nunc
                olim patria pecus valetudo velit.</p>
            </div>
          </div>
        </div>
        <!--Timeline item 2-->
        <div class="timeline-item animated fadeIn de-02">
          <div class="row">
            <div class="col-md-4">
              <img src="assets/img/blog/ape.jpg" alt="Picture of frog by Ben Fredericson" class="img-fluid" />
            </div>
            <div class="col-md-8">
              <div class="mt-4 timeline-item-date">Sun 27th May</div>
              <h4 class="timeline-item-title">
                <a href="#">Aptent Damnum Refero Tincidunt Venio</a>
              </h4>
              <p class="timeline-item-description">At decet dolor duis gemino jus quia sed turpis. Amet brevitas oppeto sit. Antehabeo camur iriure neo nisl quidne roto secundum torqueo utrum. Decet hendrerit ludus meus os probo saluto te vindico virtus. Distineo meus sino. Amet blandit
                exerci hos mauris nimis tego tum.</p>
              <p class="timeline-item-description">Cogo luptatum neo nibh plaga proprius ymo. Decet ille pneum roto venio vereor. Abdo dolor mauris os tation. Acsi decet jus loquor pagus suscipere. Cogo feugiat os. Distineo exputo hos humo macto mauris meus saluto. Appellatio conventio jugis
                neque quia typicus utinam.</p>
            </div>
          </div>
        </div>
        <!--Timeline item 3-->
        <div class="timeline-item animated highlight right fadeIn de-02">
          <div class="row">
            <div class="col-md-4">
              <img src="assets/img/blog/fly.jpg" alt="Picture of frog by Ben Fredericson" class="img-fluid" />
            </div>
            <div class="col-md-8">
              <div class="mt-4 timeline-item-date">Sat 26th May</div>
              <h4 class="timeline-item-title">
                <a href="#">Elit Enim Eum Quis Velit</a>
              </h4>
              <p class="timeline-item-description">Eu gemino lobortis pala. Abico consequat iaceo olim paulatim quadrum quidem quis refoveo vulputate. Defui quidem similis validus veniam vero. Aliquam inhibeo probo. Adipiscing aptent gemino iriure modo plaga sit. Amet cogo euismod fere melior
                mos ratis utrum uxor. Adipiscing aliquip consectetuer facilisis haero importunus qui roto torqueo uxor.</p>
              <p class="timeline-item-description">Abluo iriure meus pagus. Capto eu facilisi minim natu nulla refero secundum vel. Comis eu facilisi feugiat incassum mauris persto quidne. Ad ea humo magna metuo oppeto similis. Brevitas capto luctus nisl odio vulputate. Blandit et facilisis
                inhibeo saluto utrum.</p>
            </div>
          </div>
        </div>
        <div class="timeline-breaker timeline-breaker-middle">2013</div>
        <!--Timeline item 4-->
        <div class="timeline-item animated fadeIn de-02">
          <div class="row">
            <div class="col-md-4">
              <img src="assets/img/blog/water-pump.jpg" alt="Picture of frog by Ben Fredericson" class="img-fluid" />
            </div>
            <div class="col-md-8">
              <div class="mt-4 timeline-item-date">Sun 27th May</div>
              <h4 class="timeline-item-title">
                <a href="#">Aliquip Augue Illum Jus Utinam</a>
              </h4>
              <p class="timeline-item-description">Accumsan augue commoveo dolore iaceo iustum letalis ludus voco vulputate. Eu euismod feugiat gemino jumentum quidne utinam vereor. Cogo hendrerit interdico iustum praemitto. Aliquam in jumentum letalis lucidus. Ad dolor eligo illum incassum
                neque paulatim tamen valde vindico. Comis fere hos importunus patria scisco ullamcorper.</p>
              <p class="timeline-item-description">Iusto jugis vicis. Distineo esca sudo turpis zelus. Abdo cui jugis vulputate. Diam distineo duis neo os patria quadrum tego usitas uxor. Comis duis erat et exerci iustum letalis probo quadrum usitas. Abluo acsi sagaciter. Causa paratus qui.
                Hendrerit jumentum os vereor.</p>
            </div>
          </div>
        </div>
        <!--Timeline item 5-->
        <div class="timeline-item animated right fadeIn de-02">
          <div class="row">
            <div class="col-md-4">
              <img src="assets/img/blog/butterfly.jpg" alt="Picture of frog by Ben Fredericson" class="img-fluid" />
            </div>
            <div class="col-md-8">
              <div class="mt-4 timeline-item-date">Wed 23rd May</div>
              <h4 class="timeline-item-title">
                <a href="#">Dolus Neque Nulla Paratus Refero</a>
              </h4>
              <p class="timeline-item-description">Dolus erat eros magna saluto singularis turpis validus vicis. Consequat quibus roto. Abigo amet antehabeo ea elit imputo quadrum. Causa exputo persto. Abbas ad cogo ex facilisi modo odio ullamcorper venio. Bene eligo esse eu lenis molior
                qui tum utrum volutpat.</p>
              <p class="timeline-item-description">Aliquam gemino roto. Feugiat jus lenis plaga validus velit zelus. Capto pagus paratus paulatim ullamcorper. At dolor facilisis neque. Euismod ibidem inhibeo praemitto sed tamen vulpes. Abdo camur dolore enim incassum iriure natu nostrud.
                Cogo commoveo eum exputo utinam virtus wisi.</p>
            </div>
          </div>
        </div>
        <!--Timeline item 6-->
        <div class="timeline-item animated fadeIn de-02">
          <div class="row">
            <div class="col-md-4">
              <img src="assets/img/blog/bee.jpg" alt="Picture of frog by Ben Fredericson" class="img-fluid" />
            </div>
            <div class="col-md-8">
              <div class="mt-4 timeline-item-date">Wed 23rd May</div>
              <h4 class="timeline-item-title">
                <a href="#">Autem Jumentum Lucidus Nunc Si</a>
              </h4>
              <p class="timeline-item-description">Decet illum vel. Conventio ex facilisis te typicus venio vulpes. At camur cui erat iusto metuo praemitto quae vereor. Aliquip ideo imputo ratis similis ulciscor vulpes. At blandit dignissim genitus neo proprius. Letalis ludus praemitto si.
                Brevitas eu hos magna praemitto suscipit tego vulputate.</p>
              <p class="timeline-item-description">Dolus enim jugis luptatum sed. Commodo immitto mauris vereor. Conventio exputo magna mos pala quia vicis wisi. Comis genitus praemitto ratis. Aliquip ea haero interdico loquor magna occuro tincidunt. Dignissim modo paulatim praemitto. Saluto
                vereor volutpat. At diam gravis interdico refero utinam vero.</p>
            </div>
          </div>
        </div>
        <!--Timeline item 7-->
        <div class="timeline-item animated right fadeIn de-02">
          <div class="row">
            <div class="col-md-4">
              <object width="560" height="315">
                  <param name="movie" value="//www.youtube.com/v/YXVoqJEwqoQ?version=3&amp;hl=en_US&amp;rel=0"></param>
                  <param name="allowFullScreen" value="true"></param>
                  <param name="allowscriptaccess" value="always"></param>
                  <embed src="//www.youtube.com/v/YXVoqJEwqoQ?version=3&amp;hl=en_US&amp;rel=0" type="application/x-shockwave-flash" width="560" height="315" allowscriptaccess="always" allowfullscreen="true"></embed>
                </object>
            </div>
            <div class="col-md-8">
              <div class="mt-4 timeline-item-date">Sun 27th May</div>
              <h4 class="timeline-item-title">
                <a href="#">Abico Blandit Dolus Plaga Premo</a>
              </h4>
              <p class="timeline-item-description">Aliquip iaceo ibidem tation. Abluo accumsan esca facilisis oppeto premo ratis sino. Refero saluto usitas. Consequat damnum euismod meus venio voco. Odio rusticus saepius si. Amet camur capto commoveo dolor facilisi macto pala tation vindico.
                Abbas ille pecus pneum suscipit virtus.</p>
              <p class="timeline-item-description">Luctus nobis quidem sudo. Abigo accumsan antehabeo at eligo gravis iaceo molior nunc paulatim. Abico euismod iriure luctus modo proprius typicus usitas. Ille letalis velit. Commodo defui ea. Erat exerci olim praemitto. Brevitas ullamcorper
                velit vindico. Haero persto praemitto qui similis singularis.</p>
            </div>
          </div>
        </div>
        <div class="timeline-breaker timeline-breaker-bottom">The End</div>
      </div>
      <!-- Timeline -->
     
    </div>
  </div>

  <!-- ======== @Region: #content-below ======== -->
  <div id="content-below">
    <!-- Awesome features call to action -->
    <div class="bg-primary bg-op-9 text-white py-4">
      <div class="container">
        <div class="row text-center text-lg-left align-items-lg-center">
          <div class="col-12 col-lg-7 text-white">
            <h3 class="font-weight-bold my-0 text-uppercase">
              Awesome Features
            </h3>
            <p class="font-weight-normal op-9 my-0"> <i class="la la-check-circle-o"></i> 99.9% Uptime 
              <i class="la la-check-circle-o ml-lg-3"></i> Free Upgrades <i class="la la-check-circle-o ml-lg-3"></i> Fully Responsive <i class="la la-check-circle-o ml-lg-3"></i> Bug Free </p>
          </div>
          <div class="col-12 col-lg-5 py-2 text-lg-right">
            <a href="#" class="btn btn-xlg btn-white btn-rounded shadow-lg bg-light bg-op-8 bg-hover-white">Get Started<i class="fa fa-arrow-right ml-2 mt-1"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>

 <?php include 'footer.php' ?>