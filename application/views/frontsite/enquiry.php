<?php include 'header.php' ?>

  <!-- ======== @Region: #content ======== -->
  <div id="content">
    <div class="container">
   <?php if($this->session->flashdata('permission_message'))
	 		{
			?>
					
                        <div class="alert alert-block alert-success " style="background-color:#64aea2 ;height:50px;width:400px; margin-left: 300px">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p > <?php echo $this->session->flashdata('permission_message'); ?></p>
                        </div>						
									
			<?php } ?>
            <?php if($this->session->flashdata('flash_message'))
	 		{
			?>
					
                        <div class="alert alert-block alert-success" style="background-color:#ff708a">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p ><h4 style="background-color:#ff708a"> Error!</h4> <?php echo $this->session->flashdata('flash_message'); ?></p>
                        </div>						
									
			<?php } ?>
      <form  action="<?php echo base_url();?>superadmin/FrontManager/saveEnquiry"     class="form-login form-wrapper form-medium" method="post" role="form">
        <h3 class="title-divider">
          <span>Enquiry Form</span>
          <small class="mt-4">You want to any enquiry </small>
        </h3>
        <h5>
         Enter your Name
        </h5>
        <div class="form-group">
          <input type="text" class="form-control name"  name="name" onblur="abc();" id="signup-first-name-page" placeholder="Name">
              <span id="name" style="color:red"></span>

        </div>
        <h5>
          Email Address
        </h5>
        <div class="form-group">
            <input type="email" class="form-control email" name="email" onblur="abc();" id="signup-first-name-page" placeholder="Email Id" required="">
                    <span id="email" style="color:red"></span>

        </div>
        <h5>
          Contact
        </h5>
        <div class="form-group">
          <input type="text" class="form-control contact"  name="contact" onkeypress="return isNumber(event)" onblur="abc();" id="signup-first-name-page" placeholder="Contact">
                     <span id="contact" style="color:red"></span>

        </div>
        <h5>
         Message
        </h5>
        <div class="form-group">
            <textarea type="text" class="form-control message"  name="message"  onblur="abc();" id="signup-first-name-page" rows="5" placeholder="Message"></textarea>
                     <span id="message" style="color:red"></span>

        </div>
        
        
        
        <hr />
        <button class="btn btn-primary" type="submit" onclick="return MYtest();">Submit</button>
      </form>
    </div>
  </div>

  
  <?php include 'footer.php' ?>

  
  
  <script>
      
      function MYtest()
      {
          
         
          
          
       
          var name = $(".name").val();
          var email = $(".email").val();
          var contact = $(".contact").val();
          var message = $(".message").val();
          
          if(name == "")
          {
              
              $("#name").html("Please filled  name");
              return false;
          }
        
          if(email == "")
          {
              $("#email").html("Please filled email");
              return false;
          }
          if(contact == "")
          {
              $("#contact").html("Please filled contact");
              return false;
          }
           if(message == "")
          {
              $("#message").html("Please filled message");
              return false;
          }
          
          $("#FormId").submit();
          
          
    }
    
    
    
    function abc()
    {
        $("#name").html(" ");
          $("#email").html(" ");
          $("#contact").html(" ");
          $("#message").html(" ");
    }
          </script>
          
          
            <script>
                    
                      function isNumber(evt) {
            var iKeyCode = (evt.which) ? evt.which : evt.keyCode
            if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
                return false;

            return true;
        
                    }
                    
                    
                </script>
                