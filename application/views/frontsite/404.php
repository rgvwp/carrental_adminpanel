<?php include 'header.php' ?>

  <!-- ======== @Region: #content ======== -->
  <div id="content">
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <div class="w-90 mx-auto">
            <h2 class="error-code text-xs-x7 text-md-x10">
              404 <i class="fa fa-file-text text-primary text-xs-x6 text-md-x8"></i>
            </h2>
            <h2 class="error-message text-xs-x2 text-md-x2">
              Oops, This Page Could Not Be Found!
            </h2>
            <p class="error-details text-xs-x1">Enim nisi massa nascetur, pulvinar porttitor ut sed penatibus, tincidunt? Phasellus lacus, eros, mid lorem ridiculus amet, urna? Ultricies et et mus nisi lundium! Pulvinar enim, duis et. </p>
          </div>
        </div>
        <div class="col-md-4">
          <h4>
            Maybe you were looking for:
          </h4>
          <ul class="list-unstyled list-lg list-bordered">
            <li><i class="fa fa-angle-right list-item-icon"></i> <a href="index.php">Home</a></li>
            <li><i class="fa fa-angle-right list-item-icon"></i> <a href="features.php">Features</a></li>
            <li><i class="fa fa-angle-right list-item-icon"></i> <a href="pricing.php">Pricing</a></li>
            <li><i class="fa fa-angle-right list-item-icon"></i> <a href="customers.php">Customers</a></li>
          </ul>
          <form class="error-search mt-4">
            <h4>
              Or Search for it:
            </h4>
            <div class="input-group">
              <input type="text" class="form-control" placeholder="Search the site">
              <span class="input-group-append">
                  <button class="btn btn-secondary" type="button">Search</button>
                </span>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <?php include 'footer.php' ?>