<?php include 'header.php' ?>

  <!-- ======== @Region: #content ======== -->
  <div id="content">
    <div class="container">
      <!-- Services -->
      <h2 class="title-divider">
        <span>Our <span class="font-weight-normal text-muted">Services</span></span>
        <small>Our Services included in all plans.</small>
      </h2>
      <!-- Features -->
      <div class="row mb-4">
        <div class="col-lg-4 py-2">
            <div class="iconshow mb-3"><i class="fa fa-tachometer icon-3x text-primary"></i></div>
          <h4 class="mb-3">
            Fully Optimized
          </h4>
          <p>Limos King offers superb lim service in New York and Manhattan.  We are the most popular and has been chosen by many important people. We also provide premier service to the airport, With our services, we can ensure you to enjoy the whole spectacular ride.</p>
        </div>
        <div class="col-lg-4 py-2">
            <div class="iconshow mb-3"><i class="fa fa-wrench icon-3x text-primary"></i></div>
          <h4 class="mb-3">
            Free Support
          </h4>
          <p>Limos King offers superb lim service in New York and Manhattan.  We are the most popular and has been chosen by many important people. We also provide premier service to the airport, With our services, we can ensure you to enjoy the whole spectacular ride.</p>
        </div>
        <div class="col-lg-4 py-2">
            <div class="iconshow mb-3"><i class="fa fa-rocket icon-3x text-primary"></i></div>
          <h4 class="mb-3">
            Free Upgrades
          </h4>
          <p>Limos King offers superb lim service in New York and Manhattan.  We are the most popular and has been chosen by many important people. We also provide premier service to the airport, With our services, we can ensure you to enjoy the whole spectacular ride.</p>
        </div>
        <div class="col-lg-4 py-2">
          <div class="showcaseopt">
              <div class="iconshow mb-3"><i class="fa fa-user icon-3x text-primary"></i></div>
              <h4 class="mb-3">Optional Panel</h4>
        <p>Limos King offers superb lim service in New York and Manhattan.  We are the most popular and has been chosen by many important people. We also provide premier service to the airport, With our services, we can ensure you to enjoy the whole spectacular ride. </p>
        </div>
        </div>
        <div class="col-lg-4 py-2">
            <div class="iconshow mb-3"><i class="fa fa-users icon-3x text-primary"></i></div>
          <h4 class="mb-3">
            Multiuser
          </h4>
          <p>Limos King offers superb lim service in New York and Manhattan.  We are the most popular and has been chosen by many important people. We also provide premier service to the airport, With our services, we can ensure you to enjoy the whole spectacular ride.</p>
        </div>
        <div class="col-lg-4 py-2">
            <div class="iconshow mb-3"> <i class="fa fa-plug icon-3x text-primary"></i></div>
          <h4 class="mb-3">
            Plug n play
          </h4>
          <p>Limos King offers superb lim service in New York and Manhattan.  We are the most popular and has been chosen by many important people. We also provide premier service to the airport, With our services, we can ensure you to enjoy the whole spectacular ride.</p>
        </div>
      </div>
      <div class="row">
        <!-- Secondary features -->
        <div class="col-md-9">
          <h3 class="title-divider">
            <span>Bonus <span class="font-weight-normal text-muted">Features</span></span>
            <small>Bonus features only included in our Pro & Pro + plans.</small>
          </h3>
              <div class="feature tab-pane active">
                <i class="fa fa-mobile icon-5x text-primary float-right ml-3" data-animate="fadeIn"></i>
                <h3>
                  Mobile Friendly
                </h3>
                <p>Rhoncus adipiscing, magna integer cursus augue eros lacus porttitor magna. Dictumst, odio! Elementum tortor sociis in eu dis dictumst pulvinar lorem nec aliquam a nascetur.</p>
                <p>Rhoncus adipiscing, magna integer cursus augue eros lacus porttitor magna. </p>
              </div>
              <div class="feature tab-pane">
                <i class="fa fa-wrench icon-4x text-primary float-right ml-3" data-animate="fadeIn"></i>
                <h3>
                  24/7 Support
                </h3>
                <p>Rhoncus adipiscing, magna integer cursus augue eros lacus porttitor magna. Dictumst, odio! Elementum tortor sociis in eu dis dictumst pulvinar lorem nec aliquam a nascetur.</p>
                <p>Rhoncus adipiscing, magna integer cursus augue eros lacus porttitor magna. </p>
              </div>
              
              
        </div>
        <!--Contact block-->
        <div class="col-md-3">
          <h3 class="title-divider">
            <span>Questions?</span>
            <small>Get in touch!</small>
          </h3>
          <p>
            <abbr title="Phone"><i class="fa fa-phone"></i></abbr>  +91-522-4043950
          </p>
          <p>
            <abbr title="Email"><i class="fa fa-envelope"></i></abbr> info@livesoftwaresolution.com
          </p>
          <p>
            <abbr title="Address"><i class="fa fa-home"></i></abbr>330 Lekhraj Khajana, Faizabad Road
          </p>
          <hr />
          <div class="bg-primary bg-shadow border-primary-darkend text-white p-3 mt-3 pos-relative">
            <i class="fa fa-phone-square icon-10x icon-rotate-25 icon-bg icon-bg-r-ol icon-bg-b-ol op-3 text-white"></i>
            <h4 class="my-0 font-weight-bold text-uppercase">
              Request a callback!
            </h4>
            <hr class="hr-lg my-2 ml-0 hr-white w-20" />
            <form id="callback-form" action="#" role="form">
              <div class="input-group">
                <label class="sr-only" for="callback-number">Your Number</label>
                <input type="tel" class="form-control" id="callback-number" placeholder="Your Number">
                <span class="input-group-append">
                    <button class="btn btn-dark" type="button">Go</button>
                  </span>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- ======== @Region: #content-below ======== -->
  <div id="content-below">
    <!-- Awesome features call to action -->
    <div class="bg-primary bg-op-9 text-white py-4">
      <div class="container">
        <div class="row text-center text-lg-left align-items-lg-center">
          <div class="col-12 col-lg-7 text-white">
            <h3 class="font-weight-bold my-0 text-uppercase">
              Awesome Features
            </h3>
            <p class="font-weight-normal op-9 my-0"> <i class="la la-check-circle-o"></i> 99.9% Uptime <i class="la la-check-circle-o ml-lg-3"></i> Free Upgrades <i class="la la-check-circle-o ml-lg-3"></i> Fully Responsive <i class="la la-check-circle-o ml-lg-3"></i>              Bug Free </p>
          </div>
          <div class="col-12 col-lg-5 py-2 text-lg-right">
            <a href="#" class="btn btn-xlg btn-white btn-rounded shadow-lg bg-light bg-op-8 bg-hover-white">Get Started<i class="fa fa-arrow-right ml-2 mt-1"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php include 'footer.php' ?>