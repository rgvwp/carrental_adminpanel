<?php include 'header.php' ?>

  <!-- ======== @Region: #content ======== -->
  
  
  <div id="content" class="pt-4 pb-4" data-bg-img="<?php echo base_url();?>front/assets/img/parallax.jpg" data-animate="fadeIn">
    <div id="projects" class="container p-3 py-lg-6" data-toggle="magnific-popup" data-magnific-popup-settings='{"delegate": "a.project", "gallery":{"enabled":true}}'>
     
      <h2 class="text-center text-uppercase font-weight-bold my-0 text-white">
        CLIENTS SAY
      </h2>
      <h5 class="text-center font-weight-light mt-2 mb-0 text-muted text-white">
        Explore Our Consultancy Services
      </h5>
      
      <div class="mt-4 owl-nav-over owl-nav-over-lg owl-dots-center" data-toggle="owl-carousel" data-owl-carousel-settings='{"responsive":{"0":{"items":1, "nav":false, "dots":true}, "600":{"items":2, "dots":true}, "940":{"items":3, "nav":true, "dots":true}}, "margin":1, "nav":true, "dots":false}'>

       <?php    $clientquery = $this->db->get_where('tblcmsfolio',array('cmsfoliostatus'=>1));
       foreach($clientquery->result() as $k=>$v){?>
          
        <!--Project item 1-->
         <div class="client_content_inner text-center pt-5 pb-5 pl-5 pr-5">
             <div class="client_image_holder mb-4"><img src="<?php echo base_url();?>uploads/images/<?php echo $v->cmsfolioimg;?>" class="img-responsive"></div>
             <h4 class="text-uppercase"><?php echo $v->cmsfoliotitle;?></h4>
             <div class="client_rating_holder mb-4">
                 <span class="testimonial_star_holder"><i class="fa fa-star"></i></span>
                 <span class="testimonial_star_holder"><i class="fa fa-star"></i></span>
                 <span class="testimonial_star_holder"><i class="fa fa-star"></i></span>
                 <span class="testimonial_star_holder"><i class="fa fa-star"></i></span>
                 <span class="testimonial_star_holder"><i class="fa fa-star"></i></span>
             </div>
             <div class="client_text_holder">
                 <div class="client_text_inner">
                     <p><?php echo $v->cmsfoliodesc;?></p>
                     <p class="text-uppercase" style="font-weight: 400;color: #292a24;font-size: 16px;">- Lydia Lavell</p>
                 </div>
             </div>
         </div>   
       <?php  } ?>

      
        <!--Project item 3-->
<!--        <div class="client_content_inner text-center pt-5 pb-5 pl-5 pr-5">
             <div class="client_image_holder mb-4"><img src="<?php echo base_url();?>front/assets/img/testimonial.png" class="img-responsive"></div>
             <h4 class="text-uppercase">These guys always amaze me</h4>
             <div class="client_rating_holder mb-4">
                 <span class="testimonial_star_holder"><i class="fa fa-star"></i></span>
                 <span class="testimonial_star_holder"><i class="fa fa-star"></i></span>
                 <span class="testimonial_star_holder"><i class="fa fa-star"></i></span>
                 <span class="testimonial_star_holder"><i class="fa fa-star"></i></span>
                 <span class="testimonial_star_holder"><i class="fa fa-star"></i></span>
             </div>
             <div class="client_text_holder">
                 <div class="client_text_inner">
                     <p>3Phasellus finibus velit ut turpis venenatis rutrum. Aliquam erat volutpat. Donec sit amet auctor enim, ut eleifend felis</p>
                     <p class="text-uppercase" style="font-weight: 400;color: #292a24;font-size: 16px;">- Lydia Lavell</p>
                 </div>
             </div>
         </div> -->

        <!--Project item 4-->
<!--        <div class="client_content_inner text-center pt-5 pb-5 pl-5 pr-5">
             <div class="client_image_holder mb-4"><img src="<?php echo base_url();?>front/assets/img/testimonial.png" class="img-responsive"></div>
             <h4 class="text-uppercase">These guys always amaze me</h4>
             <div class="client_rating_holder mb-4">
                 <span class="testimonial_star_holder"><i class="fa fa-star"></i></span>
                 <span class="testimonial_star_holder"><i class="fa fa-star"></i></span>
                 <span class="testimonial_star_holder"><i class="fa fa-star"></i></span>
                 <span class="testimonial_star_holder"><i class="fa fa-star"></i></span>
                 <span class="testimonial_star_holder"><i class="fa fa-star"></i></span>
             </div>
             <div class="client_text_holder">
                 <div class="client_text_inner">
                     <p>4Phasellus finibus velit ut turpis venenatis rutrum. Aliquam erat volutpat. Donec sit amet auctor enim, ut eleifend felis</p>
                     <p class="text-uppercase" style="font-weight: 400;color: #292a24;font-size: 16px;">- Lydia Lavell</p>
                 </div>
             </div>
         </div> -->

        <!--Project item 5-->
<!--        <div class="client_content_inner text-center pt-5 pb-5 pl-5 pr-5">
             <div class="client_image_holder mb-4"><img src="<?php echo base_url();?>front/assets/img/testimonial.png" class="img-responsive"></div>
             <h4 class="text-uppercase">These guys always amaze me</h4>
             <div class="client_rating_holder mb-4">
                 <span class="testimonial_star_holder"><i class="fa fa-star"></i></span>
                 <span class="testimonial_star_holder"><i class="fa fa-star"></i></span>
                 <span class="testimonial_star_holder"><i class="fa fa-star"></i></span>
                 <span class="testimonial_star_holder"><i class="fa fa-star"></i></span>
                 <span class="testimonial_star_holder"><i class="fa fa-star"></i></span>
             </div>
             <div class="client_text_holder">
                 <div class="client_text_inner">
                     <p>5Phasellus finibus velit ut turpis venenatis rutrum. Aliquam erat volutpat. Donec sit amet auctor enim, ut eleifend felis</p>
                     <p class="text-uppercase" style="font-weight: 400;color: #292a24;font-size: 16px;">- Lydia Lavell</p>
                 </div>
             </div>
         </div> -->

        

      </div>
    </div>
  </div>

  <!-- ======== @Region: #content-below ======== -->
  <div id="content-below">
    <!-- Awesome features call to action -->
    <div class="bg-primary bg-op-9 text-white py-4">
      <div class="container">
        <div class="row text-center text-lg-left align-items-lg-center">
          <div class="col-12 col-lg-7 text-white">
            <h3 class="font-weight-bold my-0 text-uppercase">
              Awesome Features
            </h3>
            <p class="font-weight-normal op-9 my-0"> <i class="la la-check-circle-o"></i> 99.9% Uptime <i class="la la-check-circle-o ml-lg-3"></i> Free Upgrades <i class="la la-check-circle-o ml-lg-3"></i> Fully Responsive <i class="la la-check-circle-o ml-lg-3"></i>              Bug Free </p>
          </div>
          <div class="col-12 col-lg-5 py-2 text-lg-right">
            <a href="#" class="btn btn-xlg btn-white btn-rounded shadow-lg bg-light bg-op-8 bg-hover-white">Get AppStrap<i class="fa fa-arrow-right ml-2 mt-1"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>

 <?php include 'footer.php' ?>