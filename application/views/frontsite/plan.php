
<?php include 'header.php' ?>

 <!--Pricing Table-->
    <div id="pricing" class="bg-light py-3 py-lg-6">
      <div class="container">
<!--        <h2 class="title-divider">
          <span>Pricing <span class="font-weight-normal text-muted">Plans Tables</span></span>
          <small>Compare our pricing plans.</small>
        </h2>
        
          -->
        <hr class="hr-lg mt-0 mb-3 w-10 mx-auto hr-primary" />
        <h2 class="text-center text-uppercase font-weight-bold my-0">
          Pricing Plans
        </h2>
        <h5 class="text-center font-weight-light mt-2 mb-0 text-muted">
          Competitive pricing plans to suit your needs
        </h5>
        <hr class="mb-5 w-50 mx-auto" />
<!--        <div class="row pricing-stack">
          
          <div class="col-md-4">
            <div class="card bg-shadow text-center border-0">
              <div class="card-ribbon card-ribbon-top card-ribbon-right bg-light text-muted">Best Buy</div>
              <h4 class="card-title py-3 my-0 text-shadow op-8">
                Budget
              </h4>
              <p class="price-banner bg-light shadow-sm card-body-overlap">
                <span class="price-currency">$</span>
                <span class="price-digits">19<span>.95</span></span>
                <span class="price-term">/MO</span>

              </p>
              <div class="card-body">
                <ul class="list-unstyled text-sm text-black-50">
                  <li>Free vehicle delivery</li>
                  <li>Car navigation system</li>
                  <li>Standard fuel plan</li>
                  <li>Breakdown assistance</li>
                  <li>Full insurance</li>
                </ul>
                <a href="#" class="btn btn-primary btn-rounded btn-lg">Sign Up</a>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card bg-white bg-shadow text-center rounded-0 border-0 border-top-3-primary">
              <div class="card-ribbon card-ribbon-top card-ribbon-left bg-primary text-white">Popular</div>
              <h4 class="card-title py-3 my-0 text-shadow op-8">
                Business
              </h4>
              <p class="price-banner bg-light shadow-sm card-body-overlap">
                <span class="price-currency">$</span>
                <span class="price-digits">199<span>.95</span></span>
                <span class="price-term">/MO</span>

              </p>
              <div class="card-body">
                <ul class="list-unstyled text-sm text-black-50">
                  <li>Free vehicle delivery</li>
                  <li>Car navigation system</li>
                  <li>Standard fuel plan</li>
                  <li>Breakdown assistance</li>
                  <li>Full insurance</li>
                </ul>
                <a href="#" class="btn btn-primary btn-rounded btn-lg mt-4">Sign Up</a>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card bg-shadow text-center border-0">
              <div class="card-ribbon card-ribbon-top card-ribbon-right bg-light text-muted">Best Buy</div>
              <h4 class="card-title py-3 my-0 text-shadow op-8">
                Enterprise
              </h4>
              <p class="price-banner bg-light shadow-sm card-body-overlap">
                <span class="price-currency">$</span>
                <span class="price-digits">19<span>.95</span></span>
                <span class="price-term">/MO</span>

              </p>
              <div class="card-body">
                <ul class="list-unstyled text-sm text-black-50">
                  <li>Free vehicle delivery</li>
                  <li>Car navigation system</li>
                  <li>Standard fuel plan</li>
                  <li>Breakdown assistance</li>
                  <li>Full insurance</li>
                </ul>
                <a href="#" class="btn btn-primary btn-rounded btn-lg">Sign Up</a>
              </div>
            </div>
          </div>
        </div>-->
       
      </div>
    </div>
  <!-- ======== @Region: #content ======== -->
  <div id="content">
    <div class="container">
      <div class="block">
        
        
        <!--Stack 1: 3 plans-->
        <div class="row card-grid-overlap pricing-table mt-4">
          <!-- Key column - hidden on mobile, use .pricing-title-hidden class to push down top title (.pricing-title) -->
          <div class="col-lg-3 d-none d-lg-block">
            <div class="card bg-shadow card-header-hidden text-right">
              <h3 class="card-header font-weight-normal pt-4">
                Features
              </h3>
              <div class="price-banner"> <span class="price-digits">Price</span> </div>
              <ul class="list-group list-group-flush list-group-striped">
                <li class="list-group-item">User Accounts</li>
                <li class="list-group-item">Private Projects</li>
                <li class="list-group-item">Public Projects</li>
                <li class="list-group-item">Disk Space</li>
                <li class="list-group-item">Monthly Bandwidth</li>
                <li class="list-group-item">24/7 Email Support</li>
                <li class="list-group-item">Phone Support</li>
              </ul>
            </div>
          </div>
          <!-- Plan 1 -->
          <?php $query =$this->db->get_where("tblplan",array('planprice'=>"free"))->result_array();?>
          
          <div class="col-lg-3">
            <div class="card bg-shadow text-center">
              <h3 class="card-header font-weight-normal pt-4">
                  
                   <?php  $compdemo = $this->session->userdata['companyid'];
                  
                  if($compdemo !=""){?>
              
              
                           <form action="<?php echo base_url();?>frontcarrental/Signup/updateDemo" method="post" id="demoForm">

                  <?php }else { ?>
                  
                  
                           <form action="<?php echo base_url();?>frontcarrental/Signup/signupDemo" method="post" id="demoForm">

                           <?php } ?>   
                               
                               
                               
           <?php echo $query[0]['planname'];?>
              </h3>
                <input type="hidden" name="demoid" value="<?php echo $query[0]['planid'];?>" >
              <div class="price-banner"> <span class="price-digits"><?php echo $query[0]['planprice'];?></span> </div>
              <ul class="list-group list-group-flush list-group-striped">
                <li class="list-group-item"><span class="d-md-none">User Accounts: </span><?php echo $query[0]['planaccount'];?></li>
                <li class="list-group-item"><span class="d-md-none">Private Projects: </span><?php echo $query[0]['planprivproject'];?></li>
                <li class="list-group-item"><span class="d-md-none">Public Projects: </span><?php echo $query[0]['planpubproject'];?></li>
                <li class="list-group-item"><span class="d-md-none">Disk Space: </span><?php echo $query[0]['plandiskspace'];?></li>
                <li class="list-group-item"><span class="d-md-none">Monthly Bandwidth: </span><?php echo $query[0]['planbandwidth'];?></li>
                <li class="list-group-item"><span class="d-md-none">24/7 Email Support: </span><i class="fa fa-check"></i></li>
                <li class="list-group-item"><span class="d-md-none">Phone Support: </span><i class="fa fa-times"></i></li>
              </ul>
              
              <?php $checkfree = $this->session->userdata('password');
              
//           ******************
                $comp = $this->session->userdata['companyid'];
                  
                  if($comp !=""){?>
              
              
              <div class="card-body card-footer"> <a href="#" class="btn btn-primary btn-block" onclick="signUpDemo()" >Buy</a> </div>

                  <?php } 
              
              
              
  //**********************            
              
                 elseif($checkfree !=""){
              ?>
              <div class="card-body card-footer"> <a href="#" class="btn btn-primary btn-block" onclick="signUpDemo()" >Buy</a> </div>
                 <?php } else { ?>
              
                <div class="card-body card-footer"> <a href="<?php echo base_url();?>Frontuser/signup" class="btn btn-primary btn-block"  >Sign Up</a> </div>

                 <?php } ?>
              </form>
            </div>
          </div>
          
          <?php
                  $this->db->where('planstatus',1);
                       $this->db->where('planprice!=',"free");

          $query = $this->db->get("tblplan");
          foreach($query->result() as $k=>$vl) { ?>
          <div class="col-lg-3">
            <div class="card bg-shadow text-center">
              <h3 class="card-header font-weight-normal pt-4">
           <?php echo $vl->planname;?>
              </h3>
              <div class="price-banner"> <span class="price-digits"><?php echo $vl->planprice;?></span> </div>
              <ul class="list-group list-group-flush list-group-striped">
                <li class="list-group-item"><span class="d-md-none">User Accounts: </span><?php echo $vl->planaccount;?></li>
                <li class="list-group-item"><span class="d-md-none">Private Projects: </span><?php echo $vl->planprivproject;?></li>
                <li class="list-group-item"><span class="d-md-none">Public Projects: </span><?php echo $vl->planpubproject;?></li>
                <li class="list-group-item"><span class="d-md-none">Disk Space: </span><?php echo $vl->plandiskspace;?></li>
                <li class="list-group-item"><span class="d-md-none">Monthly Bandwidth: </span><?php echo $vl->planbandwidth;?></li>
                <li class="list-group-item"><span class="d-md-none">24/7 Email Support: </span><i class="fa fa-check"></i></li>
                <li class="list-group-item"><span class="d-md-none">Phone Support: </span><i class="fa fa-times"></i></li>
              </ul>
              
                  <?php $checkfree1 = $this->session->userdata('password');
                  
//                  ************************
                     $comp1 = $this->session->userdata['companyid'];
                  
                  if($comp1 !=""){?>
              
              
                  <div class="card-body card-footer"> <a href="#" class="btn btn-primary btn-block" onclick="setAmount('<?php echo $vl->planprice;?>','<?php echo $vl->planid;?>');">Buy</a> </div>

                  <?php }
                  
                  
                  
//              *****************************    
                  
                 elseif($checkfree1 !=""){
              ?>
              <div class="card-body card-footer"> <a href="#" class="btn btn-primary btn-block" onclick="setAmount('<?php echo $vl->planprice;?>','<?php echo $vl->planid;?>');">Buy</a> </div>
                 <?php } else { ?>
              
                <div class="card-body card-footer"> <a href="<?php echo base_url();?>Frontuser/signup" class="btn btn-primary btn-block"  >Sign Up</a> </div>

                 <?php } ?>
              
              
            
              
              
              
              <!--<div class="card-body card-footer"> <a href="#" class="btn btn-primary btn-block" onclick="setAmount('<?php echo $vl->planprice;?>','<?php echo $vl->planid;?>');">Sign Up</a> </div>-->
            
            
            
            </div>
          </div>
          
          <?php } ?>
          
                 <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" id="makepayment1" >

                        <input type="hidden" name="business" value="apnigang102@gmail.com">
                        <input type="hidden" name="cmd" value="_xclick">

                        <input type="hidden" name="amount" id="amount" value="500">

                        <input type="hidden" name="currency_code" value="USD">
                       
                         <?php $companyid = $this->session->userdata['companyid'];
                         if($companyid !="") {?>
                        <input type='hidden' name='return' id="subpay" value="<?php echo base_url();?>frontcarrental/Signup/checkupdatestatus/success" />
                        <input type='hidden' name='cancel_return' id="cancelpay" value="<?php echo base_url();?>frontcarrental/Signup/checkupdatestatus/fail" />
                    <!--<button type="button" class="btn greenone" onclick="postadd();">Post Ad</button>-->
                         <?php } 
                          else { ?>
                        <input type='hidden' name='return' id="subpay" value="<?php echo base_url();?>frontcarrental/Signup/checkstatus/success" />
                        <input type='hidden' name='cancel_return' id="cancelpay" value="<?php echo base_url();?>frontcarrental/Signup/checkstatus/fail" />

            <?php      }?>
                    
               <input type="button" value="Submit" class="btnUpload" style="display:none" >

                    </form>
          <!-- Plan 2 -->
<!--          <div class="col-lg-3">
            <div class="card card-outline-primary bg-shadow text-center card-offset-y">
              <div class="card-ribbon card-ribbon-top card-ribbon-left bg-primary text-white">Popular</div>
              <h2 class="card-header pt-4">
                Pro <span class="text-fancy">Plus</span>
              </h2>
              <div class="price-banner bg-primary text-white"> $<span class="price-digits">49.95</span>/MO </div>
              <ul class="list-group list-group-flush list-group-striped">
                <li class="list-group-item"><span class="d-md-none">User Accounts: </span>50</li>
                <li class="list-group-item"><span class="d-md-none">Private Projects: </span>50</li>
                <li class="list-group-item"><span class="d-md-none">Public Projects: </span>Unlimited</li>
                <li class="list-group-item"><span class="d-md-none">Disk Space: </span>50GB</li>
                <li class="list-group-item"><span class="d-md-none">Monthly Bandwidth: </span>10GB</li>
                <li class="list-group-item"><span class="d-md-none">24/7 Email Support: </span><i class="fa fa-check"></i></li>
                <li class="list-group-item"><span class="d-md-none">Phone Support: </span><i class="fa fa-times"></i></li>
              </ul>
              <div class="card-body card-footer"> <a href="#" class="btn btn-primary btn-block">Sign Up</a> </div>
            </div>
          </div>-->
          <!-- Plan 3 -->
<!--          <div class="col-lg-3">
            <div class="card bg-shadow text-center">
              <h3 class="card-header font-weight-normal pt-4">
                Starter <span class="text-fancy">Plus</span>
              </h3>
              <div class="price-banner"> $<span class="price-digits">19.95</span>/MO </div>
              <ul class="list-group list-group-flush list-group-striped">
                <li class="list-group-item"><span class="d-md-none">User Accounts: </span>10</li>
                <li class="list-group-item"><span class="d-md-none">Private Projects: </span>10</li>
                <li class="list-group-item"><span class="d-md-none">Public Projects: </span>Unlimited</li>
                <li class="list-group-item"><span class="d-md-none">Disk Space: </span>10GB</li>
                <li class="list-group-item"><span class="d-md-none">Monthly Bandwidth: </span>2GB</li>
                <li class="list-group-item"><span class="d-md-none">24/7 Email Support: </span><i class="fa fa-check"></i></li>
                <li class="list-group-item"><span class="d-md-none">Phone Support: </span><i class="fa fa-check"></i></li>
              </ul>
              <div class="card-body card-footer"> <a href="#" class="btn btn-primary btn-block">Sign Up</a> </div>
            </div>
          </div>-->
        </div>
       
        <!-- Plan features -->
        
      </div>
    </div>
  </div>

  <!-- ======== @Region: #content-below ======== -->
  <div id="content-below">
    <!-- Awesome features call to action -->
    <div class="bg-primary bg-op-9 text-white py-4">
      <div class="container">
        <div class="row text-center text-lg-left align-items-lg-center">
          <div class="col-12 col-lg-7 text-white">
            <h3 class="font-weight-bold my-0 text-uppercase">
              Awesome Features
            </h3>
            <p class="font-weight-normal op-9 my-0"> <i class="la la-check-circle-o"></i> 99.9% Uptime <i class="la la-check-circle-o ml-lg-3"></i> Free Upgrades <i class="la la-check-circle-o ml-lg-3"></i> Fully Responsive <i class="la la-check-circle-o ml-lg-3"></i>              Bug Free </p>
          </div>
          <div class="col-12 col-lg-5 py-2 text-lg-right">
            <a href="#" class="btn btn-xlg btn-white btn-rounded shadow-lg bg-light bg-op-8 bg-hover-white">Get Started<i class="fa fa-arrow-right ml-2 mt-1"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>

 <?php include 'footer.php' ?>

  
  <script>
      
      
      function setAmount(x,y)
      {
          $("#amount").val(x);
          
          $.ajax({
                  type : "POST",
                  url : "<?php echo site_url('frontcarrental/Signup/addplanid');?>",
                  data : {'planid':y},
                  success : function(response) {
                  		//	alert(response);
							//$("#selval").html(response);
                  		
                  			
                  	 }
              });
          
          
          
          
          $("#makepayment1").submit();
          
          
          
          
      }
      
      
      
      </script>
      
      <script>
          
          function signUpDemo()
          {
              $("#demoForm").submit();
          }
          </script>
          
          
          
          