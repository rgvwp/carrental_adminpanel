<?php include 'header.php' ?>

  <!-- ======== @Region: #content ======== -->
  <div id="content">
    <div class="container">
      <h2 class="title-divider">
        <span>Our <span class="font-weight-normal text-muted">Customers</span></span>
        <small>99% of our customers recommend us!</small>
      </h2>
      
      
      <h3>
        Default Grid (with filtering)
      </h3>
      <!-- Filter links -->
      <ul class="nav nav-tabs" id="customers-filter">
        <li class="nav-item"><a href="#" class="nav-link active" data-isotope-fid="*">All Industries</a></li>
        <li class="nav-item"><a href="#" class="nav-link" data-isotope-fid=".type-web">Web</a></li>
        <li class="nav-item"><a href="#" class="nav-link" data-isotope-fid=".type-design">Design</a></li>
        <li class="nav-item"><a href="#" class="nav-link" data-isotope-fid=".type-media">Media</a></li>
      </ul>
      <!--Customers: Example 1-->
      <div class="row" data-toggle="isotope-grid" data-isotope-options='{ "itemSelector": ".customer", "layoutMode": "masonry"}' data-isotope-filter="#customers-filter li a">
        <div class="col-md-4 customer type-web">
          <div class="card mb-4 bg-gradient bg-shadow" data-clickable="#">
            <img src="assets/img/customers/customer-1.png" alt="Customer 1" class="card-img-top img-fluid pt-1 d-block" />
            <div class="card-body">
              <h4 class="card-title">
                <a href="#">Customer 1</a>
              </h4>
              <p class="card-text">Brevitas genitus ille importunus laoreet singularis usitas. Aliquip capto distineo patria sagaciter saluto vulputate. Abico appellatio consectetuer facilisi luctus nimis quidne torqueo utinam uxor.</p>
            </div>
          </div>
        </div>
        <div class="col-md-4 customer type-media">
          <div class="card mb-4 bg-gradient bg-shadow" data-clickable="#">
            <img src="assets/img/customers/customer-2.png" alt="Customer 2" class="card-img-top img-fluid pt-1 d-block" />
            <div class="card-body">
              <h4 class="card-title">
                <a href="#">Customer 2</a>
              </h4>
              <p class="card-text">Abbas damnum exputo gilvus luctus nisl nunc nutus qui quis. Appellatio camur elit quibus quidem singularis.</p>
            </div>
          </div>
        </div>
        <div class="col-md-4 customer type-design">
          <div class="card mb-4 bg-gradient bg-shadow" data-clickable="#">
            <img src="assets/img/customers/customer-3.png" alt="Customer 3" class="card-img-top img-fluid pt-1 d-block" />
            <div class="card-body">
              <h4 class="card-title">
                <a href="#">Customer 3</a>
              </h4>
              <p class="card-text">Antehabeo eu ex illum lenis nutus scisco tation verto vicis. Esse incassum jus melior ratis saluto sino suscipit veniam vero.</p>
            </div>
          </div>
        </div>
        <div class="col-md-4 customer type-design">
          <div class="card mb-4 bg-gradient bg-shadow" data-clickable="#">
            <img src="assets/img/customers/customer-4.png" alt="Customer 4" class="card-img-top img-fluid pt-1 d-block" />
            <div class="card-body">
              <h4 class="card-title">
                <a href="#">Customer 4</a>
              </h4>
              <p class="card-text">Causa elit incassum letalis obruo valde venio ymo. Accumsan esca exerci gravis huic tum typicus.</p>
            </div>
          </div>
        </div>
        <div class="col-md-4 customer type-design">
          <div class="card mb-4 bg-gradient bg-shadow" data-clickable="#">
            <img src="assets/img/customers/customer-5.png" alt="Customer 5" class="card-img-top img-fluid pt-1 d-block" />
            <div class="card-body">
              <h4 class="card-title">
                <a href="#">Customer 5</a>
              </h4>
              <p class="card-text">Antehabeo autem erat eros immitto natu pala quis uxor. Blandit fere haero ibidem luptatum neque nisl sed torqueo.</p>
            </div>
          </div>
        </div>
        <div class="col-md-4 customer type-web">
          <div class="card mb-4 bg-gradient bg-shadow" data-clickable="#">
            <img src="assets/img/customers/customer-6.png" alt="Customer 6" class="card-img-top img-fluid pt-1 d-block" />
            <div class="card-body">
              <h4 class="card-title">
                <a href="#">Customer 6</a>
              </h4>
              <p class="card-text">Cogo exerci facilisi iaceo ludus neque nulla torqueo ut wisi. Aliquip eligo esca. Damnum diam distineo dolore gemino genitus haero occuro ullamcorper.</p>
            </div>
          </div>
        </div>
        <div class="col-md-4 customer type-media">
          <div class="card mb-4 bg-gradient bg-shadow" data-clickable="#">
            <img src="assets/img/customers/customer-7.png" alt="Customer 7" class="card-img-top img-fluid pt-1 d-block" />
            <div class="card-body">
              <h4 class="card-title">
                <a href="#">Customer 7</a>
              </h4>
              <p class="card-text">Consectetuer feugiat natu obruo praesent quadrum torqueo. Abluo amet imputo natu nutus plaga venio. Facilisi immitto ludus metuo nunc nutus ulciscor verto.</p>
            </div>
          </div>
        </div>
        <div class="col-md-4 customer type-design">
          <div class="card mb-4 bg-gradient bg-shadow" data-clickable="#">
            <img src="assets/img/customers/customer-8.png" alt="Customer 8" class="card-img-top img-fluid pt-1 d-block" />
            <div class="card-body">
              <h4 class="card-title">
                <a href="#">Customer 8</a>
              </h4>
              <p class="card-text">Amet eum imputo lucidus mauris nostrud nulla plaga validus wisi. Accumsan capto lobortis nunc patria ullamcorper.</p>
            </div>
          </div>
        </div>
        <div class="col-md-4 customer type-web">
          <div class="card mb-4 bg-gradient bg-shadow" data-clickable="#">
            <img src="assets/img/customers/customer-9.png" alt="Customer 9" class="card-img-top img-fluid pt-1 d-block" />
            <div class="card-body">
              <h4 class="card-title">
                <a href="#">Customer 9</a>
              </h4>
              <p class="card-text">Adipiscing defui humo pecus tego venio wisi. Gemino nisl obruo. Blandit illum neque. Abdo abluo decet defui facilisis luctus natu nunc.</p>
            </div>
          </div>
        </div>
        <div class="col-md-4 customer type-media">
          <div class="card mb-4 bg-gradient bg-shadow" data-clickable="#">
            <img src="assets/img/customers/customer-10.png" alt="Customer 10" class="card-img-top img-fluid pt-1 d-block" />
            <div class="card-body">
              <h4 class="card-title">
                <a href="#">Customer 10</a>
              </h4>
              <p class="card-text">Fere gemino immitto ludus sed. Cogo esse loquor macto saepius. Abbas aliquam aptent capto si.</p>
            </div>
          </div>
        </div>
        <div class="col-md-4 customer type-web">
          <div class="card mb-4 bg-gradient bg-shadow" data-clickable="#">
            <img src="assets/img/customers/customer-11.png" alt="Customer 11" class="card-img-top img-fluid pt-1 d-block" />
            <div class="card-body">
              <h4 class="card-title">
                <a href="#">Customer 11</a>
              </h4>
              <p class="card-text">Abico blandit distineo nostrud. Minim ratis sagaciter. Dolore exerci venio. Et facilisi iustum nibh nobis paratus patria persto quae utrum.</p>
            </div>
          </div>
        </div>
      </div>
      <hr class="my-5" />
      <h3>
        4 Items per row (no filtering)
      </h3>
      <!--Customers: Example 2-->
      <div class="row" data-toggle="isotope-grid" data-isotope-options='{ "itemSelector": ".customer"}'>
        <div class="col-md-3 customer type-design">
          <div class="card mb-4 bg-gradient bg-shadow" data-clickable="#">
            <img src="assets/img/customers/customer-1.png" alt="Customer 1" class="card-img-top img-fluid pt-1 d-block" />
            <div class="card-body">
              <h4 class="card-title">
                <a href="#">Customer 1</a>
              </h4>
              <p class="card-text">Appellatio dignissim enim tum ut utrum. Abdo damnum duis lucidus populus proprius sagaciter sudo tum voco.</p>
            </div>
          </div>
        </div>
        <div class="col-md-3 customer type-design">
          <div class="card mb-4 bg-gradient bg-shadow" data-clickable="#">
            <img src="assets/img/customers/customer-2.png" alt="Customer 2" class="card-img-top img-fluid pt-1 d-block" />
            <div class="card-body">
              <h4 class="card-title">
                <a href="#">Customer 2</a>
              </h4>
              <p class="card-text">Cogo distineo loquor ludus neque nulla os pala singularis zelus. Aliquam capto ea euismod ideo nutus plaga pneum suscipere torqueo.</p>
            </div>
          </div>
        </div>
        <div class="col-md-3 customer type-media">
          <div class="card mb-4 bg-gradient bg-shadow" data-clickable="#">
            <img src="assets/img/customers/customer-3.png" alt="Customer 3" class="card-img-top img-fluid pt-1 d-block" />
            <div class="card-body">
              <h4 class="card-title">
                <a href="#">Customer 3</a>
              </h4>
              <p class="card-text">At commodo defui laoreet lucidus melior paratus proprius ratis. Brevitas conventio dignissim populus quis refero similis tamen ullamcorper.</p>
            </div>
          </div>
        </div>
        <div class="col-md-3 customer type-web">
          <div class="card mb-4 bg-gradient bg-shadow" data-clickable="#">
            <img src="assets/img/customers/customer-4.png" alt="Customer 4" class="card-img-top img-fluid pt-1 d-block" />
            <div class="card-body">
              <h4 class="card-title">
                <a href="#">Customer 4</a>
              </h4>
              <p class="card-text">Dolore nisl nutus te. Caecus camur cui iriure nulla patria. Appellatio quae tum turpis valetudo.</p>
            </div>
          </div>
        </div>
        <div class="col-md-3 customer type-web">
          <div class="card mb-4 bg-gradient bg-shadow" data-clickable="#">
            <img src="assets/img/customers/customer-5.png" alt="Customer 5" class="card-img-top img-fluid pt-1 d-block" />
            <div class="card-body">
              <h4 class="card-title">
                <a href="#">Customer 5</a>
              </h4>
              <p class="card-text">Comis conventio tation velit venio ymo. Abluo eros iustum letalis pneum proprius secundum tamen. Esca euismod illum nisl nulla pneum praemitto quidne volutpat.</p>
            </div>
          </div>
        </div>
        <div class="col-md-3 customer type-media">
          <div class="card mb-4 bg-gradient bg-shadow" data-clickable="#">
            <img src="assets/img/customers/customer-6.png" alt="Customer 6" class="card-img-top img-fluid pt-1 d-block" />
            <div class="card-body">
              <h4 class="card-title">
                <a href="#">Customer 6</a>
              </h4>
              <p class="card-text">Jugis nunc odio olim suscipere tum. Dolus laoreet quae. Capto haero macto pagus. Aliquip antehabeo feugiat neque nobis quia secundum.</p>
            </div>
          </div>
        </div>
        <div class="col-md-3 customer type-media">
          <div class="card mb-4 bg-gradient bg-shadow" data-clickable="#">
            <img src="assets/img/customers/customer-7.png" alt="Customer 7" class="card-img-top img-fluid pt-1 d-block" />
            <div class="card-body">
              <h4 class="card-title">
                <a href="#">Customer 7</a>
              </h4>
              <p class="card-text">Hos ludus meus minim nostrud paratus tamen vereor. Abico acsi brevitas jumentum melior mos suscipere wisi.</p>
            </div>
          </div>
        </div>
        <div class="col-md-3 customer type-media">
          <div class="card mb-4 bg-gradient bg-shadow" data-clickable="#">
            <img src="assets/img/customers/customer-8.png" alt="Customer 8" class="card-img-top img-fluid pt-1 d-block" />
            <div class="card-body">
              <h4 class="card-title">
                <a href="#">Customer 8</a>
              </h4>
              <p class="card-text">Appellatio dignissim dolor jus turpis. Eu neque tincidunt ymo. Abdo ludus nisl nostrud paratus saepius.</p>
            </div>
          </div>
        </div>
      </div>
      <hr class="my-5" />
      <h3>
        Different width items (no filtering)
      </h3>
      <!--Customers: Example 2-->
      <div class="row" data-toggle="isotope-grid" data-isotope-options='{ "itemSelector": ".customer", "layoutMode": "fitRows"}'>
        <div class="col-md-4 customer type-media">
          <div class="card mb-4 bg-gradient bg-shadow" data-clickable="#">
            <img src="assets/img/customers/customer-1.png" alt="Customer 1" class="card-img-top img-fluid pt-1 d-block" />
            <div class="card-body">
              <h4 class="card-title">
                <a href="#">Customer 1</a>
              </h4>
              <p class="card-text">Euismod exerci feugiat nobis ymo. Acsi ex refero rusticus scisco typicus. Aptent eum hos nostrud persto similis suscipit torqueo utinam.</p>
            </div>
          </div>
        </div>
        <div class="col-md-4 customer type-design">
          <div class="card mb-4 bg-gradient bg-shadow" data-clickable="#">
            <img src="assets/img/customers/customer-2.png" alt="Customer 2" class="card-img-top img-fluid pt-1 d-block" />
            <div class="card-body">
              <h4 class="card-title">
                <a href="#">Customer 2</a>
              </h4>
              <p class="card-text">Imputo jumentum mos natu praemitto proprius quis sudo. Aptent ex neque nutus praesent verto. Diam exerci genitus luptatum olim pecus proprius singularis ullamcorper volutpat.</p>
            </div>
          </div>
        </div>
        <div class="col-md-4 customer type-design">
          <div class="card mb-4 bg-gradient bg-shadow" data-clickable="#">
            <img src="assets/img/customers/customer-3.png" alt="Customer 3" class="card-img-top img-fluid pt-1 d-block" />
            <div class="card-body">
              <h4 class="card-title">
                <a href="#">Customer 3</a>
              </h4>
              <p class="card-text">Duis incassum jugis nostrud pala quidne wisi. Accumsan eligo ex exerci incassum lobortis ludus neque singularis.</p>
            </div>
          </div>
        </div>
        <div class="col-md-3 customer type-web">
          <div class="card mb-4 bg-gradient bg-shadow" data-clickable="#">
            <img src="assets/img/customers/customer-4.png" alt="Customer 4" class="card-img-top img-fluid pt-1 d-block" />
            <div class="card-body">
              <h4 class="card-title">
                <a href="#">Customer 4</a>
              </h4>
              <p class="card-text">Dolus jugis magna melior nimis praesent veniam volutpat. Diam eu humo ille jugis nutus quidem refero verto.</p>
            </div>
          </div>
        </div>
        <div class="col-md-5 customer type-media">
          <div class="card mb-4 bg-gradient bg-shadow" data-clickable="#">
            <img src="assets/img/customers/customer-5.png" alt="Customer 5" class="card-img-top img-fluid pt-1 d-block" />
            <div class="card-body">
              <h4 class="card-title">
                <a href="#">Customer 5</a>
              </h4>
              <p class="card-text">Dolus eum lobortis minim natu praemitto qui si similis suscipere. Immitto interdico os praesent tincidunt zelus.</p>
            </div>
          </div>
        </div>
        <div class="col-md-4 customer type-media">
          <div class="card mb-4 bg-gradient bg-shadow" data-clickable="#">
            <img src="assets/img/customers/customer-6.png" alt="Customer 6" class="card-img-top img-fluid pt-1 d-block" />
            <div class="card-body">
              <h4 class="card-title">
                <a href="#">Customer 6</a>
              </h4>
              <p class="card-text">Adipiscing causa comis cui feugiat gilvus nobis. Diam dolus ibidem ratis refero saepius vereor. Abico fere hos luctus nutus oppeto paulatim tego tincidunt uxor.</p>
            </div>
          </div>
        </div>
        <div class="col-md-8 customer type-design">
          <div class="card mb-4 bg-gradient bg-shadow" data-clickable="#">
            <img src="assets/img/customers/customer-7.png" alt="Customer 7" class="card-img-top img-fluid pt-1 d-block" />
            <div class="card-body">
              <h4 class="card-title">
                <a href="#">Customer 7</a>
              </h4>
              <p class="card-text">Aliquam iaceo nunc obruo te turpis. Acsi genitus mos. Elit hendrerit humo oppeto qui saepius velit.</p>
            </div>
          </div>
        </div>
        <div class="col-md-4 customer type-design">
          <div class="card mb-4 bg-gradient bg-shadow" data-clickable="#">
            <img src="assets/img/customers/customer-8.png" alt="Customer 8" class="card-img-top img-fluid pt-1 d-block" />
            <div class="card-body">
              <h4 class="card-title">
                <a href="#">Customer 8</a>
              </h4>
              <p class="card-text">Huic jus sit uxor. Duis elit facilisis ibidem laoreet os probo torqueo. Erat luptatum patria paulatim pertineo quidne valetudo.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- ======== @Region: #content-below ======== -->
  <div id="content-below">
    <!-- Awesome features call to action -->
    <div class="bg-primary bg-op-9 text-white py-4">
      <div class="container">
        <div class="row text-center text-lg-left align-items-lg-center">
          <div class="col-12 col-lg-7 text-white">
            <h3 class="font-weight-bold my-0 text-uppercase">
              Awesome Features
            </h3>
            <p class="font-weight-normal op-9 my-0"> <i class="la la-check-circle-o"></i> 99.9% Uptime <i class="la la-check-circle-o ml-lg-3"></i> Free Upgrades <i class="la la-check-circle-o ml-lg-3"></i> Fully Responsive <i class="la la-check-circle-o ml-lg-3"></i>              Bug Free </p>
          </div>
          <div class="col-12 col-lg-5 py-2 text-lg-right">
            <a href="#" class="btn btn-xlg btn-white btn-rounded shadow-lg bg-light bg-op-8 bg-hover-white">Get Started<i class="fa fa-arrow-right ml-2 mt-1"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>

 <?php include 'footer.php' ?>