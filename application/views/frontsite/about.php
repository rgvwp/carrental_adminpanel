<?php include 'header.php' ?>

  <!-- ======== @Region: #content ======== -->
  <div id="content">
    <div class="container" id="about">
      <div class="row">
        
        <!--main content-->
        <div class="col-md-12">
          <h2 class="title-divider">
            <span>About <span class="font-weight-normal text-muted">Us</span></span>
            <!--<small>What & who makes us tick!</small>-->
          </h2>
<?php  $query = $this->db->get_where('tblaboutus',array('aboutusstatus'=>1));
foreach($query->result() as $ke=>$ve){?>
          <!-- About company -->
          <h4>
         <?php echo $ve->aboutustitle;?>
          </h4>
          <p><?php echo $ve->aboutusdesc;?></p>
<?php } ?>
              
              
              
              <!--
            Our Philosophy
          </h4>
          <p>Brevitas exerci iustum ludus minim nisl. Aliquam blandit consequat sino typicus velit verto virtus vulputate zelus.</p>
          <p>Facilisi genitus hendrerit inhibeo qui quia quidem utrum. Aliquip bene elit enim haero refoveo sagaciter volutpat. Et humo mauris natu neque paratus quidem sagaciter similis. Capto consequat defui hos meus nobis patria quidem typicus. Dolor
            esse saluto secundum volutpat. Abdo aliquam caecus decet neo os suscipit te velit. Abbas amet consequat exerci melior metuo quis tincidunt. Dolore nostrud suscipere utinam. Brevitas hos turpis. Immitto inhibeo olim praesent utrum valde.</p>
          <p>Aliquam brevitas caecus imputo lucidus modo oppeto os quadrum vulputate. Abigo accumsan at duis haero nobis rusticus valetudo volutpat wisi. Abluo cui jus neo si singularis tincidunt usitas ymo.</p>-->

<!--          <div class="title-divider mt-4" id="stats">
            <h3>
              <span>Vital <span class="font-weight-normal text-muted">Stats</span></span>
              <small>Stats to impress!</small>
            </h3>
          </div>
          <div class="row stats">
            <div class="col-md-3 stat">
              <div class="stat-header">1280</div>
              <small>Happpy Customers</small>
            </div>
            <div class="col-md-3 stat">
              <div class="stat-header">1634</div>
              <small>GB Transfered</small>
            </div>
            <div class="col-md-3 stat">
              <div class="stat-header">2143</div>
              <small>Bugs Fixed</small>
            </div>
            <div class="col-md-3 stat">
              <div class="stat-header">12</div>
              <small>Share Holders</small>
            </div>
          </div>-->

          <!--Customer testimonial-->
          <div class="testimonials my-2" id="testimonials">
            <h3 class="title-divider">
              <span>Highly <span class="font-weight-normal text-muted">Recommended</span></span>
              <!--<small>99% of our customers recommend us!</small>-->
            </h3>
              
              <?php $rquery = $this->db->get_where('tblrecommend',array('recommedstatus'=>1));
              foreach($rquery->result() as $k1=>$v1){ ?>
            <div class="row">
              <div class="col-md-4">
                <blockquote class="blockquote-bubble">
                  <p class="blockquote-bubble-content"><?php echo $v1->recommenddesc;?></p>
                  <footer>
                    <img src="<?php echo base_url();?>uploads/images/<?php echo  $v1->recommmendimage; ?>" alt="Jimi Bloggs" class="img-circle" /> <?php echo $v1->recommendtitle; ?>  <span class="text-primary font-weight-bold">/</span> <a href="<?php echo $v1->recommendheading; ?>"><?php echo $v1->recommendheading; ?></a>
                  </footer>
                </blockquote>
              </div>
              
              <?php } ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- ======== @Region: #content-below ======== -->
  <div id="content-below">
    <!-- Awesome features call to action -->
    <div class="bg-primary bg-op-9 text-white py-4">
      <div class="container">
        <div class="row text-center text-lg-left align-items-lg-center">
          <div class="col-12 col-lg-7 text-white">
            <h3 class="font-weight-bold my-0 text-uppercase">
              Awesome Features
            </h3>
            <p class="font-weight-normal op-9 my-0"> <i class="la la-check-circle-o"></i> 99.9% Uptime <i class="la la-check-circle-o ml-lg-3"></i> Free Upgrades <i class="la la-check-circle-o ml-lg-3"></i> Fully Responsive <i class="la la-check-circle-o ml-lg-3"></i>              Bug Free </p>
          </div>
          <div class="col-12 col-lg-5 py-2 text-lg-right">
            <a href="#" class="btn btn-xlg btn-white btn-rounded shadow-lg bg-light bg-op-8 bg-hover-white">Get Started<i class="fa fa-arrow-right ml-2 mt-1"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>

 <?php include 'footer.php' ?>