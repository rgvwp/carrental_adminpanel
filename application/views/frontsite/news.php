<?php include 'header.php' ?>


  <!-- ======== @Region: #content ======== -->
  <div id="content">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div>
                    <h2 class="title-divider">
            <span>News<span class="font-weight-normal text-muted">& Events</span></span>
            <small>What &amp; who makes us tick!</small>
          </h2>
            <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li class="nav-item">
        
      <a class="nav-link active" data-toggle="tab" href="#all">All News</a>
    </li>
    <?php $tabquery = $this->db->get('tbltab')->result_array(); ?>
    <li class="nav-item">
      <a class="nav-link" data-toggle="tab" href="#trading"><?php echo $tabquery[0]['tabname'];?></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="tab" href="#acquisition"><?php echo $tabquery[1]['tabname'];?></a>
    </li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div id="all" class="container tab-pane active"><br>
        <div class="row">
            
            <?php 
            
                $this->db->select('*');
               $this->db->where('newseventstatus',1);
               $this->db->order_by("newsenentid","desc");
               $this->db->from('tblnewsevent');
               $query=$this->db->get();
             //  return $query->result();

          //  $query = $this->db->get_where("tblnewsevent",array('newseventstatus'=>1));
       
            foreach($query->result() as $k=>$v){
          
              ?>
            
            <div class="col-md-4 col-sm-6 col-xs-12" style="width:100%;">
					<div class="productads_box">
						<div class="news-post">
						<div class="post-img" >
                                                    <a href="#"> <img src="<?php echo base_url();?>uploads/images/<?php echo $v->newseventimage;?>" alt="" class="img-responsive"> </a>
						</div>
						
						<div class="news-info"> 
						<h4><a href="#"><?php echo $v->newseventheading;?></a></h4>
						<ul>
                                                    <li><?php echo $v->newseventtitle;?></li>
                                                    <li> <i class="la la-clock-o"></i> <?php echo $v->newseventdate;?></li>
                        
                                        </ul>
                                                <p><?php echo $v->newseventdesc;?></p>
                                                <a href="#" class="btn btn-primary btn-lg border-bottom">Read More</a>
						 </div>
						
						</div>
					</div>
				</div>
            <?php } ?>


        </div>
    </div>
      
    <div id="trading" class="container tab-pane fade"><br>
      <div class="row">
          
           <?php 
            
               // $this->db->select('*');
               $this->db->where('newseventstatus',1);
                $this->db->where('newseventtab',2);

               $this->db->order_by("newsenentid","desc");
               $this->db->from('tblnewsevent');
               $query2=$this->db->get();
               
              // echo $this->db->last_query();
             //  return $query->result();

          //  $query = $this->db->get_where("tblnewsevent",array('newseventstatus'=>1));
       
            foreach($query2->result() as $k=>$v12){
          
              ?>
            
          
            <div class="col-md-4 col-sm-6 col-xs-12">
					<div class="productads_box">
						<div class="news-post">
						<div class="post-img">
                                                    <a href="#"> <img src="<?php echo base_url();?>uploads/images/<?php echo $v12->newseventimage;?> " alt="" class="img-responsive"> </a>
						</div>
						
						<div class="news-info"> 
						<h4><a href="#"><?php echo $v12->newseventheading;?></a></h4>
						<ul>
                                                    <li><?php echo $v12->newseventtitle;?></li>
                                                    <li> <i class="la la-clock-o"></i> <?php echo $v12->newseventdate;?></li>
                        
                                        </ul>
                                                <p><?php echo $v12->newseventdesc;?></p>
                                                <a href="#" class="btn btn-primary btn-lg border-bottom">Read More</a>
						 </div>
						
						</div>
					</div>
				</div>
            <?php } ?>
<!--            <div class="col-md-4 col-sm-6 col-xs-12">
					<div class="productads_box">
						<div class="news-post">
						<div class="post-img">
                                                    <a href="#"> <img src="assets/img/car-3.png" alt="" class="img-responsive"> </a>
						</div>
						
						<div class="news-info"> 
						<h4><a href="#">Donec luctus imperdiet</a></h4>
						<ul>
                                                    <li>By John Doe,</li>
                                                    <li> <i class="la la-clock-o"></i> 03 Dec 2013</li>
                        
                                        </ul>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Idque Caesaris...</p>
						 <a href="#" class="btn btn-primary btn-lg border-bottom">Read More</a>
                                                </div>
						
						</div>
					</div>
				</div>
            <div class="col-md-4 col-sm-6 col-xs-12">
					<div class="productads_box">
						<div class="news-post">
						<div class="post-img">
                                                    <a href="#"> <img src="assets/img/car-3.png" alt="" class="img-responsive"> </a>
						</div>
						
						<div class="news-info"> 
						<h4><a href="#">Donec luctus imperdiet</a></h4>
						<ul>
                                                    <li>By John Doe,</li>
                                                    <li> <i class="la la-clock-o"></i> 03 Dec 2013</li>
                        
                                        </ul>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Idque Caesaris...</p>
						 <a href="#" class="btn btn-primary btn-lg border-bottom">Read More</a>
                                                </div>
						
						</div>
					</div>
				</div>-->
            
            
        </div>
    </div>
    <div id="acquisition" class="container tab-pane fade"><br>
      <div class="row">
            
            
           <?php 
            
               // $this->db->select('*');
              $this->db->where('newseventstatus',1);
                $this->db->where('newseventtab',1);

               $this->db->order_by("newsenentid","desc");
               $this->db->from('tblnewsevent');
               $query2=$this->db->get();
               
              // echo $this->db->last_query();
             //  return $query->result();

          //  $query = $this->db->get_where("tblnewsevent",array('newseventstatus'=>1));
       
            foreach($query2->result() as $k=>$v123){
          
              ?>
            
         
    
            <div class="col-md-4 col-sm-6 col-xs-12">
					<div class="productads_box">
						<div class="news-post">
						<div class="post-img">
                                                    <a href="#"> <img src="<?php echo base_url();?>uploads/images/<?php echo $v123->newseventimage;?> "" alt="" class="img-responsive"> </a>
						</div>
						
						<div class="news-info"> 
						<h4><a href="#"><?php echo $v123->newseventheading;?></a></h4>
						<ul>
                                                    <li><?php echo $v123->newseventtitle;?></li>
                                                    <li> <i class="la la-clock-o"></i> <?php echo $v123->newseventdate;?></li>
                        
                                        </ul>
                                                <p>  <?php echo $v123->newseventdesc;?> </p>
						 <a href="#" class="btn btn-primary btn-lg border-bottom">Read More</a>
                                                </div>
						
						</div>
					</div>
				</div>
          
            <?php } ?>
          
          
          
        </div>
    </div>
  </div>
                </div>
            </div>
        </div>
      
    </div>
  </div>

  <!-- ======== @Region: #content-below ======== -->
  <div id="content-below">
    <!-- Awesome features call to action -->
    <div class="bg-primary bg-op-9 text-white py-4">
      <div class="container">
        <div class="row text-center text-lg-left align-items-lg-center">
          <div class="col-12 col-lg-7 text-white">
            <h3 class="font-weight-bold my-0 text-uppercase">
              Awesome Features
            </h3>
            <p class="font-weight-normal op-9 my-0"> <i class="la la-check-circle-o"></i> 99.9% Uptime <i class="la la-check-circle-o ml-lg-3"></i> Free Upgrades <i class="la la-check-circle-o ml-lg-3"></i> Fully Responsive <i class="la la-check-circle-o ml-lg-3"></i>              Bug Free </p>
          </div>
          <div class="col-12 col-lg-5 py-2 text-lg-right">
            <a href="#" class="btn btn-xlg btn-white btn-rounded shadow-lg bg-light bg-op-8 bg-hover-white">Get AppStrap<i class="fa fa-arrow-right ml-2 mt-1"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>

 <?php include 'footer.php' ?>