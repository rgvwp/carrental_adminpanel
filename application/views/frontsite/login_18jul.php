<?php include 'header.php' ?>

  <!-- ======== @Region: #content ======== -->
  <div id="content">
    <div class="container">
      <!-- Login form -->
      <form  action="<?php echo base_url();?>frontcarrental/Signup/clientLogin"                  class="form-login form-wrapper form-narrow"  method="post">
     

           <?php if($this->session->flashdata('echo_message'))
	 		{
			?>
					
                        <div class="alert alert-block alert-success " style="background-color:#64aea2 ;height:50px;width:400px; margin-left: -20px">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p > <?php echo $this->session->flashdata('echo_message'); ?></p>
                        </div>						
									
			<?php            } ?>
          
          
          
          
          
          


  <?php if($this->session->flashdata('permission_message'))
	 		{
			?>
					
                        <div class="alert alert-block alert-success " style="background-color:#64aea2 ;height:50px;width:400px; margin-left: -20px">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p > <?php echo $this->session->flashdata('permission_message'); ?></p>
                        </div>						
									
			<?php  $this->session->sess_destroy();          } ?>
            <?php if($this->session->flashdata('flash_message'))
	 		{
			?>
					
                        <div class="alert alert-block alert-success" style="background-color:#64aea2 ;height:50px;width:400px; margin-left: -20px">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p ><?php echo $this->session->flashdata('flash_message'); ?></p>
                        </div>						
									
			<?php $this->session->sess_destroy();              } ?>
          <h3 class="title-divider">
          <span>Login</span>
          <small class="mt-4">Not signed up? <a href="<?php echo base_url();?>Frontuser/signup">Sign up here</a>.</small>
        </h3>
        <div class="form-group">
          <label class="sr-only" for="login-email-page">Email</label>
          <input type="email" id="login-email-page email"  name="email" onblur="abc();" class="form-control email" placeholder="Email" required>
            <span id="email" style="color:red"></span>
        </div>
        <div class="form-group">
          <label class="sr-only" for="login-password-page">Password</label>
          <input type="password" id="login-password-page" name="password" class="form-control password" placeholder="Password">
           <span id="password" style="color:red"></span>
        </div>
        <button type="button" class="btn btn-primary" onclick=" return MYtest();">Login</button> 
        <!--<small><a href="#">Forgotten Password?</a></small>-->
      </form>
    </div>
  </div>

  <!-- ======== @Region: #content-below ======== -->
  <div id="content-below">
    <!-- Awesome features call to action -->
    <div class="bg-primary bg-op-9 text-white py-4">
      <div class="container">
        <div class="row text-center text-lg-left align-items-lg-center">
          <div class="col-12 col-lg-7 text-white">
            <h3 class="font-weight-bold my-0 text-uppercase">
              Awesome Features
            </h3>
            <p class="font-weight-normal op-9 my-0"> <i class="la la-check-circle-o"></i> 99.9% Uptime <i class="la la-check-circle-o ml-lg-3"></i> Free Upgrades <i class="la la-check-circle-o ml-lg-3"></i> Fully Responsive <i class="la la-check-circle-o ml-lg-3"></i>              Bug Free </p>
          </div>
          <div class="col-12 col-lg-5 py-2 text-lg-right">
            <a href="#" class="btn btn-xlg btn-white btn-rounded shadow-lg bg-light bg-op-8 bg-hover-white">Get Started<i class="fa fa-arrow-right ml-2 mt-1"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php include 'footer.php' ?>

  
  <script>
      
      function MYtest()
      {
           var email = $(".email").val();
          var password = $(".password").val();
         
        
        
          if(email == "")
          {
              $("#email").html("Email is require");
              return false;
          }
        
           if(password == "")
          {
              $("#password").html("Password is require");
              return false;
          }
          
          $(".form-narrow").submit();
          
          
    }
    
    
    
    function abc()
    {
       
          $("#email").html(" ");
        
          $("#password").html(" ");
    }
          </script>