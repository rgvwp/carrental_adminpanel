<?php $query = $this->db->get('tblsupadmprofile')->row_array();


?>
 
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/datatables/media/css/dataTables.bootstrap.css">


<style>
    .panel-heading
    {
        display:inline-block;
    }
</style>



<section>
         <!-- Page content-->
         <div class="content-wrapper">
            
            <!-- START row-->
            
            <!-- END row-->
            <!-- START row-->
            <?php if($this->session->flashdata('permission_message'))
	 		{
			?>
					
                        <div class="alert alert-block alert-success fade in" style="background-color:#3ec0e8">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p ><h4 style="background-color:#3ec0e8"> Successful!</h4> <?php echo $this->session->flashdata('permission_message'); ?></p>
                        </div>						
									
			<?php } ?>
            <?php if($this->session->flashdata('flash_message'))
	 		{
			?>
					
                        <div class="alert alert-block alert-success fade in" style="background-color:#ff708a">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p ><h4 style="background-color:#ff708a"> Error!</h4> <?php echo $this->session->flashdata('flash_message'); ?></p>
                        </div>						
									
			<?php } ?>
            <div class="row">
               <div class="col-md-12">
                     <!-- START panel-->
                     <div class="panel panel-default">
                        <div class="panel-heading">
                             <h4>Rental Company
                             </h4>
                        </div>
                         
                        <ul class="nav nav-tabs">
                            
                      <?php if($id == "pending"){
                          $s = "active";
                      } 

                             if($id == "approved"){
                                 
                               
                          $t = "active";
                      } 
                       if($id == "dissapproved"){
                          $d = "active";
                      }
                      if($id!="approved" && $id!="dissapproved") {
     $s = "active";
 }
                      
                      
                      
                      ?> 
                            
                            
     
    <li class="<?php echo $s;?>"><a data-toggle="tab" href="#pending">Pending Company</a></li>
                       
    <li class="<?php echo $t;?> "><a data-toggle="tab" href="#profile">Approved Company</a></li>
   
    <li class="<?php echo $d;?>"><a data-toggle="tab" href="#changepass">Disapproved Company</a></li>

  </ul>
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         

  <div class="tab-content">
     <div id="pending" class="tab-pane fade in <?php echo $s;?>">
      <div class="panel-body">
          <div class="container-fluid">
              <div class="row">
                  <div class="col-lg-12">
                     <div class="panel panel-default">
                    <div class="panel-body">
                           <div class="table-responsive">
                              <table class="table table-striped table-hover" id="datatable1">
                                    <div class="panel-heading">
                                        <b> Export</b>&nbsp&nbsp <a href="<?php echo base_url();?>superadmin/SuperAdmin/printCsv/pending" ><b><button type="button" class="btn btn-info">CSV</button>
  </b></a>&nbsp&nbsp
              <a href="<?php echo base_url();?>superadmin/SuperAdmin/printPdf/pending" ><b><button type="button" class="btn btn-info">PDF</button></b></a>
           </div>
                                 <thead>
                                    <tr>
                                        <th>S.No</th>
                                       <th>Company Name</th>
                                       <th>Company Code</th>
                                       <th>Company Address</th>
                                       <th >Registration Date</th>
                                       <!--<th>Status</th>-->
                                         <th>Approval Status</th>
                                       <th>Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                     <?php 
                                     $this->db->order_by("clientcompid", "desc");
                                      $this->db->where('clientcompapproval ', "pending");
                                      
                                     $query = $this->db->get('tblclientcompany');
                                     $i=1;
                                     foreach($query->result() as $k=>$vl)
                                     {?>
                                    <tr class="gradeX">
                                        <td><?php echo $i;?></td>
                                       <td><?php echo $vl->clientcompname;?></td>
                                       <td><?php echo $vl->clientcompcode;?></td>
                                       <td><?php echo $vl->clientcompaddress;?></td>
                                       <td><?php echo $vl->clientcompregistdate;?></td>
                                          <?php $st =  $vl->clientcompstatus;
                                        if($st == 1)
                                        {
                                            $stat = "Active";
                                        }
                                        else{
                                             $stat = "Inactive";
                                        }
                                       
                                       ?>
                                       <!--<td><?php echo $stat;?></td>-->
                                      
                                       <?php $st =  $vl->clientcompapproval;
                                       
                                        if($st == 1){
                                             $status = "Approved";
                                        }
                                        if($st == 0){
                                             $status = "Not Approved";
                                        }
                                         if($st == "pending"){
                                             $status = "Pending";
                                       
                                             }
                                       
                                       ?>
                                       <?php if($st == 1 || $st == "pending"){?>
                                       <!--<td><a href="<?php echo base_url();?>superadmin/SuperAdmin/statusCompany/<?php echo $vl->clientcompid;?>"   ><button type="button" class="btn btn-success" onclick="return confirm('Are you sure want to approve?');"  ><?php echo $status;?></button></a></td>-->
                                       <td><button type="button" class="btn btn-success" onclick="return confirm1('<?php echo base_url();?>superadmin/SuperAdmin/statusCompany/<?php echo $vl->clientcompid;?>','<?php echo base_url();?>superadmin/SuperAdmin/disapproveCompany/<?php echo $vl->clientcompid;?>');"  ><?php echo $status;?></button></td>

                                       <?php } ?>
                                       
                                       
                             <td><a href="<?php echo base_url();?>superadmin/SuperAdmin/rentalcompany/<?php echo $vl->clientcompid;?>/pending" ><i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i></a>
                     <a href="<?php echo base_url();?>superadmin/SuperAdmin/saveCompany/delete/<?php echo $vl->clientcompid?>/pending" onclick="return confirm('Are you sure want to delete?');"><i class="fa fa-trash-o fa-2x" aria-hidden="true" style="color:#f31a04;"></i></a></td>
                                 
                                    </tr>

                                     <?php $i++; } 
                                      
                                     ?>
                                   </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
          
          
          
          
          
                        </div>
    </div>
      
      
      
 <!--*********************************************-->     
      
        
      
    <div id="profile" class="tab-pane fade in <?php echo $t;?> ">
      <div class="panel-body">
          <div class="container-fluid">
              <div class="row">
                  <div class="col-lg-12">
                     <div class="panel panel-default">
                          <div class="panel-body">
                           <div class="table-responsive">
                              <table class="table table-striped table-hover" id="datatable2">
                                    <div class="panel-heading">
                                      <strong> Export</strong>&nbsp&nbsp <a href="<?php echo base_url();?>superadmin/SuperAdmin/printCsv/approved" ><b><button type="button" class="btn btn-info">CSV</button></b></a>&nbsp&nbsp&nbsp
              <a href="<?php echo base_url();?>superadmin/SuperAdmin/printPdf/approved" ><b<button type="button" class="btn btn-info">PDF</button></b></a>
          </div>
                                 <thead>
                                    <tr>
                                        <th>S.No</th>
                                       <th>Company Name</th>
                                       <th>Company Code</th>
                                       <th>Company Address</th>
                                       <th >Registration Date</th>
                                       <!--<th>Status</th>-->
                                         <th>Approval Status</th>
                                       <th>Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                     <?php 
                                     $this->db->order_by("clientcompid", "desc");
                                     $this->db->where('clientcompapproval',1);
//                                      $this->db->or_where('clientcompapproval ', "pending");
                                      
                                     $query = $this->db->get('tblclientcompany');
                                     $i=1;
                                     foreach($query->result() as $k=>$vl)
                                     {?>
                                    <tr class="gradeX">
                                        <td><?php echo $i;?></td>
                                       <td><?php echo $vl->clientcompname;?></td>
                                       <td><?php echo $vl->clientcompcode;?></td>
                                       <td><?php echo $vl->clientcompaddress;?></td>
                                       <td><?php echo $vl->clientcompregistdate;?></td>
                                          <?php $st =  $vl->clientcompstatus;
                                        if($st == 1)
                                        {
                                            $stat = "Active";
                                        }
                                        else{
                                             $stat = "Inactive";
                                        }
                                       
                                       ?>
                                       <!--<td><?php echo $stat;?></td>-->
                                        <?php $st =  $vl->clientcompapproval;
                                       
                                        if($st == 1){
                                             $status = "Approved";
                                        }
                                        if($st == 0){
                                             $status = "Not Approved";
                                        }
                                         if($st == "pending"){
                                             $status = "Pending";
                                       
                                             }
                                       
                                       ?>
                                       <?php if($st == 1 || $st == "pending"){?>
         <td><button type="button" class="btn btn-success" onclick="return confirm2('<?php echo base_url();?>superadmin/SuperAdmin/disapproveCompany/<?php echo $vl->clientcompid;?>');"  ><?php echo $status;?></button></td>

                                       <?php } ?>
                              <td ><a href="<?php echo base_url();?>superadmin/SuperAdmin/rentalcompany/<?php echo $vl->clientcompid;?>/approved" ><i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i></a>
                     <a href="<?php echo base_url();?>superadmin/SuperAdmin/saveCompany/delete/<?php echo $vl->clientcompid?>/approved" onclick="return confirm('Are you sure want to delete?');"><i class="fa fa-trash-o fa-2x" aria-hidden="true" style="color:#f31a04;"></i></a></td>
                             </tr>

                                     <?php $i++; } 
                                        
                                     ?>
                                  
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
           </div>
         </div>
    </div>
      
   <!--*****************************************************-->   
      
    <div id="changepass" class="tab-pane fade in <?php echo $d;?>">
         <div class="container-fluid">
               <div class="row">
                  <div class="col-lg-12">
                     <div class="panel panel-default">
                         <div class="panel-body">
                           <div class="table-responsive">
                              <table class="table table-striped table-hover" id="datatable3">
                                   <div class="panel-heading">
                                       <strong> Export</strong>&nbsp&nbsp <a href="<?php echo base_url();?>superadmin/SuperAdmin/printCsv/disapproved" ><b><button type="button" class="btn btn-info">CSV</button></b></a>&nbsp&nbsp&nbsp
              <a href="<?php echo base_url();?>superadmin/SuperAdmin/printPdf/disapproved" ><b><button type="button" class="btn btn-info">PDF</button></b></a>
  </div>
                                 <thead>
                                    <tr>
                                        <th>S.No</th>
                                       <th>Company Name</th>
                                       <th>Company Code</th>
                                       <th>Company Address</th>
                                       <th >Registration Date</th>
                                        <!--<th>Status</th>-->
                                         <th>Approval Status</th>
                                        <th>Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                     <?php 
                                   $this->db->order_by("clientcompid", "desc");
                                     $this->db->where('clientcompapproval',0);
                                    $this->db->where('clientcompapproval !=', "pending");
                                      
                                     $query3 = $this->db->get('tblclientcompany');
                                    
                                     $i=1;
                                     foreach($query3->result() as $k3=>$vl3)
                                     {?>
                                    <tr class="gradeX">
                                        <td><?php echo $i;?></td>
                                       <td><?php echo $vl3->clientcompname;?></td>
                                       <td><?php echo $vl3->clientcompcode;?></td>
                                       <td><?php echo $vl3->clientcompaddress;?></td>
                                       <td><?php echo $vl3->clientcompregistdate;?></td>
                                          <?php $st =  $vl3->clientcompstatus;
                                        if($st == 1)
                                        {
                                            $stat = "Active";
                                        }
                                        else{
                                             $stat = "Inactive";
                                        }
                                       
                                       ?>
                                       <!--<td><?php echo $stat;?></td>-->
                                      
                                       <?php $st =  $vl3->clientcompapproval;
                                       
                                        if($st == 1){
                                             $status = "Approved";
                                        }
                                        if($st == 0){
                                             $status = "Not Approved";
                                        }
                                         if($st == "pending"){
                                             $status = "Pending";
                                       
                                             }
                                       
                                       ?>
                                     
         <td><button type="button" class="btn btn-success" onclick="return confirm3('<?php echo base_url();?>superadmin/SuperAdmin/statusCompany/<?php echo $vl3->clientcompid;?>');"  ><?php echo $status;?></button></td>
            
                             <td ><a href="<?php echo base_url();?>superadmin/SuperAdmin/rentalcompany/<?php echo $vl3->clientcompid;?>/dissapproved" ><i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i></a>
                     <a href="<?php echo base_url();?>superadmin/SuperAdmin/saveCompany/delete/<?php echo $vl3->clientcompid?>/dissapproved" onclick="return confirm('Are you sure want to delete?');" ><i class="fa fa-trash-o fa-2x" aria-hidden="true" style="color:#f31a04;"></i></a></td>
                       
                  </tr>

                                     <?php $i++; } 
                                       ?>
                                  
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
           </div>
    </div>
      
      
   <!--******************************************************-->   
   
   
   
   
<!--   <div id="active" class="tab-pane fade in active">
         <div class="container-fluid">
               <div class="row">
                  <div class="col-lg-12">
                     <div class="panel panel-default">
                         <div class="panel-body">
                           <div class="table-responsive">
                              <table class="table table-striped" id="datatable7">
                                   <div class="panel-heading">
                                       <strong> Export</strong>&nbsp&nbsp <a href="<?php echo base_url();?>superadmin/SuperAdmin/printCsv/disapproved" ><b><button type="button" class="btn btn-info">CSV</button></b></a>&nbsp&nbsp&nbsp
              <a href="<?php echo base_url();?>superadmin/SuperAdmin/printPdf/disapproved" ><b><button type="button" class="btn btn-info">PDF</button></b></a>
  </div>
                                 <thead>
                                    <tr>
                                        <th>S.No</th>
                                       <th>Company Name</th>
                                       <th>Company Code</th>
                                       <th>Company Address</th>
                                       <th >Registration Date</th>
                                        <th>Status</th>
                                         <th>Approval Status</th>
                                        <th>Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                     <?php 
                                   $this->db->order_by("clientcompid", "desc");
                                     $this->db->where('clientcompapproval',0);
                                    $this->db->where('clientcompapproval !=', "pending");
                                      
                                     $query3 = $this->db->get('tblclientcompany');
                                    
                                     $i=1;
                                     foreach($query3->result() as $k3=>$vl3)
                                     {?>
                                    <tr class="gradeX">
                                        <td><?php echo $i;?></td>
                                       <td><?php echo $vl3->clientcompname;?></td>
                                       <td><?php echo $vl3->clientcompcode;?></td>
                                       <td><?php echo $vl3->clientcompaddress;?></td>
                                       <td><?php echo $vl3->clientcompregistdate;?></td>
                                          <?php $st =  $vl3->clientcompstatus;
                                        if($st == 1)
                                        {
                                            $stat = "Active";
                                        }
                                        else{
                                             $stat = "Inactive";
                                        }
                                       
                                       ?>
                                       <td><?php echo $stat;?></td>
                                      
                                       <?php $st =  $vl3->clientcompapproval;
                                       
                                        if($st == 1){
                                             $status = "Approved";
                                        }
                                        if($st == 0){
                                             $status = "Not Approved";
                                        }
                                         if($st == "pending"){
                                             $status = "Pending";
                                       
                                             }
                                       
                                       ?>
                                     
         <td><button type="button" class="btn btn-success" onclick="return confirm3('<?php echo base_url();?>superadmin/SuperAdmin/statusCompany/<?php echo $vl3->clientcompid;?>');"  ><?php echo $status;?></button></td>
            
                             <td ><a href="<?php echo base_url();?>superadmin/SuperAdmin/rentalcompany/<?php echo $vl3->clientcompid;?>/dissapproved" ><i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i></a>
                     <a href="<?php echo base_url();?>superadmin/SuperAdmin/saveCompany/delete/<?php echo $vl3->clientcompid?>/dissapproved" onclick="return confirm('Are you sure want to delete?');" ><i class="fa fa-trash-o fa-2x" aria-hidden="true" style="color:#f31a04;"></i></a></td>
                       
                  </tr>

                                     <?php $i++; } 
                                       ?>
                                  
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
           </div>
    </div>-->
   
   
   
   
   
   
   
   
   
   
   <!--***********************************************-->
      
</div> 
             
                        
                        
                     </div>
                  
                  </form>
               </div>
            </div>
            <!-- END row-->
         </div>
         
         <div class="container">
  <h2></h2>
   <!--Trigger the modal with a button--> 
  <button type="button" class="btn btn-info btn-lg abcde" data-toggle="modal" data-target="#myModal" style="display:none;">Open Modal</button>

   <!--Modal--> 
 <div id="myModal" class="modal fade">
 <div class="modal-dialog modal-confirm">
  <div class="modal-content">
   <div class="modal-header">
    <div class="icon-box">
     <i class="material-icons"></i>
    </div>    
    <h4 class="modal-title">Are you sure?</h4> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
   </div>
   
   <div class="modal-footer">
    <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>  
  <a id="approve_link" class="btn btn-success ">Approve</a> 
  <a id="delete_link" class="btn btn-danger btn-flat ">Disapprove</a> 

   </div>
  </div>
 </div>
</div>
  
  
  
 
  
</div>
             
         
         
         
         
         
         
         
         <div class="container">
  <h2></h2>
  <!-- Trigger the modal with a button -->
  <button type="button" class="btn btn-info btn-lg abcdef" data-toggle="modal" data-target="#myModal2" style="display:none;">Open Modal</button>

  <!-- Modal -->
 <div id="myModal2" class="modal fade">
 <div class="modal-dialog modal-confirm">
  <div class="modal-content">
   <div class="modal-header">
    <div class="icon-box">
     <!--<i class="material-icons">&#xE5CD;</i>-->
    </div>    
    <h4 class="modal-title">Are you sure?</h4> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
   </div>
   
   <div class="modal-footer">
    <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>  
  <!--<a id="approve_link" class="btn btn-success ">Approve</a>--> 
  <a id="delete_link23" class="btn btn-danger btn-flat ">Disapprove</a> 

   </div>
  </div>
 </div>
</div>
  
  
  
 
  
</div>
         
         
         
         
         
         
         
         <div class="container">
  <h2></h2>
  <!-- Trigger the modal with a button -->
  <button type="button" class="btn btn-info btn-lg abcdefg" data-toggle="modal" data-target="#myModal3" style="display:none;">Open Modal</button>

  <!-- Modal -->
 <div id="myModal3" class="modal fade">
 <div class="modal-dialog modal-confirm">
  <div class="modal-content">
   <div class="modal-header">
    <div class="icon-box">
     <!--<i class="material-icons">&#xE5CD;</i>-->
    </div>    
    <h4 class="modal-title">Are you sure?</h4> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
   </div>
   
   <div class="modal-footer">
    <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>  
  <a id="approve_link23" class="btn btn-success ">Approve</a> 
  <!--<a id="delete_link23" class="btn btn-danger btn-flat ">Disapprove</a>--> 

   </div>
  </div>
 </div>
</div>
  
  
  
 
  
</div>
      </section>











<script>
    
    
    
    
    
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah')
                    .attr('src', e.target.result)
                    .width(80)
                    .height(80);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    
    </script>
    
     <script>
                    
                      function isNumber(evt) {
            var iKeyCode = (evt.which) ? evt.which : evt.keyCode
            if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
                return false;

            return true;
        
                    }
                    
                    
                </script>
                
                <script>
                    function passSubmit()
                    {
                    alert();
         $('#passForm').submit();
                  
                        
                    } 
                    </script>
                    
                    
                     
    
   <!-- =============== VENDOR SCRIPTS ===============-->
  
   <script src="<?php echo base_url();?>assets/vendor/matchMedia/matchMedia.js"></script>
    JQUERY
   <script src="<?php echo base_url();?>assets/vendor/jquery/dist/jquery.js"></script>
  
  
   <script src="<?php echo base_url();?>assets/vendor/datatables/media/js/jquery.dataTables.min.js"></script>
   <script src="<?php echo base_url();?>assets/vendor/datatables-colvis/js/dataTables.colVis.js"></script>
   <script src="<?php echo base_url();?>assets/vendor/datatables/media/js/dataTables.bootstrap.js"></script>
   <!--<script src="<?php echo base_url();?>assets/vendor/datatables-buttons/js/dataTables.buttons.js"></script>-->
   <!--<script src="<?php echo base_url();?>assets/vendor/datatables-buttons/js/buttons.bootstrap.js"></script>-->
   <!--<script src="<?php echo base_url();?>assets/vendor/datatables-buttons/js/buttons.colVis.js"></script>-->
   <!--<script src="<?php echo base_url();?>assets/vendor/datatables-buttons/js/buttons.flash.js"></script>-->
   <script src="<?php echo base_url();?>assets/vendor/datatables-buttons/js/buttons.html5.js"></script>
   <script src="<?php echo base_url();?>assets/vendor/datatables-buttons/js/buttons.print.js"></script>
   <script src="<?php echo base_url();?>assets/vendor/datatables-responsive/js/dataTables.responsive.js"></script>
   <script src="<?php echo base_url();?>assets/vendor/datatables-responsive/js/responsive.bootstrap.js"></script>
   <script src="<?php echo base_url();?>assets/js/demo/demo-datatable.js"></script>
   
   
   
   
   
   
   
   
   
   
   
   
  
   <script>
   function confirm1(x,y)
    {
 
 //alert(x);
 //alert(y);
       // var delete_url = x;
       // $('#myModal1').modal('show');
       document.getElementById('approve_link').setAttribute('href', x);

        document.getElementById('delete_link').setAttribute('href', y);
        
        $(".abcde").click();
    }
    
    
    function confirm2(x)
    {
 
 //alert(x);
 //alert(y);
       // var delete_url = x;
     //   $('#myModal2').modal('show');
      // document.getElementById('approve_link').setAttribute('href', x);

        document.getElementById('delete_link23').setAttribute('href', x);
        
      $(".abcdef").click();
    }
    
    
     function confirm3(z)
    {
 
// alert(z);
 //alert(y);
       // var delete_url = x;
     //   $('#myModal2').modal('show');
      // document.getElementById('approve_link').setAttribute('href', x);

        document.getElementById('approve_link23').setAttribute('href', z);
        
      $(".abcdefg").click();
    }
    
    
    
    
    
    
</script>