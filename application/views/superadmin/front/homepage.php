
<?php



if($id)
{
      $query = $this->db->get_where('tblcmshome', array('cmshomeid' => $id))->row_array();
  
  $formaction = "saveBanner";
  $method="edit";
  $button_name = "Update";
  $title = "Edit User";
}else
{
  
   $formaction = "saveBanner";
   $button_name = "Save";
   $method="create";
    $title = "Create User";
}

?>

      <section>
         <!-- Page content-->
         <div class="content-wrapper">
            <h3>Manage Banner Images
               <!--<small>Validating forms frontend have never been so powerful and easy.</small>-->
            </h3>
            <!-- START row-->
            
            <!-- END row-->
            <!-- START row-->
            <div class="row">
               <div class="col-md-12">
                   <?php if($this->session->flashdata('permission_message'))
	 		{
                       
			?>
					
                        <div class="alert alert-block alert-success fade in" style="background-color:#3ec0e8">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p ><h4 style="background-color:#3ec0e8"> Successful!</h4> <?php echo $this->session->flashdata('permission_message'); ?></p>
                        </div>						
									
			<?php } ?>
            <?php if($this->session->flashdata('flash_message'))
	 		{
			?>
					
                        <div class="alert alert-block alert-success fade in" style="background-color:#ff708a">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p ><h4 style="background-color:#ff708a"> Error!</h4> <?php echo $this->session->flashdata('flash_message'); ?></p>
                        </div>						
									
			<?php } ?>
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <div class="panel-title">Banner Images</div>
                        </div>
                        <div class="panel-body">
<!--                           <h4>Type validation</h4>-->
                                      <form class="form-horizontal" action="<?php echo base_url();?>superadmin/FrontManager/<?php echo $formaction;?>/<?php echo $method;?>/<?php echo $id;?>" method="post" enctype='multipart/form-data'>

        <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label" style="padding-top: 92px;">Banner Image</label>
                                 <div class="col-sm-6">
                                     <?php $pic = $query['cmshomeimage'];
                                     if($pic !="") {?>
                                     <img  id="blah" name="image" class=" img-circle " src="<?php echo base_url();?>uploads/images/<?php echo $query['cmshomeimage'];?>" alt="Image" style="width: 85px; margin-left: 82px;" >

                                     <?php     }else {?>
                                 <img  id="blah" name="image" class=" img-circle " src="<?php echo base_url();?>/assets/img/user/02.jpg" alt="Image" style="width: 85px; margin-left: 82px;" >
                                <?php } ?>
                                   <input type='file' id="im123" name="image" onchange="readURL(this);" style=" margin-left: 82px;"  />

                                 </div>
                                 
                              </div>
                           </fieldset>
<fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Title</label>
                                 <div class="col-sm-6">
                                     <input class="form-control" type="text" value="<?php echo $query['cmshometitle'];?>" name="title" data-validation="length" data-validation-length="min1" data-validation-error-msg="User Name is requirred"  >
                                 </div>
                                 <!--<input type="text" name="parameter" value="<?php echo $parameter;?>">-->
                               
                              </div>
                           </fieldset>
                                          <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Description</label>
                                 <div class="col-sm-6">
                                     <input class="form-control" type="text" value="<?php echo $query['cmshomedescription'];?>" name="description" data-validation="length" data-validation-length="min1" data-validation-error-msg="User Email is requirred"  >
                                 </div>
                                 <!--<input type="text" name="parameter" value="<?php echo $parameter;?>">-->
                               
                              </div>
                           </fieldset>
                                          
                                                         
                                          
                                          
                                          
                                        
                        
                          
                                          

                                 
                                                   
                                          
                           
<!--                           <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Status</label>
                                 <div class="col-sm-6">
                                     <?php $st = $query['cmshomestatus'] ; ?>
                                    <select name="status"  class="form-control" data-validation="length" data-validation-length="min1" data-validation-error-msg="Status is requirred" >
                                    <option value=" ">Select Status</option>
                                    <option value="1"  <?php if ($st == 1 ) echo 'selected' ; ?>>Active</option>
                                    <option value="0"  <?php if ($st == 0 ) echo 'selected' ; ?>>Inactive</option>
                                  </select>
                                 </div>
                                 
                              </div>
                           </fieldset>
                           -->
                           
                           
                           
                            <div class="panel-footer text-center">
                           <button class="btn btn-info" type="submit" style="margin-left: -193px;"><?php echo $button_name;?></button>
                        </div>
                          
                                      </form>
                         
                        </div>
                     
                     </div>
                     <!-- END panel-->
                  </form>
               </div>
            </div>
            <!-- END row-->
         </div>
      </section>
    <script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js" type="text/javascript"></script>
       
     <!--<script src="<?php echo base_url();?>assets/datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>-->
 

    
    
    
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"> </script>
                


<script>
    
    
    
    
    
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah')
                    .attr('src', e.target.result)
                    .width(80)
                    .height(80);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    
    </script>
    
     <script>
                    
                      function isNumber1(evt) {
            var iKeyCode = (evt.which) ? evt.which : evt.keyCode
            if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
                return false;

            return true;
        
                    }
                    
                    
                </script>
                
                <script>
                    function passSubmit()
                    {
                    alert();
         $('#passForm').submit();
                  
                        
                    }
                    </script>