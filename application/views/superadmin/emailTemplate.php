<?php $query = $this->db->get_where('tbltemplate', array('templateid' => $param))->row_array();


?>


      <section>
         <!-- Page content-->
         <div class="content-wrapper">
            <h3> Template
               <!--<small>Validating forms frontend have never been so powerful and easy.</small>-->
            </h3>
            <!-- START row-->
            
            <!-- END row-->
            <!-- START row-->
            <div class="row">
               <div class="col-md-12">
                   <?php if($this->session->flashdata('permission_message'))
	 		{
                       
			?>
					
                        <div class="alert alert-block alert-success fade in" style="background-color:#3ec0e8">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p ><h4 style="background-color:#3ec0e8"> Successful!</h4> <?php echo $this->session->flashdata('permission_message'); ?></p>
                        </div>						
									
			<?php } ?>
            <?php if($this->session->flashdata('flash_message'))
	 		{
			?>
					
                        <div class="alert alert-block alert-success fade in" style="background-color:#ff708a">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p ><h4 style="background-color:#ff708a"> Error!</h4> <?php echo $this->session->flashdata('flash_message'); ?></p>
                        </div>						
									
			<?php } ?>
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <div class="panel-title">Update  Template</div>
                        </div>
                        <div class="panel-body">
<!--                           <h4>Type validation</h4>-->
                                      <form class="form-horizontal" action="<?php echo base_url();?>superadmin/SuperAdmin/templateUpdate" method="post" enctype='multipart/form-data'>


<fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Template Name</label>
                                 <div class="col-sm-6">
                                     <input class="form-control" type="text" value="<?php echo $query['templatetitle'];?>" name="templatename"  value="<?php echo $query['supadmprofilecontaccnum'];?>">
                                 </div>
                                 <input type="hidden" name="upId"  value="<?php echo $param;?>" >
                              </div>
                           </fieldset>
                           <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label"> Template</label>
                                 <div class="col-sm-8">
                                                <textarea class="ckeditor"    name="discription"><?php echo $query['templatedescription'];?> </textarea>
                                 </div>
                                
                                 </div>
                              </div>
                           </fieldset>
                           <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Status</label>
                                 <div class="col-sm-6">
                                     <?php $st = $query['templatestatus'] ; ?>
                                    <select name="status"  class="form-control">
                                    <option value=" ">Select Status</option>
                                    <option value="1"  <?php if ($st == 1 ) echo 'selected' ; ?>>Active</option>
                                    <option value="0"  <?php if ($st == 0 ) echo 'selected' ; ?>>Inactive</option>
                                  </select>
                                 </div>
                                 
                              </div>
                           </fieldset>
                           
                           
                           
                           
                            <div class="panel-footer text-center">
                           <button class="btn btn-info" type="submit">Update</button>
                        </div
                          
                                      </form>
                         
                        </div>
                     
                     </div>
                     <!-- END panel-->
                  </form>
               </div>
            </div>
            <!-- END row-->
         </div>
      </section>
    <script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js" type="text/javascript"></script>
    
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"> </script>
