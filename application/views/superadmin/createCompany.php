
<?php
if($id)
{
    
   $query = $this->db->get_where('tblclientcompany', array('clientcompid' => $id))->row_array();
  
  $formaction = "saveCompany";
  $method="edit";
  $button_name = "Update";
}else
{
  
   $formaction = "saveCompany";
   $button_name = "Save";
   $method="create";
}

?>

      <section>
         <!-- Page content-->
         <div class="content-wrapper">
            <h3>Manage Rental Company
               <!--<small>Validating forms frontend have never been so powerful and easy.</small>-->
            </h3>
            <!-- START row-->
            
            <!-- END row-->
            <!-- START row-->
            <div class="row">
               <div class="col-md-12">
                   <?php if($this->session->flashdata('permission_message'))
	 		{
                       
			?>
					
                        <div class="alert alert-block alert-success fade in" style="background-color:#3ec0e8">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p ><h4 style="background-color:#3ec0e8"> Successful!</h4> <?php echo $this->session->flashdata('permission_message'); ?></p>
                        </div>						
									
			<?php } ?>
            <?php if($this->session->flashdata('flash_message'))
	 		{
			?>
					
                        <div class="alert alert-block alert-success fade in" style="background-color:#ff708a">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p ><h4 style="background-color:#ff708a"> Error!</h4> <?php echo $this->session->flashdata('flash_message'); ?></p>
                        </div>						
									
			<?php } ?>
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <div class="panel-title">Create Company</div>
                        </div>
                        <div class="panel-body">
<!--                           <h4>Type validation</h4>-->
                                      <form class="form-horizontal" action="<?php echo base_url();?>superadmin/SuperAdmin/<?php echo $formaction;?>/<?php echo $method;?>/<?php echo $id;?>" method="post" enctype='multipart/form-data'>


<fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Company Name</label>
                                 <div class="col-sm-6">
                                     <input class="form-control" type="text" value="<?php echo $query['clientcompname'];?>" name="companyname" data-validation="length" data-validation-length="min1" data-validation-error-msg="Company Name is requirred"  >
                                 </div>
                                 <input type="hidden" name="parameter" value="<?php echo $parameter;?>">
                               
                              </div>
                           </fieldset>
<!--                                          <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Company Code</label>
                                 <div class="col-sm-6">
                                     <input class="form-control" type="text"  name="companycode" value="<?php echo $query['clientcompcode'];?>" data-validation="length" data-validation-length="min1" data-validation-error-msg="Company Code is requirred" > 
                                 </div>
                                
                              </div>
                           </fieldset>-->
                                          <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Company Address</label>
                                 <div class="col-sm-6">
                                     <input class="form-control" type="text"  name="address" value="<?php echo $query['clientcompaddress'];?>" data-validation="length" data-validation-length="min1" data-validation-error-msg="Company Address is requirred" >
                                 </div>
                               
                              </div>
                           </fieldset>
                                          
                                          <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Contact Number</label>
                                 <div class="col-sm-6">
                                     <input class="form-control" type="text" onkeypress="return isNumber1(event)" value="<?php echo $query['clientcompcontact'];?>" name="contactnum"  data-validation="length" data-validation-length="max10" data-validation-error-msg="Please enter 10 digit  contact number" >
                                 </div>
                                
                              </div>
                           </fieldset>
                                          <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Registration Date</label>
                                 <div class="col-sm-6">
                                     <input class="form-control " type="text"  id="datetimepicker" value="<?php echo $query['clientcompregistdate'];?>" name="registdate" >
                                 </div>
                               
                              </div>
                           </fieldset>
                                        
                                          
<!--                                          <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Approval</label>
                                 <div class="col-sm-6">
                                  <select name="aprstatus"  class="form-control" data-validation="length" data-validation-length="min1" data-validation-error-msg="Status is requirred" >
                                  <?php $ap =  $query['clientcompapproval'];?>
                                      <option value=" ">Select Status</option>
                                    <option value="1"  <?php if ($ap == 1 ) echo 'selected' ; ?>>Approved</option>
                                    <option value="0"  <?php if ($st == 0 ) echo 'selected' ; ?>>Pending</option>
                                    <option value="0"  <?php if ($ap == 0 ) echo 'selected' ; ?>>Not Approved</option>

                                  </select>    
                                 </div>
                               
                              </div>
                           </fieldset>-->
                                         
                                          
                                          
                           
<!--                           <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Status</label>
                                 <div class="col-sm-6">
                                     <?php $st = $query['clientcompstatus'] ; ?>
                                    <select name="status"  class="form-control" data-validation="length" data-validation-length="min1" data-validation-error-msg="Status is requirred" >
                                    <option value=" ">Select Status</option>
                                    <option value="1"  <?php if ($st == 1 ) echo 'selected' ; ?>>Active</option>
                                    <option value="0"  <?php if ($st == 0 ) echo 'selected' ; ?>>Inactive</option>
                                  </select>
                                 </div>
                                 
                              </div>
                           </fieldset>-->
                           
                           
                           
                           
                            <div class="panel-footer text-center">
                           <button class="btn btn-info" type="submit" style="margin-left: -193px;"><?php echo $button_name;?></button>
                        </div>
                          
                                      </form>
                         
                        </div>
                     
                     </div>
                     <!-- END panel-->
                  </form>
               </div>
            </div>
            <!-- END row-->
         </div>
      </section>
    <script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js" type="text/javascript"></script>
<!--        <script src="<?php echo base_url();?>assets/datepicker/datepicker1/bootstrap-datetimepicker.css" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/datepicker/datepicker1/bootstrap-datetimepicker.min.css" type="text/javascript"></script>

     <script src="<?php echo base_url();?>assets/datepicker/datepicker1/js/bootstrap-datetimepicker.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/datepicker/datepicker1/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>-->

    
    
    <!--<script src="<?php echo base_url();?>assets/datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>-->
 

<!--    <script>
        
        $('.datepicker').datepicker();
        </script>-->
    
    
    
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"> </script>
                


<script>
                    
                      function isNumber(evt) {
            var iKeyCode = (evt.which) ? evt.which : evt.keyCode
            if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
                return false;

            return true;
        
                    }
                    
                    
                      function isNumber1(evt) {
            var iKeyCode = (evt.which) ? evt.which : evt.keyCode
            if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
                return false;

            return true;
        
                    }
                    
                    
                </script>
