
<?php
if($id)
{
    
   $query = $this->db->get_where('tblplan', array('planid' => $id))->row_array();
  
  $formaction = "updatePlan";
  $button_name = "Update";
}else
{
  
   $formaction = "savePlan";
   $button_name = "Save";
}

?>

      <section>
         <!-- Page content-->
         <div class="content-wrapper">
            <h3>Plan And Package
               <!--<small>Validating forms frontend have never been so powerful and easy.</small>-->
            </h3>
            <!-- START row-->
            
            <!-- END row-->
            <!-- START row-->
            <div class="row">
               <div class="col-md-12">
                   <?php if($this->session->flashdata('permission_message'))
	 		{
                       
			?>
					
                        <div class="alert alert-block alert-success fade in" style="background-color:#3ec0e8">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p ><h4 style="background-color:#3ec0e8"> Successful!</h4> <?php echo $this->session->flashdata('permission_message'); ?></p>
                        </div>						
									
			<?php } ?>
            <?php if($this->session->flashdata('flash_message'))
	 		{
			?>
					
                        <div class="alert alert-block alert-success fade in" style="background-color:#ff708a">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p ><h4 style="background-color:#ff708a"> Error!</h4> <?php echo $this->session->flashdata('flash_message'); ?></p>
                        </div>						
									
			<?php } ?>
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <div class="panel-title">Plan And Package</div>
                        </div>
                        <div class="panel-body">
<!--                           <h4>Type validation</h4>-->
                                      <form class="form-horizontal" action="<?php echo base_url();?>superadmin/SuperAdmin/<?php echo $formaction;?>/<?php echo $id;?>" method="post" enctype='multipart/form-data'>
<?php if($formaction == "savePlan") { ?>
                                          
                                          
<fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Plan Type</label>
                                 <div class="col-sm-6">
                               <select name="planType" class="form-control" onchange="hidePrice(this.value);">
                                    <option value=" ">Select Type</option>
                                    <option value="demo">Demo</option>
                                    <option value="plan" selected="">Paid</option>
                                  </select>
                                 
                                 
                                 </div>
                                 
                              </div>
                           </fieldset>
                                          
<?php } ?>           
                                          
                                          
                                          
                                          
<fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Plan Name</label>
                                 <div class="col-sm-6">
                                     <input class="form-control" type="text" value="<?php echo $query['planname'];?>" name="planname" data-validation="length" data-validation-length="min1" data-validation-error-msg="Plan Name is requirred"  >
                                 </div>
                                 <input type="hidden" name="planId"  value="<?php echo $id;?>" >
                              </div>
                           </fieldset>
                                          <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Plan Duration</label>
                                 <div class="col-sm-6">
                                     <input class="form-control" type="text"  name="duration" value="<?php echo $query['planduration'];?>" data-validation="length" data-validation-length="min1" data-validation-error-msg="Plan Duration is requirred" > 
                                 </div>
                                
                              </div>
                           </fieldset>
                                          <fieldset>
                              <div class="form-group abc">
                                 <label class="col-sm-2 control-label">Plan Price</label>
                                 <div class="col-sm-6">
                                     <input class="form-control " type="text" value="<?php echo $query['planprice'];?>" name="planprice"  >
                                 </div>
                                 <input type="hidden" name="upId"  value="<?php echo $id;?>" >
                              </div>
                           </fieldset>
                                          
                                          <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">User Accounts</label>
                                 <div class="col-sm-6">
                                     <input class="form-control" type="text" value="<?php echo $query['planaccount'];?>" name="account"   >
                                 </div>
                                 <input type="hidden" name="upId"  value="<?php echo $param;?>" >
                              </div>
                           </fieldset>
                                          <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Private Projects</label>
                                 <div class="col-sm-6">
                                     <input class="form-control" type="text" value="<?php echo $query['planprivproject'];?>" name="priproject"  >
                                 </div>
                                 <input type="hidden" name="upId"  value="<?php echo $param;?>" >
                              </div>
                           </fieldset>
                                          <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Public  Projects</label>
                                 <div class="col-sm-6">
                                     <input class="form-control" type="text" value="<?php echo $query['planpubproject'];?>" name="pubproject"   >
                                 </div>
                                 <input type="hidden" name="upId"  value="<?php echo $param;?>" >
                              </div>
                           </fieldset>
                                          
                                          <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Disk Space</label>
                                 <div class="col-sm-6">
                                     <input class="form-control" type="text" value="<?php echo $query['plandiskspace'];?>" name="diskspace"  >
                                 </div>
                                 <input type="hidden" name="upId"  value="<?php echo $param;?>" >
                              </div>
                           </fieldset>
                                          <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Monthly Bandwidth</label>
                                 <div class="col-sm-6">
                                     <input class="form-control" type="text" value="<?php echo $query['planbandwidth'];?>" name="bandwidth"   >
                                 </div>
                                 <input type="hidden" name="upId"  value="<?php echo $param;?>" >
                              </div>
                           </fieldset>
                                          <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Email Support</label>
                                 <div class="col-sm-6">
                                     <input class="form-control" type="text" value="<?php echo $query['planemailsupport'];?>" name="esupport"   >
                                 </div>
                                 <input type="hidden" name="upId"  value="<?php echo $param;?>" >
                              </div>
                           </fieldset>
                                          <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Phone Support</label>
                                 <div class="col-sm-6">
                                     <input class="form-control" type="text" value="<?php echo $query['planphonesupport'];?>" name="psupport"   >
                                 </div>
                                 <input type="hidden" name="upId"  value="<?php echo $param;?>" >
                              </div>
                           </fieldset>
                                          
                           
<!--                           <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Status</label>
                                 <div class="col-sm-6">
                                     <?php $st = $query['planstatus'] ; ?>
                                    <select name="status"  class="form-control" data-validation="length" data-validation-length="min1" data-validation-error-msg="Status is requirred" >
                                    <option value=" ">Select Status</option>
                                    <option value="1"  <?php if ($st == 1 ) echo 'selected' ; ?>>Active</option>
                                    <option value="0"  <?php if ($st == 0 ) echo 'selected' ; ?>>Inactive</option>
                                  </select>
                                 </div>
                                 
                              </div>
                           </fieldset>-->
                           
                           
                           
                           
                            <div class="panel-footer text-center">
                           <button class="btn btn-info" type="submit" style="margin-left: -193px;"><?php echo $button_name;?></button>
                        </div
                          
                                      </form>
                         
                        </div>
                     
                     </div>
                     <!-- END panel-->
                  </form>
               </div>
            </div>
            <!-- END row-->
         </div>
      </section>
    <script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js" type="text/javascript"></script>
    
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"> </script>

                    <script>
                       
                       function hidePrice(a)
                       {
                          if(a == "demo")
                          {
                              $(".abc").hide();
                          }
                          if(a == "plan")
                          {
                              $(".abc").show();
                          }
                       }
                        
                        
                        
                        
                        </script>
                            