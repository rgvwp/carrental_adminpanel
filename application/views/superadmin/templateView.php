
      <!-- Main section-->
      <section>
         <!-- Page content-->
         <div class="content-wrapper">
            <h3>Templates
             
            </h3>
            <div class="container-fluid">
             <?php if($this->session->flashdata('permission_message'))
	 		{
                       
			?>
					
                        <div class="alert alert-block alert-success fade in" style="background-color:#3ec0e8">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p ><h4 style="background-color:#3ec0e8"> Successful!</h4> <?php echo $this->session->flashdata('permission_message'); ?></p>
                        </div>						
									
			<?php } ?>
            <?php if($this->session->flashdata('flash_message'))
	 		{
			?>
					
                        <div class="alert alert-block alert-success fade in" style="background-color:#ff708a">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p ><h4 style="background-color:#ff708a"> Error!</h4> <?php echo $this->session->flashdata('flash_message'); ?></p>
                        </div>						
									
			<?php } ?>
               <div class="row">
                  <div class="col-lg-12">
                     <div class="panel panel-default">
                        <div class="panel-heading">Template List |
                        
                        </div>
                        <div class="panel-body">
                           <div class="table-responsive">
                              <table class="table table-striped table-hover" id="datatable1">
                                 <thead>
                                    <tr>
                                        <th>SNo.</th>
                                       <th>Template Name</th>
                                       <!--<th>Template Format</th>-->
                                       <th>Status</th>
                                       <th>Action</th>
                                       
                                    </tr>
                                 </thead>
                                 <tbody>
                                 <?php  $i =1;
                                 $query = $this->db->get('tbltemplate');
                              
                                     foreach($query->result() as $k=>$vl)       {   ?>
                                    <tr class="gradeC">
                                        <td><?php echo $i;?></td>
                                       <td><?php echo $vl->templatetitle;?></td>
                                       <!--<td><?php echo $vl->templatetitle;?></td>-->
                                      <?php $st =$vl->templatestatus;
                                      if($st == "1")
                                      { ?>
                                          <td>Active</td>
                                 <?php     }
                                 else{?>
                                           <td> In Active</td>
                                 <?php } ?>
                                 
                                       
                                       <th ><a href="<?php echo base_url();?>superadmin/SuperAdmin/updateTemplate/<?php echo $vl->templateid;?>" ><i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i>Edit</a></th>
                                     </tr>
                                     <?php $i++; } ?>
                                 
                                
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
              