
<style>
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input {display:none;}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}


.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>























<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
   <meta name="description" content="Bootstrap Admin App + jQuery">
   <meta name="keywords" content="app, responsive, jquery, bootstrap, dashboard, admin">
   <title>Car Renatl</title>
   <!-- =============== VENDOR STYLES ===============-->
   <!-- FONT AWESOME-->
   <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/fontawesome/css/font-awesome.min.css">
   <!-- SIMPLE LINE ICONS-->
   <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/simple-line-icons/css/simple-line-icons.css">
   <!-- ANIMATE.CSS-->
   <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/animate.css/animate.min.css">
   <!-- WHIRL (spinners)-->
   <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/whirl/dist/whirl.css">
   <!-- =============== PAGE VENDOR STYLES ===============-->
   <!-- DATATABLES-->
   <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/datatables-colvis/css/dataTables.colVis.css">
   <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/datatables/media/css/dataTables.bootstrap.css">
   <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/dataTables.fontAwesome/index.css">
   <!-- =============== BOOTSTRAP STYLES ===============-->
   <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.css" id="bscss">
   <!-- =============== APP STYLES ===============-->
   <link rel="stylesheet" href="<?php echo base_url();?>assets/css/app.css" id="maincss">
</head>

<body>
   <div class="wrapper">
      <!-- top navbar-->
      
      <!-- sidebar-->
      
      <!-- offsidebar-->
      
      <!-- Main section-->
      
      <section>
         <!-- Page content-->
         <div class="content-wrapper">
             
             
             <div class="alert alert-block alert-success fade in  messagestatus" style="background-color:#3ec0e8;display:none" >
                                <a class="close"  aria-hidden="true">&times;</a>
                                <h4>  <p id="message"></p> </h4>
                        </div>	
             
             
             
             
             
             
             
               <?php if($this->session->flashdata('permission_message'))
	 		{
			?>
					
                        <div class="alert alert-block alert-success fade in" style="background-color:#3ec0e8">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p ><h4 style="background-color:#3ec0e8"> Successful!</h4> <?php echo $this->session->flashdata('permission_message'); ?></p>
                        </div>						
									
			<?php } ?>
            <?php if($this->session->flashdata('flash_message'))
	 		{
			?>
					
                        <div class="alert alert-block alert-success fade in" style="background-color:#ff708a">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p ><h4 style="background-color:#ff708a"> Error!</h4> <?php echo $this->session->flashdata('flash_message'); ?></p>
                        </div>						
									
			<?php } ?>
            <h3>Plan And Package
               
            </h3>
            <div class="container-fluid">
               <!-- START DATATABLE 1-->
               <div class="row">
                  <div class="col-lg-12">
                     <div class="panel panel-default">
<!--                        <div class="panel-heading">Data Tables |
                           <small>Zero Configuration + Export Buttons</small>
                        </div>-->
                        <div class="panel-body">
                           <div class="table-responsive">
                              <table class="table table-striped table-hover" id="datatable1">
                                 <thead>
                                    <tr>
                                        <th>S.No</th>
                                       <th>Plan Name</th>
                                       <th>Plan Duration</th>
                                       <th>Plan Price</th>
                                       <th >User Accounts</th>
                                       <th>Private Project</th>
                                       
                                       
                                        <th>Public Projects</th>
                                       <th>Disk Space</th>
                                       <th>Monthly Bandwidth</th>
                                       <th class="sort-numeric">24/7 Email Support </th>
                                       <th class="sort-alpha">Phone Support</th>
                                       <!--<th>Status</th>-->
                                       <th>Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                     <?php $query = $this->db->get('tblplan');
                                     $i=1;
                                     foreach($query->result() as $k=>$vl)
                                     {?>
                                    <tr class="gradeX">
                                        <td><?php echo $i;?></td>
                                       <td><?php echo $vl->planname;?></td>
                                       <td><?php echo $vl->planduration;?></td>
                                        <?php $ty = $vl->plantype;
                                        if($ty == "demo")
                                        {
                                            $price = "free";
                                            ?>   
                                       <td><?php echo $price;?></td>

                                       
                                       
                                   <?php         }
                                        else{?>
                                                                               <td><?php echo $vl->planprice;?></td>

                                        <?php }  ?>
                                       
                                       
                                       
                                       
                                       <td><?php echo $vl->planaccount;?></td>
                                       <td><?php echo $vl->planprivproject;?></td>
                                       <td><?php echo $vl->planpubproject;?></td>
                                         <td><?php echo $vl->plandiskspace;?></td>
                                       <td><?php echo $vl->planbandwidth;?></td>
                                       <td><?php echo $vl->planemailsupport;?></td>
                                       <td><?php echo $vl->planphonesupport;?></td>
                                     
                                       
                                       
                                       
<!--                                       <?php  $st =  $vl->planstatus;?>
                                      -->
                                      
                                      
<!--            <td><label class="switch"><input type="checkbox"<?php if($st == 1){echo checked;}?>>
               <a  onclick="changeStatus('<?php echo $vl->planid;?>');"   <span class="slider round"></a></span></label></td>            -->

                                      
                                      
                                      
                                      
                                      
                                      
                                      
                                      

                             <td ><a href="<?php echo base_url();?>superadmin/SuperAdmin/formPlan/<?php echo $vl->planid;?>" ><i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i></a>
                     <!--<a href="<?php echo base_url();?>superadmin/SuperAdmin/deletePlan/<?php echo $vl->planid;?>" ><i class="fa fa-trash-o fa-2x" aria-hidden="true" style="color:#f31a04;"></i></a></td>-->

                                    </tr>
                                  
                                     <?php $i++; } 
                                     
                                     
                                     
                                     
                                     
                                     ?>
                                  
                              
                                 
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- END DATATABLE 1-->
               <!-- START DATATABLE 2-->
               
               <!-- END DATATABLE 2-->
               <!-- START DATATABLE 3-->
               
               <!-- END DATATABLE 3-->
               
            </div>
         </div>
      </section>
      <!-- Page footer-->
      
   </div>
    
       <script src="https://code.jquery.com/jquery-3.3.1.js"></script>

       <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

    
   
    
    
    
    
    
    
    
    
    
    
    
    
   <!-- =============== VENDOR SCRIPTS ===============-->
   <!-- MODERNIZR-->
   <script src="<?php echo base_url();?>assets/vendor/modernizr/modernizr.custom.js"></script>
   <!-- MATCHMEDIA POLYFILL-->
   <script src="<?php echo base_url();?>assets/vendor/matchMedia/matchMedia.js"></script>
   <!-- JQUERY-->
   <script src="<?php echo base_url();?>assets/vendor/jquery/dist/jquery.js"></script>
   <!-- BOOTSTRAP-->
   <script src="<?php echo base_url();?>assets/vendor/bootstrap/dist/js/bootstrap.js"></script>
   <!-- STORAGE API-->
   <script src="<?php echo base_url();?>assets/vendor/jQuery-Storage-API/jquery.storageapi.js"></script>
   <!-- JQUERY EASING-->
   <script src="<?php echo base_url();?>assets/vendor/jquery.easing/js/jquery.easing.js"></script>
   <!-- ANIMO-->
   <script src="<?php echo base_url();?>assets/vendor/animo.js/animo.js"></script>
   <!-- SLIMSCROLL-->
   <script src="<?php echo base_url();?>assets/vendor/slimScroll/jquery.slimscroll.min.js"></script>
   <!-- SCREENFULL-->
   <script src="<?php echo base_url();?>assets/vendor/screenfull/dist/screenfull.js"></script>
   <!-- LOCALIZE-->
   <!--<script src="<?php echo base_url();?>assets/vendor/jquery-localize-i18n/dist/jquery.localize.js"></script>-->
   <!-- RTL demo-->
   <script src="<?php echo base_url();?>assets/js/demo/demo-rtl.js"></script>
   <!-- =============== PAGE VENDOR SCRIPTS ===============-->
   <!-- DATATABLES-->
   <script src="<?php echo base_url();?>assets/vendor/datatables/media/js/jquery.dataTables.min.js"></script>
   <script src="<?php echo base_url();?>assets/vendor/datatables-colvis/js/dataTables.colVis.js"></script>
   <script src="<?php echo base_url();?>assets/vendor/datatables/media/js/dataTables.bootstrap.js"></script>
   <!--<script src="<?php echo base_url();?>assets/vendor/datatables-buttons/js/dataTables.buttons.js"></script>-->
   <!--<script src="<?php echo base_url();?>assets/vendor/datatables-buttons/js/buttons.bootstrap.js"></script>-->
   <script src="<?php echo base_url();?>assets/vendor/datatables-buttons/js/buttons.colVis.js"></script>
   <!--<script src="<?php echo base_url();?>assets/vendor/datatables-buttons/js/buttons.flash.js"></script>-->
   <script src="<?php echo base_url();?>assets/vendor/datatables-buttons/js/buttons.html5.js"></script>
   <script src="<?php echo base_url();?>assets/vendor/datatables-buttons/js/buttons.print.js"></script>
   <script src="<?php echo base_url();?>assets/vendor/datatables-responsive/js/dataTables.responsive.js"></script>
   <script src="<?php echo base_url();?>assets/vendor/datatables-responsive/js/responsive.bootstrap.js"></script>
   <script src="<?php echo base_url();?>assets/js/demo/demo-datatable.js"></script>
   <!-- =============== APP SCRIPTS ===============-->
   <script src="<?php echo base_url();?>assets/js/app.js"></script>
</body>

</html>


<script>
    
    $(".close").click(function(){
   // alert("The paragraph was clicked.");
    $(".messagestatus").hide();
});
    
  function changeStatus(val)
    {
        
       
        
      
        
                 // $(".close").setAttribute("aria-hidden", "true");

        
       // alert(val);
        
          $.ajax({
          url: '<?php echo base_url();?>superadmin/SuperAdmin/changestatusPlan',
          type : "POST",
         
          data : {"statusid" :val},
          success : function(data) {
            
            if(data == "active"){
                 // alert('active');
                $(".messagestatus").show();
             $("#message").html("Status active Successfully");
            }
        
            if(data == "inactive"){
                // alert('inactive');
                $(".messagestatus").show();
             $("#message").html("Status inactive Successfully");
            }
        
        
            
          }
        
          
      });
      
      
    }
    
    
    </script>