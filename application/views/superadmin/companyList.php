
   <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/datatables-colvis/css/dataTables.colVis.css">
   <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/datatables/media/css/dataTables.bootstrap.css">
   <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/dataTables.fontAwesome/index.css">
 
<body>
   <div class="wrapper">
   
      <section>
         <!-- Page content-->
         <div class="content-wrapper">
               <?php if($this->session->flashdata('permission_message'))
	 		{
			?>
					
                        <div class="alert alert-block alert-success fade in" style="background-color:#3ec0e8">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p ><h4 style="background-color:#3ec0e8"> Successful!</h4> <?php echo $this->session->flashdata('permission_message'); ?></p>
                        </div>						
									
			<?php } ?>
            <?php if($this->session->flashdata('flash_message'))
	 		{
			?>
					
                        <div class="alert alert-block alert-success fade in" style="background-color:#ff708a">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p ><h4 style="background-color:#ff708a"> Error!</h4> <?php echo $this->session->flashdata('flash_message'); ?></p>
                        </div>						
									
			<?php } ?>
            <h3>Rental Companies
               
            </h3>
            <div class="container-fluid">
               <!-- START DATATABLE 1-->
               <div class="row">
                  <div class="col-lg-12">
                     <div class="panel panel-default">
<!--                        <div class="panel-heading">Data Tables |
                           <small>Zero Configuration + Export Buttons</small>
                        </div>-->
                        <div class="panel-body">
                           <div class="table-responsive">
                              <table class="table table-striped table-hover" id="datatable1">
                                 <thead>
                                    <tr>
                                        <th>S.No</th>
                                       <th>Company Name</th>
                                       <th>Company Code</th>
                                       <th>Company Address</th>
                                       <th >Registration Date</th>
                                      
                                        
                                        <th>Status</th>
                                         <th>Approval Status</th>
                                       
                                       <th>Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                     <?php 
                                     $this->db->order_by("clientcompid", "desc");
                                     $query = $this->db->get('tblclientcompany');
                                     $i=1;
                                     foreach($query->result() as $k=>$vl)
                                     {?>
                                    <tr class="gradeX">
                                        <td><?php echo $i;?></td>
                                       <td><?php echo $vl->clientcompname;?></td>
                                       <td><?php echo $vl->clientcompcode;?></td>
                                       <td><?php echo $vl->clientcompaddress;?></td>
                                       <td><?php echo $vl->clientcompregistdate;?></td>
                                          <?php $st =  $vl->clientcompstatus;
                                        if($st == 1)
                                        {
                                            $stat = "Active";
                                        }
                                        else{
                                             $stat = "Inactive";
                                        }
                                       
                                       ?>;
                                       <td><?php echo $stat;?></td>
                                      
                                       
                                       
                                      
                                       <?php $st =  $vl->clientcompapproval;
                                       
                                        if($st == 1){
                                             $status = "Approved";
                                        }
                                        if($st == 0){
                                             $status = "Not Approved";
                                        }
                                         if($st == "pending"){
                                             $status = "Pending";
                                       
                                             }
                                       
                                       ?>
                                       <?php if($st == 1 || $st == "pending"){?>
                                       <td><a href="<?php echo base_url();?>superadmin/SuperAdmin/statusCompany/<?php echo $vl->clientcompid;?>"    ><button type="button" class="btn btn-success" ><?php echo $status;?></button></a></td>

                                       <?php } ?>
                                       
                                       
                             <td ><a href="<?php echo base_url();?>superadmin/SuperAdmin/rentalcompany/<?php echo $vl->clientcompid;?>" ><i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i></a>
                     <a href="<?php echo base_url();?>superadmin/SuperAdmin/saveCompany/delete/<?php echo $vl->clientcompid?>" ><i class="fa fa-trash-o fa-2x" aria-hidden="true" style="color:#f31a04;"></i></a></td>
                            
               <a href="javascript:;" onClick="confirm_modal();" class="fa fa-trash-o" onclick="return delete_confirm();">  drfyhthtg</a>
                        
                                    
                                    </tr>

                                     <?php $i++; } 
                                     
                                     
                                     
                                     
                                     
                                     ?>
                                  
                              
                                 
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
          
            </div>
         </div> 
          <div class="modal fade" id="modal_delete2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Are you sure about this?</h4>
                                </div>
                                <!--<div class="modal-body">
                                  
                    
                                </div>-->
                                <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
                                    <button type="button" class="btn btn-info" data-dismiss="modal"><?php echo 'close'; ?></button>
                                    <a href="javascript:;" class="btn btn-danger" id="delete_link"><?php echo 'delete'; ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
        
    <!-- End of the boostrap modal popup -->
      </section>
      <!-- Page footer-->
      
   </div>
    
    
    
    
    <!--<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>-->
<!-- BS JavaScript -->
<!--<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.js"></script>-->
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   <!-- =============== VENDOR SCRIPTS ===============-->
  
   <script src="<?php echo base_url();?>assets/vendor/matchMedia/matchMedia.js"></script>
   <!-- JQUERY-->
   <script src="<?php echo base_url();?>assets/vendor/jquery/dist/jquery.js"></script>
  
  
   <script src="<?php echo base_url();?>assets/vendor/datatables/media/js/jquery.dataTables.min.js"></script>
   <script src="<?php echo base_url();?>assets/vendor/datatables-colvis/js/dataTables.colVis.js"></script>
   <!--<script src="<?php echo base_url();?>assets/vendor/datatables/media/js/dataTables.bootstrap.js"></script>-->
   <!--<script src="<?php echo base_url();?>assets/vendor/datatables-buttons/js/dataTables.buttons.js"></script>-->
   <!--<script src="<?php echo base_url();?>assets/vendor/datatables-buttons/js/buttons.bootstrap.js"></script>-->
<!--   <script src="<?php echo base_url();?>assets/vendor/datatables-buttons/js/buttons.colVis.js"></script>-->
   <!--<script src="<?php echo base_url();?>assets/vendor/datatables-buttons/js/buttons.flash.js"></script>-->
   <!--<script src="<?php echo base_url();?>assets/vendor/datatables-buttons/js/buttons.html5.js"></script>-->
   <!--<script src="<?php echo base_url();?>assets/vendor/datatables-buttons/js/buttons.print.js"></script>-->
   <script src="<?php echo base_url();?>assets/vendor/datatables-responsive/js/dataTables.responsive.js"></script>
   <script src="<?php echo base_url();?>assets/vendor/datatables-responsive/js/responsive.bootstrap.js"></script>
   <script src="<?php echo base_url();?>assets/js/demo/demo-datatable.js"></script>
   <script>
       
       
        function confirm_modal(x)
                {
                    alert(x);
                    var delete_url = x;
                    $('#modal_delete2').modal('show', {backdrop: 'static'});
                    document.getElementById('delete_link').setAttribute('href', delete_url);
                }
                
                </script>