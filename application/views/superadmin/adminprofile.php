<?php $query = $this->db->get('tblsupadmprofile')->row_array();


?>

<style>
    .panel-heading
    {
        display:inline-block;
    }
</style>



<section>
         <!-- Page content-->
         <div class="content-wrapper">
            
            <!-- START row-->
            
            <!-- END row-->
            <!-- START row-->
            <?php if($this->session->flashdata('permission_message'))
	 		{
			?>
					
                        <div class="alert alert-block alert-success fade in" style="background-color:#3ec0e8">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p ><h4 style="background-color:#3ec0e8"> Successful!</h4> <?php echo $this->session->flashdata('permission_message'); ?></p>
                        </div>						
									
			<?php } ?>
            <?php if($this->session->flashdata('flash_message'))
	 		{
			?>
					
                        <div class="alert alert-block alert-success fade in" style="background-color:#ff708a">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                                        <p ><h4 style="background-color:#ff708a"> Error!</h4> <?php echo $this->session->flashdata('flash_message'); ?></p>
                        </div>						
									
			<?php } ?>
            <div class="row">
               <div class="col-md-12">
                     <!-- START panel-->
                     <div class="panel panel-default">
                        <div class="panel-heading">
                             <h4>Profile Update</h4>
                        </div>
                        <ul class="nav nav-tabs">
                            
                            
    <?php if($data == "pro")
        {
            $up ="active";
        }
        if($data == ""){
            
            $up ="active";
        }
        
        
        ?>
                            
    <li class="<?php echo $up;?>"><a data-toggle="tab" href="#profile">Profile Update</a></li>
    <?php if($data == "pass")
        {
            $p ="active";
        }?>
    <li class="<?php echo $p;?>"><a data-toggle="tab" href="#changepass">Change Password</a></li>
<!--    <li><a data-toggle="tab" href="#menu2">Menu 2</a></li>
    <li><a data-toggle="tab" href="#menu3">Menu 3</a></li>-->
  </ul>

  <div class="tab-content">
    <div id="profile" class="tab-pane fade in <?php echo $up;?>">
      <div class="panel-body">
                                      <form class="form-horizontal" action="<?php echo base_url();?>superadmin/SuperAdmin/profileUpdate" method="post" enctype='multipart/form-data'>
        
                            
                           <!--<h4>Type validation</h4>-->
                           <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label" style="padding-top: 92px;">Profile Image</label>
                                 <div class="col-sm-6">
                                     <?php $pic = $query['supadmprofilepicture'];
                                     if($pic !="") {?>
                                     <img  id="blah" name="image" class=" img-circle " src="<?php echo base_url();?>uploads/images/<?php echo $query['supadmprofilepicture'];?>" alt="Image" style="width: 85px; margin-left: 82px;" >

                                     <?php     }else {?>
                                 <img  id="blah" name="image" class=" img-circle " src="<?php echo base_url();?>/assets/img/user/02.jpg" alt="Image" style="width: 85px; margin-left: 82px;" >
                                <?php } ?>
                                   <input type='file' id="im123" name="image" onchange="readURL(this);" style=" margin-left: 82px;"  />

                                 </div>
                                 
                              </div>
                           </fieldset>
                           <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Name</label>
                                 <div class="col-sm-6">
                                <input class="form-control" type="text" name="name"  value="<?php echo $query['supadmprofilename'];?>" data-validation="length" data-validation-length="min1" data-validation-error-msg="Name is requirred" >
                                <?php    $sess = $this->session->all_userdata(); ?>
                                    <input type="hidden" value="<?php echo $sess['id'];?>" name="supid">
                                        <input type="hidden" value="pro" name="parameter">

                                 </div>
                                 
                              </div>
                           </fieldset>
                           <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Email</label>
                                 <div class="col-sm-6">
                                    <input class="form-control" type="email" name="email" data-validation="email" value="<?php echo $query['supadmprofileemail'];?>" data-validation="length" data-validation-length="min1" data-validation-error-msg="Email is requirred" >
                                 </div>
                                 
                              </div>
                           </fieldset>
                           <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Contact Number</label>
                                 <div class="col-sm-6">
                                     <input class="form-control" type="text" name="contactno" data-parsley-type="number" onkeypress="return isNumber(event)" value="<?php echo $query['supadmprofilecontaccnum'];?>" data-validation="length" data-validation-length="max10" data-validation-error-msg="Please enter 10 digit contact number" >
                                 </div>
                                 
                              </div>
                           </fieldset>
                           <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Address</label>
                                 <div class="col-sm-6">
                                    <input class="form-control" type="text" name="address" data-validation="length" data-validation-length="min4" data-validation-error-msg="Address is requirred"  value="<?php echo $query['supadmprofileaddress'];?>">
                                 </div>
                                 
                              </div>
                           </fieldset>
<!--                           <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Password</label>
                                 <div class="col-sm-6">
                                    <input class="form-control" type="password" name="pass_confirmation"  value="" data-validation="strength"  data-validation-strength="2" >
                                 </div>
                                
                              </div>
                           </fieldset>-->
<!--                           <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Confirm Password</label>
                                 <div class="col-sm-6">
                                    <input class="form-control" type="password" name="pass" data-validation="confirmation"  value="">
                                 </div>
                                
                              </div>
                           </fieldset>-->
                          
                           
                            <div class="panel-footer text-center"  >
                           <button class="btn btn-info" type="submit"  style="margin-right: 241px;" >Update</button>
                        </div> 
                                      </form>
                        </div>
    </div>
    <div id="changepass" class="tab-pane fade in <?php echo $p;?>">
                <form  action="<?php echo base_url();?>superadmin/SuperAdmin/checkPassword" method="post" id="passForm" >

        <br>
        <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Old Password</label>
                                 <div class="col-sm-6">
                                    <input class="form-control" type="password" name="oldPassword" id="oldPass"   value=""  data-validation="length" data-validation-length="min4" data-validation-error-msg="Old Password is requirred" >
                                     <?php  //  $sess = $this->session->all_userdata(); ?>
                                    <input type="hidden" value="<?php echo $sess['id'];?>" name="supid">
                                           <input type="hidden" value="pass" name="parameter">

                                    <span >
                                 </div>
                                
                              </div>
                           </fieldset>
    <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Password</label>
                                 <div class="col-sm-6">
                                    <input class="form-control" type="password" name="pass_confirmation"  value="" data-validation="strength"  data-validation-strength="2" >
                                 </div>
                                
                              </div>
                           </fieldset>
                           <fieldset>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Confirm Password</label>
                                 <div class="col-sm-6">
                                    <input class="form-control" type="password" name="pass" data-validation="confirmation"  value="">
                                 </div>
                                
                              </div>
                           </fieldset>
                    
        <div class="panel-footer text-center"  >
                           <button class="btn btn-info" type="submit"  style="margin-right: 241px;" >Update</button>
                        </div> 
                   
                     </form>
    </div>
<!--    <div id="menu2" class="tab-pane fade">
      <h3>Menu 2</h3>
      <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
    </div>-->
<!--    <div id="menu3" class="tab-pane fade">
      <h3>Menu 3</h3>
      <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
    </div>-->
  </div> 
             
                        
                        
                     </div>
                     <!-- END panel-->
                  </form>
               </div>
            </div>
            <!-- END row-->
         </div>
      </section>

                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"> </script>

<script>
    
    
    
    
    
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah')
                    .attr('src', e.target.result)
                    .width(80)
                    .height(80);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    
    </script>
    
     <script>
                    
                      function isNumber(evt) {
            var iKeyCode = (evt.which) ? evt.which : evt.keyCode
            if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
                return false;

            return true;
        
                    }
                    
                    
                </script>
                
                <script>
                    function passSubmit()
                    {
                    alert();
         $('#passForm').submit();
                  
                        
                    }
                    </script>